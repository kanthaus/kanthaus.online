---
title: Newsletter
form:
    name: newsletter
    fields:
        email:
            type: text
            placeholder: E-Mail
            display_label: false
            size: x-small
            validate:
                required: true
    buttons:
        submit:
            type: submit
            value: subscribe
    process:
        -
            mailtrain_subscribe:
                host: 'https://mailtrain.livingutopia.org'
                list_id: Y9kNgoaJ
                require_confirmation: true
                email: '{{ form.value.email }}'
        -
            message: '<b>Amazing!</b><br /> You will now receive an email with a link with which you can confirm your subscription.'

---

# Newsletter
Interested in coming to a building week? or just interested in our events?
feel free to sign up here :)

_Don't worry: you will probably not receive more than 2-3 mails per year and your address will only end up in our own  [self hosted mail system](https://github.com/Mailtrain-org/mailtrain), not with any third party provider._
