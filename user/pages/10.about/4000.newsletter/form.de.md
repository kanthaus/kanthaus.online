---
title: Newsletter
form:
    name: newsletter
    fields:
        email:
            type: text
            placeholder: E-Mail
            display_label: false
            size: x-small
            validate:
                required: true
    buttons:
        submit:
            type: submit
            value: abonnieren
    process:
        -
            mailtrain_subscribe:
                host: 'https://mailtrain.livingutopia.org'
                list_id: Y9kNgoaJ
                require_confirmation: true
                email: '{{ form.value.email }}'
        -
            message: '<b>Wunderbärchen!</b><br /> Du erhälst nun eine E-Mail mit einem Link, mit welchem du dein Abonnement bestätigen kannst.'

---

# Newsletter
Lust mal zu einer Bauwoche zu kommen? oder einfach so an Veranstaltungen von uns interessiert?
trag dich doch gern hier ein :)

_Keine Sorge: du wirst vorraussichtlich nicht mehr als 2-3 Mails pro Jahr bekommen und deine Adresse landet nur bei unserem, von Kanthaus-Menschen [selbst-gehosteten Mailsystem](https://github.com/Mailtrain-org/mailtrain), nicht bei irgendwelchen Drittanbieter:innen._
