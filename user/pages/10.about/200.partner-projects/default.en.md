---
title: 'Partner projects'
description: 'Other places which are similar to us'
---

We have personal ties to the following projects which also share a lot - if not all - of our ideals:<br>

### [Freie Feldlage](https://freiefeldlage.de) in Harzgerode

[![](freiefeldlagePic.jpg)](https://freiefeldlage.de)

The former children's clinic in the Harz mountains is a place full of opportunity. The huge area with its hospital buildings, forests and meadows offers as many tasks as it provides freedom. Many of us spent months there before Kanthaus even started and especially in summer it still is the place to be for those of us who prefer nature over city life.

---

### [Funkenhaus](https://gelebteutopie.de/?target=_blank) in Greene

[![](funkenhausLogo.png)](https://gelebteutopie.de/?target=_blank)

One year younger than Kanthaus but probably much more known Funkenhaus (formerly called 'Kollektivhaus') is like a sibling project to us. It emerged from the [living utopia](https://livingutopia.org) network which is a major player in the transformative scene of Germany. Our contact to this network goes back to [yunity](https://yunity.org) days for some and even further for others.

---

### [K20](https://k20-projekthaus.de/) in Salzderhelden

[![](k20.png)](https://k20-projekthaus.de/)

Kanthaus and this project house are closely linked, and not only namewise. K20 sees itself as a learning and meeting space, where new ideas for communal living and political projects are developed. That is why this place is also vegan, drug-free and free of exchange logic. In addition, this place is accessible to all people. This is to decouple giving and taking from each other, so that everyone can contribute according to needs and abilities. The K20 stands for solidary, social-ecological and hierarchy-critical cooperation with each other, in order to shape the good life for all. Many people from Kanthaus are frequent visitors in the K20 and vice versa, and we work on some projects together.

---

### [Projektwerkstatt Saasen](http://www.projektwerkstatt.de/index.php?p=10316)

[![](prowe.jpg)](http://www.projektwerkstatt.de/index.php?p=10316)

"Come to Saasen, to Ludwigstr. 11. There stands Projektwerkstatt. It's the other world, the not normal, mostly money free and completely free of property. You would dive into the - hopeless - try to build the right thing into the wrong one. And even though this cannot work out, at least celebrate the clash with what's existing and get out the maximum of what is possible from this other world, which is oh so necessary."

---

### [Lebenstraum Gemeinschaft](https://ltgj.de/) Jahnishausen

[![](jahnishausen.jpg)](https://ltgj.de/)

Jahnishausen has been a home for people since decades. It is quite close to us in two ways: Just one day of cycling away and housing the dad of one of us.

---

### [AAA Pödelwitz](https://aaapoedi.noblogs.org/)

[![](poedi.jpg)](https://aaapoedi.noblogs.org/)

Alternatives at the abyss: In Pödelwitz people are fighting for the end of coal mining, for saving the climate and for a just world for everybody.

---

### [Luftschlosserei](https://luftschlosserei.org/)

[![](luftschlosserei.png)](https://luftschlosserei.org)

A lovely and nicely structured commune south of Leipzig with which we share quite some values and friendly bonds.