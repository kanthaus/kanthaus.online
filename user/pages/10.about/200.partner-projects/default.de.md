---
title: 'Befreundete Projekte'
description: 'Andere uns ähnliche Orte'
---

Wir haben persönliche Beziehungen zu den folgenden Projekten, die viele - wenn nicht alle - unserer Ideale teilen:<br>
 

### [Freie Feldlage](https://freiefeldlage.de) in Harzgerode

[![](freiefeldlagePic.jpg)](https://freiefeldlage.de)

Die ehemalige Kinderklinik im Harz ist ein Ort voller Möglichkeiten. Das riesige Gelände mit seinen Gebäuden, Wäldern und Wiesen bietet soviele Aufgaben wie Freiheiten. Viele von uns haben hier Monate verbracht bevor es das Kanthaus überhaupt gab, und gerade im Sommer ist es für diejenigen unter uns, die es vorziehen in der Natur zu sein, nach wie vor ein Lieblingsort.

---

### [Funkenhaus](https://gelebteutopie.de/?target=_blank) in Greene

[![](funkenhausLogo.png)](https://gelebteutopie.de/?target=_blank)

Das ein Jahr jüngere, aber wohl deutlich bekanntere Funkenhaus (früher 'Kollektivhaus' genannt), ist sowas wie ein Schwesterprojekt für uns. Es entstand aus dem [living utopia](https://livingutopia.org) Netzwerk, das einen wichtigen Akteur in der deutschen Wandelbewegung darstellt. Unser Kontakt zu diesem Netzwerk geht für einige auf [yunity](https://yunity.org)-Zeiten zurück und für andere sogar noch weiter.

---

### [K20](https://k20-projekthaus.de/) in Salzderhelden

[![](k20.png)](https://k20-projekthaus.de/)

Das Kanthaus und die K20 sind eng verknüpft und zwar nicht nur namentlich. Das Projekthaus in Salzderhelden begreift sich als Lern- und Begegnungsraum, in dem andere Selbstverständlichkeiten gelebt werden. Deswegen ist dieser Ort auch vegan, drogenfrei und tauschlogikfrei. Außerdem soll dieser Ort allen Menschen zugänglich sein. Damit soll Geben und Nehmen voneinander entkoppelt werden, sodass alle nach Bedürfnissen und Fähigkeiten beitragen können. Die K20 steht für ein solidarisches, sozial-ökologisches und hierarchiekritisches Miteinander für das gute Leben für alle. Viele Menschen vom Kanthaus sind häufig zu Besuch in der K20 und andersum und wir machen immer wieder gemeinsame Projekte.

---

### [Projektwerkstatt Saasen](http://www.projektwerkstatt.de/index.php?p=10316)

[![](prowe.jpg)](http://www.projektwerkstatt.de/index.php?p=10316)

"Komm mal nach Saasen, in die Ludwigstr. 11. Dort steht die Projektwerkstatt. Die ist die andere Welt, das nicht Normale, weitgehend ohne Geld und völlig ohne Eigentum. Du würdest eintauchen in den - hoffnungslosen - Versuch, das Richtige im Falschen aufzubauen. Und wenn das schon nicht gelingen kann, wenigstens in voller Konfrontation mit dem Bestehenden das Maximale herauszuholen an dieser anderen Welt, die so nötig ist."

---

### [Lebenstraum Gemeinschaft](https://ltgj.de/) Jahnishausen

[![](jahnishausen.jpg)](https://ltgj.de/)

Jahnishausen bietet seit Jahrzehnten ein Heim für gemeinschaftsuchende Menschen. Es ist uns auf zweierlei Art recht nah: Nur einen Fahrradtag entfernt und unter anderem bewohnt vom Vater eines der unsrigen.

---

### [AAA Pödelwitz](https://aaapoedi.noblogs.org/)

[![](poedi.jpg)](https://aaapoedi.noblogs.org/)

Alternativen am Abgrund: In Pödelwitz machen sich Menschen für ein Ende des Braunkohletagebaus, die Rettung des Klimas und eine gerechte Welt für alle stark.

---

### [Luftschlosserei](https://luftschlosserei.org/)

[![](luftschlosserei.png)](https://luftschlosserei.org)


Eine sehr nette und gut strukturierte Kommune im Süden von Leipzig. Viele unserer Werte finden sich auch dort und wir kennen und mögen uns.