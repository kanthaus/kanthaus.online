---
title: "Visiting"
routes:
    aliases:
        - '/docs/visiting'
---

So you'd like to visit us? Great! We are happy to host individuals as well as groups. But before you come, please read the following info.

## Hosting
To visit Kanthaus you'll need a host. A host is a current Volunteer or Member. They will take care of showing you around, introducing you to the others, and generally making sure that both you and everyone else have a pleasant time during your visit. Please arrange this **before you arrive**. People in Kanthaus are often quite busy, it's not certain that there will always be someone available to host you.  You can contact us via [Slack](https://slackin.yunity.org) or [email](mailto:hello@kanthaus.online), or if you know someone who can host you, you can just arrange it through them.

## Groups
If you would like to come to us as a group to work on projects that are in line with our values, we would be happy to have you! Our house is well suited for groups of up to 10 people, under certain circumstances it is also possible to come to us as a group of up to 15 people. A group only needs one host and not one for every individual.

We already had quite some groups here and this process proved useful:
- Write to us and explain your ideas.
- The more info you can give, the easier for us. The amount of people, your room needs and the time frame are the points of most interest.
- We really appreciate having one person acting as the main contact to ensure good communication.
- If nobody of you has ever visited Kanthaus, we recommend you send someone before your planned meeting to check out in person if your expectations and our house fit together.
- During your stay here your group should get involved in saving food and/or cooking. Before you leave you should make sure the rooms you were using are in the same state as they were when you arrived.

## Joining for the long term
We are happy if people are interested in staying longer and becoming a part of our community. Since we are functionally living and not assigning permanent rooms, our process may be a little different than in other houses. First just come for a visit and see how that works out. Usually both sides get a feeling for each other pretty quickly. As long as a host is found for you, you can stay at Kanthaus. If you are ready to stay longer and take on more responsibility, you can aspire to become a volunteer during an evaluation. You can learn more about our position system and evaluations [here](/governance/positionsandevaluations).

## Rules
We have a [Constitution](/governance/constitution) and [Collective Agreements](/governance/collectiveagreements/) which you accept by visiting. These documents outline the processes and rules we have set for ourselves. Just to highlight two points: we do not permit indoor smoking and we wish to discuss bringing non-human animals (e.g. dogs) on a case-by-case basis. Additionally:
- Please do not up- or download any illegal content.
- Please be quiet in the garden after 22.00 and limit grilling/garden fires out of respect for our neighbors.

## Safety
The buildings were built in the 1880s and have suffered decades of questionable building practices. Our ongoing renovation works mean that some parts of Kanthaus are more or less building-sites. We must ask you to take responsibility for your safety and to be generally observant. Please be aware of:
- Sharp objects on the floor/ground. (Take care if walking barefoot.)
- [Hylotox 59 pesticide](https://de.wikipedia.org/wiki/Hylotox) (Lindane and DDT) in the wood of the attic: please read and follow safety signs before going there.

## Accessibility
Unfortunately Kanthaus has accessibility issues: everything is spread out over 3 floors and there are three steps at the front door. If you have issues with accessibility and would like to visit, please let us know about your needs and we can talk about how/if we can make it work.

## Other info
- There is no individual price for staying here; we have collective costs of ~€15,000 p.a. If you would like to donate, you can.
- We practice functional living and don't allocate personal rooms by default. If this topic is new to you or if you'd like a room reserved for you, please talk to your host.
- We save almost all food we eat. That means you don't do anybody a favor if you buy food for us. If you want to be nice and contribute something, it's much better to simply put some money into our donation box.
- Your host should give you a house tour and explain the basics soon after your arrival. If you want a sneak peak you can already read [the accompanying document](visiTour).
- Kanthaus is a clean but not dust free place. In the past that was a health issue for some people. A visitor with allergies to dust can request a room that they can try to keep freer of dust than others, but we cannot reduce the dust in the social and working areas of the house.

The address is [Kantstraße 20, Wurzen 04808](https://www.openstreetmap.org/search?query=20%20kantstrasse%20wurzen#map=19/51.36711/12.74075&layers=N). Please mention "phancy physalis" when you first contact us so that we know you've read this document.

Kanthaus eagerly awaits you!
