---
title: Besuchen
routes:
    aliases:
        - '/docs/visiting'
---

Du möchtest uns besuchen? Toll! Wir freuen uns, sowohl Einzelpersonen, als auch Gruppen bei uns aufzunehmen. Doch bevor du kommst, lies dir bitte die nachfolgenden Infos durch.

## Hosting
Um das Kanthaus zu besuchen brauchst du einen Host. Ein Host ist eine Person, die gerade Freiwillige:r oder Mitglied ist. Sie kümmert sich darum, dich herumzuführen, dich den anderen vorzustellen und schaut ganz allgemein, dass sowohl du als auch alle anderen die Zeit deines Besuches als angenehm empfinden. Kümmere dich bitte darum **bevor du ankommst**. Die Menschen im Kanthaus sind oft beschäftigt und es ist nicht sicher, dass sich immer wer finden wird, der das Hosting übernimmt. Du kannst uns per [Slack](https://slackin.yunity.org) oder [Email](mailto:hello@kanthaus.online) erreichen, oder fallst du eine Person im Kanthaus kennst, welche dich hosten könnte, das persönlich absprechen.

## Gruppen
Wenn ihr als Gruppe zu uns kommen möchtet, um hier an Projekten zu arbeiten, die unseren Werten entsprechen, freuen wir uns! Unser Haus ist für Gruppen bis 10 Personen gut geeignet, unter Umständen ist es auch möglich, als Gruppe von bis zu 15 Personen bei uns unterzukommen. Eine Gruppe braucht nur einen einzigen Host und nicht einen für jede Teilnehmer:in.
Das Kanthaus ist kein klassisches Seminarhaus. Wir können euch Räume und Material für eure Vorhaben zur Verfügung stellen. Es gibt jedoch keine separate Küche und Bäder für Gruppen, so wird z.B. das Kochen gemeinsam mit den Hausbewohner:innen koordiniert.
Inzwischen waren bereits einige Gruppen hier, und folgender Ablauf hat sich als sinnvoll erwiesen:
- Schreib uns an und erklär uns, was ihr vorhabt.
- Je mehr Infos du uns geben kannst, desto leichter läuft alles. Dabei interessiert uns vor allem, wieviele Menschen kommen, was ihr für Raumbedürfnisse habt und um welchen Zeitraum es geht.
- Für uns ist es besonders angenehm, wenn es eine Person gibt, die als Hauptansprechpartner:in fungiert.
- Falls bisher niemand von euch bei uns war, laden wir euch herzlich ein schon vor eurem Gruppentreffen mal jemanden vorbeizuschicken, um unverbindlich reinzuschauen und festzustellen, ob eure Erwartungen mit unserem Haus zusammenpassen.
- Während ihr hier seid, solltet ihr euch am Lebensmittelretten und/oder kochen beteiligen und bevor ihr wieder abreist die Räume, die ihr genutzt habt wieder in den Ausgangszustand versetzen.

## Langfristig mitmachen
Wir freuen uns, wenn Menschen Interesse haben, längerfristig da zu bleiben und ein fester Teil unserer Gemeinschaft zu werden. Da wir funktional wohnen und keine feste Zimmer vergeben, mag unser Prozess ein wenig anders sein als in anderen Häusern. Komm erstmal nur für einen Besuch vorbei und warte ab, wie das so läuft. Meistens bekommen beide Seiten recht schnell ein Gespür füreinander. So lange sich ein Host für dich findet, kannst du im Kanthaus bleiben. Wenn du bereit bist, länger zu bleiben und mehr Verantwortung zu übernehmen, kannst du bei einer Evaluation anstreben, Freiwillige:r zu werden. Mehr über unser Positionssystem und Evaluationen kannst du [hier](/governance/positionsandevaluations) erfahren.

## Regeln
Wir haben eine [Verfassung](/governance/constitution) und [Gemeinschaftliche Vereinbarungen](/governance/collectiveagreements/), die du mit einem Besuch automatisch akzeptierst. Diese Dokumente beschreiben die Abläufe und Regeln, die wir uns selbst gesetzt haben. Nur zwei Dinge an dieser Stelle: Rauchen ist innerhalb der Häuser nicht gestattet und über das Mitbringen nicht-menschlicher Tiere (z.B. Hunde) entscheiden wir immer im Einzelfall. Außerdem:
- Lade bitte keine illegalen Daten hoch oder runter, wenn du hier bist.
- Sei nach 10 Uhr Abends bitte leise im Garten. Grillen oder Feuer machen sollte aus Respekt für die Nachbar:innen die Ausnahme sein.

## Sicherheit
Die Häuser wurden in den 1880er Jahren gebaut und haben seitdem Jahrzehnte fragwürdiger Baupraktiken durchgemacht. Unsere stetigen Renovierungsarbeiten bedeuten auch, dass einige Teile des Kanthauses mehr oder weniger Baustellen sind. Deshalb müssen wir darum bitten, dass du selbst für deine Sicherheit Verantwortung übernimmst und generell aufmerksam bist. Bitte achte besonders auf:
- Scharfe Gegenstände auf dem Boden. (Pass auf, wenn du barfuß läufst!)
- Das Pestizid [Hylotox 59](https://de.wikipedia.org/wiki/Hylotox), das im Holz des Dachbodens vorkommt und Lindan und DDT enthält. Bitte lies die Sicherheitshinweise und befolge sie, wenn du in die Dächer hinauf gehst.

## Barrierefreiheit
Leider hat das Kanthaus Probleme mit der Barrierefreiheit: Alles erstreckt sich über drei Etagen und schon vor der Eingangstür befinden sich bereits drei Stufen. Wenn du auf Barrierefreiheit angewiesen bist und uns besuchen möchtest, sag uns Bescheid und dann schauen wir wie/ob wir das bewerkstelligen können.

## Weitere Infos
- Es gibt keinen fixen Preis für den Aufenthalt hier; unsere gemeinsamen Kosten belaufen sich auf ca. 15.000€ pro Jahr. Wenn du etwas spenden möchtest, kannst du das gerne tun.
- Wir leben funktional und vergeben im Normalfall keine Privatzimmer. Falls du gern einen nur für dich reservierten Raum haben möchtest, sprich darüber mit deinem Host. Falls du garnicht weißt worum es beim funktionalen Wohnen geht, lies z.B. [diesen Artikel](https://www.deutschlandfunknova.de/beitrag/funktionales-wohnen-wg-mit-gemeinsamem-schlafzimmer).
- Wir retten so gut wie alle Lebensmittel, die wir essen. Das bedeutet, dass du niemandem einen Gefallen tust, wenn du Essen für uns kaufst. Falls du nett sein und etwas beitragen möchtest, tu lieber ein paar Euro in den Spendenschuh.
- Wenn du ankommst, sollte dir dein Host bald eine Hausführung geben und dir die Grundlagen erklären. Wenn du schonmal einen Vorgeschmack bekommen möchtest, kannst du [das dazugehörige Dokument](visiTour) lesen.
- Das Kanthaus ist ein sauberer, aber kein staubfreier Ort. In der Vergangenheit hat das bei manchen Menschen zu Gesundheitsproblemen geführt. Ein_e Besucher_in mit Staub-Allergie kann einen Raum anfragen, welchen er_sie versuchen kann, staubfreier als andere zu halten. Wir können den Staub in den Gemeinschafts- und Arbeitsbereichen des Hauses jedoch nicht reduzieren.

Die Adresse ist [Kantstraße 20, Wurzen 04808](https://www.openstreetmap.org/search?query=20%20kantstrasse%20wurzen#map=19/51.36711/12.74075&layers=N). Bitte erwähne "phancy physalis", wenn du uns das erste Mal schreibst, sodass wir wissen, dass du dieses Dokument gelesen hast.

Das Kanthaus erwartet dich schon!
