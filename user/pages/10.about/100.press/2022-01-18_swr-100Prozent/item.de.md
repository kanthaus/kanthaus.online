---
title: "100 Prozent"
date: "2020-08-03"
---

_Eine Sendung über Menschen, bei denen die Grenze zwischen Leben und Arbeit verschwimmt. Neben uns werden zwei weitere Projekte vorgestellt._

===

#### Stream
Die Sendung "100 Prozent - Leben ohne Kompromisse: Wir teilen alles! Mein Leben ist meine Arbeit" kann kostenlos in der [ARD Mediathek](https://www.ardmediathek.de/video/100-prozent-leben-ohne-kompromisse/wir-teilen-alles-mein-leben-ist-meine-arbeit/swr/Y3JpZDovL3N3ci5kZS9hZXgvbzE1OTY3MzM/) gestreamt werden.
