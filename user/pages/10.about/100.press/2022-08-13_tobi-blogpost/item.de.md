---
title: "Suche nach dem richtigen Leben im Falschen"
date: "2020-08-13"
---

_Ein Blogpost, in dem Tobi über seine Erfahrung mit Kanthaus als Teil einer größeren Reise durch Deutschland schreibt._

===

#### Blogpost
Der Blogpost kann barrierfrei auf [Tobi's blog](https://nimble-tartufo-0e4a61.netlify.app/post/07-kanthaus/) gelesen werden. ([archive.org capture](https://web.archive.org/web/20231002164659/https://nimble-tartufo-0e4a61.netlify.app/post/07-kanthaus/))