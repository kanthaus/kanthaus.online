---
title: "Just common care?"
date: "2022-11-01"
---

**Kollektive Reproduktionsarbeit im Spannungsfeld von Kritik, Transformation und Utopie**

(_Collective, reproductive work in the tension field of Critique, Transformation and Utopia_)

Master Thesis in Transformation Studies. Marie Marwege, University Flensburg.

===

In her thesis, Marie interviews people from [Barrierfreehouse](https://barrierfreehouse.wordpress.com/), [Haus des Wandels](https://hausdeswandels.org), Kuhkoppel, Kanthaus and the social workers of a housing association. Marie examines through the lense of house work (e.g. cleaning) how collectives organise and generally live out their structures and how this relates to socio-ecological transformation.

With her kind permission, we can offer her thesis for [download as a pdf](./JustCommonCare_Masterarbeit%20MM_KS.pdf)