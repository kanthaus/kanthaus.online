---
title: "Just common care?"
date: "2022-11-01"
---

**Kollektive Reproduktionsarbeit im Spannungsfeld von Kritik, Transformation und Utopie**

Masterarbeit im Studiengang Transformationsstudien. Marie Marwege, University Flensburg.

===

Diese Arbeit basiert auf Interviews, die Marie mit Menschen aus dem [Barrierfreehouse](https://barrierfreehouse.wordpress.com/), dem [Haus des Wandels](https://hausdeswandels.org), der Kuhkoppel, dem Kanthaus und Sozialarbeiter*in einer Wohnungsgenossenschaft geführt hat. Sie analysiert anhand der Art und Weise wie die Gruppen ihre Hausarbeit (z.B. putzen) organisieren und erleben generelle Strukturen und deren Zusammenhang mit der sozial-ökologische Transformation.

Vielen Dank, dass wir diese Arbeit zum [Download als pdf](./JustCommonCare_Masterarbeit%20MM_KS.pdf) anbieten dürfen!