---
title: "Kanthaus"
slogan: A house for sustainable projects
imports: 'projects.yaml'
content:
    items: '@self.children'
    limit: 10
    pagination: true
    order:
        by: date
        dir: desc
---

<!-- projects are included from projects.yaml, template from home.html.twig, style from gallery.css -->

<p style="text-align: center">You are welcome to stay with us and participate in these projects!</p>

If you have a new idea that would fit us well, please send us a [message](/contact)!

<a rel="me" href="https://kolektiva.social/@kanthaus"></a>
