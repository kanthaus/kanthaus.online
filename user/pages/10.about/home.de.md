---
title: "Kanthaus"
slogan: Haus für nachhaltige Projekte
imports: 'projects.yaml'
content:
    items: '@self.children'
    limit: 10
    pagination: true
    order:
        by: date
        dir: desc
---

<!-- projects are included from projects.yaml, template from home.html.twig, style from gallery.css -->

<p style="text-align: center">Du bist herzlich eingeladen, bei uns zu bleiben und dich in diese Projekte einzubringen!</p>

Wenn du eine neue Idee hast, die gut zu uns passen würde, schreib uns gerne eine [Nachricht](/contact)!

<a rel="me" href="https://kolektiva.social/@kanthaus"></a>
