---
title: Renovation of the house
description: All the manual work that still is to do
---

**We want to make the houses in Kantstraße 20 and 22 into the spaces we dream of!** <br>
That means that there is still a lot to do...

===

### What do we want?

The general idea is that we want a space which is suitable for focused work on projects, while at the same time providing us with everything we need to actually _live_ there and have a nice, homely community life. We want to support others with our infrastructure, and we want to have room to be alone. We want a design that naturally divides the space into areas of either work, play or retreat. We want everything to work easily, for newcomers as well as for people who have been Kanthausians for a longer period of time already. We want the structure of the house to support our social structure, and in the longer run we also want everything to look kinda good.

To bring all these dreams together and to then make them reality is no easy task!

### How do we manage this?

We currently don't have a refined process for this (anymore). Aside from individual do-ocratic action and small group actions that gets most of the work done, we have a brief monthly teams meeting to make transparent what people are up to and where others can join.
