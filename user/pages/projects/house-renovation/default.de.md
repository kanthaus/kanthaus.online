---
title: Hausrenovierung
description: All die Arbeit, die nich zu tun ist
---

**Wir wollen die Häuser in Kantstraße 20 und 22 zu den Häusern unserer Träume machen!** <br>
Das heisst es ist noch viel zu tun...

===

### Was wollen wir?

Generell wollen wir einen Raum, an dem produktiv an Projekten gearbeitet werden kann, wir gleichzeitig aber auch alles finden, um tatsächlich dort zu _leben_, und zwar in einer schönen, heimeligen Gemeinschaft. Wir wollen Andere mit unserer Infrastruktur unterstützen, und wir wollen den Raum haben allein sein zu können. Wir wollen ein Layout, das ganz natürlich Bereiche entstehen lässt, die entweder der Arbeit, der Gemeinschaft oder dem Rückzug gewidmet sind. Wir wollen, dass das alles einfach erkennbar ist und für Neulinge wie für alt eingesessene Kanthausians gleichermaßen funktioniert. Wir wollen, dass die physischen Strukturen unsere sozialen Strukturen unterstützen, und irgendwann wollen wir auch mal, dass alles ein bisschen hübsch aussieht.

All diese Träume zusammen zu bringen und Realität werden zu lassen ist keine leichte Aufgabe!

### Wie gehen wir das an?

Momentan haben wir keinen ausgefeilten Prozess mehr dafür. Neben spontaner individueller Betätigung gibt es ein kurzes monatliches Team-Treffen, wo geplante Tätigkeiten transparent gemacht werden.
