---
title: Kontakt
---
*Wenn Du uns besuchen möchtest, lies Dir bitte zunächst [diese Informationen](../about/visiting) durch und kontaktiere uns wenn möglich bevor Du ankommst.*

- Email: [hello@kanthaus.online](mailto:hello@kanthaus.online)
- Telefon: <a href="tel:+4934258527995">+49 3425 8527995</a>
- Slack: #kanthaus auf [yunity.slack.com](https://yunity.slack.com) (Lass dich auf [slackin.yunity.org](https://slackin.yunity.org) einladen)
- Facebook: [Kanthaus](https://www.facebook.com/kanthaus.online/)
- Mastodon: [Kanthaus](https://kolektiva.social/@kanthaus) (auf Englisch)
- Adresse: [Kantstraße 20, 04808 Wurzen, Germany](https://www.openstreetmap.org/search?query=20%20kantstrasse%20wurzen#map=19/51.36711/12.74075&layers=N)
- Bankverbindung:
  - Wandel Würzen e.V.
  - IBAN: DE78 4306 0967 1233 1253 00
  - BIC: GENODEM1GLS
  - Bitte verwende unbedingt **ausschließlich den Betreff "Spende", wenn du uns Geld spenden möchtest**, damit wir das Geld entsprechend verbuchen dürfen. Für die Ausstellung einer Spendenbescheinigung, kontaktiere uns.

## Anfahrt

### per Bahn
Ab Leipzig Hauptbahnhof einmal die Stunde mit dem RE50 Richtung Dresden (17 Minuten) oder mit der S-Bahn S3 Richtung Oschatz/Wurzen (37 Minuten).
Ticket: Bahncard 25 Einzelfahrt, MDV EasyGo App Hopperticket oder normales Ticket vom Automaten.

### trampen
Auf A14 Richtung Dresden bis Autobahnanschluss Grimma.
Von dort weiter nach Norden bis Wurzen (klappt im Dunkeln schlecht).

Direkt an der A9 Autobahnanschluss Großkugel, dort gegenüber vom Autohof geht ein Fußweg zur S-Bahn Schkeuditz West (die S3 fährt in einer Stunde ohne Umstieg bis Wurzen).
Nutz am besten ein MDV EasyGo App Hopperticket, das kostet nur die Hälfte der Einzelfahrt.

Alternativ nach Leipzig reinbringen lassen und dort direkt S-Bahn/RE.

### mit dem Auto
A14 bis Anschlussstelle Wurzen, dann der B6 bis Wurzen folgen. Beim Lidl halb links rein, bei der Kriegerdenkmalsäule 2x rechts in die Kantstraße.


## Impressum
Angaben gemäß § 5 TMG

Wandel würzen e.V.<br />
Kantstraße 20<br />
04808 Wurzen<br />
Vertreten durch:<br />
Douglas Webb, Janina Becker<br />
Kontakt:<br />
Telefon: 03425-8527995<br />
Fax: 03425-8541805<br />
E-Mail: wandel-wuerzen@kanthaus.online

Registereintrag:<br />
Registergericht: Amtsgericht Leipzig<br />
Registernummer: VR 6493<br />

Verantwortlich für den Inhalt nach § 18 Abs. 2 MStV:<br />
Janina Becker<br />
Kantstraße 20<br />
04808 Wurzen
