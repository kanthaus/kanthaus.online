---
title: Kanthaus 5 year anniversary!
date: "2022-07-12"
taxonomy:
    tag: [blog]
    author: Kanthaus
---

5 years ago today, we first got the keys to 20 and 22 Kantstraße. It was a small crew then, in an unfamiliar town, tired from a strained planning phase at Harzgerode. The buildings were completely empty; there was no connection to water, gas, electricity or internet. And the roofs were leaking.

But people came during that hot Summer of 2017, and the small crew became a bigger group. The mixture of donations and loans which bought the opportunity seemed not to be wasted. Sustained by cans of ravioli and beer rescued from festivals, people connected utilities, made connections in Wurzen and began to form a community.

5 years on, and here we still are. The roof finally became waterproof last year after almost 4 years of work, ~5 building months and a small team keeping it going. The freeshop has become an establishment in the town. The Finances Are OK. A Foodsharing Fairteiler was built...

It's not all been easy. Although many relationships began here, some strained, and some ended. The Kanthaus project was not able to provide for the needs of all who came. Amid the celebration of many successes, we ask for forgiveness from anyone who felt unfairly treated or neglected.

This year sees several longer-term residents from Kanthaus (and Wurzen) moving on. We wish them the best in their new situations. We also exit the buy-back clause of the purchase—we're here forever now! (Joke?)

If you're reading this post, it's because you are somehow connected. Many of you will have been part of this community at some point of time, in one (or many) of the shifting social constellations that have occurred. Perhaps you were here for a WuWiTa (Wurzener Winter Tage) or a building week (or building month?) Perhaps you were visiting a resident or just needed to be somewhere else for a while. Maybe you came with a group? However it came to be, thank you. You were all needed.

Come say hi a week from now at 17:00 (CEST) on the Tuesday the 19th of July on [BigBlueButton](https://meet.livingutopia.org/b/dou-hio-vh0-7jv) to meet current residents and residents from different points of history, to catch up and exchange stories :)
	
Kanthaus

![](1.jpg)
![](2.jpg)
![](3.jpg)
![](4.jpg)
![](6.jpg)
![](7.jpg)
![](8.jpg)
![](9.jpg)
![](10.jpg)
![](11.jpg)
![](12.jpg)
![](13.jpg)
![](14.jpg)
![](15.jpg)
![](16.jpg)
![](17.jpg)
![](18.jpg)
![](19.jpg)
![](20.jpg)
![](21.jpg)
![](22.jpg)
![](25.jpg)
![](26.jpg)
![](27.jpg)
![](28.jpg)
![](29.jpg)
![](30.jpg)
![](31.jpg)
![](32.jpg)
![](33.jpg)
![](34.jpg)
![](35.jpg)

Some stats for the nerds:
* 615 different people stayed here
* 22248 nights spent here
* 1077.6075 m³ of tap water used
* 5607.569 m³ of gas used
* at least 24 MWh of electricity sent to the grid with our solar plant
* 257 evaluations
* 251 CoMe's
