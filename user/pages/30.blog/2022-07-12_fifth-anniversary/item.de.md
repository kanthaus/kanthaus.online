---
title: Kanthaus 5 Jahre Jubiläum!
date: "2022-07-12"
taxonomy:
    tag: [blog]
    author: Kanthaus
---

Vor genau 5 Jahren hielten wir zum ersten Mal die Schlüssel zu den Häusern Kantstraße 20 und 22 in den Händen. Wir waren damals eine kleine Gruppe, Neuankömmlinge in einer fremden Stadt und müde von der anstrengenden Planungsphase in Harzgerode. Die Häuser waren komplett leer; es gab weder Wasser, Gas, Strom noch Internet. Und die Dächer waren undicht.

Doch während des heißen Sommers 2017 kamen immer mehr Menschen zu uns und wir wuchsen zu einer größeren Gruppe heran. Das durch Spenden und Direktkredite aufgebrachte Geld, das den Start des Kanthauses ermöglicht hatte, schien nicht verschwendet worden zu sein. Und die Menschen begannen zu arbeiten: Kabel wurden verlegt, die Heizung angeschlossen, erste Kontakte geknüpt. Anfangs noch mit der Kraft von auf Festivals geretteten Ravioli- und Bierdosen fingen wir an eine Gemeinschaft zu bilden.

5 Jahre später, und hier sind wir. Nach fast 4 Jahren Arbeit, inklusive ~5 Baumonaten und einem kleinen Team Unermüdlicher, die das Projekt stets vorangetrieben haben, sind die Dächer endlich dicht. Der Verschenkeladen ist zu einer bekannten Größe in der Stadt geworden. Die Finanzen sind in Ordnung. Ein öffentlich zugänglicher Foodsharing Fairteiler wurde aufgebaut…

Es war nicht immer leicht. Viele Beziehungen begannen hier, doch manche erlebten auch Krisen und manche endeten. Das Kanthaus konnte nicht die Bedürfnisse aller Menschen, die hierher kamen, erfüllen. Während wir also unsere Erfolge feiern, wollen wir gleichzeitig auch diejenigen um Verzeihung bitten, die sich ungerecht behandelt oder vernachlässigt gefühlt haben.

In diesem Jahr verlassen einige der Langzeit-Bewohnis das Kanthaus und Wurzen. Wir wünschen ihnen alles Gute beim Voranschreiten. Auch das Rückkaufrecht auf unsere Häuser, das uns anfangs in den Kaufverträgen unsicher machte, läuft nun aus - wir bleiben also für immer! (…oder?

Du liest diesen Blogpost, weil du zu irgendeiner Zeit mal Teil der Gemeinschaft warst - egal wie kurz. Vielleicht warst du bei den WuWiTa, den Wurzener Winter Tagen, oder einer Bauwoche (oder einem Baumonat?). Vielleicht hast du ein Bewohni besucht oder brauchtest einfach mal etwas Neues um dich herum. Vielleicht kamst du als Teil einer Gruppe? Wie auch immer es war, schön, dass du hier warst! Und danke, du und all die Anderen habt das Kanthaus mitgeprägt.

Komm gern am 19.7. um 17:00 ins [BigBlueButton](https://meet.livingutopia.org/b/dou-hio-vh0-7jv) und triff einige der momentanen Bewohnis, sowie andere dem Kanthaus verbundene Menschen aus unserer nun schon 5 jahre währenden Geschichte. Lasst uns mal wieder schnacken und Geschichten austauschen! :)

Kanthaus

![](1.jpg)
![](2.jpg)
![](3.jpg)
![](4.jpg)
![](6.jpg)
![](7.jpg)
![](8.jpg)
![](9.jpg)
![](10.jpg)
![](11.jpg)
![](12.jpg)
![](13.jpg)
![](14.jpg)
![](15.jpg)
![](16.jpg)
![](17.jpg)
![](18.jpg)
![](19.jpg)
![](20.jpg)
![](21.jpg)
![](22.jpg)
![](25.jpg)
![](26.jpg)
![](27.jpg)
![](28.jpg)
![](29.jpg)
![](30.jpg)
![](31.jpg)
![](32.jpg)
![](33.jpg)
![](34.jpg)
![](35.jpg)

Einige Statistiken für Nerds:
* 615 verschiedene Personen waren hier
* 22.248 hier verbrachte Personen-Nächte
* 1077.6075 m³ Leitungswasser benutzt
* 5607.569 m³ Gas verbrannt
* mindestens 24 MWh Strom ins Stromnetz eingespeist
* 257 Evaluationen
* 251 Coordination Meetings
