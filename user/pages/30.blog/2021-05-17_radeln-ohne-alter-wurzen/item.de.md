---
title: 'Radeln Ohne Alter Wurzen'
date: "2021-05-17"
taxonomy:
    tag: [blog]
    author: Antonin
---


Wenn ich eine Pause vom Leben im Kanthaus brauche, ist eine kurze Fahrradfahrt oft sehr wirksam.
Ist es die frische Luft? Die Lust, reibungslos auf einen tadellosen Radweg zu gleiten? Der Blickkontakt mit beliebigen Leuten auf der Straße?
Ich weiß nicht genau, aber es klappt einfach.
Ohne diese Fahrten auf dem Sattel wäre mir das Leben in Pandemiezeiten ziemlich unerträglich.

Beim Fahrradfahren in Wurzen sind die Chancen hoch, dass man einem Altersheim begegnet. [Fast das Drittel der Wurzener sind älter als 65 Jahre](https://www.demografie.sachsen.de/monitor/html/atlas.html) und die Stadt ist keine Ausnahme von dem Alterungstrend.
In Wurzen sind mehr als 10 Pflegeeinrichtungen zu finden und mehr werden gebaut.
Wenn ich in ihrer Nähe fahre, kann ich Licht hinter den Gardinen sehen und manchmal auch jemanden am Fenster.
Die meisten Bewohner können nicht einfach ein Fahrrad holen, um ihrem Tagesablauf zu fliehen.
Die Pandemie hat sie gezwungen zusätzlich zu ihren Bewegungseinschränkungen eine klösterliche Abgeschiedenheit zu verfolgen.

===

Gleichzeitig hört man im Kanthaus oft von einer Lust sich mehr mit der nahen Umgebung außerhalb des Hauses zu beschäftigen.
In gewisser Weise bilden wir unsere eigene Blase und die Pandemie hat das nicht verbessert.
Unser Verschenkeladen am Offenen Dienstag war eine beliebte Einrichtung, die unsere Nachbarn oft besucht haben.
Dieser ist allerdings seit Monaten geschlossen.
Deshalb haben wir versucht, mehr Kontakte mit der Stadt zu knüpfen und uns entschlossen einen Standort von Radeln Ohne Alter zu gründen.

[Radeln Ohne Alter](https://radelnohnealter.de/) ([Cycling Without Age](https://cyclingwithoutage.org/) auf English) ist eine Bewegung, die 2012 in Kopenhagen angefangen hat. In den letzen Jahren sind viele weitere Länder dazugekommen. Das Grundprinzip ist einfach: Ehrenamtliche bieten Rickschafahrten für Pflegeheimbewohner an.
Rickschas sind dreirädrige Fahrräder mit einem Sitzplatz für zwei Fahrgäste in der Front.
Die Fahrten sind kostenlos und zum Spaß. Das Ziel ist nur draußen eine schöne Zeit zusammen zu verbringen.
Außerdem werden die Räder elektronisch unterstützt und die Sitzpositionen erleichtert das Gespräch zwischen Radfahrern und Fahrgästen. So einfach ist es!

Zusätzlich zu den sozialen Aspekten gibt es viel mehr Gründe, diese Bewegung zu unterstützen.
Viele von uns engagieren sich für eine Verkehrswende, für eine Förderung des Fahrradverkehrs, protestieren gegen Autokonzerne oder sind in Waldbesetzungen gegen Autobahnprojekte aktiv.
Dabei wird leicht vergessen, dass sich viele Menschen vom Radfahren ausgeschlossen fühlen. Sie haben Angst vor dem Verkehr, ihr Gesundheitszustand erlaubt es ihnen nicht oder sie haben nie gelernt Fahhrad zu fahren.
Dreiräder auf der Straße zu fahren ist also auch eine Art zu zeigen, dass Fahrräder nicht immer gleich aussehen sollen.
[Spezielle Fahrräder](https://www.spezialradmesse.de/home.html) sind für alle möglichen Bedingungen verfügbar und nicht alle Menschen sollen wie ein Tour-de-France-Fahrer aussehen, wenn sie einkaufen fahren.

Mit unserem [Standort von Radeln Ohne Alter in Wurzen](https://radelnohnealter.de/wurzen/) hoffen wir im Sommer 2021, wenn die Impfungkampagne gut vorangekommen ist, unsere ersten Fahrten anzubieten.
Dank Hygienekonzepten war es in anderen Standorten möglich, auch in der Pandemie weiter zu fahren. Wir sind daher hoffnungsvoll bald loslegen zu können.
Viele von uns im Kanthaus würden gerne mit einer Rickscha durch Wurzen fahren. Es ist aber auch das Ziel, Ehrenamtliche außerhalb vom Kanthaus zu finden.
Das sollte eine gute Gelegenheit sein neue Kontakte zu knüpfen und dafür zu sorgen, dass die Rickschas gut benutzt werden.
Wenn Du gern mitmachen willst, kannst du dich bei [unserer Karrot-Gruppe](https://karrot.world/#/groupPreview/135) bewerben, wo wir das Projekt koordinieren.

Einer der ersten Schritte, um unseren Standort zu starten, ist Rickschas zu kaufen.
Dafür brauchen wir Deine Hilfe! Wir haben [eine Crowdfunding-Kampagne angefangen, um unser erstes Fahrrad zu finanzieren](https://betterplace.org/p94806).
Ein weiteres Fahrrad wird dank einer Förderung der [Deutschen Postcode Lotterie](https://www.postcode-lotterie.de/projekte/) finanziert. Verbreitet die Nachricht, wir brauchen Deine Unterstützung!

