---
title: 'Freiwilliges Freies Jahr in Wurzen - jetzt anmelden!'
date: "2023-09-21"
taxonomy:
    tag: [blog]
    author: Kito
---

Wir möchten euch darauf aufmerksam machen, dass das **Freiwillige Freie Jahr (FFJ)** ab Februar 2024 erstmals im Kanthaus in Wurzen stattfinden wird. Ein kleines Team von Engagierten hat sich zusammengefunden, um ein buntes, spannendes Programm zusammenzustellen.

===

<div markdown="1" style="text-align:center;">
![](Sharepic_FFJ.jpg)
</div>

**Was ist das FFJ?** Das FFJ ist ein selbstorganisiertes und bedürfnisorientiertes Vollzeit-Bildungsprogramm für Veränderungslustige, Ideenspinner:innen und Abenteurer:innen mit Mut zur kritischen Auseinandersetzung mit sich und der Welt.

Gemeinsam mit einer Gruppe von 7 bis 8 Menschen wirst du für **sechs Monate** im **Kanthaus** in Wurzen (bei Leipzig) wohnen und deinen persönlichen Weg des Politisch-aktiv-Seins erforschen und finden. In dieser Zeit hast du die Möglichkeit, den Einsatz für eine bessere Welt und ein gutes Leben für alle in den Mittelpunkt deines Lebens zu stellen. Wir wollen gesellschaftliche Normen gemeinsam kritisch hinterfragen und Utopien im Kleinen Wirklichkeit werden lassen. Während des FFJs wirst du transformative Lebensformen und Konzepte kennenlernen können und kannst durch inspirierende Projekte und Inputs neue Perspektiven für eine nachhaltige Gesellschaft bekommen. Uns ist es dabei wichtig, diese alternativen Lebenswege nicht nur theoretisch zu durchdenken, sondern auch gemeinsam praktisch zu beschreiten.

Wenn du neugierig geworden bist, schau gern auf unserer Website unter https://freiwilliges-freies-jahr.de/ffj-wurzen-2024 vorbei. Die Anmeldung ist bis zum **10. November** möglich, das FFJ startet am **05. Februar 2024**.

Wir würden uns sehr freuen, wenn ihr unser Programm an Menschen weiterleitet, die Interesse daran haben könnten. Im Anhang der Mail ist ein Sharepic, dass du gern nutzen kannst. Wenn ihr möchtet, können wir euch ein kleines Paket mit Infomaterialien zusenden. Außerdem können wir uns vorstellen, für eine kleine Infoveranstaltung vorbeizukommen, fragt uns dafür gern einfach an. Wir wünschen uns eine möglichst diverse Gruppe von Teilnehmenden. Es sind keine Vorerfahrungen in politischem Engagement notwendig, daher freuen wir uns, wenn die Einladung mit möglichst vielen verschiedenen Menschen geteilt wird.

Wir sind zudem noch auf der Suche nach Menschen, die das FFJ mitgestalten wollen, z.B. durch Inputs/Workshops zu verschiedenen Themen oder für bestimmte andere Aufgaben. Wenn du dir vorstellen kannst, mitzuwirken, schreib uns gern!

Ihr erreicht uns unter kanthaus@freiwilliges-freies-jahr.de

Liebe Grüße,
das FFJ-Team


