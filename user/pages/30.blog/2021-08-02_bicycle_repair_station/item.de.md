---
title: 'Fahrradreparaturstation'
date: "2021-08-02"
taxonomy:
    tag: [blog]
    author: Matthias
header_image: true
---

Wie oft passiert es, dass du mit dem Fahrrad unterwegs bist, und merkst, dass du deinen Sattel gern ein paar Zentimeter verstellen würdest? Oder dir fällt auf, dass ein kleines bisschen mehr Luft auf dem Vorderreifen praktisch wäre? Das Schutzblech klappert schon seit Wochen... doch dafür extra die Werkzeugkiste auspacken?

Bei all diesen Problemen könnte doch eine Fahrradreparaturstation, an der du zufällig vorbeikommst, helfen.

Deswegen haben wir mal eine gebaut...

===

Die Idee kam schon 2018: Wir haben von der Ausschreibung des Sächsischen Mitmachfonds gehört; ein Förderprogramm, für lokale Aktivitäten u.A. im Bereich "Mobilität". Wäre es nicht nett, so eine Fahrradreparaturstation zu haben? Was gibts denn da so auf dem Markt? Ah ja, die kosten so 1500-4000€. Wo? Na, direkt am Kanthaus, da ist genug Platz, der Ort ist einigermaßen gut besucht, nah am Bahnhof, nah an der Stadt; Mulderadweg nicht zu weit weg. Okay, der Rahmen ist da, lasst uns mal auf die Förderung bewerben.

Die Zusage erfolgte dann nach wenigen Wochen. Da haben wir uns gefreut und sind detaillierter in die Planung gegangen.

![](repair_station_drawing.jpg)

Schnell war der Bürgersteig ausgemessen und die Position bestimmt: Zwischen den südlichen Fenstern der Kantstraße 22 - denn hier ist nur ein wenig weiter die Straßenlaterne, an der wir uns auch direkt orientieren, um den Bürgersteig nicht weiter als nötig mit Hindernissen zu versehen.

Die Absprache mit den zuständigen Personen der Stadtverwaltung Wurzen war einfach, unserem Plan wurde zugestimmt und eine Nutzungserlaubnis für den Platz erteilt.

Leider dauert es nun ein wenig länger, da die Fahrradreparaturstation bei uns nicht immer die höchste Priorität besaß.

Wie wollen wir es nun genau machen?

Nun, ich wünsche mir...

* die wichtigsten Werkzeuge (Schraubenschlüssel, Schraubendreher, Inbusschlüssel)
* eine komfortable Möglichkeit, Reifen aufzupumpen
* eine Steckdose (schon auch ein Selbstzweck, wir laden bestimmt 4x im Jahr die Autobatterie unseres Keinmüllwagens auf; und es bietet sich einfach an)
* eine Wassernachfüllstation
* eine Möglichkeit, das Fahrrad an einen Halter zu hängen (weil das alle kommerziellen Stationen bieten, **muss** es doch sinnvoll sein, oder?)

... und recherchiere, was es auf dem Markt gibt.

Leider nichts, was dieses so vereint, dass es praktisch an den von uns ausgesuchten Ort passt.

Deswegen wirds ein Selbstbau: Ein bisschen Schweißen macht Spaß und lässt Raum zum lernen. Zudem haben wir volle Freiheit, was für Werkzeuge in welcher Qualität wir anbringen und wie das alles genau werden soll.

![](innenansicht_bau.jpg)

Die Möglichkeit, Reifen zu füllen, ist mir die wichtigste.
Deshalb soll ein Anschluss an das zentrale Druckluftsystem vom Kanthaus her, damit per Tastendruck einfach der Reifen gefüllt werden kann.
Dazu siehst du im Bild die Technik in der Station: Magnetventil, Kugelhahn als Durchflussregler, Manometer. Eine Halterung im Rahmen fängt die Kräfte direkt am Anschluss des Schlauchs auf, um das Innenleben zu schützen, wenn der Schlauch mutwillig durch die Kabelverschraubung herausgezogen wird.
Der Ventilkopf soll so vandalismussicher wie möglich sein und auf alle Ventile passen.
Leider müssen wir hier Abstriche machen, es wird der SKS MultiValve. Der Ventilkopf kostet 8€, die Dichtungsgummis sind tauschbar.
Bei dem Preis ist es okay, wenn der einmal im Jahr geklaut wird, denn theoretisch geht er mit der Hand vom Schlauch abzuschrauben.
Da könnte dann auch mal ein Schrumpfschlauch drüber.
Praktisch ist, dass die Dichtungsgummis tauschbar sind, unpraktisch, dass an der Reparaturstaton die benötigten Schraubendreher hängen, um den Kopf auseinanderzunehmen.

Alle Werkzeuge werden mit 4mm Edelstahlseil aufgehängt - dazu dient das Rohr links im Bild, um die das Stahlseil innerhalb der Station mit Seilklemme verschlossen wird. Der Deckel macht dies unerreichbar.
Der Verschluss am Werkzeug wahlweise per Seilklemme (kaputtes Werkzeug leicht tauschbar) oder Alu-Presshülse (nur mit Bolzenschneider klaubar).

Die Station selbst aus 5mm Stahl, dick mit Rostschutzfarbe gestrichen.

Das Manometer mit 8mm Acrylglas abgedeckt - sollte uneinschlagbar sein. Leider ist der Ausschnitt etwas zu tief und damit die Skala nicht so gut ablesbar. Naja, passiert ;-)

Der Deckel hat oben zwei Führungsstifte und wird unten mit zwei M12 Muttern gesichert - das sollte als Vandalismusschutz reichen, das Werkzeug ist zwar handelsüblich, aber in der Regel nicht mitgeführt :-)

Alles in allem hängt die Station 3 Jahre nach Planung. Die Materialkosten sind überschaubar, die Reparierbarkeit sehr gut, der Zeitaufwand (also Personalaufwand) mit mehr als 100 Stunden Arbeit leider durchaus höher als gedacht.
Das Ergebnis erfreut uns doch - und wir sind gespannt, zu beobachten, wie die Station genutzt wird. Und auch gespannt, wie lange es dauert, bis der Vandalismus auch hier ankommt, oder ob wir vielleicht sogar verschont werden.

Komm vorbei, schau sie dir an und reparier doch mal mindestens eine Sache, die dich an deinem Fahrrad seit Monaten nervt :-)
