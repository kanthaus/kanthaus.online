---
title: 'Keeping network simple?'
date: "2022-06-24"
taxonomy:
    tag: [blog]
    author: chandi
---

After 5 years, the Kanthaus network has reached a state that sometimes causes headaches even to somewhat experienced network admins trying to get an overview.

To be exact, it was _my_ headaches and a lot of time spent trying to figure out which cable goes where.

To make it easier for people to take responsibility for the Kanthaus network in the future, I have decided to change some things in the network, which can be summarized in the following points:
- Simplification of the topology
- Easier WiFis
- Central management of access points
- Updating documentation


## Simplification of the topology
Probably the best way to describe and justify the network is with one word: **grown**

#### Before: a grown chaos
It all started in K20-1, with the first rooms being populated and the need for internet access there. From there on, new cables were laid wherever one was needed, and from there, with the least effort, to wherever the next one was needed.

Those who were there in the early days might remember: There was an unlimited number of construction sites in the house and the most important thing was to somehow get the basic infrastructure up and running so that people could live here - beauty, accessibility or even empowerment were not relevant values at this stage.


_**(Please do not waste too much time trying to understand this)**_
![network topology before - a mess](https://pad.kanthaus.online/uploads/d40f9e85-d0d5-4779-a528-a23d15799a24.png)

All devices have their legitimate purpose, but the cabling doesn't make it easy to keep track of the network.


#### Now
In order to have the network structure easier to understand in the future, the new structure follows the following points:
- A central location in the basement
- One cable from the basement to each floor
- An access point on each floor as a central point for further distribution, if necessary.

![network topology now: central rack in the basement, one cable to each floor](https://pad.kanthaus.online/uploads/a265c069-94d2-47b2-900c-3cd9c6f59afb.png)

Hopefully a little easier to keep an overview for you, too.



## Easier WiFis
At Kanthaus, we use different networks for more security and to give people access without a password.

**Examples**
- Public guest network routed via the Netherlands so we don't get any problems with illegal usage.
- Network for easily hackable devices, which should rather run isolated without internet access [#IoT](https://en.wikipedia.org/wiki/Internet_of_things#Security) (front door, printer, inverter, water meter,...)
- Network in which devices cannot see each other because there is no need for it (e.g. smartphones) -> increased security.

#### Before
- Use of "WPA2 Enterprise", which offers technological advantages, but a very high barrier in the connection setup which was not manageable for almost everyone [without detailed instructions](https://kanthaus.online/de/blog/2019-08-27-kanthaus-wifi)
- Different passwords that no one remembers
- Sense of the different networks hard to understand, partly also simply senseless
- Problems with compatibility of some features

In the end, almost everyone stayed in the old `wuppdays` WiFi, because at least one of the points made the new networks inaccessible for them.

#### Now

- Clear decision path
  ![](https://pad.kanthaus.online/uploads/9ce05938-c56f-443b-b320-d2699882f064.png)

- The same, easy to remember, password works for both encrypted networks.
- People can log in like in any other network (click on name -> enter password -> done)
- `kanthaus` with highest security standard as default
- `kanthaus-insecure` with compatibility for (hopefully) all devices



## Central management of the access points
As of now we have 7 access points (AP) in use, which all want to be (correctly) configured. Not only is changing something very time-consuming, it is also very prone to introducing new errors.

#### Before
The problem of too many access points to individually configure has already been addressed with a [self-made script](https://gitlab.com/kanthaus/openwrt-config), but this solution came with other problems:

- Script has to be kept up-to-date with changes to OpenWRT
- Missing documentation -> very high threshold

#### Now
Central configuration with [OpenWISP](https://openwisp.org/whatis.html), which is built and maintained for exactly this purpose.


## Updating documentation
It needs documentation for everything to store knowledge not only in the heads of individuals. Unfortunately, much of the existing documentation was no longer up-to-date. Now this should also be better:

- Wifi instructions: [https://handbook.kanthaus.online/technical/wifi.html](https://handbook.kanthaus.online/technical/wifi.html)
- Detailed technical information for admins: [https://handbook.kanthaus.online/technical/network.html](https://handbook.kanthaus.online/technical/network.html)
- This blog post


## Last words
I think I understand more and more how something has to be in order to be easily understood and maintained, but what is still a big mystery to me is how something can be kept that way? It seems to me that chaos/complexity is indeed the state that everything moves toward throughout time, but what can we do against it? Maybe this here is at least a start :)

