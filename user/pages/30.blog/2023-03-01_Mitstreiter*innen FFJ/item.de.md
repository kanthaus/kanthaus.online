---
title: 'Mitstreiter*innen für die Orga eines selbstorganisierten Bildungsprojektes gesucht'
date: "2023-03-01"
taxonomy:
    tag: [blog]
    author: Kito
---
<div markdown="1" style="text-align:center;">
![](FFJ-Logo-weiss-1.png)
</div>



Du willst anderen Menschen eine utopisch-widerständige Lebensweise vermitteln, anstatt ihnen die Karriereleiter im Hamsterrad zu zeigen? Du hast Lust, Teil eines tauschlogikfreien, diskriminierungssensiblen und hierarchiekritischen Bildungsprojekt zu sein und weißt, dass du selbst nicht die Weisheit mit Löffeln gefressen hast? Dann tu dich mit uns zusammen und organisier eins!

===

Wir planen, angelehnt an das Konzept des Freiwilligen Freien Jahres (FFJ), ein längerfristiges, selbstorganisiertes Bildungsprojekt auf die Beine zu stellen. Dieses soll seine Basis im Kanthaus in Wurzen haben und einer kleinen Gruppe von ca. 8 bis 10 jungen Menschen die Möglichkeit geben, sich über mehrere Monate selbstorganisiert mit sich selbst und verschiedenen Themen auseinanderzusetzen. Die Teilnehmenden bilden eine Bezugsgruppe, die sich gemeinsam den großen Fragen des (politischen) Lebens widmet: Wie kann ich sinnvoll wirken? Was sind die Qualitäten, die ich dafür mitbringe? Was braucht es, damit ich langfristig dabei bleibe? Zudem sollen die Teilnehmenden Wissen und Zugange zu Räumen bekommen, um weiter in Graswurzelprojekten aktiv sein zu können.

Das Konzept des FFJs hat in den letzten 5 Jahren schon zweimal Anwendung gefunden und mega schöne Projekte sowie Bezugsgruppen hervorgebracht. Wir haben Lust auf ein weiteres FFJ, wollen uns aber in der Organisation nicht übernehmen. Wir wünschen uns, ein Konzept zu finden, das zu unseren Bedürfnissen als Orga-Crew passt :) Wir haben da auch schon einige Ideen und freuen uns das Konzept dann gemeinsam im größeren Orgateam auszugestalten.

Aktuell sind wir noch ein sehr kleines Team, daher suchen wir nach weiteren Mitstreiter*innen für dieses Projekt. Da der Projektmittelpunkt in Wurzen (20 min mit der Bahn von Leipzig) sein soll, wäre es toll, wenn auch du irgendwo aus der Gegend kommst bzw. dir vorstellen kannst öfters in der Gegend zu sein.
Wenn du Interesse hast, ein Teil unsereres Teams zu werden, schreib uns gerne bald eine Mail an **kontakt@freiwilliges-freies-jahr.de** mit ein paar Infos zu dir und was dich daran interessiert, mit ins Orga-Team zu kommen.

Zu uns: Wir sind Kito, Amsel und Emmi, zum Teil (gender-)queer, zum Teil männlich&questioning, 1 1/2 von uns mit chronischen Kranksein im Alltag beschäftigt und alle drei weiß und ohne Antisemitismus- oder Klassismuserfahrungen.  Mensch findet uns unter anderem in Projekten, Kreisen und Aktionen zu Queerfeminismus, Klimagerechtigkeit und solidarischer Unterstützung von People on the Move. Unter anderem bringen wir Wissen zu und Bock auf Waldbesetzungen, Clowning, Trampen und Jugendzentren mit uns. In irgendeiner Form haben wir auch alle mit Sozialer Arbeit zu tun. Neben den Projekten verbindet uns der Wunsch nach ehrlichen, stabilen sozialen Beziehungen und dass wir alle gern viel draußen sind.

Wir freuen uns sehr, unsere Gruppe um weitere Perspektiven ergänzen zu können!

PS: Aktuell wird noch ein weiteres Bildungsprojekt in Anlehnung an das FFJ geplant. Dieses soll schwerpunktmäßig im K20 Projekthaus (bei Göttingen) stattfinden. Falls dies geografisch besser für dich passt oder du Lust hast dir mal beide Orgateams anzuschauen, können wir auch Kontakt zu dieser Gruppe herstellen.

<div markdown="1" style="text-align:center;">
![](cropped-FFJ-Logo.png)
</div>
