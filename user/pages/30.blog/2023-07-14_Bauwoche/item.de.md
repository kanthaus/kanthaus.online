---
title: 'Komm und werde Teil unserer Bauwoche im Kanthaus'
date: "2023-07-14"
taxonomy:
    tag: [blog]
    author: Anneke
---

Vom 7. bis 13. August wollen wir im Kanthaus weiter an der Renovierung des Dachbodens arbeiten und 
würden uns riesig über deine Unterstützung freuen!

===

Vor allem wollen wir Gipskarton anbringen, aber 
auch sehr gerne schon die Elektrik installieren. Um einen produktiven Arbeitsmodus zu garantieren, 
braucht es natürlich auch Menschen, die sich liebevoll um die Repro-Aufgaben kümmern (Lebensmittel 
retten, kochen, Kinder betreuen). Wenn du dich also nicht so gerne handwerklich betätigst, trotzdem aber 
gerne das Kanthaus und seine Bewohnis unterstützen möchtest (bzw. es kennenlernen möchtest falls du 
noch nie hier warst), dann finden sich sicherlich genügend Aufgaben, die du gerne machst.  
 
Natürlich gibt es nicht nur Arbeit, sondern auch der Spaß steht im Fokus: Wir wollen zusammen essen 
und die Abende mit verschiedenen Aktivitäten wie Filmen und Spielen verbringen und einen Ausflug zum 
See soll es auch geben. Gerne sind auch deine eigenen Ideen für eine gemeinsame Freizeitgestaltung 
willkommen! :-) 
 
![](wuppweek_outsideLunch.jpg)<br>

Es wird ein anarchistischer Arbeitsplatz sein: Manche Menschen werden mehr Fähigkeiten, Wissen oder 
Verantwortung haben als andere, aber all dies soll geteilt werden, niemand ist der Boss von irgendwem 
und alle sollen dafür respektiert werden, wer und was sie sind. 
Wir freuen uns riesig auf dich! 

