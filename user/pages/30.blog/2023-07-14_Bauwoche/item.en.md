---
title: 'Come and support our building week in Kanthaus!'
date: "2023-07-14"
taxonomy:
    tag: [blog]
    author: Anneke
---

From 7 to 13 August, we want to continue working on the renovation of the attic in Kanthaus and would be very happy about your support!

===

We mainly want to put up plasterboard, but we'd also love to install the electrics already. In order to guarantee a productive work mode, we of course also need people who lovingly take care of the repro tasks (saving food, cooking, looking after children). So if you 
don't like to do building work, but would still like to support Kanthaus and its residents (or get to know it if you have never been here before), there are certainly enough tasks you would like to do.  
 
Of course, there is not only work, but also fun: we want to eat together and spend the evenings with various activities such as films and games, and there will also be an excursion to the lake. Your own ideas for spending our free time together are also welcome! :-)
 
![](wuppweek_outsideLunch.jpg)<br>

It will be an anarchist workplace: Some people will have more skills, knowledge or responsibility than others, but all this is to be shared, no one is anyone's boss and all are to be respected for who and what they are.
 
We are looking forward to seeing you!

