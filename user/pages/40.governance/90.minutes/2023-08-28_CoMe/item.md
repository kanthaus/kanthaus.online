---
title: Coordination Meeting
date: "2023-08-28"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #308
* Date: 2023-08-28
* Facilitator: Larissa
* Notary: Janina
* Physical calendar: Kito
* Digital calendar: Doug
* Reservation sheets: Cecilia
* Weather forecast: Martin
* Present: Martin, Doug, Cecilia, ASZ1, Maxime, Kito, Larissa, Anneke

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 
![usage graph last 90 days](https://pad.kanthaus.online/uploads/a938b5c4-a001-4c31-aeb6-295330af3b16.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 15.0 people/day (+4.6)
- **⚡ Electricity**
    - usage: 30.12 €/week (⬆️+9%)
    - of that for heating: 0.42 €/week
    - of that for hot water: 1.98 €/week
    - paid: -19.01 €/week 
    - ☀️ self produced: 68% (⬆️+6%)
    - emissions: 14 kg CO₂ₑ/week
- **💧 Water**
    - paid: 25.26 €/week (⬇️-1%)
    - emissions: 2.0 kg CO₂ₑ/week


### Expenditure
* [Doug] 40€ for paint and small metal piece for lantern

### Income
* 95€ Pfand
* 13€ donations

### Things that happened
#### In or around Kanthaus
* a big Kanthaus outing to KoLa Hoffest
* card game at mitmachcafé
* doug became a member again
* a storm damaged stuff: the pavillion, and some window joints

#### Wider world
* crazy storm messed up public transport and more on Friday
* new EU law attempts to force big social media to promptly remove illegal and harmful content

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

<!-- ### Tekmîl-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** 
* **Wed.:** kito leaves
* **Thu.:** 
* **Fri.:** 
* **Sat.:** Franzi comes for a night, Lena+2 might come for a night, Mila arrives
* **Sun.:** 
* **Mon.:** Miel arrives
* **Some day:** Luise [Anneke] arrives (wed/thu) for some days

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)

- GRAY all week, with mild temperatures, rain here and there, except Tuesday when it will be continuous. 

### Evaluations and check-ins

* [Thore] I would like to do an evaluation. Choose a slot in the following time periods: Wednesday from 15:00 - 17:00, Thursday from 15:00 - 17:00 or Friday from 11:00 - 17:00.
* [Anneke] I also could have my evaluation this week :-)
* Due for evaluation (and seen within the last 7 days):
    - Larissa Member: Days Visited threshold 197/180 (+9%)
* Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
    - Maxime Member: 2 days until Days Visited threshold (180)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Larissa]
    * 11:00 - Power Hour
* Tuesday
    * Rest waste [Martin]
    * 15:00 - 17:00 Open Tuesday [lottery] 
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * 15:00 - Thore's evaluation [Anneke]
    * 18:00 - Punkrocktresen
* Thursday
    * 13:00 - 18:00 MitMachCafé
    * 18:30 - FLINTA thai boxing @ D5
* Friday
    * Organic waste [Maxime]
    * 10:00 - [Planning meeting] [Doug]
* Saturday
    * 10:00 - Burkartshain FFW fest
    * 14:00 - 21:00 Kanthaus Fest!
* Sunday
    *  
* Next Monday
    * 10:00 CoMe [Janina]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
* [tilmann] 2€ some metal sponges for kitchen (don't throw them away when dirty, but put them in the top tray of the dishwasher!)
  * [Martin] I have thrown some away when they start breaking apart and leave metal pieces. Maybe we could hang them from a hook so they stay less humid? 
  * Yes, please!
* [tilmann] 30€ LED bulbs for standing lamps in dragon and cloud room
* [tilmann] ~100€ for kanthaus.online domain renewal for ~5 years
    * Thanks for taking care of that!

## 4. To do
_Newest tasks on top_
* [ ] Small bin for every toilet
* [ ] Fix Dorm window holder []
* [ ] Fix Dorm window joint [Maxime]
* [x] Fix the black cupboard for hats, gloves and scarves in K20 entrance which is slowly falling apart [Larissa]
* [ ] wash everything in the K20 entrance black cupboard [Dorota]
* [X] curtain for the Cave [Anneke] 
* [ ] curtain for Lantern []
* [x] make K20-2 bathroom privacy indicator realiable
    * [Tilmann] what kind of problems did you encounter? I tried to simplify the logic to make it more reliable already, but I wonder if the motion sensor could be flaky. 
    * [Martin] I know Dorota has been working on it. 
    * [Dorota] If you first press the private button and then close the door, it turns public again
    * [Tilmann] made a few changes to ignore the door status, hope it's better now
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
* [from last week] Did it happen?
    - [Martin] Gartenfest prep/to-do list? I propose we start doing some stuff to not have to run around last minute. Also we might be able to give extra touches. 
      - [x] sweep trash corners [Maxime]
      - [ ] weed the walkways [Martin]
      - [ ] bricks 
      - [ ] ADVERTISE THE EVENT! 
    - highfield follow-up
      - [x] sort away toilet paper [Doug]
      - [ ] sort away trash bags in wash room [Martin]
      - [ ] sort away bbq stuff in K18 
      - [x] sort cans in basement [Larissa]
      - [x] move unwanted drinks to open tuesday or K18 Kleinanzeigen room [Cecilia]
          - [ ] some more drinks need to be sorted
      - [ ] moth-proofing food in K20-1 [PH food sorting task for today]
* [Janina] Sommerfest!
    * Did you all invite the people you know? ;)
    * Ralf, Nicky and I will do pickups and set up a Fairteiler in the K22 entrance
    * Who can take the hat for the KüFa?
        * Anneke
    * Who can do a ~30 minutes house tour? (I'd like to have several in the end.)
        * Larissa, Janina, maybe Doug
    * Who can play music?
        * Maxime, but more people putting songs are welcome
    * Who can put up decorations and signage?
        * Anneke is happy to put it up, Janina and Doug look for things
    * Shall we do a communal garden clean-up and fest prep session on Friday afternoon and/or Saturday morning?
        * Clean-up on Friday and infrastructure/decoration setup on Saturday?
        * Will be clarified in PlaMe
    * Which toilet shall we recommend to Sommerfest guests? Last time it was the compost toilet and the lower staircase toilet. Cecilia will check sometimes.
    * General infrastructure will be decided on in PlaMe
        * Janina will ask Ralf and Heidi about beer tables
* [kito] group request for 8 people on September 15-17, kito can host.
* [Anneke] hosting Emma (workaway) (somewhere between Aug 30 and Sep 20)
    * Janina can do it after Sommerfest
* [Maxime] hosting request from Hannah (sociology student) and Nikita (studying playwright) on Trustroots: "Falls es euch passen würde, wenn wir irgendwann zwischen 11. und 17.09. mal bei euch vorbeikommen, sag gerne Bescheid!". I could host if no resistance.

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Janina] [Bike name ukuvota](https://legacy.ukuvota.world/#/app/1d496f30-2228-c4b9-7b85-4898736ca0d5/collect) open for proposals for 6 more days. Then 7 days of voting.
* [kito] Stand-up meetings
    * 9:30 in the elephant room
* [Doug] Do we want to coordinate Kanthaus off-days? I'm motivated to chat about that today.

<!-- check if anyone has a point that didn't speak already -->

### Round 3
* [Janina] From next week on the potato/onion pile of Landgut Nemt will most probably be available. Then we just need to spontaneously call and announce that we'll be coming. Talk to me if you're interested to go and get some produce!

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** ASZ1
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Janina
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** Maxime, Doug
* **Tue.:** Larissa
* **Wed.:** Doug
* **Thu.:** Larissa, Maxime
* **Fri.:** Larissa
* **Open Tuesday:** Larissa, Maxime
* **Week:** Martin, Anneke, kito, Tilmann

### Result
### Result
- **Mon.:** Ahmed
- **Tue.:** Maxime
- **Wed.:** Larissa
- **Thu.:** Doug
- **Fri.:** janina
- **Open Tuesday:** Cecilia


## 7. For next week
