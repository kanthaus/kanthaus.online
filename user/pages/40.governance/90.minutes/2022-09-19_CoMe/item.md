---
title: Coordination Meeting
date: "2022-09-19"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #261
* Date: 2022-09-19
* Facilitator: Antonin
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Tilmann, Matthias
* Physical board caretaker: Nathalie
* Digital calendar: Antonin
* Reservation sheet purifier: Kito
* Present: Kito, Janina, Matthias, Andrea, Maxime, Thore, Nathalie, Antonin

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/85d31a07-8183-474a-8a28-742b932b2644.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 9.3 people/day (+0.9)
- **⚡ Electricity**
    - usage: 40.84 €/week (⬆️+2%)
    - of that for heating: 3.56 €/week
    - paid: -9.05 €/week 
    - ☀️ self produced: 60% (⬇️-2%)
    - emissions: 14 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.63 €/week (⬇️-27%)
    - emissions: 1.0 kg CO₂ₑ/week

### Expenditure

None.

### Income

None.

### Things that happened
#### In or around Kanthaus
- Winter is coming: Tilmann switches on the heating right now
- Thore's birthday with very nice cake
- A nice and short social sauna
- Good meeting about gleaning actions

#### Wider world

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* see https://de.wikipedia.org/wiki/Wikipedia:Hauptseite

## 2. This week planning

### People arriving and leaving
* **Mon.:** Silvan comes back, kito came back, Martin_Andrea is here for a night, Matthias leaves
* **Tue.:**
* **Wed.:** kito leaves probably
* **Thu.:** Eric arrives
* **Fri.:** Silvan leaves for the weekend
* **Sat.:** Eric leaves (bye KH!), Janina leaves for a night, Antonin leaves
* **Sun.:**
* **Mon.:**
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_A couple of showers early in the week and in the weekend. Temperatures remain stable._

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Kito
- Nathalie Volunteer : Absolute Days threshold 108/60 (+80%)
- Andrea Volunteer : Absolute Days threshold 81/60 (+35%)
- Eric_WA Visitor : Days Visited threshold 28/21 (+33%)
- Martin Volunteer : Absolute Days threshold 76/60 (+27%)
- Tilmann Member : Absolute Days threshold 209/180 (+16%)
- Matthias Volunteer : Absolute Days threshold 69/60 (+15%)
- Maxime Member : Absolute Days threshold 185/180 (+3%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * Post-Sunday-Dumpster-Diving Food sorting/washing [Maxime]
* Tuesday
    * Clara's birthday party in Jena [JaTiMiLe, Andrea]
    * 15:00 - 17:00 Open Tuesday [Antonin, Kito]
* Wednesday
    * 09:30 casual MCM team meeting at breakfast [Janina]
    * 10:00 kito's evaluation [Thore]
    * Repark KMW []
* Thursday
    * 10:00 Power Hour [Fac.: Antonin, DJ: ?]
    * 10:00 Fokusgruppe Nachhaltigkeit @ALM [Janina]
    * 15:00 Nathalie's evaluation [Antonin]
* Friday
    * Yellow bin is collected [Maxime]
    * 12:00 Market pickup [Andrea]
* Saturday
    * Flatwarming party @Anja+Antonin's [everyone! xD]
        * slackline and picnic in the afternoon
        * inside party in the evening
* Sunday

* Next Monday
    * 10:00 CoMe []
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit


## 3. Shopping plans
- [tilmann] 42€ for a fine (F7) fresh air filter for the ventilation system - should last for one year
    - I would get this one: https://rt-handel.de/Ersatzfilter-Lueftung/Systemair/VTC-700/Taschenfilter-passend-fuer-Systemair-VTC-700-Filterklasse-F7?number=RT000865.1 is cheaper than what we had so far, with less capacity, but maybe good enough.
    - details: https://yunity.slack.com/archives/CFDNQL9C6/p1663165380772469
- [tilmann] ~120€ for pipes and parts to finally reconnect toilet pipes to roof (details: https://yunity.slack.com/archives/CN9H631BM/p1663525305732479)
- [janina] chest freezer, probably new, for about ~500€
    - just put out there for consideration: do we want one? does it make sense?


## 4. To do
_Newest tasks on top_
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put up and down?
* [ ] remount snack kitchen radiator [Martin/Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
- [janina] gleaning actions @landgut nemt
    - who's generally up for it?
    - soon we can pul onions and potatoes out of dirt piles in burkartshain
    - later there'll be carrots and beetroot
    - beans at some unspecified time
        - antonin and kito would be in this Wednesday
- [Antonin] lunch or dinner lottery?
    - it's happening! lunch trial this week
    - serving time at 1 pm
- [maxime] KMW is getting more capricious
    - maxime is getting anxious that he won't be able to restart the car at some point, so he doesn't want to take responsibility of reparking anymore
    - instead, maxime wants to push for the discussion about selling the KMW to happen
    - he'll talk to silvan about reparking this week
- [kito] FFJ in Kanthaus in 2023?
    - earliest starting in August 2023, then it would lasts some months to a year
    - the last group was 10 people, but it could also be less
    - they would be going to different places but use Kanthaus as their main base
    - the detailed concept is not clear yet
    - kito would have it has his main project during that time
    - reaction: slight concerns about fullness, but general support
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [janina] tilmann does attic focus weeks + karrot dev : childcare anybody? :3
    - yes the plan is to insulate the roofs before winter
    - ... and to grab as much money as possible for karrot
    - I'd personally be very much appreciative for any childcare shift! :3
        - nathalie: attic work update session would be much appreciated!
- [kito] Open Spaces in Trebsen (27.09.)
    - does someone want to participate? no
- [maxime] evaluation system reform reminder https://yunity.slack.com/archives/C3RS56Z38/p1660493720040789 Let's have a fishbowl and votes during Autumn?
    - yes!

### Round 3
- [janina] some cleaning tasks that need attention:
    - taking care of the leftovers of the market pickup in the staircase
    - vacuuming the freeshop as anja has asked
        - Janina will put spidey there

### Round 4
- [janina] heating season starts today! :D
    - really take care to keep doors to heated areas closed, please!
    - turn radiators fully on or off, the nuances don't work
        - some unhappiness about the process: no discussion about turning it on?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:**
* **Thu.:** zui
* **Fri.:** Thore
* **Open Tuesday:**

### Unavailabilities
* **Mon.:** Maxime
* **Tue.:** Janina
* **Wed.:** 
* **Thu.:** Maxime, Janina
* **Fri.:** Antonin
* **Open Tuesday:** Janina, Maxime
* **Week:** Eric, Silvan, Nathalie, kito, Andrea, Matthias

### Result
- **Mon.:** 
- **Tue.:** Antonin
- **Wed.:** Janina
- **Thu.:** zui
- **Fri.:** Thore
- **Open Tuesday:** Antonin+Kito

## 7. For next week
* 
