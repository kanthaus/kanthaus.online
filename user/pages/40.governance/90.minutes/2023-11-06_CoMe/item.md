---
title: Coordination Meeting
date: "2023-11-06"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #318
* Date: 2023-11-06
* Facilitator: Maxime
* Notary: Janina
* Physical calendar: Dorota
* Digital calendar: Kito
* Reservation sheets: Loup
* Weather forecast: Kito
* Present: Maxime, Doug, Janina, Riell, Kito, Venla, Tilmann, Dorota, Loup

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

![usage graph last 90 days](https://pad.kanthaus.online/uploads/3d06b058-44f2-4c5c-b35e-74c4116e610f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 14.7 people/day (+3.0)
- **⚡ Electricity**
    - usage: 56.63 €/week (⬆️+15%)
    - of that for heating: 13.62 €/week
    - of that for hot water: 3.27 €/week
    - paid: 42.15 €/week 
    - ☀️ self produced: 52% (0%)
    - emissions: 27 kg CO₂ₑ/week
- **💧 Water**
    - paid: 17.64 €/week (⬆️+24%)
    - emissions: 1.4 kg CO₂ₑ/week

### Expenditure
- [Antonin] 90€ for 50kg of plaster for drywall
- [Kate] 20 euro for a steam brush for the freeshop

### Income
None

### Things that happened
#### In or around Kanthaus
* COVID quarantines (finished!)
* Mika's 3rd 4th Birthday
* attic work pushed forward a lot
* nice big project updates
* casual halloween kitchen party
* more vacuum cleaners xD
* nachtshopping and job fair

#### Wider world
- world youth conference in paris

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Popcorn of feedback
<!--
Is there any feedback you would like to give to others - the whole groups or individuals? Is their any reflection about yourself you would like to share with the others? If you would like to give feedback to a person in the room, please do it in third person.
-->


## 2. This week planning

### People arriving and leaving
* **Mon.:** Larissa comes back
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Sat.:** 
* **Sun.:** Klara comes (does she..?)
* **Mon.:** 
* **Some day:** 

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->
![](https://pad.kanthaus.online/uploads/06a6642f-c659-43e1-b2e1-24927d74d580.png)
 
### Evaluations and check-ins

- Maxime Member: Days Visited threshold 222/180 (+23%)
- Antonin Volunteer: Days Visited threshold 65/60 (+8%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Maxime]
    * 11:00 - Power Hour
    * afternoon -  mushroom picking [Dorota, Doug]
* Tuesday
    * 9:30 - 13:30 TOI#1 session @Dragon Room [Kate, Kito]
        * heating? better heatable should be Yoga or Piano Room
    * 19:00 - Penny pickup [Janina, Martin]
    * 20:00 - Rust meeting [Antonin]
* Wednesday
	* Hausmüll [Tilmann]
    * morning - attic session [Janina, Tilmann, you?]
    * 12:00 - Maxime's evaluation [Kito]
    * 12:00 - 17:00 MitMachCafé
        * 14:00 Current situation discussion [Kito]
    * 18:00 - Punkrocktresen @ D5
* Thursday
    * morning - attic session [Janina, Tilmann, you?]
    * 13:00 - 18:00 MitMachCafé
        * Doug, Martin and Unkraut want to go
    * 18:30 - FLINTA thai boxing @ D5
* Friday
	* Biotonne [Maxime]
    * 10:00 - Social Sauna [Janina]
    * 18:00 - foodsharing LKL district meetup @Bad Lausick [Janina]
* Saturday
    * 11:00 - Anja's (& Janina's) birthday brunch @Ostpost
    * 16:00 - chandi's flat party (talk to invited people before going)
* Sunday
* Next Monday
    * 10:00 CoMe [Janina]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
- [janina] 1-2 nail clippers
    - matthias threw away the last one from the bathroom because it was unbearably shitty
    - big ones are usually more stable than small ones
- [Kate] would like to spend circa 12 euro on a clothes rail for the free shop. desperately need more hanging space for kids clothes (and then it can go to flohmarkts or outside in summer too)
    - used or new?
    - might not last long
    - but no resistance to trying it
- [kito] 15-20€ for a big pinboard
    - janina and kito look at existing boards first

## 4. To do
* idea: move to an analog list in the elephant room, could be brought to every CoMe [kito]
    * who is maintaining it? [janina]
        * me and the CoMe?! [kito]

* [ ] proof-reading heating page in handbook https://handbook.kanthaus.online/technical/heating.html [kate]
* [ ] fix/improve K20-1 staircase night light (can ask Tilmann for details about wiring)
* [ ] put kanthaus logo on the new beer benches and tables
* [ ] curtains for the dragon room that prevent passersby from staring inside
* [ ] cut branch from tree in the compost corner (after leaves fall)
* [ ] cover the pipes from the rest of the room in the snack kitchen (wooden board angle?) []
    * [tilmann] maybe directly go for drywall as final measure?
      * [maxime] that would be the right but more difficult remediation indeed. I looked at options to hack a wooden corner, but didn't find satisfying options in the wood shed, so I'll leave there.
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights [Dorota]
     * [Martin] Cables got pinched, I suggest redoing the whole thing. Also, use a diode to prevent over voltage. 
* [ ] replace shower curtain by (plexi)glass construction

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [doug] will remove virtual printers after Friday
    - cool
- [Martin] Compost motion: stop it for now, throw everything in biotonne, deal with what we've got.
    - Context: https://yunity.slack.com/archives/C3RS56Z38/p1699032428920029
    - sure, let's do it!
    - the round hopes that kate and martin are communicating about this...^^'
- [kito] Group requests
    - Bausyndikat ~~and other group~~ in the end of November
        - Kito continues communications for now, Doug and Janina support with hosting when the time comes
    - 23 people would like to do a "future weekend" on the topic of communal living, either 19th -21st or 26th - 28th of January
    - group of 10 people, would like to live and learn together for 3 monthes, starting in December
    - [Larissa] definitly in favour of a month of calm before the ffj starts (in case that desicion is made in this meeting because of the group requests)
    - [kate] most concerned about this group of 10 for 3 months request! Feel like it's a no at 1st glance, but can't be in this meeting to hear other points of view - willing to hear about possible advantages. not bothered about the big weekend group - easy to leave the house for a weekend. not easy to leave for 3 months. month of calm - i feel it's easy to have a month of calm between end of toi 2 and toi 3 if people really need it. Selfishly I have really relied on visitors willing to do freeshop for help. So probably have a different perspective to others who don't need help ;-)
    - moi seems to be desired, but could work from mid-december to mid-january. the exact design of moi frame can still be discussed (what about e.g. new year's guests?)
    - general interest to host the future weekend group, kito will talk to larissa about it and accept them if there's no resistance
- [janina] [bike hitch storing spot poll](https://yunity.slack.com/archives/CQPK7722K/p1699028426975379)
    - let's keep the round bits on the trailers and not on the bikes from now on!
- [riell] being here
    - great!
    - let's do a check-in soon (and generally announce check-ins on slack)
- [dorota] my server: it's intended to stay. how to formalize this?
    - a better location is needed, electricity costs could be contributed to but it's not obligatory
    - dorota will come up with a proposal and put it on slack
    - a small presentation of what's actually on there would be interesting :)
<!-- check if anyone has a point that didn't speak already -->

### Round 2 
- [kito] TOI tomorrow, don't forget to think about [the questions](https://yunity.slack.com/archives/G8JTYBZFD/p1698412428674969)
    - volunteers and members only
    - we need people to prepare breakfast and for preparing tea and coffee (and snacks?) for the session
- [Martin] potatoes need sorting out of moldy ones
    - will be put into power hour
- [Antonin] rust meetup tomorrow (Tuesday) evening? Dima would be up for it :) Dorota are you in?
    - [dorota] yes of course
    - [Antonin] amazing! let's say 20:00?

### Round 3
- [kito] CryptoKüfa on the 27th?
    - Monday in 3 weeks. Kito would need people with some knowledge to support, he himself feels mostly responsible for advertisement and food.
- [Antonin] about Klara coming on Sunday, it would be just for a few days before her longer stay, I would be hosting
    - aaah. we were wondering already... ;)

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** Riell
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Kito
* **Open Tuesday:** opentuesdaykate, opentuesdaykate2

### Unavailabilities
* **Mon.:** Kate, Maxime
* **Tue.:** Martin, Kate, Janina, Dorota
* **Wed.:** Doug, Riell
* **Thu.:** Martin, Kate
* **Fri.:** Janina, Doug
* **Open Tuesday:** Maxime
* **Week:** Tilmann


### Result
- **Mon.:** martin, Venla
- **Tue.:** Riell, Doug
- **Wed.:** Loup, Janina
- **Thu.:** Maxime, Dorota
- **Fri.:** Kito, Kate
- **Open Tuesday:** opentuesdaykate, opentuesdaykate2


## 7. For next week
* attic clean-up! :tada:
* FFJ get-together on the weekend