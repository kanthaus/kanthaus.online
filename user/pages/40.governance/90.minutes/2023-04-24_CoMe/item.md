---
title: Coordination Meeting
date: "2023-04-24"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #290
* Date: 2023-04-24
* Facilitator: Larissa
* Notary: Janina
* Children caretaker: Kita
* Physical board caretaker:  Anneke
* Digital calendar: Martin
* Reservation sheet purifier: Janina
* Present: Larissa, Martin, Anneke, Tilmann, Silvan, Janina

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/6301e2a6-cdd5-4668-81e6-2614b163d4b2.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 2.7 people/day (-8.7)
- **⚡ Electricity**
    - usage: 35.93 €/week (⬇️-1%)
    - of that for heating: 14.02 €/week
    - of that for hot water: 1.41 €/week
    - paid: -12.98 €/week 
    - ☀️ self produced: 82% (⬆️+13%)
    - emissions: 17 kg CO₂ₑ/week
- **💧 Water**
    - paid: 10.85 €/week (⬇️-31%)
    - emissions: 0.8 kg CO₂ₑ/week


### Expenditure
- 160€ ventilation parts for attic, baby bathroom, piano room [tilmann]
- 195€ four F7 fresh air filters [tilmann]

### Other Income


### Things that happened
#### In or around Kanthaus
- project updates
- Lise cut the plum tree
- nice summery BBQ with drinks around the fire afterwards
- SiLa took MiLe to Auerstedt for the weekend
- almost everyone in kh went to punkrock-tresen for unkrauts birthday

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
- agreement in ver.di strike season for public service work
- wars are going on, no end in sight

## 2. This week planning

### People arriving and leaving
* **Mon.:** Gaia_WA arrives, Clara_Jena might stop by
* **Tue.:** Maxime arrives, kito comes back
* **Wed.:** two people and a baby spend a night, Anneke leaves
* **Thu.:** kito leaves
* **Fri.:** 
* **Sat.:** Franzi comes by for the weekend
* **Sun.:** Antonin and Naomi_FQ arrive, kito comes back
* **Mon.:** rest of Feuerqualle group arrives
* **Some day:** Olli+2? People for A-Days?


### Weather forecast

<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_Colder again, rain and clouds, then sun again towards the end of the week_


### Evaluations and check-ins
- Tilmann Member : 4 days until Days Visited threshold (180)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Larissa]
    * Monday food care [Martin, Janina]
    * 18:00 Vegutopia KüfA @D5 [Anneke]
* Tuesday
    * black bin [Larissa]
    * 13:00 FGNW meeting in Dragon Room [Janina]
    * 15:00 - 17:00 Open Tuesday [lottery]
    * ~21:00 Anneke's backstory
* Wednesday
    * 10:00 attic work session
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Larissa]
    * 15:00 Social Sauna [?] lack of people to participate and facilitate
* Friday
    * organic waste [Janina]
    * 10:00 Tilmann's evaluation [Janina]
* Saturday
    * 10:00 attic work session

* Sunday

* Next Monday
    * 1st of May holiday
    * 10:00 CoMe [Tilmann]
* Next week summary
    * Friday (May 5th) PlaMe


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [Silvan] about 62€ spare parts for borrowed Auerworld battery drills
    - no resistance, Tilmann has some plans of fixing and will talk to Silvan


## 4. To do
_Newest tasks on top_
* [ ] fix the extension to the shortterm storage to the wall next to the main bathroom
* [ ] flip the compost
* [ ] give away saved windows in K20 basement (a lot) maybe https://www.trashgalore.de/? (martin)
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [x] check all fire alarms [Martin]
* [ ] check fire extinguishers [Antonin]
* [ ] treat mold in Freeshop Lounge and Hipster Room
* [ ] de-clog compost toilet drain
* [ ] modify the task lottery to prioritize open tuesday []
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
* [Martin] Smoke detectors report. All existing SDs seem to work, yay! Rooms without: both basements, freeshop, lounge, intermediate, both kitchens, ex-food storage, private, dorm, yoga, and the attics. There is just one in a staircase, in front of the private. Finally, there are two spare SDs that can be used. So, which of these locations should have a SD? Any resistance to some randomized tests? They cost around 10euro a piece.
    * no resistance to tests
    * last two SDs maybe to dorm, attics, basement
    * buying more is also fine, some spare ones could be good
* [Antonin] the Feuerqualle group would like to reserve the Dragon and Cloud rooms from Monday to Wednesday next week.
* [Janina] the small potatoes are sprouting. anybody up for putting them up on kleinanzeigen as seedlings?
    * Larissa can do the coordinating, other people can also be tasked with opening the doors
* [Silvan] can Kanthaus fix the Auerworld power drills, please?
    * all AW tools will go to Auerstedt in the beginning of June, then everything should work
    * Silvan will inventorize, then people can help fixing
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Antonin] there is the general assembly of our association Haus Kante Wurzen on 7th May. The board of the association will be renewed during that meeting. The current board members are: Larissa, Zui, Silvan and Doug. Maybe it's good if volunteers and members think in advance if they could imagine joining (or stay on) the board? (background: because there is a protest in the Lausitz on that day I am worried not many show up to that meeting)

### Last spontaneous points?
