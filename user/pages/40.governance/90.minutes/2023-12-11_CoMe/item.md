---
title: Coordination Meeting
date: "2023-12-11"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #323
* Date: 2023-12-11
* Facilitator: Kito
* Notary: Janina
* Physical calendar: Dorota
* Digital calendar: Antonin
* Task board: Skip
* Reservation sheets: Larissa
* Weather forecast: Kito
* Present: Martin, Larissa, Tilmann, Dorota, Kate, Kito, Janina, Klara, Antonin, Riell

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/859a7bee-7547-4a61-92c6-af14629ef36f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.4 people/day (-2.9)
- **⚡ Electricity**
    - usage: 164.63 €/week (⬇️-9%)
        - of that for heating: 110.81 €/week
        - of that for hot water: 7.74 €/week
        - of that for K20-3 heatpump: 5.42 €/week
    - paid: 159.69 €/week 
    - ☀️ self produced: 11% (⬆️+9%)
    - emissions: 136 kg CO₂ₑ/week
- **💧 Water**
    - paid: 14.83 €/week (⬇️-9%)
    - emissions: 1.2 kg CO₂ₑ/week


### Expenditure
- [martin] 15€ vaccum bags + tea
- [kate] 11.50€ clothes rail
- [kate] 9€ olive oil

### Income
- [kito] 50€ donation from the "shoe"
- [maxime] 40€ Pfand
- 100€ donation on account

### Things that happened
#### In or around Kanthaus
- social sauna with focus on fullness issues
- new tea label shelf emerged
- a kola pickup and a big soup prep action
- visitors not coming
- in-house air quality monitoring started
- trip to modellbahnclub wurzen exibition
- tour with schlendrian
- weihnachtsmarkt wurzen stands got approached by foodsharing
- come pad is down :scream:

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Popcorn of feedback
<!--
Is there any feedback you would like to give to others - the whole groups or individuals? Is their any reflection about yourself you would like to share with the others? If you would like to give feedback to a person in the room, please do it in third person.
-->
- door to attic needs to be properly closed when the heating is on
- please take your hair out of the drain in the bathroom after taking a shower

## 2. This week planning

### People arriving and leaving
* **Mon.:** Jon might arrive
* **Tue.:** Eumel+Fynn arrive
* **Wed.:** Zui comes, Andrea might come for 1 night
* **Thu.:** Zui leaves
* **Fri.:** Kate and Antonin leave
* **Sat.:** Katja+Konrad arrive for one night
* **Sun.:** Eumel+Fynn leave
* **Mon.:** 
* **Some day:** Swinda might come for dinner, Lara will arrive
                       

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->
- it's getting a bit colder during the week but all in all very positive! some snowy rain and maybe some sun


### Evaluations and check-in's
Due for evaluation (and seen within the last 7 days):
- Nathalie Visitor: Days Visited threshold 25/21 (+19%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Dorota Volunteer: 6 days until Days Visited threshold (60)

Absent:
- Cecilia Volunteer: Absence threshold 98/90 (+9%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 09:30 - CoMe [Kito]
    * 10:15 - start of ToI#2 @attic
    * after lunch - Power Hour
* Tuesday
    * 09:30 - 13:30 ToI#2 @attic
    * 16:00 - Mika's Kita performance @Christmas market stage
* Wednesday
    * 09:30 - 13:30 ToI#2 @attic
    * 12:00 - 17:00 - MitMachCafé 
    * 18:00 - Punkrocktresen
    * Quizzz and movie projection
* Thursday
    * 09:30 - 13:30 ToI#2 @attic
    * 13:00 - 18:00 - MitMachCafé
    * 15:30 - Special freeshop opening for NDK ladies
    * 18:30 FLINTA* thai-boxing @d5
* Friday
    * Yellow bins [Martin]
    * 09:30 - 13:30 ToI#2 @attic
    * 18:00 - Penny pickup [Janina, Martin]
    * 20:00 - concert @ Café Ocka, Merseburgerstr. 88, Leipzig [Antonin]
* Saturday
    
* Sunday
    * third advent
* Next Monday
    * 10:00 CoMe [Janina]
* Next week summary
    * RIFF!

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
- Oats

## 4. To do
*See analog list in the elephant room*

**Done**

**Feedback**

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [ToI schedule](https://yunity.slack.com/archives/C3RS56Z38/p1702113334999069) pinned here.
- [kito] bring your calendars and internet devices to the session
- [Janina] Group request this week
    - janina will look at it again and answer
- [Kate] Freeshop opening over Christmas when I'm not here
    - 19th: Riff day, so closed
    - 2nd: let's keep it simple and close
    - Kate will communicate accordingly
- [Larissa] Quiz and movie time collision
    - let's try to have both on Wednesday starting at 20:30 with quiz and at 21:30 with movie
<!-- check if anyone has a point that didn't speak already -->

### Round 2

### Round 3

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers/Result
* **Mon.:** Kate
* **Tue.:** Riell, Klara
* **Wed.:** Janina
* **Thu.:** Martin, Antonin
* **Fri.:** Kito
* **Open Tuesday:** opentuesdaykate

### Unavailabilities
* **Mon.:** Janina, Kito
* **Tue.:** Antonin, Kito, Martin
* **Wed.:** Antonin, Martin
* **Thu.:** Klara,
* **Fri.:** Janina, Martin, Antonin, Klara
* **Open Tuesday:** Antonin, Kito
* **Week:** Tilmann, Larissa


## 8. For next week

