---
title: Coordination Meeting
date: "2021-12-13"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #222
- Date: 2021-12-13
- Facilitator: Maxime
- Notary: chandi
- Mika caretaker: Kita
- Levi caretaker: Tilman
- Physical board caretaker: andrea
- Digital calendar: Zui
- Reservation sheet purifier: Doug
- Present: Charly, zui, andrea, larissa, anja, silvan, maxime, doug, chandi

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
- for heating: 18 €/day
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_5f6c0f70d3583c6d7820717099cad3fc.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 14.4 people/day (-3.0)
- **⚡ Electricity**
    - usage: 25.13 €/day (⬆️+25%)
    - paid: 23.05 €/day 
    - ☀️ self produced: 8% (⬇️-5%)
    - emissions: 113 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.43 €/day (⬇️-7%)
    - emissions: 1 kg CO₂ₑ/week


### Expenditure
- [Matthias/Larissa] 417€ for Insulation stuff
- [larissa] 5€ for Bauschutt
- [maxime] 10€ Rapsöl (just 2€ remaining after pfand tickets)
- [tilmann/matthias] 398€ for more ventilation parts (for remaining rooms in 1st and 2nd floor)
- [matthias] 198€ for heating pipe from basement to attic

### Income
- 60€ for old metal
- 8€ pfand
- donations in the shoe: 5.43€

### Things that happened
- the scaffolding was brought down and stored
- ventilation was installed in the snack kitchen and kitchen
- there was a [TOI](https://www.youtube.com/watch?v=6cuv2irrtWQ) meeting
- another roof window got mounted (window 10 of 12 -> 2 more to go!)
- ebike is fun again with improvised 48V battery
- we watch Metropolis
- there was knot workshop
- we have sauce dispensers

#### Wider world
- Germany has a new government
- rising tensions between NATO and Russia over Ukraine
- consequently, gas prices on the rise againt ???? - why consequently?
- log4j 2 0-day exploit
- [#Bennewitz was in trending topic due to Corona-demo](https://twitter.com/PolizeiSachsen/status/1470106110497599490)

## 2. This week planning

### People arriving and leaving
- **Mon.:** Nathalie and Thore come back, änski leaves, martin comes and might stay
- **Tue.:** findus, Matthias, Clara come
- **Wed.:** bodhi arrives?, Silvan + Larissa leave
- **Thu.:** Anja, chandi & findus leave
- **Fri.:** Larissa + Silvan come back
- **Sat.:** Antonin and Anja come back, Nathalie and Thore leave
- **Sun.:** chandi comes back
- **Mon.:** 
- **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

- [Thore] I would be happy if we can make my evaluation on Thursday at 13:00 in the flat.  

- *Zui* _Member_ : _Absolute Days_ threshold 215/180 (+19%)
- *Silvan* _Volunteer_ : _Absolute Days_ threshold 68/60 (+13%)
- *Änsky_KIDZ* _Visitor_ : _Days Visited_ threshold 23/21 (+10%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Maxime]
    * 16:00 Workout Session at Yogaroom
    * Park cars on even/this side [Maxime]
- Tuesday
    * 14:00 fairteiler deep clean
    * 15:00 Bitte Wenden meeting
- Wednesday
    * Park cars on odd/that side [Maxime]
    * 12:00 Zui evaluation [Doug]
    * 14:00 TOI meeting
- Thursday
    * 10:00 Power Hour [Fac.: Anja, DJ: ?]
    * 13:00 Thore eval at the flat [Zui]
    * 18:00 foodsharing Wurzen christmas party, in WR7 ("the flat")
- Friday
    * Yellow bin [Maxime]
    * 20:00 'Bitte Wenden aber wie?' Talk about successful mobility transformation initiatives [Clara]
- Saturday
    * 13:00 Critical Mass [EVERYONE! :)]
- Sunday
    * 4. Advent
- Next Monday
    * 10:00 CoMe [Andrea]
- Next week summary
    * 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
- Item Liberation Day! (a.k.a personal stuff sorting, together) Watcha a Marie Kondo episode to get in mood? (Comedy) Prizes for the best sorters? Easy way to find Christmas gifts? All this and more! [Doug]

## 3. Shopping plans
- [chandi] 18€ 48V E-Bike battery charger https://www.aliexpress.com                       /item/32961535632.html
    - [matthias] maybe from germany? https://www.ebay.de/itm/353804277999 https://www.ebay.de/itm/313743677054
- [chandi] 10€ Paint for fixing scratches in roof window metal parts https://www.ebay.de/itm/223818047572
    - potentially not necessary, because maybe it doesn't rust -> discussion after come
- [matthias/tilmann] more Dachlatten for insulation construction (~200€)
   - approved

## 4. To do
_Small fixes of things in the house_

* [ ] fix hipster room window curtain [Larissa & anja]
* [ ] more storage spaces for visitors [charly]
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Building week clean up tasks
    * [ ] scaffolding net in the garden. Hanging now in the garden for drying
* [ ] KMW: fix driver side door (https://yunity.slack.com/archives/C3RS56Z38/p1636467207030900)

### New this week

## 5. Discussion & Announcements

### Round 1
- [Larissa, Janina, Doug, Nathalie] TOI-update: date (31st January-> 6th February, with arriving day before and leaving day after = '9 days') place (hopefully external! List of requirements. May cost up to €1500), external moderation (a day could be 500 - 800€, what do people think about that?) money
    - [matthias] definitively concerns about such a long time at an external location. Will there be alternative suggestions?
        - [chandi] could you expand on your concerns? difficult to understand otherwise
    - there is no clear topic for an external facilitation yet
    - poll on topics will be opened this afternoon
- [Silvan] What to do with all the sprouted potatoes? - sorting-party? giving away?
    - [doug/tilmann] Could store them in a shed instead of the basement
    - outcome: it's in our head! :D
        - use sprouted ones for cooking
- [Anja] I'll fix some more clothes in the next days: please put any clothes with holes you find in the communal wardrobe in the "to-be-fixed-box" there. (Even with small holes, because holes don't disappear if you don't fix them and might even get bigger!)
- [maxime] have you seen the coffee spoon?
    - no. maybe ask mika
- [chandi] lot's of people coming and leaving this week. corona concerns? any measures to take?
    - let's all do tests when people come, even if someone was

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Silvan] there is much food to be washed
- [chaaaaaaaaannnnndiiii] Food tasks concept for this week
    - we do the sheet again this week
    - agreement will be found this week!
- [maxime] evaluations attendance, there was almost noone in the last evaluations
    - [doug] with 16 mem/vol, we could be 4 people on average if people attended at least 1/4 of evaluations
    - [doug] question: would be interesting to know why people are not so interested in faciliation

### Round 3
- [Silvan] beet root pile at landgut nemt
    - can stay there for months but maybe we get a box for using now
    - take gummistiefel! if you go there

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Andrea, Zui
- **Tue.:** 
- **Wed.:** Matthias
- **Thu.:**
- **Fri.:** 
- **Open Tuesday:** Anja

### Unavailabilities
- **Mon.:** 
- **Tue.:** 
- **Wed.:** 
- **Thu.:** Doug
- **Fri.:** 
- **Open Tuesday:** Maxime
- **Week:** Larissa, Silvan, chandi

### Result
- **Mon.:** andrea, zui
- **Tue.:** maxime
- **Wed.:** matthias
- **Thu.:** charly
- **Fri.:** doug
- **Open Tuesday:** Anja

## 7. For next week
- 
