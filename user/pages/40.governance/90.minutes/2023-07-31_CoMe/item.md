---
title: Coordination Meeting
date: "2023-07-31"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #304
* Date: 2023-07-31
* Facilitator: Martin
* Notary: Kito
* Mika + Levi caretaker: unnecesary
* Physical board caretaker: Antonin
* Digital calendar: Antonin
* Reservation sheet purifier: Anneke
* Present: Cecilia, Antonin, Nofal, Anneke, Dorota, Maxime, Martin, Kito

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

- **Present:** 8.0 people/day (+0.7)
- **⚡ Electricity**
    - usage: 22.58 €/week (⬇️-13%)
    - of that for heating: 0.38 €/week
    - of that for hot water: 0.82 €/week
    - paid: -28.06 €/week 
    - ☀️ self produced: 67% (⬇️-4%)
    - emissions: 11 kg CO₂ₑ/week
- **💧 Water**
    - paid: 16.21 €/week (⬇️-12%)
    - emissions: 1.3 kg CO₂ₑ/week

### Expenditure

- 16€ soap, toothpaste, dishwasher tablets, etc. [Martin]
- 8€ flour and oil [Martin]
- 7€ olive oil [Arrakis]
- 50ct for corona tests [Antonin]
- 8€ for key copy [Cecilia]

### Income
- peep


### Things that happened
#### In or around Kanthaus
- the mermaid moved out of K20
- video connection specters
- a new volunteer

#### Wider world
- Auerword ended
- Turkey attacked Kurdish regions in Iraque 
- storm in Milano destroyed the parque

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Tekmîl
- Anneke kind of forgot to check the workaway account
- toilet wetness situation has improved, but still the toilet cleanliness could be improved - don't pee while standing
- please remember to remove the hair from the shower sink


## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** Cecilia's parents arrive, maaaaybee Anneke leaves
* **Wed.:** Lena arrives, Rudi and Juliette probably arrive
* **Thu.:** JaTiMiLe come back, Annekes Mama arrives
* **Fri.:** Franzi passes by, Dorota leaves
* **Sat.:** 
* **Sun.:** Larissa comes back, Linnea arrives
* **Mon.:** Cecilia's parents leave, Dorota comes back
* **Some day:** Bodhi passes by towards the end of the week, kito leaves some day and comes back sunday

### Weather forecast
<!-- 
    https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Pleasent temperatures between 14 and 25 degrees. But still switching constantly between cloudy, rainy and sunny. 

### Evaluations and check-ins

- Martin Volunteer: Days Visited threshold 114/60 (+90%)
- ASZ1 Visitor: Days Visited threshold 32/21 (+52%)
- ASZ2 Visitor: Days Visited threshold 32/21 (+52%)
- Anneke Volunteer: Days Visited threshold 78/60 (+30%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Martin]
    * 11:00 Power Hour [Martin]
* Tuesday
    * 10:00 Martin's evaluation [Maxime]
    * trip to the System Change Camp [Antonin]
    * Black bin Hausmüll [Cecilia]
    * 15:00 - 17:00 Open Tuesday [] 
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * 13:00 - 15:00 IT maintenance [Antonin, you?]
* Thursday
    * 13:00 - 18:00 MitMachCafé
    * 10:00 - 12:00 getting more Gipskarton with Carla [Antonin, you?]
* Friday
    * Biotonne [Nofal]
    * 12:00 Market pickup [Martin]
* Saturday
    * 11:00 PlaMe [Martin]
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Maxime]
* Next week summary
    * attic construction week!

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- Gipskarton [Antonin]
- rice or something similar for next week?, check oats and oil [Anneke]

## 4. To do
_Newest tasks on top_

* [ ] Fix the projector in Piano room (different cables and laptops were tested unsuccesfully)
* [ ] trash tour to special trash depo (also take unkraut's fridges) [Martin]
* [ ] fix window in Compost toilet
* [ ] curtain for the Cave
* [x] maintenance of Phoenix bike. [Martin] 
* [ ] make K20-2 bathroom privacy indicator realiable - ask Tilmann
* [ ] fill holes in Snack kitchen floor where moths breed [Akos]
    * it started but it not done yet, unclear what is the right material
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing
* [ ] give away saved windows in K18 (a lot) 
     * maybe https://www.trashgalore.de/ ? [Martin]
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [x] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
* [kito] Visitor requests
    * Mediation group of Vroni, 24.-25.08., 3-10 people, need private room(s) for practicing
        * Cecilia takes over
    * Nikhil, one week in september, knows Doug?!
        * refer to Doug for now

* [Martin] Workaway requests
    * Emma last two weeks of August or the beginning of September
        * Cecilia could maybe do it if it's the end of August
    * Jake somewhere between 26/08/2023 and 16/09/2023
    * Maeve from tomorrow on, maybe found already sth else
    * Rudi and Juliette should arrive on wednesday [Anneke I can host but I may ask someone to take over for the first evening]
    * Lisa & Jerom for the building week, they could also already arrive some days earlier or stay longer

* [Anneke] my Mums visit
    * my Mum wants to come either thursday evening or on friday and stay until saturday morning. Challenge: the cutest dog in the world (Mila) will be with her. I guess during the day we can be mostly outside, but I guess we would also be a bit inside (for eating or so) and the question is if it's okay if Mila would walk a bit through for example the kitchen. I guess my Mum would also be okay with keeping her on the leash if people are scared. Mila sleeps only rarely in other places than home, so for her not to be scared it would be good to sleep in the same room as my Mum. I was thinking about the lantern. My Mum takes a basket/pillo thingie with her and Mila is a clean dog. Anyway any objections?
        * what about the people who are not here? (family?)
        * ground floor (especially Dragon Room) as a solution for bad weather?
        * 

* [Cecilia] Trees: alive, dead and soon to be dead
    * Cecilia will make a slack post
     
<!-- check if anyone has a point that didn't speak already -->

### Round 2

* [Martin] House fullness! Yay! Are we ready? 
    * don't leave your stuff laying around
    * open windows during the night
    * [Antonin] make space in the backpack storage area
    * who is here next week? 9 people right now + family, Larissa, Doug, Linnea, Rudi, Juliette, Lena (19 in total) 

* [Anneke] building week meeting at the weekend?
    * will be part of PlaMe

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Result
* **Mon.:** Dorota
* **Tue.:** Nofal
* **Wed.:** 
* **Thu.:** Maxime
* **Fri.:** Antonin
* **Open Tuesday:** Cecilia
* **Week:** 

