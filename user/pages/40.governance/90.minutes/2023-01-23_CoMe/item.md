---
title: Coordination Meeting
date: "2023-01-23"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #278
* Date: 2023-01-23
* Facilitator: Kito
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Kita
* Physical board caretaker: Doug
* Digital calendar: Maxime
* Reservation sheet purifier: Matthias
* Present: Matthias, Anneke, William, Maxime, Doug, Marieke, Martin, Janina, Kito

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/5eada82e-9e2c-431f-9a40-30737aa74988.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.0 people/day (+6.4)
- **⚡ Electricity**
    - usage: 210.8 €/week (⏫+76%)
    - of that for heating: 139.8 €/week
    - paid: 183.74 €/week 
    - ☀️ self produced: 12% (⬇️-16%)
    - emissions: 89 kg CO₂ₑ/week
- **💧 Water**
    - paid: 14.87 €/week (⬆️+40%)
    - emissions: 1.2 kg CO₂ₑ/week
    - [Antonin]: this is inaccurate because of last week's glitch

### Expenditure
* 15 euro toilet paper [Martin]
* 85 euro oats [kito]
* 13 euro label tape [doug]

### Income
* 22,73€ pfand

### Things that happened

#### In or around Kanthaus
* kiki's gemök meeting
* the last meeting of what once was the kanthaus gemök
* signing the grundstücksverschmelzung-thing at the notary
* andrea came for shooting a kanthaus film
* a snow punk and their friend appeared in the garden
* ftt and anarchist input in leipzig
* an organized attic session with 4 participants
* a hike with 5 people in total on Sunday
* a turkish food pickup from eisenbahnstraße
* ilona brought her bike to get it fixed
* no critical mass due to sickness
* lots of bags were delivered for the free shop
* cinema club restarted in late night edition

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* mask requirement in ÖPNV lifted
* the prime minister of new zealand stepped back
* pension reform gets people in France on the streets
* biden is still being criticized for his behavior with documents

## 2. This week planning

### People arriving and leaving
* **Mon.:** Kiki-GemÖk leaves, Larissa comes back, Tobi and Astrid come for some hours, Marieke leaves, Anja comes for a day or two
* **Tue.:** Matthias leaves
* **Wed.:** 
* **Thu.:** Letti arrives, Martin and Anneke leave for at least a week
* **Fri.:** 
* **Sat.:** 
* **Sun.:** kito leaves for more than a week
* **Mon.:** 
* **Some day:** Anja comes at least on monday/tuesday

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_around the freezing point the whole week, not much snow or sun_

### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Matthias Volunteer : Absolute Days threshold 195/60 (+225%)
- Nathalie Visitor : Days Visited threshold 50/21 (+138%)
- Andrea Volunteer : Absolute Days threshold 117/60 (+95%)
- Martin Volunteer : Absolute Days threshold 107/60 (+78%)
- Anneke_Martin Visitor : Days Visited threshold 31/21 (+48%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Doug Member : 1 days until Absolute Days threshold (180)
- Bodhi Volunteer : 4 days until Absolute Days threshold (60)
- Will_Anneke Visitor : 3 days until Days Visited threshold (21)
- Silvan Visitor : 6 days until Days Visited threshold (21)
- Zuzanna Visitor : 6 days until Days Visited threshold (21)

[Anja] My evaluation monday or tuesday evening?
[Matthias] My evaluation monday?

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Kito]
	* Monday food care [Martin half, Marieke half]
	* 14:00 Matthias' evaluation [Janina]
    * Park trailer on even/this side [maxime]
* Tuesday
    * 10:00 - 13:00 Attic work session [Tilmann, you?]
    * 13:00 Anja's evaluation [Kito]
    * 15:00 - 17:00 Open Tuesday
* Wednesday
    * 10:00 - 13:00 Attic work session [Tilmann, Antonin, you?]
    * Park trailer on odd/that side [maxime]
    * 18:00 @ALM 1st meeting of people interested in participating in Wenceslaigassenfest
    * Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Anneke]
* Friday
    * yellow bins [William]
    * 11:00 Project Updates [Kito]
* Saturday
    * 10:00 - 13:00 Attic work session [Tilmann, you?]
    * Bad Taste Party @NDK
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Janina]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* Ball bearings for vacuum cleaner 10 euros [Martin]
* Cheap safety glasses x6 15 euros [Martin]
* some more vegan spreads [janina]

## 4. To do
_Newest tasks on top_
* [ ] deep-clean shower tap to make it easier to adjust temperature
* [ ] Potato sorting action 
* [ ] Elektroschrott tour [Martin and Carla]
* [ ] General trash tour
* [ ] Sign: Renew the "Klingel" - Sign (it's less and less readable)
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: elephant room
* [ ] Light: Yoga room 
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
* [Doug] OBI trailer: Sell? Store? (If store, in the concrete-rectangle? Antonin's garage?) Continue moving twice a week?
    * Anneke's car doesn't have a hitch, so it would be only Silvan's car we could use it with
    * it costs ~30€ ayear for tax and insurance
    * it sounded like Harzgerode would be very interested in having it, Doug will inquire
    * so we store it in the concrete rectangle for now?
    * Doug will talk to silvan if he was planning on using it
* [Antonin] the [second voting round](https://ukuvota.world/#/app/d9b3a17f-866c-3df8-65e6-5f3227787884/collect) of the constitution change process around the position system is open
    * there were quite different proposals
    * only one will win, so people would need to make merged proposals if they want several things to happen
    * desire for a discussion group? kito will open a slack discussion
* [Janina] workaway requests
    * [Sophie](https://www.workaway.info/en/workawayer/Ringelblume1), after February - not vaccinated
    * [Greta](https://www.workaway.info/en/workawayer/gretiii), ~2 weeks in March
    * [Cristofer y Marjorie](https://www.workaway.info/en/workawayer/CPYMHC), unclear but soon, message in Spanish
        * Martin will take over communication

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Doug] Remove "Mika caretaker" and "Levi caretaker" CoMe roles?
    * [Janina] as long as they're so frequently sick and thus not in kita, I feel better about having it visible that someone needs to take care of them during a meeting. But we could merge them into one. :)
* [Doug] Would someone newer like to call Ole for a chat about power structurs and community topics?
    * [Anneke] yes, I could do that

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** Anneke, William
* **Wed.:** kito
* **Thu.:** maxime
* **Fri.:** 
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** 
* **Tue.:** Matthias, Marieke
* **Wed.:** Matthias, Marieke
* **Thu.:** Matthias, Doug, Marieke
* **Fri.:** Matthias, Doug, Marieke
* **Open Tuesday:** Matthias, Marieke
* **Week:** Martin, Janina

### Result
- **Mon.:** Marieke, Matthias
- **Tue.:** Anneke, William
- **Wed.:** Kito
- **Thu.:** Maxime
- **Fri.:** 
- **Open Tuesday:** anja, Doug

## 7. For next week
* 
