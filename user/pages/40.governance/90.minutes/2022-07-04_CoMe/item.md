---
title: Coordination Meeting
date: "2022-07-04"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #250
* Date: 2022-07-04
* Facilitator: Larissa
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: JaTi
* Physical board caretaker: Jelli
* Digital calendar: zui
* Reservation sheet purifier: Larissa
* Present: Tilmann, Zui, Pilz, Doug, Findus, Jelli, Matthias, Janina, Larissa

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/9759ca27-1d32-4fcb-9155-364832230251.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.7 people/day (+0.0)
- **⚡ Electricity**
    - usage: 41.6 €/week (⬆️+2%)
    - of that for heating: 3.49 €/week
    - paid: -48.61 €/week
    - ☀️ self produced: 75% (⬇️-3%)
    - emissions: 11 kg CO₂ₑ/week
- **💧 Water**
    - paid: 20.04 €/week (⬇️-3%)
    - emissions: 1.6 kg CO₂ₑ/week

### Expenditure
* [tilmann] 541€ for Dachlatten
* [matthias] 5€ for a drillbit for the bicycle repair station
* [matthias] 60€ for 50 corona tests (if we want this as communal expense)

### Income
* no income

### Things that happened

#### In or around Kanthaus
* kanthaus network knowledge sharing session
* the ebike broke and inspired general bike maintenance
* chandi moved to Leipzig and Anja and Antonin moved in together
* Andrea reaccepted as volunteer
* wood delivery and quick storing away action
* the Gemök dissolved in good spirit
* there was a nice trip to the lake

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* there was a shooting in a shopping center Copenhagen
* Russia claimed another city in Ukraine. Belarus says they're also getting attacked by Ukraine and might retaliate.

## 2. This week planning

### People arriving and leaving
* **Mon.:** Unser-Aller-Wald group arrives
* **Tue.:**
* **Wed.:** Silvan comes back, maybe Matthias leaves for two days, UAW group leaves today and tomorrow
* **Thu.:** Pilz leaves
* **Fri.:** Martin leaves, Eric_WA arrives
* **Sat.:** zui leaves, Silvan and Larissa might leave for the weekend
* **Sun.:** Martin comes back, Antonin comes back
* **Mon.:**
* **Some day:** kito might come back at the end of the week

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Matthias Volunteer : Absolute Days threshold 103/60 (+72%)
- Doug Member : Absolute Days threshold 230/180 (+28%)
- Martin_WA Visitor : Days Visited threshold 26/21 (+24%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Findus Visitor : 4 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Larissa]
    * Park KMW & trailer on even/this side [Matthias]
    * Paper waste [Larissa]
* Tuesday
    * Rest waste [Doug]
* Wednesday
    * 14:00 Pop-up culture cafe opening @Wenceslaigasse
    * Park KMW & trailer on odd/that side []
* Thursday
    * 10:00 Power Hour [Fac.: Zui, DJ: ?]
* Friday
    * Bio waste [Larissa]
    * 10.00 Finance meeting [Larissa, Zui, Chandi]
    * 14:30 AG PSW meeting @dragon room? [Janina, Zui]
* Saturday
    * 10:00 600 Jahre Altenbach Fest [Doug, x]
* Sunday
    * Pride in Leipzig
* Next Monday
    * 10:00 CoMe [Antonin]
* Next week summary
    *

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* Martin's evaluation

## 3. Shopping plans
* [Janina] ~40€ for 100 standard size condoms by fairsquare
    * no resistance

* [Martin+chandi+Tilmann+Matthias] quite some bike parts for ~200€
    * [Martin] Rear wheel and drive train for electric bike ~$90+shipping.
        chain option 1
        https://www.bike24.com/p295723.html
        chain option 2
        https://www.taylor-wheels.com/hg40-chain-116-chain-links-7/8-speed
        cassette
        https://www.taylor-wheels.com/cassette-hg41-8-speed-nickel-plated?number=1917176
        crankset option 1
        https://bicycles.de/products/shimano-tourney-fc-ty501-6-8-fach?variant=42162842206393
        crankset option 2
        https://www.bruegelmann.de/shimano-altus-fc-m311-kurbelgarnitur-48-38-28-M129677.html
        pedals
        https://bicycles.de/products/union-pedal-872-allround?variant=41649513726137
        wheel 1
        https://www.taylor-wheels.de/28-zoll-hinterrad-ryde-zac2000-shimano-fh-tx500-7-10-schwarz
        wheel 2
        https://www.taylor-wheels.com/28inch-bike-rear-wheel-zac19-with-shimano-acera-hub-silver
        With chain option 2 and crankset 1 we can wrap it up with only two vendors.
        And I would buy at least another spare chain, as it is a great price.
            * [Matthias] I'd like to add something to the order, please tell me before/where you order :-)
            * [chandi] same :) @ bicycles.de
            * [tilmann] two 62-203 tires (for green kids scooter)
* [Zui/Matthias] ~~30€~~ 43€ for 16 original miele vaccuum cleaner bags

## 4. To do
_Newest tasks on top_
* [ ] fix or replace leaking K20-0# toilet fill valve
* [ ] fix weekly-report script
    * [x] water stats [antonin]
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?] Denkmalschutz der Fassade?
* [ ] sort out garden stuff in woodshed [Anja]
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] mount fire extinguisher
* [ ] first aid kits visible in the staricases K20-1 and K22-1
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Doug] Food: 1) tiny onions: collective chopping action or throw away? (For future reference, please do not bring onions smaller than golf ball!) 2) sweets: how much haribo/bonbons/duplo can we give away?
* [janina] pop-up kulturcafe is opening on Wednesday 2 pm!
    * they're happy about people bringing cake and sweets! (thursday, too)
    * wanna go? :)
    * i want to put a tiny freeshop there and would be happy about support
    * might be possible to use it as coworking space in the mornings, emma keeps us posted
* [Tilmann] oat milk and pea milk stacks in basement are unstable and might damage the packages soon - should distribute the stacks to more places!
    * Lets have an action to organise some areas.
    * Doug is starting tuesday around 12:00h
* [kito] Mulli from Lausitz-Spinnerei is going by train through Wurzen today and would be happy to take some of the food we have a lot of. Because she doesn't has much time it would be the best if someone can bring a barrow (or something like this) to the train at 17:18. Can someone imagine to take care of that? i'm available for questions.
    * doug is going
* [matthias] vacuum cleaner bags (...again)
    * more expensive than expected: 43€
    * Doug is going to buy some bags (maybe cheaper ones, maybe original)
    * maybe get a bag-free vacuum cleaner in the future (zui feels responsible for research)
* [Findus] group will cook most of the meals, maybe lunch. they also have repro-shifts for which there will be a sheet to sign up. Lets use it all together (it's in the dining room).
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [tilmann] food to be sorted & washed - who's up for a shift today?
    * Doug, maybe Larissa, UnserallerWald-Group

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Janina, Tilmann
* **Tue.:** Findus, Jelli
* **Wed.:** Zui, Pilz
* **Thu.:**
* **Fri.:**
* **Open Tuesday:** Doug, Matthias

### Unavailabilities
* **Mon.:**
* **Tue.:**
* **Wed.:** Matthias
* **Thu.:** Matthias
* **Fri.:** Martin, Janina, Matthias
* **Open Tuesday:**
* **Week:** Larissa

### Result
- **Mon.:** Janina, Tilmann
- **Tue.:** Findus, Jelli
- **Wed.:** Zui, Pilz
- **Thu.:** martin
- **Fri.:**
- **Open Tuesday:** Doug, Matthias

## 7. For next week
*
