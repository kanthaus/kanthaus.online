---
title: Coordination Meeting
date: "2023-09-11"
taxonomy:
    tag: [come]
---

# CoMe #310
* Date: 2023-09-11
* Facilitator: Anneke
* Notary: Janina
* Physical calendar: Doug
* Digital calendar: Maxime
* Reservation sheets: Anneke
* Weather forecast: Kate
* Present: Tilmann, Martin, Larissa, Doug, Maxime, Kate, Janina, Alejandro, Anneke, Dorota, Bodhi, Nikhil

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

![usage graph last 90 days](https://pad.kanthaus.online/uploads/8266ccca-e9d1-4a0d-bc1f-6d9ea175f730.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.4 people/day (-6.1)
- **⚡ Electricity**
    - usage: 30.37 €/week (⬆️+6%)
    - of that for heating: 0.51 €/week
    - of that for hot water: 3.07 €/week
    - paid: -34.63 €/week 
    - ☀️ self produced: 75% (⬆️+10%)
    - emissions: 15 kg CO₂ₑ/week
- **💧 Water**
    - paid: 31.38 €/week (⬆️+36%)
    - emissions: 2.4 kg CO₂ₑ/week



### Expenditure
* 25€ electric material for attic [Martin]
* 3.45€ wax ear plugs [maxime]
* 40€ a second hand discount projector [maxime]

### Income
* 63€ donation box

### Things that happened
#### In or around Kanthaus
* Franzi's party
* A big version of Project Updates
* 1st onion saving action of the years
* we have brand new electric sockets and switches, for display already in the Lantern and the attic
* water accident in the lower staircase toilet
* stuff saving action from a neighboring flat
* somebody complained via e-mail about too much noise between 22:00 and 4:00
* singing session
* yoga class

#### Wider world
* devastating earthquake in Morocco, enormous amounts of rain that caused floods in Greece
* 50 years anniversery of the coup in Chile
* 10 years anniversary of foodsharing Leipzig

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

<!-- ### Tekmîl-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Miel leaves
* **Tue.:** Dima should arrive around 18:00
* **Wed.:** Bodhi leaves
* **Thu.:** Anneke leaves
* **Fri.:** Antonin comes back, kito arrives, group of 7 arrives
* **Sat.:** 
* **Sun.:** group of 7 leaves
* **Mon.:** 
* **Some day:** 

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
_hot until Wednesday, then temperature drop and rain again_
 
### Evaluations and check-ins

- Anneke Volunteer: Days Visited threshold 110/60 (+83%)
  - seems somebody forgot to enter the record?
- Alejandro_Martin Visitor: Days Visited threshold 26/21 (+24%)
- Larissa Member: Days Visited threshold 210/180 (+17%)
- ASZ1 Visitor: Days Visited threshold 24/21 (+14%)
- ASZ2 Visitor: Days Visited threshold 24/21 (+14%)
- Maxime Member: Days Visited threshold 191/180 (+6%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Anneke]
    * 11:00 - Power Hour
    * 18:00 - Once more onion saving [Janina]
* Tuesday
    * black bin [Martin]
    * Attic work session [Maxime, ASZ1]
    * 15:00 - 17:00 Open Tuesday [lottery] 
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * 18:00 - Punkrocktresen
    * ~20:00 - little "farewell" with fire and beer in the yard (ASZ1 wants to make falafel)
* Thursday
    * 13:00 BufDi presentation [Doug]
    * 13:00 - 18:00 MitMachCafé
    * 18:30 - FLINTA thai boxing @ D5
* Friday
    * Biomüll [Ale]
    * 10:00 - Social sauna [Larissa]
* Saturday
    * Field working event at KoLa [JaTiMiLe, you?]
* Sunday
    *  
* Next Monday
    * 10:00 CoMe [Martin]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
- Attic session this week?


## 3. Shopping plans
- [Bodhi] request 50€ as a share of gas and car usage expenses(total cost is ~115€ with 0.35eur/km) for the tour here
    - granted
- [Martin] LED strip for the 2nd level of the attic for ~15€
    - [Tilmann] we also have fairy lights that could go up, but sure, go ahead

## 4. To do
_Newest tasks on top_

* [ ] properly dispose of the lead battery in the trash corner [Maxime]
* [ ] fix IPv6 access to cloud.kanthaus.online 
* [ ] put Lantern smoke detector back on ceiling [Martin]
* [ ] upstairs toilet K-20 flushing mechanism needs small fix (ask Martin or Doug) [Janina]
* [ ] cover the pipes from the rest of the room in the snack kitchen (wooden board angle?) []
    * [tilmann] maybe directly go for drywall as final measure? also, should put some cement into the hole as fire protection
      * [maxime] that would be the right but more difficult remediation indeed. I looked at options to hack a wooden corner, but didn't find satisfying options in the wood shed, so I'll leave there.
* [ ] fix up beer benches and tables [Alejandro, Klara]
* [x] fix occupancy indicator ex-foodstorage [Martin]
* [ ] Small bin for every toilet
* [x] ~~Fix~~ Make Dorm window joint somewhat better [maxime]
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the street side facade [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
* [Larissa] private and communal shoe sale in the dragon room until thursday (14th)
* [kito] group over the weekend!
    * they arrive friday afternoon and stay until sunday midday/afternoon
    * they will use the dragon and the cloud room for their meeting. can someone reserve them?
    * communal as a FLINTA-sleeping-space during the weekend
    * can someone reserve the Lantern, too?
        * Anneke will reserve the rooms
        * [Doug] Unease about this. Would feel better if the group completely reserved the room, and then if they want it to be FLINTA only, they do that (and privately invite Kanthaus FLINTA). i do have resistance to internal kanthaus space separation based on identity.
* [Anneke] handing over workaway, stepping down to visitor on thursday
    * Kate is interested in WA
    * Larissa will put step-down in evRec
* [Doug] proposal to delete 5 calendars in a week https://yunity.slack.com/archives/C3RS56Z38/p1694294493009869
* [tilmann] bbq room very smell after leak, maybe still wet? should we empty it again?
    * move more valuable stuff out and let the room itself dry a bit more
    * no big communal action planned
* [janina] [ebike name ukuvota](https://legacy.ukuvota.world/#/app/1d496f30-2228-c4b9-7b85-4898736ca0d5/results)
    * draw between 'lightning' and 'silverado': let's decide right now! draw again xD
    * tiny emoji vote to follow up on this
* [kate] compost: i think the current kh compost is unusable. how to proceed?
    * slack post will follow, then maybe a focus group
* [dorota] physical list of 'things to be done' is now hanging in the dining room and fills up nicely

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [kito] i have a foodsharing slot for penny, thursday evening. but i won't be here. anyone with a valid foodsharing account interested to take over? i think it would be nice for the weekend (with the group) to have enough food
    * Maxime can do it and will get in contact with Kito
* [Anneke] I still don't know how to exactly carry all my stuff to Switzerland. One idea was to use one of the hand trucks. Any objection?
    * No resistance, details will be clarified
* [tilmann] let's not store our stuff in the  Kleinanzeigen room!
    * signs in are valid (even if there was no communal decision!), let's not just ignore them!
    * either remove them, make new ones or update them
* [Anneke] Today I'll make Kombucha here for the last time. Anyone willing to take over? Shall I reduce the amount of jars?
    * no volunteer found
* [Dorota] Very first draft of newcomer introduction hangs on the K20 entrance door. Feedback much appreciated!
    * feedback either on the sheet or to Kate
* [Kate] How to move forward the bigger topic of visitor onboarding?
    * Slack post would be cool, then we see from there
* [Doug] Thursday afternoon I'll hold a short presentation about the BufDi scheme

<!-- check if anyone has a point that didn't speak already -->

### Round 3


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Kate
* **Tue.:** Bodhi
* **Wed.:** ASZ1
* **Thu.:** Janina
* **Fri.:**
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** Alejandro, maxime, dorota
* **Tue.:** maxime, dorota
* **Wed.:** Doug
* **Thu.:** Doug
* **Fri.:** Doug
* **Open Tuesday:** maxime, dorota
* **Week:** Martin, Larissa, Tilmann, Anneke

### Result

* **Mon.:** Kate, Doug
* **Tue.:** Bodhi
* **Wed.:** Alejandro
* **Thu.:** Janina
* **Fri.:** Nikhil, Maxime
* **Open Tuesday:** Larissa

## 7. For next week
