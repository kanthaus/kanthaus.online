---
title: Coordination Meeting
date: "2022-02-28"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #233
* Date: 2022-02-28
* Facilitator: Matthias
* Notary: Janina
* Mika caretaker: Tilmann 
* Levi caretaker: Tilmann
* Alex caretaker: Cille
* Physical board caretaker: Andrea
* Digital calendar: Antonin
* Reservation sheet purifier: Kito
* Present: Andrea, Antonin, Anja, Silvan, Larissa, chandi, Kito, Matthias, Lise, Doug, Nathalie, Cille, Maxime, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf481f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 18.6 people/day (+0.8)
- **⚡ Electricity**
    - usage: 106.96 €/week (⬆️+8%)
    - of that for heating: 65.6 €/week
    - paid: 65.5 €/week 
    - ☀️ self produced: 29% (⬇️-5%)
    - emissions: 62 kg CO₂ₑ/week
- **💧 Water**
    - paid: 0.0 €/week (0%)
    - emissions: 0 kg CO₂ₑ/week

### Expenditure
* [matthias] 74€ for additional hot water parts

### Income
* 100€ in the box

### Things that happened

#### In or around Kanthaus
* social sunday!
    * football+dodgeball in Bennewitz
    * delicious chocolate cake
    * sponatenous acroyoga session in the piano room
* Silvan reaccepted as Volunteer and Tilmann as a Member
* a small induction plate now operational in the snack kitchen (mostly for coffee)
* some garden work (cutting trees, roses, ripping out unwanted stuff)
* the very last ToI meeting
* singing session
* yet another screening of Encanto
* finance sessions
* a lot of inventaire architecture discussions
* a very fancy playground was discovered in Leipzig and Mika is in love
* Levi can speak now! xD

#### Wider world
* bombs dropped at 1200km from Wurzen
* 100 billions for the German army

## 2. This week planning

### People arriving and leaving
* **Mon.:** Bodhi already left, Annie arrives, Kiki comes, Lise leaves, Clara leave
* **Tue.:** Julia leaves, Anna leaves today or tomorrow
* **Wed.:** Anja leaves, CiJuAl and Annie leave (or on Tuesday already), Callum might come
* **Thu.:** Kito and Maxime leave
* **Fri.:** 
* **Sat.:** Silvan leaves, Andrea might leave for a day
* **Sun.:** Antonin leaves, Anja might come back
* **Mon.:** Kiki leaves
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
- freezing coldness again, but with a lot of sun_


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Matthias]
    * 12:30 Anja's evaluation [Kito]
    * 14:00 Silent office renovation meeting @silent office [Antonin, Anja]
    * Park cars on even/this side [Silvan]
* Tuesday
    * Rest waste [Antonin]
    * 12:00 Andrea's evaluation [Maxime]
    * 14:00 longterm process meeting/after ToI coworking @fansipan [chandi, Kito, Janina]
    * 15:00 Open Tuesday
    * 18:00 workout@Yogaroom [Larissa, Silvan]
* Wednesday
    * 10:00 Breakfast table MCM @dining room [Janina]
    * 12:00 Garden meeting @garden [Anja, Kito, Janina]
    * Park cars on odd/that side [chandi]
    * 14:15 blood donation party [Antonin, Matthias]
* Thursday
    * 10:00 Power Hour [Fac.: Kito, DJ: ?]
    * 15:00 Social sauna [Larissa]
* Friday
    * Organic waste [Janina]
    * 10:00 (maybe last) Corona meeting @ piano room 
* Saturday
    * 14:00 Project updates [Antonin] 
* Sunday
    * 10:30 Finance meeting [chaLaZu]
* Next Monday
    * 10:00 CoMe [?]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
<!-- If some plans to spend more than 250€, please check back with the finance team -->
* [Silvan] 42,23€ fully refuel KMW

## 4. To do
_Newest tasks on top_
* [ ] fix water stats
* [ ] remove old gas heater from basement (to trash corner?)
* [ ] remove boiler and pass-through heater from K20-2 bathroom
* [ ] mount sink in washroom
* [ ] analog week plan needs a repair
* [ ] repair WiFi in the Hipster
* [ ] repair Schaukasten 
* [ ] repair hat/gloves/scarves cupboard in hallway of K20
* [ ] Clean K18 garden (done by wind :P )
* [ ] Making new signs for the commode in the hallway
* [ ] Install light in the freeshop hallway
* [ ] Door-closer for Elefant-staircase door
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Install light in the hallway of the K20-1 flat
* [ ] Install light in the hallway of the K20-2 flat

### more advanced tasks
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements
### Round 1
* [Antonin] potential guests looking for hosts:
   - Till, staying for ~ 10 days, not clear yet when
       - we're waiting for them to reply and tell us dates
   - Prairie, staying from 20th March to 1st April
       - Andrea and Janina can imagine hosting, Andrea will take over communications
* [Doug] Three corona points
    1. [Freeshop -> '0G'](https://www.coronavirus.sachsen.de/wir-gegen-corona-8251.html)
    2. pandemic ends for us when state says it does... right?
        * what does this mean? e.g. not buying masks/test with communal money, not factoring this in when hosting
        * what about isolation/quarantine in case of infection? if state says not necessary, do we also?
        * Doug brings a strong voice for easing measures if the states does and is expecting pushback
        * Bigger topic, it's a process. But it should be tackled before the time comes for it to be relevant.
    3. Retrospective & sharing when it's over
        * Nice idea, people are happy for Doug to bring it up again when the time comes.
* [Tilmann] (https://yunity.slack.com/archives/C3RS56Z38/p1645987464327009) Washroom construction update: Matthias and me put in ventilation and warm water pipes and largely covered the hole! We are not planning to work there again in the next weeks, but here are some tasks that could be done by others:
    * put in heating pipes for attic
    * put in cables (-> @chandi)
    	* [chandi] anyone motivated to drill holes in the ceiling attic->bathroom? :)
    * fill floor and ceiling with concrete (after pipes and cables are in)
    * make ventilation hole to silent office
* [Janina] Next building weeks in April?
    * projects: attic insulation, ventilation, room prettification
    * no expected participation, but people comitting to specific parts/projects (also repro)
    * small group work with skillsharing inside them
    * not a lot of extra structure since most coordination happens in small groups
    * invite quite some external visitors
    * impact on everyone: noise, dirt, house fullness and thus more load on general structures
    * -> if no resistance, can we find a date soon?
    * generally okay, concerns about group and visitor requests as long as time is not clear (specifically Tobi's book group), another concern about small group structutre and how good it would work
    * Janina will give more info and a duudle on Slack soon and continue bringing this forward
* [Andrea] Soli room: There is a [proposal](https://pad.kanthaus.online/solizimmer) now, please look at it!
* [Silvan] Acronyx meeting 12./13.3.
    * no resistance, no support, Silvan will give more info on Slack soon
* [larissa] Small finance team announcement
    * if you want to spend more than 250€ in cash, check back with the finance team please
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Antonin] dumpster washing - let's do it!
    * there even is warm water from the tap now! :D
    * also carrying up already washed food and sorting it in is really helpful!

### Last Points
* [Tilmann] Childcare for attic working hours: e.g. walking the kids to sleep at 11:30 or playtime from 14-16/17
* [Janina] Lunch team assemble!
    * Janina-Thu, Tilmann-Wed, Matthias-Fri, chandi-Tue, Larissa-Mon, Silvan-Thu, Mika, Levi

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** Janina, Andrea
* **Wed.:** 
* **Thu.:**
* **Fri.:** 
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** Nathalie, kiki, Matthias
* **Tue.:** Antonin, chandi, Matthias
* **Wed.:** Antonin, Nathalie, Matthias
* **Thu.:** Antonin, chandi, maxime
* **Fri.:** chandi, maxime, kiki
* **Open Tuesday:** maxime, chandi, Matthias
* **Week:** kito, Larissa, Silvan, Lise, Cille, Jums

### Result
- **Mon.:** chandi, Maxime
- **Tue.:** Janina, Andrea
- **Wed.:** kiki
- **Thu.:** Nathalie, Doug
- **Fri.:** Antonin, Matthias
- **Open Tuesday:** Anja

## 7. For next week

