---
title: Coordination Meeting
date: "2021-10-25"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #215
- Date: 2021-10-25
- Facilitator: Doug
- Notary: Janina
- Mika caretaker: Thore
- Levi caretaker: Tilmann
- Physical board caretaker: Nathalie
- Digital calendar: Zui
- Reservation sheet purifier: 
- Present: Janina, Antonia, Nathalie, Larissa, Silvan, Andrea, Zui, Saranke, Marek, Delfin, Maxime, Konrad, Guido, Doug, Matthias

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
- **Present:** 24.7 people/day (+1.6)
- **⚡ Electricity**
    - usage: 9.76 €/day (⬆️+4%)
    - paid: 2.81 €/day 
    - ☀️ self produced: 62% (⬆️+1%)
    - emissions: 24 kg CO₂ₑ/week
- **💧 Water**
    - paid: 5.21 €/day (⬆️+31%)
    - emissions: 3 kg CO₂ₑ/week

### Expenditure
- metal roof parts: 216.65
- vacuum cleaner: 50€
- vacuum cleaner bags: 9€
- oil: 20€

### Income
- shoe: 18.04
- bottle pfand: 15

### Things that happened
#### In and around Kanthaus
- lots or roof work again
    - all bitumen gone and brought away
    - bad wood parts got replaced
    - most nils were removed
    - window holes measures and cut out
- very popular FINTA* construction slot
- K22-2 bathroom was cleared, cleaned and wall plastered
- lots of dumpsterdiving
- Mika's 2nd birthday with cake party
- bassment party
- lots of game and movie nights

#### In the wider world
- Turkish president doesn´t like the other world

## 2. This week planning

### People arriving and leaving
- **Mon.:** 
- **Tue.:** Roswitha and Gerd leave, Andrea leaves
- **Wed.:** Änsky comes, Andrea comes back, Larissa + Silvan leave
- **Thu.:** Clara leaves, Delfin leaves
- **Fri.:** Artiola leaves, Anja might come
- **Sat.:** Charly arrives, Larissa + Silvan comes back
- **Sun.:** Antonia leave, Damien and Colette arrive
- **Mon.:** 
- **Some day:** 

### Weather forecast

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

_paused for roof month_

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 17:30 CoMe [Doug]
    * Park cars on even side [done]
- Tuesday
    - 14:00 roof window intro [zui]
    - 15:00 house tour [janina]
    - Rest waste [Matthias]
    * Open Tuesday
- Wednesday
    * Park cars on odd side [Matthias]
- Thursday
    - 15:00 Social sauna [Doug]
- Friday
    - Organic waste [Maxime]
    * 12:00 Market Pickup [Marek]
    * 15:00 Power Hour [Doug]
- Saturday
    * 11:00 Football game!
    * 20:00 Sauna @ Villa Klug
- Sunday
- Next Monday
    * 10:00 CoMe [Janina]
- Next week summary

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans


## 4. To do
_Small fixes of things in the house_

### High priority
* [ ] secure k22-2 bathroom sink
* [ ] fix K22-2 toilet flush pipe
* [ ] KMW: Fix the water leak under the front shield
* [ ] KMW: Fix cooling pipe so it does not touch the hot exhaust

### Medium priority
* [ ] more storage spaces for visitors
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] yellow bin in snack kitchen and kitchen - new solution
* [ ] reinforce hook for laundry rack K22-1# [Antonin]
* [ ] lubricate door hinges (which?)
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch

## 5. Discussion & Announcements

### Round 1
<!-- Anybody who hasn't spoken so far wants to add a point? -->
- [Nathalie] Advent calendar (find it)
    - [janina] Lise said she never took it, maybe it's still in the drawer?
    - [nathalie] ah cool, I'll have a look!
- [Doug] Football + Sauna on Saturday?
    - General appreciation
    - A Kanthaus delegation should go there and start the fire in the sauna roughly 2h before we want to go in.
    - Interested in going early: Doug, maybe Matthias (more people very welcome!)
- [Silvan] We are many people in the house with only one bathroom. Please think twice if you need the room private. E.g. we have many toilets in the house, maybe don't use the toilet there if you need a private toilet room.
    - [nathalie] addition to that: the shower in the flat can also be used. please approach me if you need more information about how to go about this.
- [janina] oats almost empty!
    - there's still basic muesli, so maybe it's noit urgent to buy new oats?
    - generally if someone feels like pushing this, the process should be easy, as we seem to have a general agreement that we are okay with buying oats regularly.
- [zui] what about social sauna this week? I think we should have it.
    - we're moving appointments anyway, so we can look for the best time to do social sauna as well
    - scheduling is a bitch - the time stays default: thursday 15:00
- [matthias] Please ventilate rooms when neccessary (e.g. condensation at the windows, before going to bed, after getting up); Preferrably by opening two windows on the opposite sides of the house.

### Round 2

<!-- Anybody who hasn't spoken so far wants to add a point? -->

### Round 3

- [tilmann] looks like the heatpump might arrive tomorrow! needs 4 people for carrying :)

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

Participants: Janina_1, Janina_2, Janina_3, Antonia_1, Antonia_2, Antonia_3, Nathalie_1, Nathalie_2, Nathalie_3, Andrea_1, Andrea_2, Andrea_3, Zui_1, Zui_2, Zui_3, Saranke_1, Saranke_2, Saranke_3, Marek_1, Marek_2, Marek_3, Delfin_1, Delfin_2, Delfin_3, Maxime_1, Maxime_2, Maxime_3, Konrad_1, Konrad_2, Konrad_3, Guido_1, Guido_2, Guido_3, Doug_1, Doug_2, Doug_3, Matthias_1, Matthias_2, Matthias_3, chandi_1, chandi_2, chandi_3

### Volunteers
- **Mon. lunch (3):** Matthias_1, Clara_1, Placeholder_1
- **Mon. dinner:** Antonin_1, Andrea_1
- **Mon. morning cleanup (1):** Placeholder_2
- **Mon. afternoon cleanup (1):** Placeholder_3
- **Mon. dinner cleanup (1):** chandi_1
- **Tue. lunch (3):**
- **Tue. dinner:**
- **Tue. morning cleanup (1):** Larissa_1
- **Tue. afternoon cleanup (1):**
- **Tue. dinner cleanup (1):**
- **Wed. lunch (3):** Larissa_2, Silvan
- **Wed. dinner:**
- **Wed. morning cleanup (1):**
- **Wed. afternoon cleanup (1):**
- **Wed. dinner cleanup (1):** Andrea_2
- **Thu. lunch (3):**
- **Thu. dinner:**
- **Thu. morning cleanup (1):**
- **Thu. afternoon cleanup (1):**
- **Thu. dinner cleanup (1):**
- **Fri. lunch (3):**
- **Fri. dinner:**
- **Fri. morning cleanup (1):**
~~- **Fri. afternoon cleanup (1):**~~
- **Fri. dinner cleanup (1):**
- **Sat. lunch (3):**
- **Sat. dinner:**
- **Sat. morning cleanup (1):**
- **Sat. afternoon cleanup (1):**
- **Sat. dinner cleanup (1):**
- **Sun. lunch (3):**
- **Sun. dinner:**
- **Sun. morning cleanup (1):**
- **Sun. afternoon cleanup (1):**
- **Sun. dinner cleanup (1):**
- **Open Tuesday:**

### Unavailabilities
- **Mon. all:*
- **Mon. lunch:**
- **Mon. dinner:**
- **Mon. morning cleanup:** Tilmann
- **Mon. afternoon cleanup:** Tilmann
- **Mon. dinner cleanup:** Tilmann
- **Tue. all:** Tilmann, Andrea, Matthias
- **Tue. lunch:** Antonin, Marek, Larissa
- **Tue. dinner:**, Janina, Tilmann, Nathalie, larissa
- **Tue. morning cleanup:** Antonin, Nathalie
- **Tue. afternoon cleanup:** Antonin, Nathalie, Larissa
- **Tue. dinner cleanup:** Antonin, Nathalie, Larissa
- **Wed. all:** Tilmann, Matthias, Larissa
- **Wed. lunch:**
- **Wed. dinner:** Antonin
- **Wed. morning cleanup:** Thore
- **Wed. afternoon cleanup:** Thore
- **Wed. dinner cleanup:** Thore
- **Thu. all:** Andrea, Zui, Larissa, Delfin
- **Thu. lunch:** 
- **Thu. dinner:** 
- **Thu. morning cleanup:** Thore
- **Thu. afternoon cleanup:** Thore
- **Thu. dinner cleanup:** Thore
- **Fri. all:** Antonin, Andrea, Doug, Larissa, Delfin
- **Fri. lunch:** Maxime
- **Fri. dinner:** Maxime
- **Fri. morning cleanup:**
- **Fri. afternoon cleanup:**
- **Fri. dinner cleanup:**
- **Sat. all:** Antonin, Andrea, Doug, Larissa, Delfin
- **Sat. lunch:** Maxime
- **Sat. dinner:** Maxime
- **Sat. morning cleanup:** Thore
- **Sat. afternoon cleanup:** Thore
- **Sat. dinner cleanup:** Thore
- **Sun. all:** Antonin, Doug, Larissa, Delfin
- **Sun. lunch:** Maxime
- **Sun. dinner:** Maxime
- **Sun. morning cleanup:**
- **Sun. afternoon cleanup:**
- **Sun. dinner cleanup:**
- **Open Tuesday:** Janina, Maxime, Tilmann, Thore, chandi

### End result
- **Mon. lunch:** Matthias, Clara
- **Mon. dinner:** Antonin, Andrea
- **Mon. morning cleanup:** Matthias
- **Mon. afternoon cleanup:** Matthias
- **Mon. dinner cleanup:** chandi
 
- **Tue. lunch:** Konrad, Delfin
- **Tue. dinner:** Delfin, Maxime
- **Tue. morning cleanup:** Larissa
- **Tue. afternoon cleanup:** Delfin
- **Tue. dinner cleanup:** Guido
- **Open Tuesday:** Doug, Antonia
 
- **Wed. lunch:** Larissa, Silvan
- **Wed. dinner:** Zui, Janina
- **Wed. morning cleanup:** Maxime
- **Wed. afternoon cleanup:** Maxime
- **Wed. dinner cleanup:** Andrea

- **Thu. lunch:** Saranke, Doug
- **Thu. dinner:** Doug, Marek
- **Thu. morning cleanup:** Saranke
- **Thu. afternoon cleanup:** Marek
- **Thu. dinner cleanup:** Nathalie

- **Fri. lunch:** Matthias, Guido
- **Fri. dinner:** Konrad, Matthias
- **Fri. morning cleanup:** chandi
- **Fri. dinner cleanup:** chandi

- **Sat. lunch:** Nathalie, Konrad
- **Sat. dinner:** Antonia, Antonin
- **Sat. morning cleanup:** Zui
- **Sat. afternoon cleanup:** Marek
- **Sat. dinner cleanup:** Janina

- **Sun. lunch:** Janina, Andrea
- **Sun. dinner:** Antonia, Saranke
- **Sun. morning cleanup:** Nathalie
- **Sun. afternoon cleanup:** Zui
- **Sun. dinner cleanup:** Guido

## 7. For next week