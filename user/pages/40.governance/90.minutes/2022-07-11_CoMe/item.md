---
title: Coordination Meeting
date: "2022-07-11"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #251
* Date: 2022-07-11
* Facilitator: Antonin
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Tilmann
* Physical board caretaker: Eric
* Digital calendar: Matthias
* Reservation sheet purifier: Doug
* Present: Matthias, Eric, Andrea, Doug, Antonin, Janina, Martin

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/ab73a7f3-6059-436d-9b0f-2f476e1bb487.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.9 people/day (+0.1)
- **⚡ Electricity**
    - usage: 48.16 €/week (⬆️+16%)
    - of that for heating: 3.6 €/week
    - paid: -37.8 €/week 
    - ☀️ self produced: 76% (⬆️+1%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 23.81 €/week (⬆️+19%)
    - emissions: 1.9 kg CO₂ₑ/week

### Expenditure
* 40€ for 100 fair and vegan condoms, now in bathroom
* 5€ for drill bit
* around 150€ for bike parts

### Income
* 70€ donation

### Things that happened

#### In or around Kanthaus
- 600 years Altenbach celebration with Kanthaus info stand
- UAW group meeting
- finance meeting that led to the Steuererklärung being sent out :tada:
- Martin became a volunteer
- Pop-up Kulturcafe opened and saw a lot of us
- Leek abundance
- Old but unused ebike batteries arrived and were checked out
- oatmilk restacking
- AoE party
- Food, drinks, empty buckets and other things arrived from Fusion
- Bucket sorting and cleaning action
- ...aaand pride leipzig
- Martin and Andrea's brithdays have happened / are happening

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* Shizo Abe got shot and killed
* Boris Johnson resigned

## 2. This week planning

### People arriving and leaving
* **Mon.:** Andrea leaves
* **Tue.:** Rosano visits for the day
* **Wed.:** Antonin's parents come
* **Thu.:** 
* **Fri.:** Janina leaves
* **Sat.:** Antonin's parents, Matthias leave
* **Sun.:** Janina comes back
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_23-30°C during daytime, no signs of rain, but some clouds here and there_

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Matthias Volunteer : Absolute Days threshold 110/60 (+83%)
- Doug Member : Absolute Days threshold 237/180 (+32%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Kito Volunteer : 1 days until Absolute Days threshold (60)

Check-In
- Eric

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Antonin]
    * 14:00 Matthias' evaluation [Antonin]
    * 15:30 Birthday cake for Andrea and Martin
    * Park KMW & trailer on even/this side [done]
    * Dumpster washing and sorting [Martin, Antonin]
* Tuesday
    * 5 year anniversary of Kanthaus!
    * 15:00 Open Tuesday
* Wednesday
    * Park KMW & trailer on odd/that side [Doug]
    * 18:00 Punkrocktresen @D5
* Thursday
    * 10:00 Power Hour [Fac.: Janina, DJ: ?]
    * 15:00 Project updates @Piano room [Matthias] 

* Friday
    * yellow bins [Martin]
    * 12:00 Market pickup @Jacobsplatz [Martin]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Doug] 
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [Doug] [Glitzer im Kohlenstaub](https://www.beck-shop.de/glitzer-kohlestaub/product/33996192), 20€
- [Martin] cheaper vacuum cleaner bags to test them out

## 4. To do
_Newest tasks on top_
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] fix or replace leaking K20-0# toilet fill valve
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?] ~~Denkmalschutz der Fassade?~~
* [ ] sort out garden stuff in woodshed [Anja]
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] mount fire extinguisher [chandi]
* [ ] first aid kits visible in the staricases K20-1 and K22-1
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] add a doorbell in the garden
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Matthias] Request from Utopival to borrow KMW 28.07-07.08. (Email)
    * [Doug] If they have a mechanic they can have it, but otherwise it would maybe be worse to have an unrealible car than none.
    * [Antonin] will reply to them
* [Janina] Visitor request from a person based in Leipzig
    * generally people are up for it, need to know the time
    * Janina will answer the mail and find out
* [Antonin] last chance to fill the K18 poll: https://cloud.kanthaus.online/apps/forms/AEzfKtedLMf5xNym
* [Doug] I'll make a fediverse account for Kanthaus. Happy about interested people! There already is a Slack discussion.
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Janina] Pop-up Kulturcafe continues! They're always happy about people coming by, bringing and/or eating cake and socializing. Opening hours: Wed-Sun 14-18
    * [Matthias] Also there is another Küfa on Saturday where cooking for 20 people would be appreciated
* [Martin] We have so much tea that I think we should give some away. I'm happy to make a box.
    * [Doug] Maybe put it in the vortex to have people look at it.
    * [Matthias] I'm also keen to take some of the stuff we have loads of to Jena. I'll go again on the weekend.

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:**
* **Tue.:** Janina
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** 
* **Tue.:** Antonin, Doug
* **Wed.:** Antonin, Doug
* **Thu.:** Antonin
* **Fri.:** Matthias, Doug
* **Open Tuesday:** Matthias, Antonin
* **Week:** Andrea, Eric

### Result
- **Mon.:** 
- **Tue.:** Janina
- **Wed.:** Martin
- **Thu.:** Matthias
- **Fri.:** Antonin
- **Open Tuesday:** Doug

## 7. For next week
* 

