---
title: Coordination Meeting
date: "2021-09-13"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #209
- Date: 2021-09-13
- Facilitator: Larissa
- Notary: Antonin
- Mika caretaker: Silvan
- Levi caretaker: Janina/Silvan
- Physical board caretaker: Maxime
- Digital calendar: Tilmann
- Reservation sheet purifier: Larissa
- Present: Thore, Larissa, Maxime, chandi, romii, Luna, Tilmann, Janina, Antonin, findus

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_57804ee455f5832e07ddeb443280acf6.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.0 people/day (+4.3)
- **⚡ Electricity**
    - usage: 5.96 €/day (⬇️-6%)
    - paid: -3.53 €/day 
    - ☀️ self produced: 75% (⬆️+2%)
    - emissions: 12 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.57 €/day (⬆️+10%)
    - emissions: 2 kg CO₂ₑ/week
    
### Expenditure

### Income
no income

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)

> From [Collective Agreements](https://kanthaus.online/en/governance/collectiveagreements): *If adjusted prevalence is < 0.1%, we have no formal restrictions.*
> Current **adjusted prevalence** can be found [here](https://www.microcovid.org/?duration=600&interaction=oneTime&personCount=1&riskBudget=100000&riskProfile=average&scenarioName=custom&setting=indoor&subLocation=Germany_Sachsen&theirMask=none&topLocation=Germany&voice=loud&yourMask=none&yourVaccineType=johnson): click on "Details" in the "Step 1" column

- Incidence
    - Germany: 90,6
    - Saxony: 32,5
    - Landkreis Leipzig: 21,7
- Adjusted prevalence Saxony: 0,11% 
- µCOVIDs available last week: 5000
- µCOVIDs used last week: 427.9018
- µCOVIDS balance from last week: 4572
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 9572


### Things that happened
#### In and around Kanthaus
- A meeting about technical aspects of the upcoming roof month
- Thore secured an onion gleaning possibility
- Heated discussions about a potential spontaneous heat pump installation
- KIDZ meeting, with sauna at VK
- Summer came back and people did trips to the lake
- Maxime was reaccepted as Member and Antonin became a Volunteer
- JaTiMiLe left on a bike trip ... and came back in pain :(
- Thore's parents visited briefly
- Mattress from ex-food-storage dissolved and was disposed of
- A lot of people are sick
- A lot of food was saved
- Levi got a lot of mosquito bites

#### In the wider world
- 20th memorial of 9/11

## 2. This week planning

### People arriving and leaving
- **Mon.:** Zui and Marek come
- **Tue.:** Nathalie leaves
- **Wed.:** romii & findus leave, chandi for a night
- **Thu.:** 
- **Fri.:** zui leaves, larissa leaves, andrea leaves
- **Sat.:**
- **Sun.:** zui comes back, larissa comes back
- **Mon.:**
- **Some day:** Bodhi arrives, Marek leaves, Luna leaves


### Weather forecast

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

Hot and sunny until Wednesday, more rain and clouds towards the end of the week

### Evaluations and check-ins
- Romii Visitor : Days Visited threshold 22/21 (+5%)
- Larissa Member : Absolute Days threshold 185/180 (+3%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Janina Member : 1 days until Absolute Days threshold (180)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe
    * 15:00 Knowledge sharing: -
    * Park cars on even side [Maxime]
- Tuesday
    * Restwaste [chandi]
    * 10:00 Onion gleaning [Thore]
    * 16:00 Romii's evaluation [Larissa]
    * Open Tuesday []
- Wednesday
    * Park cars on odd side [Maxime]
    * 18:00 Landgut Nemt pickup [Antonin]
- Thursday
    * 10:00 Power Hour [Fac.: Antonin, DJ: ?]
    * 15:00 Social Sauna [Tilmann]
    * 18:00 Foodsharing Wurzen meeting [Thore, Janina]
- Friday
    * Organic waste [Antonin]
    * 12:00 Market Pickup <s>[Andrea?]</s>
    * Landgut Nemt pickup [Thore]
- Saturday
- Sunday
    * Landgut Nemt Pickup [Thore]
    * Evening: Campfire in the garden [chandi]
- Next Monday
    * 10:00 CoMe [chandi]
- Next week summary

- Weeks until Roof month: 4.5!

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
- About 9000 Euros for a heatpump heating system including a central warm water system for K20. See https://pad.kanthaus.online/2021-09-09_heatpump
    - no resistance now, but it can still be raised until Wednesday
- [Janina] around 300€ for a door and some additional material to progress the breakthrough plans between elephant room and K22 staircase
    - https://www.obi.de/zimmertueren-zargen/wohnungseingangstuer-cpl-weiss-gl223-86-cm-x-198-5-cm-anschlag-l/p/5917091
    - https://www.obi.de/zimmertueren-zargen/zarge-cpl-weiss-seidenmatt-gl223-86-cm-x-198-5-cm-x-27-cm-anschlag-r/p/9008756
- [Antonin] around 400€ for frames to install roof windows (Eindeckrahmen) for the K20 street and garden sides (coordinated with Zui, Tilmann and Matthias, see https://yunity.slack.com/archives/CN9H631BM/p1630859357018200)

## 4. To do
_Small fixes of things in the house_

### High priority
* [ ] secure wash kitchen sink
* [ ] secure k22-2 bathroom sink

### Medium priority
* [ ] More storage spaces for visitors
* [ ] make k20 garden door easily closable from outside (or acquire a new door) -> apparently old door handle in workshop on left https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000
* [ ] fix loose table leg in Lantern
* [ ] empty dry food storage shelf (find new places for the stuff)
* Ventilation preparation:
    * [ ] Tear down chimney in Snack Kitchen
    * [ ] Tear down chimney in communal sleeping room
    * [ ] Tear down chimney in dining room
    * [ ] Tear down chimney in piano room
* [ ] Kitchen shelf is slanting / leaning over
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] yellow bin in snack kitchen - new solution


## 5. Discussion & Announcements

### Round 1
* [Janina] Collective Agreements change: Votes on 'English as default language' and 'project focus' will start today! In some hours from now I will open the proposal phase and have it run for 7 days, so that next week in CoMe I can remind you again and the week after again. That way I want to make sure that nobody just forgets to participate. :)
* [Larissa] Request from Max: https://yunity.slack.com/archives/G7E8RPFMH/p1631272613000100 anyone up for hosting?
    - chandi will host
* [Antonin] Who would be up for flipping the compost with me?
    - happening spontaneously or next week (chandi, Larissa can be approached spontaneously, Thore more for next week)
* [maxime] the yellow bin in the snack kitchen isn't very practical, would there be resistence against replacing it by a cardboard box, like in the dumpster kitchen
    - to be added to the todos
* [chandi] KIDZ Meeting - Feedback

### Round 2
* [Janina] Participation in flea market in Wurzen on October 3rd? Conny Hanspach specifically asked if we want to contribute. I could imagine doing it, but not alone.
    * [Larissa] maybe?
    * [Maxime] for an hour perhaps?
* [maxime] all paper bins are full, probably because not taken out 2 weeks ago (30/08) (?). As those bins are picked up on Mondays, CoMe is too late to attribute responsibility: could we try attributing Monday bins responsibility during the CoMe of the previous week?
    * [matthias] I am pretty sure they have been picked up. I filled one bin directly with paper standing in K22 entrance area, I guess somebody else filled the rest that was standing there then as well, e.g. two bins have been full shortly after the empty date.
    * [tilmann] there was a big backlog
* [chandi] There is a lot of unwashed food downstairs! :tada:
* [Tilmann] snack kitchen chimney teardown - not necessarily this week, but getting ready for it
    * Shouldn't take more than a day including cleanup
    * We would move essential equipment to the elephant room temporarily (fridge, toaster, minioven, microwave, tea, coffee, bread)
    * Do it when not many people are here, probably before roof month
    * [matthias] I'm in (if needed). This saturday seems empty? :)
    * [maxime] could imagine helping

### Round 3
* [maxime] people who have been sick should probably not cook in the next 24h, please set your unavailabilities accordingly
    * [matthias] I'd suggest not cooking before wednesday even...

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Antonin
- **Tue.:** Thore
- **Wed.:** 
- **Thu.:** 
- **Fri.:** Matthias
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** Tilmann, Janina
- **Tue.:** Janina, luna
- **Wed.:** chandi, romii, luna
- **Thu.:** romii, luna, chandi
- **Fri.:** Larissa, romii
- **Open Tuesday:** Larissa, Janina, chandi, maxime, romii
- **Week:** findus

### End result
- **Mon.:** Luna
- **Tue.:** Thore & romii
- **Wed.:** Maxime & Janina
- **Thu.:** Tilmann & Larissa
- **Fri.:** Matthias & chandi
- **Open Tuesday:** Antonin

## 7. For next week
