---
title: Coordination Meeting
date: "2023-05-22"
taxonomy:
    tag: [come]
---

# CoMe #294
* Date: 2023-05-22
* Facilitator: Martin
* Notary: Janina
* Children caretaker: Kita
* Physical board caretaker: Larissa
* Digital calendar: Antonin
* Reservation sheet purifier: Janina
* Present: Martin, Silvan, Larissa, Maxime, Tilmann, Dorota, Doug, Janina, Cecilia, Antonin

----

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/8e16d033-7d4e-4c93-b6a0-cef5dfcf5480.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 8.7 people/day (-4.0)
- **⚡ Electricity**
    - usage: 20.92 €/week (⬇️-16%)
    - of that for heating: 0.36 €/week
    - of that for hot water: 1.85 €/week
    - paid: -47.81 €/week
    - ☀️ self produced: 78% (⬇️-1%)
    - emissions: 10 kg CO₂ₑ/week
- **💧 Water**
    - paid: 14.27 €/week (⬇️-21%)
    - emissions: 1.1 kg CO₂ₑ/week

### Expenditure

### Other Income

### Things that happened

#### In or around Kanthaus
- very empty over the long weekend
    - Martin and Tilmann biking (seperately)
    - SiLa on vacation
    - Janina hiking
    - bubis at Clara's
    - Antonin at festival in Leisnig
- 3 HZ people stopping by on Sunday
- more moth removal/proofing
- people went to IDAHIT in Wurzen
- big dumpster bike trip inauguration
- some gardening work
- an old paper factory was discovered in a village nearby

#### Wider world
- many wars are continuing
- right wing movement in global politics
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Antonin arrived
* **Tue.:**
* **Wed.:** kito arrives
* **Thu.:** Anneke arrives (or friday morning)
* **Fri.:** kito leaves, silvan leaves
* **Sat.:** Larissa leaves, Antonin leaves, JaTiMiLe leave, Anneke's sisters arrive(?)
* **Sun.:**
* **Mon.:** Larissa comes back
* **Some day:**


### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_sunny and warm, maybe a tiny bit of rain tomorrow_

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Silvan Visitor : Days Visited threshold 24/21 (+14%)
- Martin Volunteer : Days Visited threshold 68/60 (+13%)
- Janina Member : Days Visited threshold 191/180 (+6%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Martin]
    * 11:00 Power Hour trial [Larissa] -> discussion
    * Monday food care [part of PH?]
    * 21:00 knowledge sharing open floor [Maxime]
* Tuesday
    * Black bin [nobody]
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 10:00 Attic session [Janina, you?]
    * 19:00 Punkrock-Tresen @D5
* Thursday
    * no Thursday power hour this week!
    * 10:00 Janina's evaluation [Maxime]
* Friday
    * Biomüll [Martin]
    * 10:00 social sauna [Kito]
    * [Electro festival in Harzgerode](https://yunity.slack.com/archives/CFDNQL9C6/p1684352059556619)
* Saturday
* Sunday
* Next **Tuesday**
    * 10:00 CoMe [Larissa]
* Next week summary

To be scheduled:
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans

## 4. To do
_Newest tasks on top_
* [ ] fix Daria trailer back lights
* [ ] install missing smoke detectors [Martin]
* [ ] give away saved windows in K20 basement (a lot) maybe https://www.trashgalore.de/? (martin)
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] de-clog compost toilet drain
* [ ] modify the task lottery to prioritize open tuesday [Maxime]
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
<!-- check if anyone has a point that didn't speak already -->
* [maxime] I threw away some 15kg of potatoes that were getting very moldy in the K20 basement. It seems that it was building up humidity against the wall. A box of sprouting potatoes is still in the basement: could it be either donated or thrown away?
    * [Janina] I already gave out 2 boxes of potatoes last open tuesday. seems they got way worse since then. let's do something about this fast!
    * food stored in the basement needs to be off the floor and away from the walls, otherwise it builds up humidity and mold. also the door and windows should be closed as much as possible.
*  [Larissa] Should we do "normal" power hour today or on thursday?
    *  temperature check points to today
*  [kito] isn't there social sauna this week? i added it, but someone removed it. if we do it thursday 3pm i could facilitate
    *  i can also facilitate 10 am ;)
    *  great, then it's friday at 10 am and kito faciliates!
*  [Doug] What expenses do we want to announce in CoMe?
    *  Proposal: They should only be about communal money and not things that were paid for by private money.
    *  There was the perspective before that it's also interesting what consumables are generally used, for whatever money. Could be that it came from chandi and he even used that data at some point? But that's not the case anymore. Zui and Larissa use the cash box record to calculate the cost of consumables.
    *  Discussion about how clear the border is between private and common money.
    *  Seems to be a bigger topic, could be moved to PlaMe.
*  [Janina] For a bike trip in the first two weeks of June (5th to ~15th) me, Tilmann and Larissa would like to take the silver ebike and Daria. Concerns? Resistance?
    *  [Maxime] If Nathanael is here it should be fine.
    *  [Antonin] I can make sure it'll be accessible.

### Round 2
* [maxime] I restarted the small fridge in the K20 basement to avoid the bananas-rotting-in-the-staircase pattern we have seen in the last weeks
    * [Janina] Don't refrigerated bananas mainly become brown..? :thinking_face:
    * [maxime] let's see if that's better or worse over a week, than letting them in the staircase
    * Yes, let's do it.
    * Main problem: Forgetting about the fridge.
        * [Larissa] I'll make a sign.
    * Side point: Maxime put newspaper in the fridge to soak up potential liquids.
    * [Doug] In my experience wet paper is much more annoying to clean that liquid.
* [Antonin] there is still about one hour for volunteers and members to [vote about incense use in Kanthaus](https://legacy.ukuvota.world/#/app/f44bd90b-3b7a-ba3d-2564-bea41f82e8ee/collect)
    * [Doug] We needs these buttons that say 'I voted!'. I'll take that on.
* [Doug] Moths! Use jars! Properly close buckets! Bags and used backing paper need to be crumb-free, otherwise I'll just trash them. Moths looove crumbs and grains, let's not give them to them!

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Result
- **Mon.:** Dorota
- **Tue.:** Maxime
- **Wed.:** Larissa
- **Thu.:** Antonin
- **Fri.:** Janina
- **Open Tuesday:** Cecilia
