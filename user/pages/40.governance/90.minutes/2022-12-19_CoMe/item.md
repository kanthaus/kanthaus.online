---
title: Coordination Meeting
date: "2022-12-19"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #274
* Date: 2022-12-19
* Facilitator: Janina
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Kita
* Physical board caretaker: Larissa
* Digital calendar: Tilmann
* Reservation sheet purifier: Anneke
* Present: Tilmann, Maxime, Anneke, Martin, Janina, Silvan, Larissa, William

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/1c736a47-a83c-415e-bea0-644c180d5d40.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **👯 Present:** 13.7 people/day (+2.9)

- **⚡ electricity import:** 767 kWh
- **☀️ additional solar production:** at least 8 kWh
- **🔥 for heating and warm water:** 615 kWh

- **💧 overall water usage:** 3210 liters
- **🫧 warm water usage:** 981 liters

- **🔑 reference:**
    - 1 washing machine at 40°C = ~1 kWh & ~80 liters cold water
    - 1 kWh imported electricity: about 0.30 €

### Expenditure
- [tilmann] 63€ for 50pcs 10x220 and 50pcs 12x260 wall anchors, and another package of drywall screws
- [tilmann] 24€ for long stone drills (10 and 12mm), 10 and 12mm wood drills and a new blade for the circular saw

### Income
- 5 euros from Free Shop [Martin]

### Things that happened
#### In or around Kanthaus
- a gemök group used our space
- the local focus group for sustainability had a meeting in fansipan
- frosty critical mass with a trip to the river
- iceskating on spitzberg lake
- cookies and cake for the 4th advent
- ex-food storage teporarily out of order because of humidity and mold risk
- more and more new ventilation pipes appearing in the baby bathroom
- people watched the fifa world cup finals

#### Wider world
- the first 5 minutes of Tagesschau were about football...
- the EU is reforming the pricing for emissions: it's gonna get expensive! :yay:
- medication is so rare that there now is the official recommendation to share and consider expired meds
- one sports team won a big sports final in the desert by kicking a ball over a painted lawn
- there are still wars in Ukraine and other places


## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** Thore leaves
* **Wed.:** Nathalie and Maxime leave
* **Thu.:** 
* **Fri.:** Larissa and Silvan leave
* **Sat.:** 
* **Sun.:** Anneke and Will leave
* **Mon.:** 
* **Some day:** Doug is probably coming, and also Anja

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins
- Silvan Volunteer : Absolute Days threshold 96/60 (+60%)
- Andrea Volunteer : Absolute Days threshold 82/60 (+37%)
- Martin Volunteer : Absolute Days threshold 72/60 (+20%)

Soon:
- Anneke_Martin Visitor : 6 days until Days Visited threshold (21)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * Paper waste [Martin]
    * 10:00 CoMe [Janina]
    * Monday food care [Will]
    * Park trailer on even/this side [Martin]
* Tuesday
    * Rest waste [Larissa]
    * 15:00-17:00 Open Tuesday []
* Wednesday
    * shortest day of the year
    * day of light? [?]
    * 11:00 Martin's evaluation [Janina]
    * 19:00 Punkrocktresen @D5
    * after dinner: short movie evening [Silvan]
    * Park trailer on odd/that side [Martin]
* Thursday
    * 10:00 Power Hour [Fac.: Anneke, DJ: maybe Janina]
* Friday
    * Organic waste [Will]
    * Cooking for Homeless Christmas Matters [Anneke, Martin, you?]
* Saturday
    * Christmas!
* Sunday
     * Still Christmas!
* January 2nd
    * 10:00 CoMe [Tilmann]
* Next week summary
    * None of the regular structures
    * Many visitors
    * Many community activities? :)

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans


## 4. To do

_Newest tasks on top_
* [ ] Sign: Renew the "Klingel" - Sign (it's less and less readable)
* [ ] Light: elephant room
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] fix bike repair station instructions [Doug]
* [x] treat mold in Ex-food-storage [Martin]
     * room needs a fresh air supply (Zuluft)
     * some more work would be welcome to finish connecting this room to the ventilation system
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

### Round 1
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->
- [janina] Christmas is here! :D
    - what do we want to do on the 24th, 25th and 26th?
    - special food prep, gifts, games, decoration, music?
        - get the tree from the intermediate
        - reverse christmas, with putting stuff in the freeshop? xD
    - what about dumpster diving?
        - maybe a sunday tour as a more communal activity
    - is the week of structurelessness happening next week?
        - free shop pause
        - no come, power hour or scheduled dinner prep
        - communal trip on Thursday [Larissa]
        - year review instead of project updates on Friday [Larissa]
        - martin takes care of the bins
- [William] Homeless Christmas Matters and Will visiting schedule
    - food recovery and help with chopping is appreciated
- [Tilmann] ventilation for office and dining room
    - too little time this week with Maxime leaving on Wednesday

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Janina] might have overcommited a bit with hosting a new workawayer, an old workawayer and a personal friend at the same time. Happy for help in the form of chatting and being inclusive...^^'

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** Janina
* **Wed.:** Zui
* **Thu.:** 
* **Fri.:** Martin, Anneke, William
* **Open Tuesday:** Silvan

### Unavailabilities
* **Mon.:** Larissa
* **Tue.:** Larissa
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Larissa
* **Open Tuesday:** Larissa
* **Week:** Tilmann, Maxime

### Result
* **Mon.:** 
* **Tue.:** Janina
* **Wed.:** Zui
* **Thu.:** Larissa
* **Fri.:** Martin, Anneke, William
* **Open Tuesday:** Silvan

## 7. For next week
- [tilmann] First penny pickup on Jan 3rd - who would like to go with Nathalie?
