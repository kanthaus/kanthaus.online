---
title: Coordination Meeting
date: "2023-09-04"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #309
* Date: 2023-09-04
* Facilitator: Janina
* Notary: Martin
* Physical calendar: Doug
* Digital calendar: Maxime
* Reservation sheets: Larissa
* Weather forecast: Tilmann
* Present: Luise, Klara, Larissa, Doug, ASZ1, Maxime, Dorota, Martin, Lena, Alejandro, Janina, Tilmann, Anneke, Thore

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 
![usage graph last 90 days](https://pad.kanthaus.online/uploads/e93b2761-ecf1-49a1-8824-ea1e7b5e86b3.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.7 people/day (+1.6)
- **⚡ Electricity**
    - usage: 34.37 €/week (⬆️+10%)
    - of that for heating: 0.44 €/week
    - of that for hot water: 2.82 €/week
    - paid: -6.32 €/week 
    - ☀️ self produced: 66% (⬇️-2%)
    - emissions: 16 kg CO₂ₑ/week
- **💧 Water**
    - paid: 23.08 €/week (⬇️-9%)
    - emissions: 1.8 kg CO₂ₑ/week


### Expenditure
- [tilmann] 125€ kanthaus.online renewal for 5 years
- [tilmann] 300€ electric wall sockets and switches
- [tilmann] 15€ for 3 special LED bulbs
- [Anneke] 50€ food stuffs for Sommerfest


### Income
- 100€ donation

### Things that happened
#### In or around Kanthaus
- Kanthaus Sommerfest
- a PlaMe full of topics with successful remote participation
- Cecilia left for her big trip
- the Lantern is usable again
- a lot of visitors!

#### Wider world
- Drama in the Bavarian politics
- Bike demos in Leipzig and Berlin

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

<!-- ### Tekmîl-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Kate will arrive, Miel_Doug
* **Tue.:** kito comes back, 3-4 people of FFJ orga team come for 1 night, Lena (maybe)
* **Wed.:** Luise leaves
* **Thu.:**
* **Fri.:** Nikhil_Doug
* **Sat.:** kito leaves
* **Sun.:** 
* **Mon.:** 
* **Some day:** Klara will leave at some point

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
- warm, sunny, no rain

 
### Evaluations and check-ins
- Anneke Volunteer: Days Visited threshold 106/60 (+77%)
- Lena_masterarbeit Visitor: Days Visited threshold 27/21 (+29%)
- Larissa Member: Days Visited threshold 206/180 (+14%)
- Alejandro_Martin Visitor: Days Visited threshold 22/21 (+5%)
- Maxime Member: Days Visited threshold 187/180 (+4%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Janina]
    * 11:00 - Power Hour
    * ~14:00 save good stuff from a flat near-by [Doug]
* Tuesday
    * 9:30 Lena's evaluation [Janina]
    * 15:00 - 17:00 Open Tuesday [lottery] 
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * ~~18:00 - Punkrocktresen~~
* Thursday
    * 11:00 Annekes evaluation [Larissa]
    * 13:00 - 18:00 MitMachCafé
    * ~~18:30 - FLINTA thai boxing @ D5~~
* Friday
    * Yellow bins [Alejandro]
    * 10:00 Project Updates [Maxime]
* Saturday
    * Attic session [Martin, Ale]
    * 16:00 Metarosa September Sause in Leipzig
* Sunday
    *  
* Next Monday
    * 10:00 CoMe [Anneke]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
- Attic session this week?


## 3. Shopping plans


## 4. To do
_Newest tasks on top_

* [ ] upstairs toilet K-20 flushing mechanism needs fixing []
* [ ] cover the pipes from the rest of the room in the snackitchen (wooden board angle?) [maxime]
* [ ] fix up beer benches and tables [Alejandro, Klara]
* [ ] fix occupancy indicator ex-foodstorage [Martin]
* [ ] Small bin for every toilet
* [x] Fix Dorm window holder [maxime]
* [ ] Fix Dorm window joint [maxime]
* [x] wash everything in the K20 entrance black cupboard [Dorota]
* [x] curtain for Lantern [larissa]
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [Janina] Potatoes and onions! The pile will be available end of the week! We need to announce beforehand that we'll come and I'd like to also make these times available on foodsharing a bit in advance. Who'd be up for digging some onions and potatoes out of a dirt pile end of this week? :)
    - Potential volunteers: Tilmann, Larissa, AZ1, Dorota, Alejandro
- [Anneke] hosting Dima (arrival ~12th Sep, for ~2 weeks, also learning Rust atm)
   * [Antonin] I can't host because I am arriving later but I would be interested in getting to know them and offering activities
   * [Maxime] I can host!
- [Larissa] Please return stuff that you moved from its' original location.
    - [Janina] Small matress next to it is Mika's, needs storage somewhere for now.
    - [Doug] leave notes on stuff! Your name or who to contact for info. Check around a room before leaving because BIG house. 
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Janina] K16 people want more sun in their garden. Therefore they asked us if we can cut the tree in the compost corner. It's about one big branch that might very well be a quarter of the tree. How to proceed?
    - They also asked for the contact of O'Conner-Close for another issue. I guess I'll give it to them?
    - Moved to Slack
- [Larissa] Private jacket sale. Thursday jackets will go into a box for six months storage, and then go to the vortex. Next is shoes. 
    - [Janina] name tags for the jacket rack? 

<!-- check if anyone has a point that didn't speak already -->

### Round 3
- [Janina] [E-bike name ukuvota](https://legacy.ukuvota.world/#/app/1d496f30-2228-c4b9-7b85-4898736ca0d5/collect) is now in voting phase!
- [Dorota] Central paper place for CoMe points? 

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Anneke, Luise
* **Tue.:** ASZ1
* **Wed.:** 
* **Thu.:** 
* **Fri.:**
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** Doug
* **Tue.:** Larissa, Alejandro, Doug
* **Wed.:** Larissa, Alejandro, Doug
* **Thu.:** Janina, Klara
* **Fri.:** Klara
* **Open Tuesday:** Larissa, Maxime, Alejandro, Doug, Dorota
* **Week:** Martin, Tilmann, Larissa, Thore, Lena

### Result
* **Mon.:** Anneke, Luise
* **Tue.:** ASZ1
* **Wed.:** Janina, Kate
* **Thu.:** Maxime, Doug
* **Fri.:** Dorota, Alejandro
* **Open Tuesday:** Klara


## 7. For next week
