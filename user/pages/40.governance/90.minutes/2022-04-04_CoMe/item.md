---
title: Coordination Meeting
date: "2022-04-04"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #238
* Date: 2022-04-04
* Facilitator: chandi
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Nono
* Physical board caretaker: Larissa
* Digital calendar: zui
* Reservation sheet purifier: Doug
* Present: chandi, Larissa, Simon, Doug, Andrea, Zui, Matthias, Tilmann, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf482f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.3 people/day (-1.1)
- **⚡ Electricity**
    - usage: 78.57 €/week (⏫+73%)
    - of that for heating: 58.83 €/week
    - paid: 20.15 €/week 
    - ☀️ self produced: 50% (⬆️+14%)
    - emissions: 31 kg CO₂ₑ/week
- **💧 Water**
    - paid: 15.5 €/week (⬇️-27%)
    - emissions: 1 kg CO₂ₑ/week


### Expenditure
- [chandi] 43€ 8-port PoE switch for the basement
- [janina] 20€ garden table
- [silvan+antonin] some oil

### Income
- 1.52€ 🎉

### Things that happened

#### In or around Kanthaus
- 1st meeting of locacl focus group: sustainability
- a birthday party
- zui came back
- more garden work
- communal trip to the lake with most people even swimming
- attic skillsharing session
- pretty busy freeshop opening time
- we gave away a lot of sprouting potatoes
- got a lot of bananas
- some visitors on the weekend

#### Wider world
- Orban got reelected in Hungary
- big mobility transformation action happened with DB as target
- inflation super high but mainly due to energy
- again/still a lot going on in Ukraine

## 2. This week planning

### People arriving and leaving
* **Mon.:** Anja comes, Silvan comes back, Nono leaves
* **Tue.:** Bodhi comes
* **Wed.:** Anja and Simon_DWE leave
* **Thu.:** 
* **Fri.:** Matthias leaves for 4 days, Larissa + Silvan leave today or tomorrow for ~2 weeks
* **Sat.:** 
* **Sun.:** 
* **Mon.:** 
* **Some day:** 
        

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_very windy, even stormy. bits of rain every day. temperatures between 5°C and 13°C_

### Evaluations and check-ins

- *Zui* _Volunteer_ : _Absolute Days_ threshold 110/60 (+83%)
    - wednesday morning oder thursday at 13:30
- *Bodhi* _Volunteer_ : Absolute Days threshold 195/60
    * [Bodhi] I guess I'm waaay overdue.. happy to do it this week, also i'm not sure if I want to stay volunteer or just be a visitor. Maybe we can figure this out at my evaluation :) not entirely sure yet when I will come. Was Thinking Tuesday or Wednesday. Friday we have the Gemök-Meeting. Maybe just find a time that fits on Wednesday or Thursday and I will be there :D


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [chandi]
    * Afternoon: Hoffest initial meeting
    * Park KMW and trailer on even/this side [matthias]
    * After dinner: Casual money talk
* Tuesday
    * Evening: Book club @Villa Klug [Doug]
* Wednesday
    * 10:00 Monthly meeting [chandi]
    * 11:00 Zui's evaluation [Larissa]
    * 16:00 NDK Spring fest [Janina, Larissa, Mika, you?]
    * Park KMW and trailer on odd/that side [chandi]
* Thursday
    * 10:00 Power Hour [Fac.: Matthias, DJ: ?]
    * Sometime: Bodhi's evaluation [Doug]
* Friday
    * Yellow bin [Doug]
    * 10:00 Attic skillshare & work session [Tilmann]
* Saturday
    * Paper [Doug]
* Sunday
* Next Monday 
    * Rest waste [Doug]
    * 10:00 CoMe [zui]
* Next week summary

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* [Doug] Hoffest initial meeting


## 3. Shopping plans
- [tilmann] 25€ wall opening for fume hood and some pipes https://mkk-shop.de/Mauerdurchfuehrung-Anschlusssystem-Mauerkasten-Abzugshaube-Abluft-Kunstst-Lamelle-weiss-R-125-mm-mit-Rueckstauklappe
- [tilmann] ~600€ wood for attic insulation prep, to be bought just before building weeks


## 4. To do

_Newest tasks on top_
* [ ] make the wall nice in K20-2 bathroom where the boiler has been
* [ ] remove old gas heater from basement to trash corner - needs 3-4 people 
* [ ] repair WiFi in K22-2
* [ ] repair Schaukasten 
* [ ] Make new signs for the hats/gloves/scarves shelf in the hallway (?)
* [ ] ~~Door-closer for Elephant-staircase door~~
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
- [Antonin] A TV crew could come a few days during one of the building weeks (9-15 Mai). I would show them around. Resistance or concerns?
    - not completely clear what it is actually about, but generally no resistance
- [Janina] Host needed for building week time: Linnea_WA was just in Taizé and likes bouldering. Maybe someone for Antonin? ;)
    - she would potentially stay all three weeks if it fits
- [Silvan] After initiative of "Fokusgruppe Nachhaltigkeit" i would like to do a fishbowl about a potential Bufdi for a Freeshop 2.0
    - [Silvan] I am unsure about the time because my week is so incredible full and i would do it when min. Anja/Doug/Janina are present. Maybe spontaneous on Tuesday (14:00?) or after Rumänien.
    - fishbowl seems a bit exaggerated, small group can handle it
- [zui] no need for the KMW on the weekend ;)


### Round 2
- [Doug] (if not already implemented) Would someone be up for updating the stats overview with me? https://yunity.slack.com/archives/CFDNQL9C6/p1648470178304569
    - [zui] if not done until one month in the future I take responsibility
- [zui] Bits & Bäume conference end of September
    - it's basically the topic of kanthaus! :D
    - they are looking for contribution and I'll probably join the orga team
    - we could also use the space and present some of our projects there
    - and general participation could also be a great experience, maybe even as a group thing
    - we could have a casual meeting sometime to think about what we could do there



## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:**
* **Wed.:** 
* **Thu.:** Larissa
* **Fri.:** Zui
* **Open Tuesday:** Anja, Janina

### Unavailabilities
* **Mon.:** doug
* **Tue.:** chandi, doug
* **Wed.:** chandi, doug
* **Thu.:** 
* **Fri.:** chandi, matthias
* **Open Tuesday:** chandi
* **Week:** Tilmann, Andrea, Simon

### Result
- **Mon.:** chandi
- **Tue.:** 
- **Wed.:** Matthias
- **Thu.:** Larissa, Doug
- **Fri.:** Zui
- **Open Tuesday:** anja, Janina


## 7. For next week
* 
