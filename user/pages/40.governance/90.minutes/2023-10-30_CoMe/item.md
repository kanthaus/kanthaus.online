---
title: Coordination Meeting
date: "2023-10-30"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #317
* Date: 2023-10-30
* Facilitator: Martin
* Notary: Kitolino
* Children caretaker: Janina and Larissa
* Physical calendar: Maxime
* Digital calendar: Kito
* Reservation sheets: Loup
* Weather forecast: Martin
* Present: Larissa, Loup, Maxime, Martin, Janina, Kito

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

![usage graph last 90 days](https://pad.kanthaus.online/uploads/6704123f-b878-423d-babf-99aa22e1fab2.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.9 people/day (+0.3)
- **⚡ Electricity**
    - usage: 48.82 €/week (⬇️-2%)
    - of that for heating: 11.66 €/week
    - of that for hot water: 2.89 €/week
    - paid: 33.93 €/week 
    - ☀️ self produced: 52% (⬇️-6%)
    - emissions: 23 kg CO₂ₑ/week
- **💧 Water**
    - paid: 14.17 €/week (⬆️+3%)
    - emissions: 1.1 kg CO₂ₑ/week

### Expenditure
- 5€ salt and quinoa [Martin]
- 25€ for eurotopia book [janina]
- ?€ for 5kg oat and ~3kg rice [Matthias]
- 10 euro for olive oil, garlic etc [Kate]

### Income
- 5€ Spende dose

### Things that happened
#### In or around Kanthaus
- (somewhat contained) COVID breakout! 
- swimming pool experience 
- Advent Calendar is up! Fill it up! 
- apparently the Spitzboden is finished
- 10% of the critical mass in Leipzig were Kanthausians
- people went to the concert in D5 and an open air in Ostend
- Mika's birthday party with sushi and cake and grandparents
- NDK autumn fest and Janina and the kids went
- Doug and Kito were the only visiors for the movie screening in NDK on monday
- Larissa remained a member

#### Wider world
- too much, too bad

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Popcorn of feedback
<!--
Is there any feedback you would like to give to others - the whole groups or individuals? Is their any reflection about yourself you would like to share with the others? If you would like to give feedback to a person in the room, please do it in third person.
-->
- nothing today 

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** kate returns, venla arrives, kito leaves
* **Fri.:** 
* **Sat.:** Katja&Konrad for one night, several day and overnight guests (like Bodhi and Lise, Matthias and Clara)
* **Sun.:** 
* **Mon.:** kito comes back
* **Some day:** AgQFiZo somehow for the weekend

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->
- grey, not so much sun, temperature between 8 and 15 degrees, sporadic rain

 
### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Maxime Member: Days Visited threshold 215/180 (+19%)
- Antonin Volunteer: Days Visited threshold 61/60 (+2%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Martin]
    * 11:00 - Power Hour
* Tuesday
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * 18:00 - Punkrocktresen
* Thursday
    * 13:00 - 18:00 MitMachCafé
    * 16:00 - "Aktuelle Lage" (current situation, talk about news) [kito] - maybe at MitMachCafé
    * 18:30 - FLINTA thai boxing @ D5
* Friday
    * 10:00 - Project Updates [Martin]
* Saturday
    * yellow bin [Loup]
    * 15:00 - Mika's Bday party! 
* Sunday
* Next Monday
    * 10:00 CoMe [Maxime]
    * 18:00 Tischgespräch: Jüdische Lebenswelten @NDK
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
- Lack for Dachboden, ? € [Doug]
    - no resistance
- LYCKSELE (HÅVET), 100 - 250 € [Doug] (space 165 X 200)
    - we would like more info
- Zimt [Maxime]
    - bigger amount in good quality would be nice
    - also paprika powder and garlic powder are interesting
    - there is a store mentioned in #kanthaus-food

## 4. To do
_Newest tasks on top_
* [ ] proof-reading heating page in handbook https://handbook.kanthaus.online/technical/heating.html [kate]
* [ ] fix/improve K20-1 staircase night light (can ask Tilmann for details about wiring)
* [ ] put kanthaus logo on the new beer benches and tables
* [ ] curtains for the dragon room that prevent passersby from staring inside
* [ ] cut branch from tree in the compost corner (after leaves fall)
* [ ] cover the pipes from the rest of the room in the snack kitchen (wooden board angle?) []
    * [tilmann] maybe directly go for drywall as final measure?
      * [maxime] that would be the right but more difficult remediation indeed. I looked at options to hack a wooden corner, but didn't find satisfying options in the wood shed, so I'll leave there.
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights [Dorota]
     * [Martin] Cables got pinched, I suggest redoing the whole thing. Also, use a diode to prevent over voltage. 
* [ ] replace shower curtain by (plexi)glass construction

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [Martin] Spitzboden DONE! Do we want to establish some rules for the use of the k20-4 or just see what happens?
    - should not be filled with stuff before cleaning if it's not needed
    - general "stuff storage" guideline would be nice, there was a proposal from Doug in PlaMe#1 we could get back to
        - but not making it too big so it's never tackled 
    - Martin writes a proposal (just for the Spitzboden) and posts it on Slack
- [Doug] Are the `Kanthaus-CanonC2025_...` virtual printers at all usable, or can we remove them?
    - Janina thinks it's a leftover from old times (meant to make printing easier, but than settings changed) and not needed anymore
    - Maxime thinks he uses it
    - let's also make a Slack discussion out of this
- [Larissa] The Haus- und Wagenrat e.V. would like to have the rights to one of our pictures (https://kanthaus.online/pics/kanthausFolk1.jpg) to use for a flyer for one of their projects and would even pay us something for it, if we want to. any thought, resistance? here is the mail: https://yunity.slack.com/files/USLACKBOT/F061FH96WMV/re__frage
    - they already used the picture in the brochure about different forms of association
    - instead of paying money they also offered free counseling to us
    - proposal to license the picture CreativeCommons ([CC-BY](https://creativecommons.org/licenses/by/4.0/)) and just ask for Kanthaus to be credited, maybe in the form "Kanthaus (kanthaus.online) - CC-BY"
    - there was a discussion about using pictures in the past
    - general support in the round, only using money if it makes sense for them (e.g. because of a funding)
    - Larissa will do a Slack point
- [kito, Kate] ToI! 
    - schedule and preparation tasks for you
        - first stage soon Nov 7! Whole morning together. See invite on Slack main channel with prep task. 
    - who's invited, who will be invited? visitors? feelings? -> final decision in TOI1
        - could be overwhelming for new visitors and also slow the process down too much, too much explanation needed
        - "alumnis" (like Nathalie, Thore, Matthias) participating could be something different, but probably they are not really interested
        - evaluation for Klara before TOI2 might be nice, also some concerns if it works out
    - it will be great!
- [kate] verschenkeladen name. i have called it Verschenkeladen Wurzen on the green friday poster. i hoped over time to add it to google maps and create a fb page. does it have an official name already? i felt it made sense to have it's location in the name, or? happy to hear your thoughts when I return.
    - [Antonin] maybe start an Ukuvota to let people propose and vote for a name?
    - [Janina] Verschenkeladen was its official name up until now. Could be changed if you see the need.
    - Slack and Ukuvota is a good idea
- [Janina] Mika's party
    - no clear plan, but probably caketime in the dining room first (or outside if the weather is unexpectedly great), then playtime in piano and/or yoga room.
    - there will be people everywhere on Saturday afternoon, hope that it's okay...
    - I'll tell people about the covid situation which hopefully resolves itself until then anyway
    - ahmed, dima, franzi, unkraut, phillip from kita and maybe more peeps :bird: wanted to come
<!-- check if anyone has a point that didn't speak already -->

### Round 2 
- [Martin] Compost. let's switch more compost for better compost. Because we have more than what we need, and it is not great. First step, less and better food waste to compost=>more waste to Biomüll. I propose to add Biomüll to the snack kitchen, equally reachable as compost. Then redefine what goes where. Then it will  be much easier to balance it. I can feel resposible to materialize it.
    - general support
    - move further discussion to Slack or next CoMe
- [kito] CryptoKüfa (https://nuudel.digitalcourage.de/sWMMbM4Rj9vRxHxV)

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Result
* **Mon.:** Larissa
* **Tue.:** Maxime
* **Wed.:** Martin
* **Thu.:** Loup
* **Fri.:** Kate
* **Open Tuesday:** closed!
