---
title: Coordination Meeting
date: "2023-05-01"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #291
* Date: 2023-05-01
* Facilitator: Tilmann
* Notary: Kito
* Children caretaker: Silvan
* Physical board caretaker: Janina
* Digital calendar: Antonin
* Reservation sheet purifier: Andrea
* Present: Tilmann, Gaia, Andrea, Antonin, Janina, Larissa, Kito, Martin, Maxime

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/6b6bda4d-c8fe-49af-aab0-db0bbfc27882.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.6 people/day (+1.4)
- **⚡ Electricity**
    - usage: 31.29 €/week (⬇️-11%)
    - of that for heating: 7.4 €/week
    - of that for hot water: 2.73 €/week
    - paid: -19.76 €/week 
    - ☀️ self produced: 83% (⬆️+1%)
    - emissions: 15 kg CO₂ₑ/week
- **💧 Water**
    - paid: 18.95 €/week (⏫+75%)
    - emissions: 1.5 kg CO₂ₑ/week

### Expenditure
- 18€ for oil and spreads

### Other Income
- 10€ donation


### Things that happened
#### In or around Kanthaus
- Anneke presented stories out of her life
- Baby bathroom got a complete ceiling again, plus some ventilation pipes that might eventually lead to the Piano Room
- a lot of dumpster diving
- FGNW met in Kanthaus
- got rid of sprouting potatoes, got new, better ones
- Tilmann stays member
- quite some people wanted to come and didn't
- new playground was checked out
- a new Döner opened in Wenceslaigasse, in the Stadtcafé!!
- Antonin shifted compost and mowed the lawn
- Gaia sorted stuff in the freeshop and made a lot of quiche and cooked even more and cleaned - wow, impressive!

#### Wider world
- 49€-Ticket starting today!
- today is first of may, so some demonstrations happen
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Kiki and Leo arrive, Andrea leaves, maybe a friend of Antonin visits
* **Tue.:** Anja comes by
* **Wed.:** Kiki and Leo leave, Shehzad_WA maybe arrives, kito leaves in the evening
* **Thu.:** 
* **Fri.:** 
* **Sat.:** Katja and Konrad come for one night, FlowMis work day
* **Sun.:**
* **Mon.:** 
* **Some day:** 


### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
sunny and moderately warm, some rain on Tuesday

### Evaluations and check-ins
- Antonin?
- Janina Member : 6 days until Days Visited threshold (180)
- Martin Volunteer : 7 days until Days Visited threshold (60)
- Andrea Volunteer : 22 days until Days Visited threshold (60)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Tilmann]
    * Monday food care [Antonin + Gaia + you?]
* Tuesday
    * 11:00 Antonin's evaluation [Maxime]
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 10:00 attic work session
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Larissa]
* Friday
    * 10:00 PlaMe [Janina]
    * Yellow Bin [Martin]
* Saturday
    * 10:00 attic work session
* Sunday
    * 10:15 NDK-bike trip: Gedenkfahrt für die Opfer der Todesmärsche 1945 starting in Borsdorf
    * 15:00 HKW general meeting @ Piano Loom [Larissa]
* Next Monday
    * 10:00 CoMe [Maxime]
    * Paper Bin [Martin]
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [attic team] up to 339€ for 4 roof window blinds for K20 attic - it'll be ~40% less if we manage to get used ones
- [tilmann] up to 36€ for Makita 18V charger - will try to get it used
- [kito] food order at Bananeira ~600€


## 4. To do
_Newest tasks on top_
* [ ] move the beams in the garden back to the K18
* [ ] fix the extension of the private storage with a new shelf to the wall next to the main bathroom
* [x] flip the compost [Antonin]
* [ ] give away saved windows in K20 basement (a lot) maybe https://www.trashgalore.de/? (martin)
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] treat mold in Freeshop Lounge and Hipster Room
* [ ] de-clog compost toilet drain
* [ ] modify the task lottery to prioritize open tuesday []
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
- [Antonin] we are looking for people to join the board of Haus Kante Wurzen. Larissa and Doug can imagine staying on the board but we need at least three board members.
- [Larissa] moving powerhour to monday-trialphase
    - 2-3 weeks of try out with different versions 
        - a) normal PH, but on monday, 11 o'clock
        - b) just task distribution after CoMe, everyone does it whenever they want during the week
        - c) your idea? :)
- [Janina] it's getting warm outside! protect the foooood! 
    - e.g. sun protection on the windows
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Antonin] what to do with our compost soil: there are many penny bags available. In our garden? In the kleingarten? Somewhere else?
    - Anja is coming on tuesday, talk to her about garden stuff
        - move beams before that!
-  [Janina] if we lack food, sign up for foodsharing pick ups!


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Martin
* **Tue.:** Kiki+Leo
* **Wed.:** kito
* **Thu.:** Larissa, Janina
* **Fri.:** Maxime, Antonin
* **Open Tuesday:** Anja, Gaia

### Unavailabilities
* **Mon.:** 
* **Tue.:** Antonin
* **Wed.:** 
* **Thu.:**
* **Fri.:** 
* **Open Tuesday:** Maxime
* **Week:** Tilmann, Andrea

### Result
- **Mon.:** Martin
- **Tue.:** kiki, leo
- **Wed.:** Kito
- **Thu.:** Larissa, Janina
- **Fri.:** Maxime, Antonin
- **Open Tuesday:** anja, Gaia 

## 7. For next week

