---
title: Coordination Meeting
date: "2023-06-19"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #298
* Date: 2023-06-19
* Facilitator: Anneke
* Notary: Kito
* Children caretaker: Kita and Hospital and Janina 
* Physical board caretaker: Cecilia
* Digital calendar: Antonin
* Reservation sheet purifier: Akos 
* Present: Cecilia, Akos, Antonin, Tilmann, Anneke, Martin, Kito, Dorota, Maxime


## 0. Check-in round

## 1.  Last week review

![usage graph last 90 days](https://pad.kanthaus.online/uploads/e2a17ca7-861b-437f-bb65-9f5eba8b3358.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 15.7 people/day (+8.0)
- **⚡ Electricity**
    - usage: 23.02 €/week (⬆️+33%)
    - of that for heating/standby: 0.5 €/week
    - of that for hot water: 2.1 €/week
    - paid: -49.48 €/week 
    - ☀️ self produced: 81% (⬆️+10%)
    - emissions: ~11 kg CO₂ₑ/week
- **💧 Water**
    - paid: 25.73 €/week (⏫+81%) - that's 5.72m³!!
    - emissions: ~2.0 kg CO₂ₑ/week

### Expenditure
* 13€ Oats, Flour, passata (Anneke)

### Other Income
* 30.28€ Pfand
* some money in the box

### Things that happened

#### In or around Kanthaus
* lots of people in Kanthaus during the weekend - Flomis, Blindspotis and more
* nice trip to Spitzenfabrik with concert and bike shopping
* people went to Leipzig, had a nice time, but there was no last train, luckily the partypeople were rescued by the OstPost
* cherries are ready to eat!

#### Wider world
* Berlusconi died

<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Possibly Doug, Adam and Eleanor+ tomorrow, Lilli [Anneke] arrives, Silvan leaves
* **Tue.:** Kate returns, Lilli leaves, Bjarne [Anneke] arrives, Maxime leaves
* **Wed.:** Larissa comes back
* **Thu.:** Bjarne leaves, Vroni arrives
* **Fri.:** Klara [Anneke] arrives
* **Sat.:** 
* **Sun.:** Martin, Antonin and Vroni leave
* **Mon.:** Klara leaves (or sunday)
* **Some day:** Janina and Levi come back hopefully


### Weather forecast
- bit rainy week, should be sunny again at the weekend
- more than 30 degrees

<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->


### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Dorota_Stilgar Visitor : Days Visited threshold 35/21 (+67%)
- Cecilia_WA Visitor : Days Visited threshold 33/21 (+57%)
- Martin Volunteer : Days Visited threshold 94/60 (+57%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Anneke]
    * 11:00 PowerHour [Anneke]
    * Monday food care [PH]
* Tuesday
    * 10:00 Cecilia's evaluation [kito]
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 10:00 - 12:00 IT maintainance co-working and knowledge sharing session [Antonin]
    * 16:30 - 17:30 visit by people from "Netzwerktreffen der Dezentrale für gemeinschaftliches Wohnen in Sachsen" [Antonin]
    * 19:00 Punkrock-Tresen @D5
* Thursday
    * 10:00 Dorota's evaluation [Antonin]
    * 18:30 Finta* thai boxing @d5
* Friday
    * organic waste [Dorota]
    * 10:00 Social Sauna 
    * 15:00 birthday cake
* Saturday
    * 13:00 NDK Summer Bash - join the football team! [kito] 
* Sunday
* Next Monday
    * CoMe [kito]
* Next week summary

To be scheduled:
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* maybe oats

## 4. To do
_Newest tasks on top_
* [ ] fix K20-2 bathroom door indicator [Dorota]
     - there is a analog sign that could be used in the meantime
* [ ] fill holes in Snack kitchen floor where moths ~~might~~ breed [Martin, Akos]
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I would redo the whole thing (but not now) 
* [ ] give away saved windows in K20 basement (a lot) 
     * maybe https://www.trashgalore.de/ ? [Martin]
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] de-clog compost toilet drain
* [x] modify the task lottery to prioritize open tuesday [Maxime]
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
* [Martin] multiple visitor requests.
  * 11.07-14.07: Feuerqualle (5 people). Request the use of the Cloud and Dragon rooms
    * [Antonin] can host if no one else is available. Because most of them know the house well, I might only be around when they arrive, if that's okay with people? 
        * [Martin, kito, Anneke] fine by me!
  * 13.07-14.07: Mediation group from Vroni (~5 people)
    * Anneke is hosting
  * 23.07-04.08: Olli (1 person)
    * [Martin] I could do it. But I don't remember this person! Does anyone?
    * [Antonin] it was a spontaneous visit some weeks ago. Easy going :)
  * 28.07-30.07: SoLaWi learning group from Anja (10-15 people)
    * [Martin] I'm confused, sounds like Anja could host?
    * [Anja] yeah I could, but I'm just not sure how people would like that since I'm part of the group. Maybe it's ok or maybe a second person wants to co-host?
    * [kito] I can co-host :)
* [Antonin] Can we have a K18 room dedicated to things to be given away? Ideally on the ground floor. I want to post many things on Kleinanzeigen and make it easy for people in the house to just redirect people who ring the bell to that room, where they can take anything.
    * could be the first room on the right side, wood can be stacked
    * let's do it
* [kito] Feedback/Critique round try-out next CoMe
<!-- check if anyone has a point that didn't speak already -->

### Round 2



### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Result
- **Mon.:** Cecilia
- **Tue.:** Akos
- **Wed.:** kate
- **Thu.:** Kito
- **Fri.:** Dorota, Anneke
- **Open Tuesday:** Antonin

- Other: 
 
## 8. For next week
