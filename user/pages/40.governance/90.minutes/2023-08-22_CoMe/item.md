---
title: Coordination Meeting
date: "2023-08-22"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #307
* Date: 2023-08-22
* Facilitator: Doug
* Notary: Tin
* Physical calendar: Dorota
* Digital calendar: Tilmann
* Reservation sheets: Cecilia
* Weather forecast: Maxime
* Present: Tilmann, Dorota, Cecilia, Alejandro, AZ1, Maxime, Doug, Janina, Martin, Larissa, Lena

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

![usage graph last 90 days](https://pad.kanthaus.online/uploads/54c21cce-62b8-453a-8b68-67d4a0da087b.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 8.6 people/day (-9.0)
- **⚡ Electricity**
    - usage: 30.32 €/week (⬇️-29%)
    - of that for heating: 0.39 €/week
    - of that for hot water: 3.46 €/week
    - paid: -26.3 €/week 
    - ☀️ self produced: 62% (⬇️-13%)
    - emissions: 14 kg CO₂ₑ/week
- **💧 Water**
    - paid: 25.54 €/week (⬇️-4%)
    - emissions: 2.0 kg CO₂ₑ/week

### Expenditure
- [tilmann/martin] 95€ for 100m electric cable (PVC-free) and two light switches
- [maxime] 26.80€ for van rental + 11€ for fuel

### Income
* 47€ Pfand
* 51.20€ donation

### Things that happened
#### In or around Kanthaus
- day trippers in Dresden
- Klein gartens maintenance
- trip to the lake
- lots saved at Highfield
- 10+ electric extension cords fixed (now in K22-B event storage)

#### Wider world
- cannabis decriminalization planned for Jan 01
- half of Canada burning
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

<!-- ### Tekmîl & building week review -->

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** Larissa leaves
* **Wed.:** larissa comes back
* **Thu.:** AZ1 bro arrives
* **Fri.:** Martin leaves, Lena's mom visits, Vroni's group stays for a night
* **Sat.:** AZ1 bro leaves. 
* **Sun.:** Martin return, Lena and mom leave
* **Mon.:** 
* **Some day:** Anneke comes back (wednesday/thursday)

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)

Sun, a lot of it, but not too warm. Might be a storm Thursday. 

### Evaluations and check-ins
- Anneke Volunteer: Days Visited threshold 95/60 (+58%)
- Doug Volunteer: Days Visited threshold 64/60 (+7%)
- Larissa Member: Days Visited threshold 192/180 (+7%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * Operation Highfield 
* Tuesday
    * 10:00 - CoMe [Doug]
    * 11:00 - Power Hour
    * 15:00 - 17:00 Open Tuesday [lottery] 
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * 18:00 - Punkrocktresen
* Thursday
    * 10:00 - Doug eval. [Janina]
    * 13:00 - 18:00 MitMachCafé (Doug goes this day)
    * 18:30 - FLINTA thai boxing @ D5
* Friday
    * Yellow bins [Dorota]
    * 10:00 - Social Sauna [Doug]
    * 17:00 - FLINTA tech meet up
    * 18:00 - Critical Mass Leipzig (Augustus platz)
* Saturday
    * 15:00 - 00:00 KoLa Hoffest 
* Sunday
    *  
* Next Monday
    * 10:00 CoMe [Larissa]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
- [doug] anti-clamp, 25€ https://eustore.ifixit.com/products/anti-clamp
    - no resistance, approved
- [Anneke relayed by Lena] White paint for Lantern
  - [tilmann] worth checking what kind of paint we bought before and how good they have been
  - otherwise no resistance, approved
- [Maxime] earplugs APPROVED
    - no resistance

## 4. To do
_Newest tasks on top_
* [x] remove weeds in front of the houses [Cecilia]
* [x] Fix Cloud Room door indicator [Martin]
* [ ] Fix Dorm window holder [Maxime]
* [ ] Fix the black cupboard for hats, gloves and scarves in K20 entrance which is slowly falling apart []
* [ ] wash everything in the K20 entrance black cupboard [Dorota]
* [ ] curtain for the Cave [Anneke] 
* [ ] curtain for lantern [Lena]
* [ ] make K20-2 bathroom privacy indicator realiable
    * [Tilmann] what kind of problems did you encounter? I tried to simplify the logic to make it more reliable already, but I wonder if the motion sensor could be flaky. 
    * [Martin] I know Dorota has been working on it. 
    * [Dorota] If you first press the private button and then close the door, it turns public again
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [doug] poll for social sauna time https://crab.fit/social-sauna-148339
- [Martin] Gartenfest prep/to do list? I propose we start doing some stuff to not have to run around last minute. Also we might be able to give extra touches. 
    - sweep trash corners
    - weed the walkways
    - bricks 
    - ADVERTISE THE EVENT! 
- [tilmann] highfield follow-up
  - sort away toilet paper
  - sort away trash bags in wash room
  - sort away bbq stuff in K18
  - sort cans in basement [Larissa]
  - move unwanted drinks to open tuesday or K18 Kleinanzeigen room
  - moth-proofing food in K20-1
- [maxime] Diesel apparently produces some 2.6kg of CO₂ per litre burnt. The trip to Highfield required burning 13L, so around 34kg CO₂. I would like to take some of the charcoal collected and bury it somewhere to compensate for those emissions.
    - [Doug] believe it would be 8.5 kg charcoal for parity (assuming 100% carbon)
      - 2.740kg CO₂/dry charcoal kg burnt (not including charcoal making) [source](https://energypedia.info/images/4/4a/EN-Charcoal%2C_carbon_emissions_and_international_onventions%3Bprotocols-Almeida_A._Sitoe.pdf) => that would be 12.4kg charcoal to match the 34kg CO₂
    - No resistance to the symbolic act, as long as there's enough charcoal for the next year ;p
- [kito] is someone interested to do the Penny pickup thursday 6:30pm? i took the slot, but i would be happy if someone else does it. there is a second person coming, so we can get only half the stuff probably ;)
    - [Doug] thinking about it
    - [Maxime] Could be backup
- [Dorota] can stuff be saved from the feminist festival?
  - [Larissa] probably not

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [doug] menstruation friendliness: bin in every toilet? Tampons/pads in every toilet? Stock a couple different sizes of menstrual cup in house for people to personalize?
  - [tilmann] would be good to distribute the tampons/pads that we have in the bathroom
- [tilmann] garden fuse was off yesterday noon, any reason why that could have happened?
  - it also cut off the freezer because they are connected to the same RCD
- [maxime] Let's make a solar-powered electric bbq this evening!
  - no resistance (but we do expect electric resistance)

<!-- check if anyone has a point that didn't speak already -->

### Round 3
- [tilmann] K18 right room at the back - it's getting quite confusing with stuff for Harzgerode, Silvan, Dorota, Lantern and some stuff that looks like it has been sorted out of free shop. Please label clearly, otherwise stuff might end up elsewhere during clean-up actions! 
  - agreed

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Highfield
* **Tue.:** Maxime
* **Wed.:** Janina
* **Thu.:** Alejandro
* **Fri.:** AZ1
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** Doug, Cecilia
* **Thu.:** Cecilia
* **Fri.:** Dorota, Cecilia
* **Open Tuesday:** 
* **Week:** Martin, Larissa, Tilmann

### Result
* **Mon.:** Highfield
* **Tue.:** Maxime
* **Wed.:** Janina
* **Thu.:** Alejandro, Doug
* **Fri.:** AZ1, Dorota
* **Open Tuesday:** Lena


## 7. For next week
* 
