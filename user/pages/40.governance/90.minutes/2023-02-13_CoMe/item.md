---
title: Coordination Meeting
date: "2023-02-13"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #281
* Date: 2023-02-13
* Facilitator: Maxime
* Notary: Kito
* Children caretaker: Kita
* Physical board caretaker: Martin
* Digital calendar: Zui
* Reservation sheet purifier: Janina
* Present: Maxime, Zui, Martin, Veronica, Janina, Larissa, Anneke, Timea, Clara, Tilmann

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/8a3d846d-25a6-4cb5-bbbb-1aae01da72fe.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 14.7 people/day (+5.3)
- **⚡ Electricity**
    - usage: 257.99 €/week (⬆️+52%)
    - of that for heating: 190.82 €/week
    - paid: 195.39 €/week 
    - ☀️ self produced: 23% (⬇️-4%)
    - emissions: 98 kg CO₂ₑ/week
- **💧 Water**
    - paid: 0.0 €/week (0%)
    - emissions: 0.0 kg CO₂ₑ/week

### Expenditure
- ~10 € for 6 spreads and a package of toilet paper

### Income
- 

### Things that happened

#### In or around Kanthaus
- the Flowmis had a meeting here over the weekend
- a very nice night out in Leipzig with ~10 people from Kanthaus
- a quite uninspired PlaMe
- Nathalie was reaccepted as visitor
- some people attended the anarchist brunch in Leipzig
- smelly snack kitchen :(
- dining room got reinaugurated
- K20-staircase to the attic was opened
- we started learning about the life of Alexander Hamilton

#### Wider world
- big earthquake in Turkey
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->


## 2. This week planning

### People arriving and leaving
* **Mon.:** Kai left
* **Tue.:** 
* **Wed.:** Anneke leaves (or thursday), zui leaves
* **Thu.:** 
* **Fri.:** JaTiMiLe and Larissa leave, Vroni might come for 2 nights
* **Sat.:** 
* **Sun.:** a lot of people leave
* **Mon.:**
* **Some day:** Timea and Clara leave towards the end of the week

### Weather forecast
- coldest tomorrow with max 5 degrees
- goes up to 11 degrees on friday
- not to much rain but cloud emojis
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->


### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Martin Volunteer : Absolute Days threshold 128/60 (+113%)
- Anneke_Martin Visitor : Days Visited threshold 40/21 (+90%)
- Silvan Visitor : Days Visited threshold 34/21 (+62%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Maxime]
	* Monday food care [Timea, Veronica]
	* 11:00 OSB lift action [Tilmann, Martin, Maxime, you?]
	* 11:15 pillow pick-up action [Antonin and you] (see discussion section)
	* finance meeting [LaZu(Cha)]
	* After dinner: Clara's birthday cake
* Tuesday
    * Restmüll [Maxime]
    * 10:00 - 13:00 Yet another attic session [Tilmann, you?]
    * finance meeting [LaZu(Cha)]
    * 15:00 - 17:00 Open Tuesday [Martin, Veronica]
    * 17:30 FTT Orga meeting [Janina+2]
* Wednesday
    * Park trailer on other side [maxime]
    * 10:00 - 13:00 The last attic session before ToI [Tilmann, you?]
    * 10:00 Anneke's evaluation [kito]
    * 12:00 Sociocracy for Kanthaus input [Nathalie]
    * 15:00 Shower party @bathroom
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Timea]
* Friday
    * Biotonne [Martin]
    * 19:00 Küfa with anarchist input @ ADI
* Saturday
    * 15:00 Critical Mass [we are hoping for the best]
* Sunday
* Next Monday
    * ToI starts!
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [Antonin] rent a van to do a trash tour, ~100€ including fuel (second attempt after [previous discussion last year](https://kanthaus.online/en/governance/minutes/2022-04-11_come))
   * the date would be fixed quite some time in advance so that it matches both the opening times of places that take our trash and the availability of not-so-expensive vans in Borsdorf
   * I would pick up the van and be responsible for driving it and returning it in a good state
   * I would need help of:
      * a few people on the day to carry stuff
      * someone who has an overview of the places where to go and what sort of trash we can take there.
   * if needed we could consider renting the van for two days, so that we can go to more places. Combining this with other pick ups is also imaginable if it can be planned appropriately.
   * [tilmann] metal waste to trebsen?
       * [Antonin] sounds like a good start for an overview of the places ;)
    * no resistance, but support!
* [Larissa] no wheat flour left, so some will be bought

## 4. To do
_Newest tasks on top_
* [ ] exchange the strings of light-brown guitar
* [ ] repair schaukasten - window is pressed in
    * [ ] first step: take it down and bring it inside
* [ ] level the compost [kito]
* [ ] bring building rubble to the dump
* [ ] oat milk ~~inspection~~ trashing
* [ ] deep-clean shower tap to make it easier to adjust temperature
* [ ] Potato sorting action
* [ ] General trash tour [Antonin]
* [ ] Sign: Renew the "Klingel" - Sign (it's less and less readable)
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: elephant room
* [ ] Light: Yoga room 
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
* [Antonin] /!\ urgent support request /!\ I offered to pick up 15 pillows from the Premier Inn Leipzig City Hotel (Goethestraße 9) and they have reserved them for me this morning, but I realized later that Nathanael is in Kanthaus and not in our basement as I thought. Could someone be so kind to take the 11:31 train to Leipzig with 3 or 4 Ikea bags (which can be found in the dumpster kitchen) and meet me in front of the hotel?
    * [kito] i will do it
    * [Antonin] thank you!!
* [Janina] ToI orga
    * travel
        * small coordination outside of CoMe (Zui, Martin, Maxime, Kito - Anneke might join at some point)
    * things to take (both ways)
        * food -> hz (but it doesn't make sense to go dumpsterdiving here and bring it there), maybe ask bakery in Sangerhausen
        * oat milk and coffee -> kh
        * your personal necessities
* [kito] morning meditation buddies?
    * at 9 am in the cloud room for the next days
* [zui] Flowmi request of the trailer, and project storage space
    * trailer: there was the feeling of general support in PlaMe, but no decision - maybe slack post is best?
    * but there are more requests, zui has an idea for the process
* [tilmann] heating next week
    * rough plan: let the house cool down and only put it back on 1-2 days before we come back (or when it's freezing)
* [Anneke] kids room of Ilona's relative will be emptied, it's all in pink and white. do we want it?
    * best would be to have a picture in the free shop so everyone can see it
* [Larissa] general assembly of the house owning association will happen may 7th
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Antonin] the [third round of proposals](https://ukuvota.world/#/app/e77a3b27-2b43-1e79-d8d8-46d8d1f36e40/collect) for the constitution change process about the position system is open.
* [Janina] Visitor request for this week: Palû
    * Martin is consering, but rather after ToI

### Round 3
* [Janina] let's have a shower party this week!
    * the idea is to make the bathroom really warm and then take turns making use of that heat
    * preferably when it's not super cold and when we have some sun
    * the kids would take a bath, the parents would shower, maybe you want, too? :)
    * [tilmann] Wednesday afternoon?
    * let's make it happen :)

### Round 4
* [Janina] kanthaus sommerfest 2023: August 26?
    * carry on, carry on


### Spontaneous points

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Anneke
* **Tue.:** 
* **Wed.:** Janina
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Martin, Veronica

### Unavailabilities
* **Mon.:** Maxime, Timea, Clara
* **Tue.:** kito, Maxime
* **Wed.:** kito
* **Thu.:** Maxime
* **Fri.:** kito, Timea, Clara
* **Open Tuesday:** Maxime
* **Week:** larissa, zui, Tilmann

### Result
- **Mon.:** Anneke
- **Tue.:** Timea
- **Wed.:** Janina
- **Thu.:** Clara, kito
- **Fri.:** Maxime
- **Open Tuesday:** Martin, Veronica

## 7. For next week
* 
