---
title: Coordination Meeting
date: "2022-03-14"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #235
* Date: 2022-03-14
* Facilitator: Janina
* Notary: Antonin
* Mika caretaker: Kita
* Levi caretaker: Tilmann
* Physical board caretaker: Larissa
* Digital calendar: Maxime
* Reservation sheet purifier: Doug
* Present: Janina, Matthias, chandi, Doug, Antonin, Maxime, Larissa, Ask

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf482b.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.7 people/day (-2.0)
- **⚡ Electricity**
    - usage: 84.48 €/week (⬇️-33%)
    - of that for heating: 77.45 €/week
    - paid: 47.95 €/week
    - ☀️ self produced: 43% (⬆️+3%)
    - emissions: 36 kg CO₂ₑ/week
- **💧 Water**
    - paid: 18.07 €/week (⬇️-12%)
    - emissions: 1 kg CO₂ₑ/week

### Expenditure
* none

### Income
* none

### Things that happened
#### In or around Kanthaus
* we have a solizimmer agreement now
* We made through Winter: Spring is coming!
* LAN party
* There is less crap in the garden
* The slackline is up
* washing machine problems, partly fixed alread
* anti-ad for tetra paks

#### Wider world
* [RAMMSTEIN published a new song!](https://www.youtube.com/watch?v=EbHGS_bVkXY)  .... lol
* the paralympics ended
* covid numbers are at an alltime high and rising but for some reason people are not freaking out anymore
* petrol prices are going crazy, but lpg is not
* and of course ukraine...

## 2. This week planning

### People arriving and leaving
* **Mon.:** Zui left, Ask arrived, Lea arrives
* **Tue.:**
* **Wed.:**
* **Thu.:**
* **Fri.:** Anja might leave
* **Sat.:**
* **Sun.:** Lea leave, Prairie arrives, kito arrives, chandi leaves
* **Mon.:** Andrea arrives
* **Some day:** Matthias might leave (50%) towards the end of the week


### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_no more night freeze, rain tonight otherwise a lot of sun_

### Evaluations and check-ins
- Maxime Member : Absolute Days threshold 185/180 (+3%)

Soon:
- Larissa Member : 6 days until Absolute Days threshold (180)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Janina]
    * 13:30 Longterm decisions process @Lantern [Janina, chandi, Kito]
    * 14:00 washing machine repair session [Antonin, Matthias, Doug]
    * 17:00 Wikidata Data Reuse Days 2022 - Opening session @cloud room [maxime]
    * 17:00 foodsharing Wurzen co-working @office [Nathalie, Janina]
    * 18:00 Wikidata/OpenFoodFacts @Piano room [maxime]
    * 18:00 birthday bonfire in the garden [Larissa, Antonin]
    * Park Trailer on even/this side [Antonin]
* Tuesday
    * Rest waste [Maxime]
* Wednesday
    * Park KMW & Trailer on odd/that side [Matthias]
    * 15:00 Kaffeekränzchen @ALM [Janina?, Silvan?]
* Thursday
    * 10:00 Power Hour [Fac.: chandi, DJ: ?]
    * 18:00 Online commons workshop [Matthias]
* Friday
    * Organic waste [Antonin]
    * 11:00 Maxime's evaluation [chandi]
* Saturday
    * 13:00 Critical Mass @Marktplatz
* Sunday
    *
* Next Monday
    * 10:00 CoMe [Maxime]
* Next week summary
    *

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* [Anja] Umtopf-Aktion

## 3. Shopping plans
* a new pan?
    * or putting the holy pan to unrestricted use
        * matthias talks to bodhi and thinks about a label with instructions
    * unclear who proposed this shopping plan
    * instead, interest in coming up with better communication around protection of pans

## 4. To do
_Newest tasks on top_
* [ ] ~~mount sink in washroom~~ (**Emoji-poll result**)
* [ ] ~~analog week plan needs a repair~~ (Use until it breaks?)
* [x] repair hat/gloves/scarves cupboard in hallway of K20
* [ ] ~~put wooden free shop and maker space signs onto the walls in K22 entrance~~ (leave this to Freeshop crew)
* [x] clean motor of Siemens washing machine (Matthias offers help)
* [x] fix water stats
* [ ] remove old gas heater from basement (to trash corner?)
* [ ] remove boiler and pass-through heater from K20-2 bathroom
* [ ] repair WiFi in the Hipster (leave for users of Hipster?)
* [ ] repair Schaukasten
* [ ] Making new signs for the commode in the hallway (?)
* [ ] Door-closer for Elefant-staircase door
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Antonin] aspiring visitors looking for a host:
    - Till, mid July to mid August (on and off)
    - ~~Katarina, end of May to mid September~~ -> Doug is following up
* [Doug] 1) Solizimmer ukuvota 2) Social sauna ukuvota (https://ukuvota.world/#/app/b317bd51-3792-af73-de27-a949c114a192/vote) 3)
* [Janina] [Building weeks planning updates!](https://pad.kanthaus.online/buildingWeeks2022-1)
    * officially scheduled for May 2 to May 22!
    * invitation text ist done and already up on workaway - please also spread!
    * we'd be super happy about people doing leisure orga - please approach us!
    * more people acting as hosts would also be great - concrete requests will follow
* [maxime] Wikidata Data Reuse Days 2022, 14 March 2022 @ 17:00 – 24 March 2022 @ 19:00
  * [homepage](https://www.wikidata.org/wiki/Wikidata:Events/Data_Reuse_Days_2022)
  * [program](https://diff.wikimedia.org/calendar/month/2022-03/?tribe_tags%5B0%5D=13446)
* [Larissa] bonfire in the garden tonight (Starting around 6pm and continue after dinner)
* [chandi] KMW usage for our self-organized theory week next week?
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* Tag der Offene Tür /& Frühling/Sommer-Fest date(s)
    * maybe right before building weeks start
* [Janina] Market pickups are back on! Someone up for going on Friday at noon?
* [Antonin] there is food to be washed
* [Larissa] Lunch team?
    * bit earlier, 12:30? but then still until 14:00 for lunch team only
    * Larissa, Tilmann, Silvan, chandi, Matthias, Antonin, Janina
        * Mon: Tilmann
        * Tue: Larissa, Janina
        * Wed: Matthias
        * Thu: Antonin
        * Fri: Chandi
        * Sat:
        * Sun:

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:**
* **Tue.:**
* **Wed.:**
* **Thu.:** Silvan
* **Fri.:**
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** Maxime, Larissa, Janina, chandi, Matthias
* **Tue.:** Larissa, Antonin, chandi
* **Wed.:** Larissa, Antonin
* **Thu.:** Antonin, Matthias
* **Fri.:** chandi, Matthias
* **Open Tuesday:** Maxime, Larissa, Antonin, chandi
* **Week:** Ask, Doug

### Result
* **Mon.:** Antonin
* **Tue.:** Janina
* **Wed.:** chandi, Matthias
* **Thu.:** Silvan, Maxime
* **Fri.:** Larissa
* **Open Tuesday:** Anja

## 7. For next week
*
