---
title: Coordination Meeting
date: "2022-11-07"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #268
* Date: 2022-11-07
* Facilitator: kito
* Notary: Doug
* Mika caretaker: Tilmann
* Levi caretaker: Janina
* Physical board caretaker: Martin
* Digital calendar: Silvan
* Reservation sheet purifier: Lara
* Present: Kito, Martin, Silvan, Tilmann, Nathalie, Lara, Doug

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/e29176ff-57c4-41d7-8732-7f1b6beba699.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.3 people/day (-1.3)
- **⚡ Electricity**
    - usage: 66.24 €/week (⬆️+18%)
    - of that for heating: ~~23.48 €/week~~ 13.8 €/week
    - paid: 22.85 €/week
    - ☀️ self produced: 49% (⬇️-13%)
    - emissions: 27 kg CO₂ₑ/week
- **💧 Water**
    - paid: 21.9 €/week (⬆️+26%)
    - emissions: 1.7 kg CO₂ₑ/week


### Expenditure



### Income
- 110€ from shoe

### Things that happened
#### In or around Kanthaus
- LiLaLuzis arrived, nice game night with them
- Reparaturräte-Workshop in Kanthaus
- Stadt im Gespräch
- Declogged: snack kitchen sink, dishwasher fan drain & compost toilet urinal!
- Breakfast discussion on Tekmîl essay

#### Wider world
- COP27 started

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->


## 2. This week planning

### People arriving and leaving
* **Mon.:** kito leaves, Bene + Kathi leave
* **Tue.:** maxime comes back in the evening
* **Wed.:** kito comes back, kröte + kiki come during the day [larissa]
* **Thu.:** LiLaLuzies leave, Larissa + Silvan leave
* **Fri.:** Anja probably comes until Sunday, Änskys gemök (4 people) comes
* **Sat.:** Luana? [Doug]
* **Sun.:** Larissa + Silvan come back, Änskys gemök (4 people) leave
* **Mon.:**
* **Some day:**

### Weather forecast


<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Anja Volunteer : Absolute Days threshold 252/60 (+320%)
- Nathalie Visitor : Days Visited threshold 26/21 (+24%)
- Lara_WA Visitor : Days Visited threshold 22/21 (+5%)
- Larissa Member : Absolute Days threshold 188/180 (+4%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Thore Volunteer : 1 days until Absolute Days threshold (60)
- Silvan Volunteer : 6 days until Absolute Days threshold (60)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * 14:00 Laras evaluation [Martin]
    * Monday food care [Martin]
* Tuesday
    * black bin gets collected [Doug]
    * 15:00 - 17:00 Open Tuesday [Martin]
    * 19:00 Murder Mystery Dinner [Lara]
* Wednesday
    * punkrocktresen @NDK
* Thursday
    * 10:00 Power Hour [Fac.: Kito, DJ: ]
    * 15:00 social sauna [Doug]
    * movie screening in ConHanHop about situation in Bosnia with kito talking
* Friday
    * organic waste bin gets collected [Doug]
    * 10:00 Plenum [Zui]
    * 12:00 Market Pickup [kito]
    * 18:45 Film + Gespräch "Der marktgerechte Mensch"@D5
* Saturday
* Sunday
* Next Monday
    * 10:00 CoMe [Martin]
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* longterm scheduling https://pad.kanthaus.online/KH-plenum#

## 3. Shopping plans
* no shopping plans

## 4. To do

_Newest tasks on top_
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put up and down? - why?
         * [Antonin] because permanent changes to the facade are supposed to be coordinated with the office in charge of historical monuments
    * [Martin]Spray paint stencil?
    * [Silvan] K20+K22 stehen nicht unter Denkmalschutz
         * [Antonin] they do not, but there are buildings nearby which are (such as K18) and that still means we are technically not allowed to do whatever we want with the facade. We have a letter from the relevant office saying this.
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements
- **put bigger discussions or decision making here: https://pad.kanthaus.online/KH-plenum#**

### Round 1
<!-- check if anyone has a point that didn't speak already -->
- [Doug] confirm Luana request (World wide wurzen)
    - event is happening on 19th, interviews will be taken before
    - Possible stay of Luana this weekend.
- [Nathalie] on new foodsharing cooperations...
    - looking for motivated people to take responsibility
    - Doug and Kito are interested
- [Janina] group requests:
    - resocializing: 4-10 ppl, 24h, 21./22.11
        - https://yunity.slack.com/files/USLACKBOT/F049EDARABX/22._november_
    - Harzgerode Bau-Schirm: 4-6 ppl + 1 dog, 25.-28.11 [Bodhi will host but wants a more local co-host]
        - https://yunity.slack.com/files/USLACKBOT/F0498QEAH2T/hz_bauschirm_meeting_end_of_november
        - talk about dog topic on slack, there are concerns
        - no co-host found in CoMe
    - GemÖk Friedrich: 7 ppl, 16.-18.12 [Janina will host]
        - https://yunity.slack.com/files/USLACKBOT/F049J1PN0TU/gem__k-treffen_am_16.12.-18.12.
    - (reminder) Refugee Law Clinic Leipzig from 9.-11. Dezember for 10-15 people
        - Martin or Kito would take Co-Host-Responsibility with Janina
- [zui] änskys group (4 people) comes from 11.-13.11. I suddenly have a whole day seminar on friday (after our plenum ;) ) and saturday, so maybe someone else can host them? i don't think it's much work since änsky lived here for a while.
    - kito can say hello on friday and can communicate with Änski
    - so Kito and Zui could be shared hosts

### Round 2
- [zui] longterm scheduling of knowledge sharing sessions in the plenum? think about what you would like to offer: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
- [Doug] invitation to comment on the ideas for Plenum

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Result
- **Mon.:** LiLaLuzies
- **Tue.:** Doug
- **Wed.:** Lara
- **Thu.:**
- **Fri.:** kito
- **Sat.:**
- **Sun.:**
- **Open Tuesday:** Martin



## 7. For next week
*
