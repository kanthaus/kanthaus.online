---
title: Coordination Meeting
date: "2023-12-04"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #322
* Date: 2023-12-04
* Facilitator: Maxime
* Notary: Janina
* Physical calendar: Dorota
* Digital calendar: Tilmann
* Task board: Janina
* Reservation sheets: Kate 
* Weather forecast: Kate
* Present: Dorota, Tilmann, Maxime, Riell, Kate, Janina, Klara

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/0dcf489a-6f35-41d2-ae93-146938b4b1de.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 15.0 people/day (-6.3)
- **⚡ Electricity**
    - usage: 231.62 €/week (⬆️+33%)
      - for heating: 149.14 €/week
      - for attic heating: about 11€
      - for hot water: 11.85 €/week
    - paid: 229.9 €/week 
    - ☀️ self produced: 4% (⬇️-10%)
    - emissions: 108 kg CO₂ₑ/week
- **💧 Water**
    - paid: 16.88 €/week (⏬-50%)
    - emissions: 1.3 kg CO₂ₑ/week

### Expenditure
- [tilmann] 63€ for two replacement bike battery cases
- [maxime] lentils

### Income
- 150€ donation from the Bausyndikat group
- 5€ in the spendenbox

### Things that happened
#### In or around Kanthaus
- many sparks were made in the vacuum cleaner fixing party
- a visit from Salzderhelden and Bausyndikat
- several people went to several NDK-events

#### Wider world

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

- COP28 started in UAE. The COP president seem to have said that there is ‘no science’ behind demands for phase-out of fossil fuels

### Popcorn of feedback
- pleeeeease put the bike gears down to a place from which starting is easy!

<!--
Is there any feedback you would like to give to others - the whole groups or individuals? Is their any reflection about yourself you would like to share with the others? If you would like to give feedback to a person in the room, please do it in third person.
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Vroni arrives
* **Tue.:** Antonin comes, chandi arrives
* **Wed.:** Lara arrives, possibly also Jon, Klara's friends visit
* **Thu.:** Antonin leaves
* **Fri.:** Kito comes back, Vroni leaves, Jon would leave
* **Sat.:** 
* **Sun.:** 
* **Mon.:** 
* **Some day:** Martin comes back 
                       

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->
_no fucking sunshine but at least it gets back above 0°C..._


### Evaluations and check-ins

Due for evaluation
- Riëll_AAA Visitor: Days Visited threshold 25/21 (+19%)
- Nathalie Visitor: Days Visited threshold 23/21 (+10%)

Due for evaluation soon
- Klara Visitor: 2 days until Days Visited threshold (21)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Maxime]
    * 11:00 - Power Hour
    * Penny pickup [Janina]
* Tuesday
    * Hausmüll [Tilmann]
    * 10:00 - 13:00 attic painting/primer session [Tilmann]
    * 11:00 Riell's evaluation [Maxime]
    * after dinner - presentation about mediation and zwischenräume Ausbildung [Vroni, chandi]
    * Penny pickup [Janina]
* Wednesday
    * 10:00 Klara's evaluation [Janina]
    * 12:00 - 17:00 - MitMachCafé 
    * 15:30 Free Shop Lounge emptying action [Kate]
    * 18:00 - Punkrocktresen
* Thursday
    * 13:00 - 18:00 - MitMachCafé
* Friday
    * Biotonne [Maxime]
    * 10:00 Social Sauna [Janina]
* Saturday
    
* Sunday
    * second advent
* Next Monday
    * 9:30 CoMe [Kito] - see discussion point
* Next week summary
    * ToI#2!!


<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
- garlic, ginger

## 4. To do
*See analog list in the elephant room*

**Done**
- [kito] get more sawdust for the compost toilet
- [antonin] grease the hinge of K20-1 staircase toilet
- [levi? xD] better place for Todo list
- [martin, kate] take away top two shelves in the free shop

**Feedback**
- put your name on the sheet when you move it to the done section for thecreadir, please :)
- the new location of the todo board might be better but it's not great either...

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [kate] Freeshop opening over Christmas. Last Tuesday I can do is 12.12. and the first one I can do when I return is 09.01.
    - 26.12. is a holiday anyway, that leaves 19.12. and 2.1.
    - 19th is Riff day, so there's gonna be closed
    - we decide in next come if we close the 2nd, too, so that we can announce it in time
- [Larissa] submit your questions for the quizz! see: https://yunity.slack.com/archives/C3RS56Z38/p1701282705944369
- [kito] Because of ToI, I suggest keeping the CoMe very short next week (half an hour) and to start at 09:30. And I would shift PowerHour either to sunday or monday afternoon.
    - Monday after lunch seems reasonable
    - Kids will be around so they need to be somehow included
- [Klara] Friends want to come over on Wednesday
    - 3 ppl in total, just for some hours and mainly private
    - might stay for dinner, but are flexible
    - sure, invite them :)
- [Riell] group request for Saturday
    - daytime only, we'd be self-sufficient
    - this would be the backup choice if we don't find anything in Leipzig
    - the round has no resistance, Janina can be formal host with Riell actually doing the work

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [kate] TOI 2 - reprotasks for the TOI 2 week, reminder of preparation tasks, rough overview of schedule, attendance (recommend full)
    - repro sheet is up now, you can already sign in if you want- no task should keep people from attending sessions
    - evidence safari is still going on, continue if you like
    - don't forget the physical artifact, it'll be presented on day 2
    - rough schedule will be presented on Slack after CoMe
- [kito] FFJ: 8 people from the get-together have confirmed that they want to take part in the program. one answer is not yet received. so hopefully everything works out nicely without drawing lots! we would like to tell them until the 11th whether they can really participate. so if there is more information needed from your side, or any concerns, let me know. if we (the FFJ-orga) have something to discuss with Kanthaus (e.g. what happens, if 9 persons confirm?), i will make a slack post and/or bring it to social sauna.
    - [kate] the numbers don't matter as long as we don't sit down and work out who sleeps where and how much space really is available
    - [tilmann] we'd also need to know the sleeping needs of the ffj people to know more details
    - [janina] sounds reasonable to speak about it in social sauna
    - [kate] but what about the practical bits like e.g. stuff storage for the group?
    - [tilmann] you (kate) should best speak to kito about these concerns
    - [dorota] we can also hold a session about how to handle the new influx of people most constructively
- [riell] book order request
- [dorota] buiseness address: can I set ot to here?
    - seems fine

### Round 3
- [kate] reclaim the free shop lounge - cave furniture moving this week. Let's set a date!?
    - yes, let's have an action on wednesday 15:30


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** kate
* **Tue.:** Riell
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** opentuesdaykate

### Unavailabilities
* **Mon.:** Janina, Antonin, Maxime
* **Tue.:** vroni, Janina
* **Wed.:** Klara
* **Thu.:** vroni, Antonin, Dorota, Klara
* **Fri.:** vroni, Janina, Antonin, Dorota
* **Open Tuesday:** vroni, Janina, Maxime
* **Week:** Tilmann

### Result
- **Mon.:** Kate, vroni
- **Tue.:** Riell, Klara
- **Wed.:** Dorota, Janina
- **Thu.:** Maxime
- **Fri.:** Klara finds someone
- **Open Tuesday:** opentuesdaykate, antonin

## 7. For next week
