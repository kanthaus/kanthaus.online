---
title: Coordination Meeting
date: "2021-11-22"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #219
- Date: 2021-11-22
- Facilitator: Doug
- Notary: Antonin
- Mika caretaker: Janina/Tilmann
- Levi caretaker: Janina/Tilmann
- Physical board caretaker: Larissa
- Digital calendar: Zui
- Reservation sheet purifier: Nathalie
- Present: Antonin, Nathalie, Larissa, Silvan, Clara, Matthias, Doug, Zui, Maxime

----

<!-- Minute of silence -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_a11fa96b19acbee11e9273d20e5b5cfe.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.6 people/day (-5.9)
- **⚡ Electricity**
    - usage: 11.72 €/day (⬇️-8%)
    - of that: 6.00 €/day for heating (93.6 kWh of heat/day)
    - paid: 9.24 €/day 
    - ☀️ self produced: 21% (⬇️-18%)
    - emissions: 47 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.36 €/day (⬇️-27%)
    - emissions: 1 kg CO₂ₑ/week

### Expenditure
- [matthias] 50,00€ for paint for the communal sleeping room + 1,xx€ for a sheet to cover the floor

### Income
- Shoe: 10€
- 7 cents found in the toilet

## Things that happened
- the family was in brief quarantine
- the parents had a stay in the flat
- an introduction to the heating system happened
- the fairteiler was made nice again
- the fansipan was redecorated
- the last firstziegel were picked up
- the wallpaper in the communal sleeping room got removed
- a bal folk session and an organ concert were enjoyed


### Wider World
- Austria is back in lockdown, partially some countries in germany; Austria will have vaccination duty
- saxony has more restricted corona measures beginning from today, mostly means have your vaccination certificates with you.
- based on calculations of one guy (see https://twitter.com/Michael_Kunz/status/1462458016503156751), beginning on the 2nd of december, all hospitals in germany will not have space in the ICU anymore even when there is not a single other covid infection.
- presidential election in Chilea
- the new coalition is planning on decriminalising cannabis

## 2. This week planning

### People arriving and leaving
- **Mon.:** Antonin came back, charly comes back
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** chandi comes back
- **Sat.:** zui leaves
- **Sun.:** Antonin leaves, lise comes (In the next days)
- **Mon.:** Änsky comes back
- **Some day:** Clara leaves

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Andrea Volunteer : Absolute Days threshold 96/60 (+60%)
- Antonin Volunteer : Absolute Days threshold 75/60 (+25%)
- Zui Member : Absolute Days threshold 194/180 (+8%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Charly_WA Visitor : 0 days until Days Visited threshold (21)
- Clara Volunteer : 5 days until Absolute Days threshold (60)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Doug]
    * 15:00 Bitte Wenden! meeting, cloud room [Clara, Janina]
    * repark cars [done]
    * Papier Tonnen [Matthias]
    * Painting the communal sleeping room [Matthias]
    * 18:00 mobile phone  screen protector knowledge sharing, in the flat [Doug]
- Tuesday
    * 15:00 Rickshaw introduction for interested pilots [Nathalie, Antonin]
    * 15:00 Open Tuesday [Zui, Doug]
    * Hausmüll [Larissa]
- Wednesday
    * trash tour (09:00? 10:00?) [Matthias, Doug]
    * repark cars [Matthias]
    * 20:00 double bass intro [Antonin]
- Thursday
    * 10:00 Power Hour [Fac.: Nathalie, DJ: Zui]
    * 15:00 Nathalie's evaluation [Larissa]
- Friday
    * 10:00 ~~scaffolding take-down action?~~ (see discussion & announcements section)
    * 12:00 Market Pick-Up [Nathalie]
    * 16:00 Antonin's evaluation [Doug]
    * Biotonne [Antonin]
- Saturday
    * 
- Sunday
- Next Monday
    * 10:00 CoMe [Nathalie]
- Next week summary


_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
- * [Larissa] bringing KMW Trailer to TÜV **within November**

## 3. Shopping plans
* [matthias/tilmann] ~7000€ for attic insulation
    * should decrease our heating cost by 20-30%
    * big step towards being in attic without dust mask
    * 4000€ for company to blow in cellulose (the main insulation material)
    * 2000€ for Dampfsperre (air sealing foil) + tape + glue
    * 1000€ for "small stuff": screws, wood, insulation for smaller regions
    - [zui] it might cost ~300-500€ more to get missing parts for the remaining windows
* [matthias] **2x 30€** for fans to put underneath radiators in badly heatable rooms (e.g. The Private, freeshop lounge, ex food storage; maybe bathroom for increased comfort). Proof of concept in hipster room, works great: about 1 degree more (17 instead of 16) when heated a few hours. 4€ per 12cm fan, 5-6 per radiator makes it 20-24€ per radiator; optional thermostate for 12€ (https://www.ebay.de/itm/331057414690) to turn fan on automatically when radiator is hot.
* [chandi] 60€ for 25 corona quick tests
    - I tried to buy some locally, but it was not possible
    - https://healthsystems24.com/product/clungene-corona-schnell-test-covid-19-antigen-schnelltest-25-test-packung-corona-schnelltest/
    - cheapest ones I could find now. 
    - If anyone knows other sources or ideas, please tell me! :)
        - [zui] in other cities there are still cheap ones available (larissa said)
        - [matthias] apparently they are at least 2.80€ everywhere in Germany
        - [clara] we could import them from Britain (they are free there)
        - [Larissa] there are also free tests at the city pharmacy (on-site)
    - no resistance, approved

## 4. To do
_Small fixes of things in the house_

* [ ] bringing KMW Trailer to TÜV **within November**
* [ ] fix K22-2 toilet flush pipe
* [ ] fix hipster room window curtain
* [ ] more storage spaces for visitors [charly]
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] install a light switch (or detector) in the washroom
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Building week clean up tasks
    * [ ] Tarp in the garden
    * [ ] scaffolding net in the garden
         - [antonin] if we are to store the scaffolding in the basement again, then I would wait for that to happen first (to leave space for big pieces first), and then store it there too, maybe?
    * [ ] Working clothes, shoes, etc. in the K22 hallway
        * [silvan] clothes already washed
* [ ] KMW: fix driver side door (https://yunity.slack.com/archives/C3RS56Z38/p1636467207030900)
* [ ] sort and introduce a categorization in the kitchen shelves

### New this week

## 5. Discussion & Announcements
 
### Round 1
- [clara] GemÖk Group Request as see in https://yunity.slack.com/archives/G7E8RPFMH/p1637162606004300. 20th to 23rd January, using the yoga room or cloud room, 7 people
    - approved
- [matthias] Freeshop 2G from now on: https://www.coronavirus.sachsen.de/wir-gegen-corona-8251.html `Zutritt zum Einzelhandel nur mit 2G-Regelung von 6 Uhr bis 20 Uhr möglich – Bau- und Gartenmärkte sind darin eingeschlossen.`
    - I'm happy to do a freeshop shift and check the people's vaccination certificates. The CovPassCheck app is really nice! :-)
    - left unresolved
- [antonin] scaffolding take-down action on friday, 10:00?
    - [matthias] so no chimney of dining room removal before?
    - [tilmann] we could consider leaving the bottom in place, up to dining room
    - [tilmann] preparation and clean-up before, e.g. Tuesday 10:00?
    - postponed
- [janina] childcare request for today 15-17 for bitte wenden! meeting - pwetty pweeeease! :3
- [Andrea] A friend of mine will come from December 7th til 11th. Also Martin might come this week (wednesday till Friday maybe?)
   - [Antonin] are they vaccinated?
   - [Matthias] reference to 3G rule in our collective agreements
- [Silvan] Please don´t remove the remote from hipster room TV. It is useless outside of this room ;)
    - was an accident, sorry :-(

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Doug] Missing: Kitchen compost lid, cup for holding pens in bathroom
    - [matthias] I took the cup and stored it shortly in the elephant room together with a screw driver, both were gone when I wanted to put it back :-( It is my habit to use that cup for letting air out of radiators, this time i went through the house then...
    - [janina] yes the compost lid is badly missing! levi is now starting to dumpster dive _inside_ the shelf... :(
        - [chandi] dumpster mubi ♥!
- [janina] task lottery: I _really_ liked the better kitchen cleanliness with assigned cleanup people! how about adding at least one person per day from monday to friday to make sure some dishes are done and tables are cleared reliably?
    - [chandi] For reasons I mentioned already in multiple places (like on slack https://yunity.slack.com/archives/C3RS56Z38/p1635536688017600 or https://yunity.slack.com/archives/C3RS56Z38/p1633968671319400?thread_ts=1633938452.318500&cid=C3RS56Z38) I don't want to extend the task lottery with more tasks. I think the "sheet method" zui brought up resolves at least some issues of the extended task lottery, why I think, if we really want to have a structure for more tasks like cleanup, we should use this method and maybe develop it further until someone comes up with a new idea
    - [larissa] what about the small group of last week?
        - [zui] outcome: just voice more appreciation when tasks are done
    - [doug] proposes to try a new sheet where people can sign up for tasks for a given day
        - [zui] would rather do something weekly than daily but is happy with trying this one for now
    - trial until next come
- [andrea] turron y cafe! i brought some turron from Spain (not super much) and it was suggested yesterday that we do an "event". I'm free tuesday, wednesday, thursday. could we schedule it this week?
   - Andrea schedules it and announces it (Wednesday looks free)
- [silvan] Does anbody need a 1./2./3. vaccination? (friday morning woud be an opportunity)
    - after how many months since the second vaccination is it necessary? 6, 5, 4 months depending on who you speak to

### Round 3
- [silvan] When you set up a device with an alarm clock, please take the device with you :)
 
## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

- Participants:
 
### Volunteers
- **Mon.:** 
- **Tue.:** Clara
- **Wed.:** 
- **Thu.:** Andrea
- **Fri.:** 
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** Matthias, Maxime, Larissa, Doug, Nathalie, Zui, Silvan
- **Tue.:** Antonin, Matthias, Maxime, Larissa, Zui
- **Wed.:** Antonin, Larissa
- **Thu.:** Antonin
- **Fri.:** Zui
- **Open Tuesday:** Maxime, Larissa
- **Week:** Nathalie

### Result
- **Mon.:** Antonin
- **Tue.:** Clara, Silvan
- **Wed.:** Maxime
- **Thu.:** Andrea, Larissa
- **Fri.:** Doug, Matthias
- **Open Tuesday:** Zui


## 7. For next week
- 


---
