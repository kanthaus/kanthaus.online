---
title: Coordination Meeting
date: "2022-03-07"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #234
* Date: 2022-03-07
* Facilitator: Doug
* Notary: Janina
* Mika caretaker: Kita! Yay! xD
* Levi caretaker: Tilmann
* Physical board caretaker: Larissa
* Digital calendar:  chandi
* Reservation sheet purifier: Doug
* Present: chandi, Doug, Thore, Nathalie, Larissa, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf4821.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.9 people/day (-4.7)
- **⚡ Electricity**
    - usage: 126.23 €/week (⬆️+15%)
    - of that for heating: 91.67 €/week
    - paid: 61.08 €/week 
    - ☀️ self produced: 40% (⬆️+8%)
    - emissions: 57 kg CO₂ₑ/week
- **💧 Water**
    - paid: 20.61 €/week (⬇️-22%)
    - emissions: 2 kg CO₂ₑ/week


### Expenditure
- 20€ (Janina, consumables)

### Income
- 10€ (Lattenroste)

### Things that happened

#### In or around Kanthaus
- the very last (?) corona meeting
- social sauna with two topics
- anja and andrea got reaccepted as volunteers
- silent office renovation meeting
- garden meeting
- monthly team meeting
- project updates
- finance session
- longterm decision making process meeting
- glögi tank got emptied

#### Wider world
- what's in all the newspapers
- big peace demos in leipzig and probably other places

## 2. This week planning

### People arriving and leaving
* **Mon.:** Kiki and zui leave
* **Tue.:** Anja leaves
* **Wed.:** Matthias might come back
* **Thu.:** 
* **Fri.:** 
* **Sat.:** silvan + ~8 acronia-people come
* **Sun.:** Anja might come back, acronia people leave, Antonin comes back
* **Mon.:** Lea arrives
* **Some day:** Maxime comes back

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_It's getting warmer and really sunny. Up to 12°C on Thursday._



### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Doug]
    * Park KMW & trailer on even/this side [done]
    * 17:00 ROAW meeting @elephant room [Nathalie, Antonin]
* Tuesday
    * Feminist fight day
    * 15:00 Open Tuesday
    * 19:00 Feminist theatre "Medusa wurde zur Gruppe hinzugefügt" @NDK [Larissa, Andrea]
* Wednesday
    * Park KMW & trailer on odd/that side [Nathalie]
* Thursday
    * 10:00 Power Hour [Fac.: Doug, DJ: ]
    * 18:00 foodsharing Wurzen meeting @flat [Nathalie, Janina, Thore]
* Friday
    * Yellow bags [Larissa]
* Saturday

* Sunday

* Next Monday
    * Paper waste [Doug]
    * 10:00 CoMe [Janina]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
<!-- If someone plans on spending more than 250€ in cash, please check back with the finance team -->


## 4. To do
_Newest tasks on top_
* [ ] put wooden free shop and maker space signs onto the walls in K22 entrance
* [ ] clean motor of Siemens washing machine
* [ ] fix water stats
* [ ] remove old gas heater from basement (to trash corner?)
* [ ] remove boiler and pass-through heater from K20-2 bathroom
* [ ] ~~mount sink in washroom~~ (maybe have a table there instead?)
* [ ] analog week plan needs a repair
* [ ] repair WiFi in the Hipster
* [ ] repair Schaukasten 
* [ ] repair hat/gloves/scarves cupboard in hallway of K20
* [ ] ~~Clean K18 garden~~ (done by wind :P )
* [ ] Making new signs for the commode in the hallway
* [ ] Install light in the freeshop hallway
* [ ] Door-closer for Elefant-staircase door
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Install light in the hallway of the K20-1 flat
* [ ] Install light in the hallway of the K20-2 flat

### more advanced tasks
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements
### Round 1
* [Andrea] Solzimmer proposal for internal vote
    * Ukuvota of 3 day proposal and 3 day voting phase will be started today
* [Doug] Burner hackathon @ KH, sometime after mid-may, ~10 people, 1 week https://pad.systemli.org/p/kantburnertech
    * general interest, Doug will proceed
* [chandi] I updated the food tasks sheet with minor adjustments and better layout.
* [Janina] Next building weeks will take place in May 2-22! Thanks for participating in the poll! There's two more days to raise resistance on my [Slack post](https://yunity.slack.com/archives/C3RS56Z38/p1646569955765719?thread_ts=1646227322.783519&cid=C3RS56Z38), otherwise it's official, kay? :)
* [Tilmann] Janina and I will go to Mecklenburgische Seenplatte on Wednesday to look at another property. Larissa already agreed to take care of the Bubis while we are gone and Doug will help. (Thank you so much for this!) We will most likely be gone the whole day, so if more people can imagine taking over small bits of childcare, please talk to Larissa!
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Doug] Proposal: move mCM, Social Sauna and Project Updates to 4-weekly rhythm.
    * [Antonin] Since the frequency of social sauna is mentioned in the collective agreements, there is an ukuvota for that (see #kh_vol_mem)
    * The plan is to have one of the mentioned events basically every week and not (as it happens sometimes now) have all three in one week.
    * We could also change the slot and/or use the same slot for all events and have them alternating.
    * Not too much change at the same time also has benefits... Doug will only push for original point.
* [Janina/chandi] Visitor requests:
	* [Lisa] (no pronoun) would like to be in Kanthaus 15.-20.3. I don't know yet how much I'll be here in that time, can anyone else imagine being the host? :) otherwise I think I could manage together with Änski
		* [chandi] I'll do it
    * [Gudrun](https://yunity.slack.com/archives/G7E8RPFMH/p1646502604269649) for a week soonish
        * Janina will answer
    * [Helen](https://www.workaway.info/en/workawayer/tohelenback/overview) without exact time frame, maybe May
        * Janina will answer
    * [Will](https://www.workaway.info/en/workawayer/wginsberg/overview) in early April
        * Doug will take over

### Round 3
* [chandi] Duudle for finding a regular date for kh events: https://www.when2meet.com/?14867620-7Ztc2
* [Doug] Concerns about ancient foot dust in old Dielen that were used in building shelves. Happy to help prepare the wood for future construction plans of that type.

### Last Points
* [chandi] there is now stats again! 🎉
* [Larissa] Lunch team?
    * Larissa will cook today

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** Thore
* **Thu.:**
* **Fri.:** Nathalie
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** Larissa, Doug
* **Tue.:** Larissa
* **Wed.:** Janina, Tilmann, Larissa, Doug
* **Thu.:** Doug
* **Fri.:** 
* **Open Tuesday:** Larissa, Janina
* **Week:** chandi

### Result
- **Mon.:** Janina
- **Tue.:** Doug
- **Wed.:** Thore
- **Thu.:** Larissa
- **Fri.:** Nathalie, Tilmann
- **Open Tuesday:** Anja

## 7. For next week
* 
