---
title: Coordination Meeting
date: "2023-07-03"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #300
* Date: 2023-07-03
* Facilitator: Antonin
* Notary: Kito
* Children caretaker: the parents
* Physical board caretaker: Cecilia
* Digital calendar: Antonin
* Reservation sheet purifier: Kito
* Present: Dorota, Cecilia, Kito, Antonin, Larissa, Kate, Akos

## 0. Check-in round

## 1.  Last week review
### Stats (past 700 days):

![usage graph last 700 days](https://pad.kanthaus.online/uploads/0c2b3190-fdc3-4d55-80c6-172978d209a4.png "Usage during the last 700 days")
*(Usage during the last 700 days)*

For the past 7 days:
- **Present:** 12.0 people/day (-2.6)
- **⚡ Electricity**
    - usage: 19.59 €/week (⬇️-9%)
    - of that for heating: 0.36 €/week
    - of that for hot water: 1.19 €/week
    - paid: -48.97 €/week 
    - ☀️ self produced: 81% (0%)
    - emissions: 9 kg CO₂ₑ/week
- **💧 Water**
    - paid: 18.98 €/week (⬇️-15%)
    - emissions: 1.5 kg CO₂ₑ/week

### Expenditure
- some lentils maybe

### Income
- nothing in the shoe

### Things that happened
#### In or around Kanthaus
- some dumpster diving, a lot of milchreis, yoghurt, spaghetti and cans with fish
- new coffee machine but we don't have the right capsules
- Phoenix got oiled
- Dorota put stuff in the dragon room
- small PlaMe with amazing contributions from Kate

#### Wider world
- first time a paid mayor from the AfD was elected
- fire works happening in France after a policemen killing a person
- Stadtfest in Wurzen
- flat fest in OstPost

<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Tekmîl
- Kito: better distribution of Solizimmer task
- Antonin: didn't invite the Kanthausians properly to the party


## 2. This week planning

### People arriving and leaving
* **Mon.:** JaTiMiLe left, kito came back, Larissa and Kate left
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Sat.:** 
* **Sun.:** Larissa comes (latest)
* **Mon.:** 
* **Some day:** Antonin leaves in the week-end


### Weather forecast
- rain on wednesday, apart from that quite stable

<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->

### Commoning pattern of the week
- Poolen, deckeln und aufteilen

### Evaluations and check-ins

- Anneke Volunteer: 0 days until Days Visited threshold (60)
- Doug Volunteer: 6 days until Days Visited threshold (60)
- Kate_WA Visitor: 4 days until Days Visited threshold (21)
- Akos_WA Visitor: 6 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * Paper bins (already out)
    * 10:00 CoMe [Antonin]
    * 11:00 PowerHour
* Tuesday
    * Hausmüll [Dorota]
    * 15:00 - 17:00 Open Tuesday [lottery]
    * 20:30 board game evening
* Wednesday
    * 19:00 Punkrock-Tresen @D5
* Thursday
    * 10:00 Akos evaluation
    * 18:30 Finta* Thai Boxing @D5
* Friday
    * Biotonne [Akos]
    * 18:00 Doug's graduation party in Leipzig (Erich-Zeigner-Allee, 64b)
* Saturday
   
* Sunday
* Next Monday
    * CoMe [Kito]

* Next week summary

To be scheduled:
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- no, we are well equipped!

## 4. To do
_Newest tasks on top_
* [ ] maintenance of Phoenix bike
* [ ] make K20-2 bathroom door indicator realiable
    * door closing detection doesn't work, but apart from that it works
* [ ] fill holes in Snack kitchen floor where moths breed [Akos]
    * it started but it not done yet, unclear what is the right material
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I would redo the whole thing (but not now) 
* [ ] give away saved windows in K18 (a lot) 
     * maybe https://www.trashgalore.de/ ? [Martin]
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the PlaMe https://pad.kanthaus.online/plame for non-urgent potentially long discussions -->

### Round 1
- [Janina] consider doing some foodsharing pickups! especially penny needs some commitment in advance, since quite some people sign up for is now. but also if you want to do the market pickup or landgut nemt: sign up! thanks and enjoy! ;)
    - Kito signed up for market pickup, Akos is interested to take over
- [Anneke] is someone up for hosting Rudi and Juliette from 28/07/2023 to 13/08/2023? (their profile: https://www.workaway.info/en/workawayer/montplez)
    - Antonin and Kito will talk about it

### Round 2
- [Janina] Benjamin (friend of Luftschlosserei people) asked for a sleeping spot on July 10-11. See mail: https://yunity.slack.com/files/USLACKBOT/F05EFEJEDCL/meine_fortbildung_am_10.___11.07._in_wurzen
    - Kito would recommend the Villa Klug
### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** 
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** Kito
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** 
- **Tue.:** Akos
- **Wed.:** 
- **Thu.:** 
- **Fri.:** Antonin
- **Open Tuesday:** Kito, Antonin, Akos
- **Week:** Larissa, Kate

### Result
- **Mon.:** 
- **Tue.:** Dorota
- **Wed.:** Akos
- **Thu.:** Antonin
- **Fri.:** Kito
- **Open Tuesday:** Cecilia


