---
title: Coordination Meeting
date: "2021-08-02"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #203
- Date: 2021-08-02
- Facilitator: Zui
- Notary: Janina
- Mika caretaker: Thore, Maxime
- Levi caretaker: Janina
- Physical board caretaker: Momo
- Digital calendar: Momo
- Reservation sheet purifier: Andrea
- Present: Michal, Laura, Matthias, Talita, Andrea, Clara, Momo, Janina, Nathalie, Tilmann, Doug, Maxime, Zui

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_cbe99c469f895776738e60fa8067d630.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 15.1 people/day (-1.0)
- **⚡ Electricity**
    - usage: 5.69 €/day (⬆️+7%)
    - paid: -4.54 €/day 
    - ☀️ self produced: 76% (0%)
    - emissions: 5 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.87 €/day (⬆️+28%)
    - emissions: 2 kg CO₂ₑ/week


### Expenditure
- 220€ for solar earthing stuff
- 7€ for 6 FFP2 masks

### Income
- at least 10€ in the shoe

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- Incidence: 
    - Germany: 18
    - Saxony: 7
    - Landkreis Leipzig: 8
- Adjusted prevalence Saxony: 0,03% (last thursday)
- µCOVIDs available last week: {COVIDS FOR LAST WEEK NOT FOUND!}
- µCOVIDs used last week: 
- µCOVIDS balance from last week:  
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week:  
- µCOVIDs/person mean for this week:  (18 people) 

### Things that happened
* Knowledge was shared around GIMP
* We had a Bassment Party!
* We saw a lot of movies: The Lobster, Disclosure, Gravity Falls, Jojo Rabbit, Knives Out
* We (Janina, Zui, Andrea, Mika, Nathalie, Momo) went to Leipzig to hold a foodsharing's stand
* People are getting better at slacklining
* There was a sharing event
* Jon visited
* We had a really big tarot session

## 2. This week planning

### People arriving and leaving
- **Mon.:** Larissa comes back
- **Tue.:** 
- **Wed.:** 
- **Thu.:** Zui and Larissa leave
- **Fri.:** 
- **Sat.:** Momo leaves (T^T)
- **Sun.:** Matthias leaves, Clara leaves
- **Mon.:** 
- **Some day:** Thore and Nathalie leave, Silvan comes back and leaves

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_Mixture of quite some rain and sun, nice temperatures and some wind._

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Anja Volunteer : Absolute Days threshold 70/60 (+17%)
- Nathalie Member : Absolute Days threshold 202/180 (+12%)
- Talita Volunteer : Absolute Days threshold 66/60 (+10%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Momo_WA Visitor : 2 days until Days Visited threshold (21)
- Andrea Volunteer : 6 days until Absolute Days threshold (60)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * Paper waste
    * 10:00 CoMe [Zui]
    * 14:00 Knowledge sharing: Revisiting the attic and going through what was done [Tilmann]
    * Park cars on even-side [Nathalie - already done]
    * 20:30 Building talk
- Tuesday
    * Hausmüll [Michal]
    * 10:00 Monthly coordination meeting [Matthias]
    * 10:30 4th roof planning meeting [Zui, Doug]
    * 11:00 board member paper work session [Zui, Larissa, Janina]
- Wednesday
    * Dresden trip and noodle pickup [Janina, ?]
    * Park cars on odd-side [Maxime]
    * 18:00 Bitte Wenden Wurzen
- Thursday
    * 10:00 Power Hour [Fac.: Andrea, DJ: Maxime as backup]
    * 15:00 Social sauna [Tilmann]
- Friday
    * 12:00 Markt Abholung [Andrea]
    * 15:00 Momo's evaluation [Clara]
    * Biotonne [Doug]
- Saturday
    * 20:00 Project updates [Matthias]
- Sunday
    * GSSX visit? [Doug, discuss]
    * 10:00 foodsharing Brunch [Doug, Janina]
- Next Monday
    * 10:00 CoMe [Talita]
- Next week summary
    * 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
- [Doug] Dacha work-party! Garden work, repair, food, music, beer. Tue/Wed/Fri/Sat?

## 3. Shopping plans
- [Doug] Dishwasher salt
- [Janina] Sunflower seeds: Too expensive right now, wouldn't pursue this for now
- [Janina] Portemonnaie: Does anybody have one laying around?

## 4. To do
_Small fixes of things in the house_

* [x] fix backpack storage shelf in k20 staircase [Tilmann]
* [ ] replace broken/string/toaster shoe rack spots
* [ ] make k20 garden door easily closable from outside (or acquire a new door) https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000
* [ ] secure wash kitchen sink
* [x] fix curtain holders in 'the private' [Zui]
* [x] fix hipster room door indicator [Matthias]
* [ ] think about plastic waste containers to be used in kitchen, snack kitchen and dumpster kitchen
    * [x] replace snack kitchen bag with plastic container [Janina]
    * [ x?] decide that the stable bag in the kitchen works well and can simply be emtied and put back
* 

## 5. Discussion & Announcements

### Round 1
* [Matthias] Who is interested in getting another flat in Wurzen? Let's have a meeting, maybe already including a bidding round.
    * [Antonin] yay, that would mean cool neighbours for the WG! :)
    * A meeting will happen tonight
* [Janina] Noodle pickup on Wednesday: Anybody up for going to Dresden with me?
    * General interest, also maybe to visit the hygiene museum.
    * Will be coordinated outside of CoMe
* [Nathalie] KMW status update
    * [Nathalie] while driving on sunday it was basically fine, only the motor makes loud sounds when pushing gas and bit stuttering at the end
    * [Nathalie] also gas has to be refilled
    * [Matthias] There is one of 8 screws broken attaching the exhaust to the motor inside the motor block, that's what makes the sound. It's a bigger thing to fix it, but it might be okay for now.
* [Tilmann] things from flat today afternoon - anybody wants to visit?
    * [Doug] Quite a lot of interesting things, but not much time.
    * [Tilmann] She has a car available today and can bring things if we tell her exactly what we want.
* [Doug] Shall we have a MCM this month and/or week?
    * [Zui] It can be a kind of summer edition in which we simply fix todos.
    * [Matthias] I think it's important to keep it going and would facilitate the session.
* [Zui] No sunflower seeds..? I'm very unhappy with the spread situation.
    * [Janina] Since they're so expensive I would rather go for buying red lentils this time. They can also be used for cooking, have better nutritional value and are something else for a change. But I'm not motivated to go into the purchase process myself right now.
    * Somebody could go forward, but for now nobody volunteered.
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Doug] Roof finale scheduling. Is it going to happen?
    * [Doug] I think we can do it this year still and it would be my preference.
    * [Zui] I have a lot to say to this but I don't think it's the right place here.
    * [Nathalie] Maybe you can just talk after the MCM tomorrow.
* [Matthias] ~~Yoga Room future?~~

### Round 3
* [Matthias] I'd like to get some ventilation working in the next months: Bathroom, Snack-Kitchen, Piano Room. I'd like to have a meeting this week with people interested in planning. Also, I'd expect to spend some money on this, the group can figure something out.
* [Nathalie] When Thore and I are gone again the flat will be empty and now ti even has 3 sleeping spots available! :) There will be a slack post about this soon as well.


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Nathalie
- **Tue.:** Thore
- **Wed.:** 
- **Thu.:** Andrea, Janina
- **Fri.:** 
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** clara
- **Tue.:** clara
- **Wed.:** clara, Matthias
- **Thu.:** 
- **Fri.:** Momo
- **Open Tuesday:** maxime, Momo, Matthias, clara
- **Week:** michal, Talita, Laura, Zui

### End result
- **Mon.:** Nathalie, Matthias
- **Tue.:** Thore
- **Wed.:** Momo, Maxime
- **Thu.:** Andrea, Janina
- **Fri.:** Tilmann, Clara
- **Open Tuesday:** Doug

## 7. For next week
- 
