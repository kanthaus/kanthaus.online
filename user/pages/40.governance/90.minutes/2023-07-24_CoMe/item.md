---
title: Coordination Meeting
date: "2023-07-24"
taxonomy:
    tag: [come]
---


# CoMe #303
* Date: 2023-07-24
* Facilitator: Cecilia
* Notary: Martin
* Mika + Levi caretaker:
* Physical board caretaker: Anneke
* Digital calendar: Martin
* Reservation sheet purifier: Cecilia 
* Present: Cecilia, Anneke, Martin, Dorota, ASZ1

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

**Present:** 6.6 people/day (-3.7)
- **⚡ Electricity**
    - usage: 24.23 €/week (⬆️+7%)
    - of that for heating: 0.36 €/week
    - of that for hot water: 2.07 €/week
    - paid: -39.24 €/week 
    - ☀️ self produced: 71% (⬇️-8%)
    - emissions: 12 kg CO₂ₑ/week
- **💧 Water**
    - paid: 18.4 €/week (⬇️-21%)
    - emissions: 1.4 kg CO₂ₑ/week


### Expenditure
- 6€ oats [Martin]
- 7€ flour, red lentils [Anneke]
- 8€ oil [Dorota]

### Income
- 100€, Donation box [Cecilia]

### Things that happened
#### In or around Kanthaus
- Ismail visited
- Shopping trip to market in Eisenbahnstrasse

#### Wider world
- 

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Tekmîl


## 2. This week planning

### People arriving and leaving
* **Mon.:** Ollie arrives (maybe), Arrakis leaves
* **Tue.:** Pluma (maybe)
* **Wed.:** Kito comes back?
* **Thu.:** 
* **Fri.:** 
* **Sat.:** 
* **Sun.:** 
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!-- 

    https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
- Martin Volunteer: Days Visited threshold 106/60 (+77%)
- Dorota_Stilgar Visitor: Days Visited threshold 30/21 (+43%)
- Anneke Volunteer: Days Visited threshold 71/60 (+18%)
- ASZ1 Visitor: Days Visited threshold 24/21 (+14%)
- ASZ2 Visitor: Days Visited threshold 24/21 (+14%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Cecilia]
    * 11:00 Power Hour
* Tuesday
    * 10:00 Dorota´s Evaluation [Martin]
    * 15:00 - 17:00 Open Tuesday [Dorota] 
* Wednesday
    * 12:00 - 17:00 MitMachCafé
* Thursday
    * 13:00 - 18:00 MitMachCafé
* Friday
    *  Yellow trash [Cecilia]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Martin]
    * Paper trash [ASZ1]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* Frying oil, 

## 4. To do
_Newest tasks on top_
* [x] new sign of forbidden stuff in dishwasher [Dorota]
* [ ] curtain for the Cave
* [ ] maintenance of Phoenix bike - ask Martin?
* [ ] make K20-2 bathroom privacy indicator realiable - ask Tilmann
* [ ] fill holes in Snack kitchen floor where moths breed [Akos]
    * it started but it not done yet, unclear what is the right material
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I would redo the whole thing (but not now) 
* [ ] give away saved windows in K18 (a lot) 
     * maybe https://www.trashgalore.de/ ? [Martin]
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1

* [Antonin] our neighbour Sven still needs cardboard boxes so if you receive big parcels it's welcome to save the boxes for him in the K22 entrance
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Antonin] Warmshowers request, Tuesday or Wednesday evening: see #kanthaus-mails channel


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)



### Result
* **Mon.:** Anneke and Arrakis
* **Tue.:** ASZ1/2
* **Wed.:** Cecilia
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Dorota
 


## 7. For next week
* 
