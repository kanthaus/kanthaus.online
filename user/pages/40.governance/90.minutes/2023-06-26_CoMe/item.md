---
title: Coordination Meeting
date: "2023-06-26"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #299
* Date: 2023-06-26
* Facilitator: Kito
* Notary: Janina
* Children caretaker: Kita
* Physical board caretaker: Anneke
* Digital calendar: Tilmann
* Reservation sheet purifier: Akos 
* Present: Larissa, Kate, Akos, Anneke, Tilmann, Kito, Cecilia, Janina


## 0. Check-in round

## 1.  Last week review
### Stats (past 7 days):

![usage graph last 90 days](https://pad.kanthaus.online/uploads/3d787144-850b-44a4-ae3e-cfd9fc5c1fb2.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 14.7 people/day (-1.3)
- **⚡ Electricity**
    - usage: 21.65 €/week (⬇️-7%)
    - of that for heating: 0.39 €/week
    - of that for hot water: 1.82 €/week
    - paid: -46.56 €/week
    - ☀️ self produced: 80% (⬇️-1%)
    - emissions: 10 kg CO₂ₑ/week
- **💧 Water**
    - paid: 22.25 €/week (⬇️-14%)
    - emissions: 1.7 kg CO₂ₑ/week

### Expenditure
- 50€ two drop down seals (Absenkdichtung) for the attic entrance doors [tilmann]
- 50€ for oats, spreads, spices and toilet paper [janina, martin]
- 6€ for steel cable clamps [martin]

### Other Income
- 30€ from donation box

### Things that happened
#### In or around Kanthaus
- SummerBash football tournament - Team Dream finished 3rd!
- two people had their first evaluation
- Silvan moved out
- people from Dezentrale visited
- knowledge about digital infrastructure was shared

#### Wider world
- in Greece the ruling party Nea Democratia won the parlamentary elections again and has now an absolute majority (because of a majority bonus system)
- the Wagner Group, a Russian paramilitary organization, initiated a rebellion against the government of Russia
- the bavarian state criminal investigation office eavesdropped on numerous conversations between activists of Last Generation and journalists for months
- AfD won for the first time the elections for the administrative head of a district (in Sonneberg in Thuringia)
- the Titan imploded
- house search in Grimma in connection with TagX

<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Tekmîl
- kito is sorry for ending up playing more competetively than he wanted in the football tournament
- please don't put coated pans on the drying rack so they don't get scratched by the metal spikes
- please don't leave fruit scraps on cutting boards in the snack kitchen
- anneke is sorry for not cleaning up something she had put up for three days and then it was moved
- janina is sorry about littering the elephant room bed with toys and wants to clear it out this week
- dorota is sorry about not putting out the bin and is thankful for someone else taking over this task


## 2. This week planning

### People arriving and leaving
* **Mon.:** Anneke leaves, Lise arrives
* **Tue.:**
* **Wed.:** Lise leaves
* **Thu.:** Kito leaves, Akos leaves for a night
* **Fri.:** Anneke comes back, Dorota leaves for a night
* **Sat.:** Anneke leaves
* **Sun.:** 
* **Mon.:** JaTiMiLe leave for a month, Kate leaves, Larissa might leave
* **Some day:** Antonin comes back (in the week-end)


### Weather forecast
![](https://pad.kanthaus.online/uploads/3351b9ec-b991-4676-9aa6-3d1617d79e40.png)


<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->

### Commoning pattern of the week
'Deepen the bond with nature'
How is the relation to nature lived?

Commoners act in a way that serves life itself and makes them experience themselves as a caring for as well as benefitting from nature. Thus they overcome the perceived separation of nature and being human. Relating to natural rhythms like annual seasons can be helpful for this.


### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Martin Volunteer: Days Visited threshold 100/60 (+67%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Anneke Volunteer: 1 days until Days Visited threshold (60)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Kito]
    * 11:00 PowerHour []
    * Monday food care []
* Tuesday
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 19:00 Punkrock-Tresen @D5
* Thursday
    * Yellow bins [Cecilia]
    * 18:30 Finta* Thai Boxing @D5
* Friday
    * 10:00 PlaMe (it's going to happen!) [Janina]
    * FLINTA* tech meetup in Leipzig
    * yellow bin [Cecilia]
* Saturday
    * Building weekend @Eine Spinnerei (Lausitz) [kito]
    * 20:00 Party at OstPost
* Sunday
* Next Monday
    * Paper bin [Larissa]
    * CoMe [Antonin]

* Next week summary

To be scheduled:
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans


## 4. To do
_Newest tasks on top_
* [x] make K20-2 bathroom bright light realiable [Dorota]
* [ ] fill holes in Snack kitchen floor where moths breed [Martin, Akos]
    * it started but it not done yet
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I would redo the whole thing (but not now) 
* [ ] give away saved windows in K18 (a lot) 
     * maybe https://www.trashgalore.de/ ? [Martin]
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] de-clog compost toilet drain
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
- [kito] Stand Up meetings this week at 09:30?
    - yes! in the elephant room.
- [doug] can someone bring the phone that got found (now in postfach) to the fundbüro? https://www.wurzen.de/buergerservice/dienstleistungen/fundsache-gefunden-900000172-0.html?myMedium=1&auswahl=0 (whoever lost it will be grateful im sure!)
    - if nobody does it kito will
- [Kate] proposal to help with recruitment/induction for Kanthaus
    - Janina is happy to support Kate, everybody else is also invited to share their thoughts
    - There will be a discussion/presentation of the outcomes in PlaMe
<!-- check if anyone has a point that didn't speak already -->

### Round 2



### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Akos
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** Larissa
- **Tue.:** larissa
- **Wed.:** kito
- **Thu.:** kito, Larissa, Janina
- **Fri.:** kito, Janina, Dorota
- **Open Tuesday:** Larissa
- **Week:** Anneke, Tilmann

### Result
- **Mon.:** Akos
- **Tue.:** Kito
- **Wed.:** Janina, Larissa
- **Thu.:** Cecilia
- **Fri.:** Kate
- **Open Tuesday:** Dorota

## 7. Power Hour preferences

* [Antonin] please remember to put newspaper at the bottom of compost bins after emptying them. Newspaper can be found in the dumpster kitchen or the snack kitchen side room

- Kitchen: Tilmann
- Snack kitchen: Tilmann
- Vacuum [3]: Akos
- Food sorting: Kito
- Food washing: Cecilia
- Plant care: Anneke
- Bin emptying: Dorota
- Communal closet: Larissa
- Bathrooms K20: Kate
- Bathrooms K22: Kate
- Vortex: Janina
- Other: 
 
## 8. For next week
