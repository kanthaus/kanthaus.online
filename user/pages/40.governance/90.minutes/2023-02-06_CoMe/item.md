---
title: Coordination Meeting
date: "2023-02-06"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #280
* Date: 2023-02-06
* Facilitator: Larissa
* Notary: Janina
* Children caretaker: Kita
* Physical board caretaker: Anneke+Timea
* Digital calendar: Maxime
* Reservation sheet purifier: Janina
* Present: Anneke, Janina, Maxime, Nathalie, Timea, Silvan, Larissa, Clara

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/2194db03-2d5d-40f2-8cf5-96b40dbbc5fe.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 9.3 people/day (-2.3)
- **⚡ Electricity**
    - usage: 170.45 €/week (⬇️-22%)
    - of that for heating: 120.18 €/week
    - paid: 121.75 €/week 
    - ☀️ self produced: 27% (⬆️+16%)
    - emissions: 62 kg CO₂ₑ/week

### Expenditure
- ~20€ for pasta, rice, polenta and spreads [Janina]

### Income
- 

### Things that happened

#### In or around Kanthaus
- construction in the dining room
    - chimney got removed
    - wallpaper as well
    - plastering the chimeny-wall
    - ventilation was extended to dining room and office
    - painting base got applied
- a huge sofa nest appeared in the piano room
- cozy 3-people social sauna
- larissa made beautiful bread and shared the recipe
- trip to emerging communkity in regis-breitingen
- walk in the sunshine to spitzberg
- a game of quantum (in the office!)

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
- major earthquake hits southern turkey and syria
- heavy snow and avalanches in several mountain areas (alps, tatra, montana)

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** 
* **Wed.:** Martin and Veronica arrive (maybe)
* **Thu.:** Kai arrives
* **Fri.:** 5 Flowmis and Zui arrive
* **Sat.:** 
* **Sun.:** Flowmis leave
* **Mon.:** Kai leaves
* **Some day:** Letti might leave towards the end of the week

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_3 days of sun in a row but very cold. barely above 0°C during the day and the weekend is gray again._

### Evaluations and check-ins
Check-In with Letti


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Larissa]
	* Monday food care [Timea, Janina]
    * Park trailer on even/this side [Maxime]
* Tuesday
    * 15:00 - 17:00 Open Tuesday
* Wednesday
    * Park trailer on odd/that side [Maxime]
    * 10:00 - 13:00 Attic session [Tilmann, you?]
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Janina]
    * 15:30 Nathalie's evaluation [Janina]
* Friday
    * yellow bin [Larissa]
    * 10:00 monthly PlaMe [Larissa]
* Saturday
    * 10:00 - 13:00 Attic session [Tilmann, you?]
    * evening trip to Leipzig [Janina, Franzi, Kai, you?]
* Sunday
    * 13:00 A-Cafe @Meuterei,Leipzig
* Next Monday
    * paper bin [Clara]
    * 10:00 CoMe [Maxime]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* 

## 4. To do
_Newest tasks on top_
* [ ] exchange the strings of light-brown guitar
* [ ] repair schaukasten - window is pressed in
* [ ] level the compost
* [ ] bring building rubble to the Müllverwertungsanlage
* [ ] oat milk inspection (?)
* [ ] treat mold around bathroom window and piano room middle window
* [ ] deep-clean shower tap to make it easier to adjust temperature
* [ ] Potato sorting action
* [ ] General trash tour
* [ ] Sign: Renew the "Klingel" - Sign (it's less and less readable)
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: elephant room
* [ ] Light: Yoga room 
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
* [Antonin] the [second voting phase of the constitution change process](https://ukuvota.world/#/app/d9b3a17f-866c-3df8-65e6-5f3227787884/vote) is ending soon: members, make sure your voice is heard! 
* [larissa] Haus Kante Wurzen Mitgliederversammlung will happen on the 5th/6th of may. Official invite will come soon! Save the date :)
* [Janina] ToI is approaching! The orga team is still looking for facilitators for some (especially fun) sessions and is working on finalizing the schedule. Please check the [pad](https://pad.kanthaus.online/toi2023-sessions?edit), consider signing up for something and share your thoughts in the [corresponding thread](https://yunity.slack.com/archives/C3RS56Z38/p1674560424496139).
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Janina] Another save-the-date: On May 13 there will be the awesome long-awaited Wenceslaigassenfest! I'm co-organizing and am happy about everyone who'll come and/or participate! :)
* [Janina] Visitor requests
    * Gudrun + Dog
    * Robin

### Spontaneous points
* [Nathalie] ToI time house care needed?
    * water the plants
    * maybe trash out
    * anything else? let nathalie know
* [Antonin] stats and evaluations are here (sorry about the outage…)
    * :tada:

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** Janina
* **Thu.:** Larissa
* **Fri.:** 
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** Maxime
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Anneke
* **Open Tuesday:** Maxime, Clara, Timea
* **Week:** Nathalie, Silvan

### Result
- **Mon.:** Timea
- **Tue.:** Maxime
- **Wed.:** Janina
- **Thu.:** Larissa
- **Fri.:** Clara
- **Open Tuesday:** Anneke

## 7. For next week
