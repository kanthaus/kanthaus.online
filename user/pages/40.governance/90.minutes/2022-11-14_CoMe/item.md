---
title: Coordination Meeting
date: "2022-11-14"
taxonomy:
    tag: [come]
---

# CoMe #269
* Date: 2022-11-14
* Facilitator: Martin
* Notary: maxime
* Mika caretaker: kita
* Levi caretaker: Janina
* Physical board caretaker: Doug
* Digital calendar: maxime
* Reservation sheet purifier: Martin
* Present: Martin, Doug, Nathalie, Zui, Silvan, Larissa, Janina, Levi, Maxime, Lara

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

- **Present:** 12.3 people/day (-1.3)
- **⚡ Electricity**
    - usage: 66.24 €/week (⬆️+18%)
    - of that for heating and warm water: 19 €/week
    - paid: 22.85 €/week
    - ☀️ self produced: 49% (⬇️-13%)
    - emissions: 27 kg CO₂ₑ/week
- **💧 Water**
    - paid: 21.9 €/week (⬆️+26%)
    - emissions: 1.7 kg CO₂ₑ/week

### Expenditure
- [tilmann] 66€ for 5000x A4 and 500x A3 paper
- [tilmann] 43€ second-hand electric multi-cutter tool (for cuts in tricky locations)


### Income
- 1 euro in the boot.
- 500 euros from Lila Luzies

### Things that happened
#### In or around Kanthaus
- We celebrated Janina's Birthday and went in town for a cocktail night!
- We had 2 groups staying over
- Some people went to the screening of a documentary about police violence against refugees in Bosnia
- A mystery dinner
- many people were sick
- The first Kanthaus plenum!

#### Wider world

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->


## 2. This week planning

### People arriving and leaving
* **Mon.:**
* **Tue.:**
* **Wed.:** Antonin, Tobi and crew arrive
* **Thu.:** Antonin, Tobi and crew leave, Nathalie leaves, Jonash and Zusanna leave, Lara (probably) leaves
* **Fri.:** Anja comes for a night, Luana comes for a night
* **Sat.:** Antonin might come back
* **Sun.:** Nathalie comes back
* **Mon.:**
* **Some day:** Larissa might leave on the weekend

### Weather forecast
Potential rain from wednesday to friday, with a drop in temperature towards Saturday.

<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

- Maxime Member : Absolute Days threshold 241/180 (+34%)
- Larissa Member : Absolute Days threshold 195/180 (+8%)
- Silvan Volunteer : Absolute Days threshold 61/60 (+2%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * ~~Monday food care~~ []
* Tuesday
    * 10:00 Electro Repair Party! [Doug]
    * 15:00 - 17:00 Open Tuesday [Lara]
* Wednesday
    * evening: Punkrocktresen @NDK
* Thursday
    * 10:00 Power Hour [Fac.: Doug, DJ: zui]
* Friday
    * 11:00 Maxime's evaluation [Janina]
    * 12:00 (last) Market Pickup [Martin]
* Saturday
    * Yellow bin [Martin]
    * Critical Mass in Wurzen
    * Wide World Wurzen at NDK
* Sunday
    * Repark KMV [maxime]
* Next Monday
    * 10:00 CoMe [maxime]
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* longterm scheduling https://pad.kanthaus.online/KH-plenum#

## 3. Shopping plans
* toasted sesame oil
* sesame seeds
* garam masala

## 4. To do

_Newest tasks on top_
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements
- **put bigger discussions or decision making here: https://pad.kanthaus.online/KH-plenum#**

### Round 1
<!-- check if anyone has a point that didn't speak already -->
- [tilmann] one last KMW trip to Baunativ in Oschatz, to buy things for the attic floor! Who's up for joining me? Date is still open, probably this week.
  - Doug and Martin would be up for it. Doug would like as soon as possible
- [nathalie] foodsharing Wurzen:
  - 1) encourage everyone to do [e-learning](https://www.metro.de/wissen/etraining/haccp) about hygiene and HACCP
    - In case we start a collaboration with the local Penny, this would be required to do the pickup
  - 2) someone needed for Betty's Bauernlädchen friday 16:30
    - Janina will go
- [Doug] should we get more hot water bottles and hot water bottles covers?
  - zui: yeah! maybe fancy ones that you can put around your neck
  - Larissa: we should also encourage people to bring them back
  - Janina: also true for tee thermos, bring them back
- [Janina] It's getting cold and we still have the summer duvet. We could add the winter layer. Larissa and I could do that over the week (especially for the De Luxe duvets)
- [Larissa] fill the advent calendar!

### Round 2
- [Doug] I will relist the KMV for selling


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:**
- **Tue.:** Silvan
- **Wed.:**
- **Thu.:** Zui
- **Fri.:** Janina
- **Open Tuesday:** Lara

### Unavailabilities
- **Mon.:** Maxime, Doug
- **Tue.:**
- **Wed.:**
- **Thu.:**
- **Fri.:**
- **Open Tuesday: Maxime
- **Week:** Nathalie, Larissa

### Results

- **Mon.:** Martin
- **Tue.:** Silvan
- **Wed.:** Maxime
- **Thu.:** Zui, Doug
- **Fri.:** Janina
- **Open Tuesday:** Lara
