---
title: Coordination Meeting
date: "2023-03-13"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #284
* Date: 2023-03-13
* Facilitator: Larissa
* Notary: Tilmann, Janina
* Children caretaker: Kita
* Physical board caretaker: Martin
* Digital calendar: Larissa
* Reservation sheet purifier: Janina
* Present: Tilmann, Martin, Janina, Larissa

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/631094ab-6436-4d33-a95c-a957d0bbd9f7.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.3 people/day (-1.0)
- **⚡ Electricity**
    - usage: 187.03 €/week (⬇️-8%)
    - of that for heating: 92.81 €/week
    - paid: 111.75 €/week 
    - ☀️ self produced: 38% (⬇️-5%)
    - emissions: 61 kg CO₂ₑ/week
- **💧 Water**
    - was fixed, but not accurate for last week


### Expenditure
- 7€ vanilla sugar, q-tips, sourdough flavor [Martin]
- 1153€ for Liaporfit (floor filling material) [Tilmann]

### Unusual Income
- none

### Things that happened
#### In or around Kanthaus
- Antonin bday and backstory
- PlaMe with many many topics and remote participation
- Women's Day demonstration in Leipzig
- FLINTA* Technik Treff in Leipzig
- there's fancy and super bright light in the yoga room now
- the snack kitchen table got real legs
- the dragon room got almost fully cleared again
- there's a gaming setup at Maxime's desk


#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
- shooting in Hamburg: a former Jevova's witness ran amok in a Jehova's witnesses mess


## 2. This week planning

### People arriving and leaving
* **Mon.:** Silvan comes back
* **Tue.:** zui comes
* **Wed.:** Silvan leaves, Kito comes back (or thursday), Feuerqualle group arrives (4 ppl), zui leaves
* **Thu.:** 
* **Fri.:** Anja leaves, Silvan comes back
* **Sat.:** 
* **Sun.:** Feuerqualle group leaves, another 4 ppl group arrives
* **Mon.:** 
* **Some day:** Antonin comes towards the end of the week

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_Quite some rain and warmth until Wednesday. Spring towards the end of the week with sun and 15°C!_

### Evaluations and check-ins

- Silvan

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Larissa]
	* Monday food care [everyone]
    * Park trailer around the corner [Martin]
* Tuesday
    * Restwaste [Martin]
    * 15:00 - 17:00 Open Tuesday [lottery]
    * After dinner: Larissa's birthday celebration
* Wednesday
    * 10:00 - 13:00 Attic work session [Tilmann, Janina, Zui, you?]
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Tilmann]
    * 15:00 Sharing Event [kito]
        * alternative: friday
* Friday
    * Biowaste [Martin]
    * 10:00 - 13:00 Attic work session [Tilmann, you?]
* Saturday
    * Trip to Sauna in Oschatz [Larissa]
* Sunday
    * Park trailer on Kantstrasse [Zui will have taken it into the garden by then :)]
* Next Monday
    * 10:00 CoMe [Janina]
* Next week summary
   

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [Martin] Pulley system to hang bikes from the ceiling in the workshop. 27 euros max. 


## 4. To do
_Newest tasks on top_
* [ ] do the oil treatment to wooden tables in dining room and snack Kitchen []
* [ ] modify the task lottery to prioritize open tuesday []
* [ ] general trash tour []
* [x] fix the main water meter reader [Antonin]
* [ ] update the residency script to show time due before evaluations []
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom [Larissa]
* [ ] Light: elephant room
* [x] Light: Yoga room [Tilmann]
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
<!-- check if anyone has a point that didn't speak already -->
- [Martin] Rent contracts could be updated to compensate the recent increases in energy price.
  - [Larissa] It's on the finances team's todo list to determine a fair price
- [Janina] Dragon and Cloud Room heating is on. Make sure to keep doors closed!
    - [Tilmann] If you manage you can switch it off at night.
    - [Janina] Will try.

### Round 2
- [Martin] Carla cargo, bike cadavers and welding station in K18? Easily retrievable, more comfortable workspace (more light and less cold), less moving stuff around in the wood shed. Just need a power extension cord.
    - Perfectly good reasons, but there's the fear of making K18 into a stuff pile.
    - [Janina] A clear agreement with room specifications and a responsible person is needed. I can make a process proposal. That could also serve as a template for future uses.
    - [Larissa] What about K22 basement? The party room purpose doesn't seem important right now.
- [Tilmann] I started planning a garden side Vordach (awning) in front of K18
    - yay!

### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Larissa
* **Tue.:** 
* **Wed.:** Janina
* **Thu.:** 
* **Fri.:** Antonin
* **Open Tuesday:** Martin

### Unavailabilities
* **Mon.:** FQ
* **Tue.:** FQ
* **Wed.:** FQ
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** FQ
* **Week:** Tilmann

### Result
- **Mon.:** Larissa
- **Tue.:** 
- **Wed.:** Janina
- **Thu.:** fq
- **Fri.:** antonin
- **Open Tuesday:** Martin




## 7. For next week
