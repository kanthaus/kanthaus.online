---
title: Coordination Meeting
date: "2022-10-03"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #263
* Date: 2022-10-03
* Facilitator: Janina
* Notary: Janina
* Mika caretaker: Tilmann
* Levi caretaker: Tilmann
* Physical board caretaker:  Antonin
* Digital calendar: Larissa
* Reservation sheet purifier: Silvan
* Present: Larissa, Antonin, Janina, Silvan, Martin, Andrea, Thore

----

<!-- Time for reflection -->

## 0. Check-in round
- done.
## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/948dba78-fa9e-429b-9bed-1a96ac4579c3.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.1 people/day (-0.7)
- **⚡ Electricity**
    - usage: 53.25 €/week (⬆️+43%)
    - of that for heating: 18.12 €/week
    - paid: -5.36 €/week
    - ☀️ self produced: 67% (0%)
    - emissions: 16 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.48 €/week (⬆️+4%)
    - emissions: 1.0 kg CO₂ₑ/week

### Expenditure

### Income

### Things that happened
#### In or around Kanthaus
- Potato pickup at Kola
- Several Kanthausians attending Bits&Bäume in Berlin
- Not one, but two Bitte Wenden coworkings
- Another great meeting of the FLINTA* programming and server working group AGPSW
- Larissa attended a Fachtagung about sexuality for people with handicaps

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
*

## 2. This week planning

### People arriving and leaving
* **Mon.:** Antonin :bug:, Matthias leaves, Andrea and Maxime come back
* **Tue.:**
* **Wed.:** Maxime leaves
* **Thu.:**
* **Fri.:** Larissa and Silvan leave for the weekend
* **Sat.:** Tito_Antonin arrives for the weekened, Anneke arrives
* **Sun.:** Antonin :baby_chick:, Lara_WA arrives
* **Mon.:** Doug :rat:
* **Some day:** chandi is around during the days

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_golden autumn is coming: up to 20°C, no rain and lots of sunshine_

### Evaluations and check-ins
- Martin Volunteer : Absolute Days threshold 90/60 (+50%)
- Matthias Volunteer : Absolute Days threshold 83/60 (+38%)
- Tilmann Member : Absolute Days threshold 223/180 (+24%)
- Maxime Member : Absolute Days threshold 199/180 (+11%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * Park KMW & trailer on even/this side [done]
    * 19:30 Attic tour video screening with Q+A @piano room
* Tuesday
    * Start of finance week for WaWü tax declaration [Zui, chandi, Larissa]
    * Potential bean harvesting action
    * 15:00 - 17:00 Open Tuesday []
* Wednesday
    * Potential bean harvesting action
* Thursday
    * 10:00 Power Hour [Fac.: Martin, DJ: ?]
    * Tbc Afternoon trip to Vegutopia [Janina, Nathalie]
    * 20:00 Project updates [Antonin]
* Friday
    * 12:00 Market Pickup [Andrea]
* Saturday
    * Yellow bin [Martin]
    * 11:00 Martin's evaluation [Antonin]
* Sunday
    * Repark KMW & trailer [Silvan]
* Next Monday
    * 10:00 CoMe [Larissa]
* Next week summary
    *

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* ~15€ Better pedals for Phoenix, to be able to use straps or clips [Martin]
    * maybe buy the ones which also support click pedals?
    * they're more expensive, but seem better
    * the more expensive ones then!

## 4. To do

_Newest tasks on top_
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put up and down?
* [ ] remount snack kitchen radiator [Martin/Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Antonin] it's not clear whether there is enthusiasm around having an introspection week again (https://yunity.slack.com/archives/C3RS56Z38/p1664112918476259 - only 4 volmems interested so far). Are we skipping it this winter then?
    * [janina] I think it would be really stupid not to take time to collectively work on Kanthaus as a project. It could be another format, but there definitely should be _something_.
    * [larissa] would be happy to have it but won't organize again
    * [janina] very possible that people take it for granted that it's gonna happen again. it might just need someone to take over the planning.
    * [antonin] mostly interested in the logistical side, will make a poll to set a date soon.
* [Janina] Beans! I need to confirm when it can happen, but there will be bean harvest this week! Who's in?
    * Janina will coordinate with Thore, Antonin, Martin and Silvan
* [larissa] for the tax declaration we need some written reports of what the association is doing. any volunteers?
    * janina is interested
* [martin] where to put photos? what's with the cloud?
    * [antonin] kanthaus cloud is still the place to go, everything on there will be migrated at some point
    * [janina] will look into making an account for martin

<!-- check if anyone has a point that didn't speak already -->

* international driver license discussion

### Round 2
*

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Janina
* **Tue.:**
* **Wed.:**
* **Thu.:**
* **Fri.:**
* **Open Tuesday:**

### Unavailabilities
* **Mon.:** Silvan
* **Tue.:** Antonin, Silvan
* **Wed.:**
* **Thu.:** Antonin
* **Fri.:** Silvan
* **Open Tuesday:** Antonin, Silvan
* **Week:** Larissa, Andrea, Thore

### Result
- **Mon.:** Janina
- **Tue.:**
- **Wed.:** Antonin
- **Thu.:** Silvan
- **Fri.:**
- **Open Tuesday:** Martin

## 7. For next week
* 
