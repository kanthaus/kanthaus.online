---
title: Coordination Meeting
date: "2023-03-20"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #285
* Date: 2023-03-20
* Facilitator: Janina
* Notary: Kito
* Children caretaker: Kita
* Physical board caretaker: Larissa
* Digital calendar: Tilmann
* Reservation sheet purifier: Martin
* Present: Martin, Larissa, Janina, Tilmann, Kito

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/080d64ce-45de-4234-b601-140734ba43f6.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.7 people/day (+0.6)
- **⚡ Electricity**
    - usage: 144.72 €/week (⬇️-23%)
    - of that for heating: 42.38 €/week
    - paid: 39.4 €/week 
    - ☀️ self produced: 59% (⬆️+21%)
    - emissions: 36 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.98 €/week (⬆️+60%) (variation not accurate since last week's stats were underestimated)
    - emissions: 1.0 kg CO₂ₑ/week


### Expenditure
- 1,49€ for 1 spread
- bike stuff was bought, will be added next week

### Unusual Income
- not checked yet

### Things that happened
#### In or around Kanthaus
- two small groups use(d) our space
- communal trip to sauna and pool in Oschatz
- party in Leipzig on a 24h rave (but we didn't stay that long...)
- lemon cake showdown for Larissa's b-day
- very nice chocolate cake
- Tilmann broke his bike, Janina broke the stove (oops!)
- sleep kitchen transformation: now 5 sleeping spots (another communal sleeping, use it!)
- spring is here: garden times without jackets
- a lot of bike work, including some welding
- informal agreement about storage next to KH changed
- a trash tour with carla cargo!


#### Wider world
- an international agreement that it's worth to protect the ocean
- debate about children stabbing

<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->



## 2. This week planning

### People arriving and leaving
* **Mon.:** Zui?
* **Tue.:** group of 4 leaves
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Sat.:** Anja comes, Katja+Konrad (Luftschlosserei) visit for 1 night
* **Sun.:** Kito and Anja leave
* **Mon.:** 
* **Some day:** Antonin likely comes towards the end of the week when OstPost runs out of Edelmarzipanstollen

### Weather forecast
- warm, but not so much sun, a bit of rain, some durchwachsenheit

<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->


### Evaluations and check-ins
- Silvan: after Monday any day in the morning


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Janina]
    * 11:30 Finance day [Larissa, Zui]
	* urgent Monday food care [Tilmann, group?, Martin, you?]
    * Park trailer around the corner [Larissa]
    * 18:00 Tischgespräch: „Hier und jetzt: Solidarität statt Polizei" @ NDK [Kito]
* Tuesday
    * 11:00 Brunch with Unkraut @dining room [Janina]
    * 14:00 Frühlingsfest der Vielfalt (spring festival of diversity) @market square
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 10:00 - 13:00 Attic work session [Tilmann, Janina, you?]
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Martin]
    * 12:00 Silvan's evaluation [Janina]
    * 12:00 Finance day [Larissa, Zui]
    * 19:00 Lesung: „Kinder von Hoy" mit Autorin Grit Lemke @NDK [Kito]
* Friday
    * yellow bins [Martin]
    * 11:00 Project updates [kito]
    * 11:30 Finance day [Larissa, Zui]
* Saturday
    * 10:00 - 13:00 Attic work session [Tilmann, you?]
* Sunday
    * Park trailer in Kantstrasse [Zui will have taken it into the garden by then :)]
    * semi-public dumpsterdiving bike trip (maybe) [kito]
* Next Monday
    * 10:00 CoMe [Larissa]
* Next week summary
    * social sauna

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- maybe more spreads

## 4. To do
_Newest tasks on top_
* [ ] do the oil treatment to wooden tables in dining room and snack Kitchen []
* [ ] modify the task lottery to prioritize open tuesday []
* [x] update the residency script to show time due before evaluations [Antonin]
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom [Larissa]
* [ ] Light: elephant room
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
- [kito] Long Term Decision Making Process is starting today!
    - please add your project ideas here: https://pad.kanthaus.online/ltdm-2023#
    - next phase starts in 2 weeks
    - get creative! more steps coming, it's about collecting ideas, don't think too much about "who will do it?"
    - about projects which should start this year
- [Tilmann] 3 palettes of Schüttung should get delivered this week; we ideally unload two palettes immediately to return them, because we just have one for exchange - much help appreciated!
    - should not take much more than half an hour
    - maybe it's possible to find out which day it will happen

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [kito] sharing people present and topics of social sauna in kh_volmem (as a responsibility for facilitator)?
    - no concerns, let's do it!


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Group
* **Tue.:** 
* **Wed.:** 
* **Thu.:** kito
* **Fri.:** Silvan
* **Open Tuesday:** Janina

### Unavailabilities
* **Mon.:**
* **Tue.:** 
* **Wed.:**
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** Martin, Larissa, Tilmann

### Result
* **Mon.:** Group
* **Tue.:** you?
* **Wed.:** you?
* **Thu.:** kito
* **Fri.:** Silvan
* **Open Tuesday:** Janina




## 7. For next week
* 
