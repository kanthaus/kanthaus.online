---
title: Coordination Meeting
date: "2023-08-14"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #306
* Date: 2023-08-14
* Facilitator: Anneke
* Notary: Martin
* Physical board caretaker: Doug
* Digital calendar: Martin
* Reservation sheet purifier: Larissa
* Present: Larissa, Linea, Cecilia, Alejandro, Martin, Anneke, Lena, Doug, Rudi, ASZ1, Dorota, Maxime

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

- **Present:** 14.9 people/day (+0.1)
- **⚡ Electricity**
    - usage: 33.9 €/week (⬆️+41%)
    - of that for heating: 0.47 €/week
    - of that for hot water: 3.22 €/week
    - paid: -24.21 €/week 
    - ☀️ self produced: 75% (⬆️+7%)
    - emissions: 16 kg CO₂ₑ/week
- **💧 Water**
    - paid: 26.62 €/week (⬆️+16%)
    - emissions: 2.1 kg CO₂ₑ/week

### Expenditure
* [Antonin] about 500€ of building materials (Bodentreppe, Gipskarton, Uniflott…)

### Income
20 ct

### Things that happened
#### In or around Kanthaus
- Building week! Yay! 
- spontanous BBQ party
- Fire show! 
- Planned pizza party 
- Mitmach Cafe was attacked
- 
#### Wider world
- Festivals and camps
- Fires in Hawaii 
- Also floods. At other places

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Tekmîl & building week review

## 2. This week planning

### People arriving and leaving
* **Mon.:** JaTiMiLe leave for the week, Linea leaves, Dorota leaves
* **Tue.:** Rudi & July leave
* **Wed.:** 
* **Thu.:** Anneke leaves for a week, ASZ1 brothers visit for a couple of days
* **Fri.:** 
* **Sat.:** 
* **Sun.:** 
* **Mon.:** 
* **Some day:** Larissa leaves for 3 days, Lena will leave some day, Dorota returns

### Weather forecast

will be hot but always some rain in between

<!-- 
    https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597
-->

### Evaluations and check-ins

- ASZ1 Visitor: Days Visited threshold 45/21 (+114%)
- ASZ2 Visitor: Days Visited threshold 45/21 (+114%)
- Anneke Volunteer: Days Visited threshold 90/60 (+50%)
- Larissa Member: Days Visited threshold 188/180 (+4%)
- Doug Volunteer: 1 days until Days Visited threshold (60)
- Rudi & July Evaluation Check-in

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Anneke]
    * 11:00 Distributing Power Hour tasks
    * Afternoon mate check-in with Rudi and July
* Tuesday
    * rest waste [Doug]
    * 11:oo AZ1&2 eval [Doug]
    * 15:00 - 17:00 Open Tuesday [lottery] 
* Wednesday
    * 12:00 - 17:00 MitMachCafé
* Thursday
    * 13:00 - 18:00 MitMachCafé
* Friday
    * organic waste [Doug]
* Saturday
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Doug]
* Next week summary

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [Tilmann/Martin] 85€ 100m cable for attic etc. (3x1.5mm² NHXMH-J PVC-free) https://www.elektromax24.de/100m-mantelleitung-halogenfrei-nhxmh-j-3x1-5-ring-grau
- [Doug] Roto ecklager circa 10€
- [Anneke] Paint 
- 
## 4. To do
_Newest tasks on top_
* [ ] remove weeds in front of the houses [Cecilia]
* [ ] Fix Cloud Room door indicator [Martin]
* [ ] Fix the black cupboard for hats, gloves and scarves in K20 entrance which is slowly falling apart/wash everything at the same time
* [x] fix window in Compost toilet [Cecilia and Pietro]
* [ ] curtain for the Cave [Anneke] 
* [ ] Lantern curtain [Lena]
* [ ] make K20-2 bathroom privacy indicator realiable
    * [Tilmann] what kind of problems did you encounter? I tried to simplify the logic to make it more reliable already, but I wonder if the motion sensor could be flaky. 
    * [Martin] I know Dorota has been working on it. 
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing
* [ ] give away saved windows in K18 (a lot) 
     * maybe https://www.trashgalore.de/ ? [Martin]
* [ ] give away roof windows (in K18)
* [x] give away roll of Dampfsperre [Antonin]
* [x] give away 2 rolls of Unterspannbahn [Antonin]
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->
### Round 1
* [larissa] MSV (mit sicherheit verliebt) sex-ed group: workshop-weekend in kanthaus 24.-26. of november, ~20 people. thoughts, concerns?
* [Antonin] I made Kleinanzeigen posts for the windows and other building materials. If people come to pick those up, please direct them to the room of the building freeshop (K18 ground floor, first room on the right) and let me know what they took (anything in that room can be given away)
* [Janina] Highfield rescue action next Monday
    * I'd really like to do it but won't be around to organize much
    * How would we even do it? Rent a car? Ebike only? Who'd be in?
    * What time do we need to be there for it to make sense?
    * Shall I contact the foodsharing people on site?
* [Anneke] Hosting Emma: Cecilia said she could host if Emma would be coming for the end of August. Now Emma is saying to arrive at the end of August/early September. Anyone would be up for hosting? [Larissa?, Doug?]
* [Doug] Social sauna this week? Or next one with more people? Doug will make a poll to figure it out. 

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Janina] The pee ukuvota results are in! So who's gonna make the signs? :) Martin will.
* [Anneke] Lantern: not finished, gonna keep working and support is welcomed. Before Thursday. Also, room could be unusable until next week? Resistance? No resistance
* [Anneke] Pfand, there is a lot! Lidl is better because it has glass dumpsters

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Larissa
* **Tue.:** ASZ1
* **Wed.:** Anneke
* **Thu.:** Alejandro
* **Fri.:** Doug
* **Open Tuesday:** Cecilia

### Result
* **Mon.:** Larissa
* **Tue.:** ASZ1
* **Wed.:** Anneke
* **Thu.:** Alejandro
* **Fri.:** Doug
* **Open Tuesday:** Cecilia


## 7. For next week
* 
