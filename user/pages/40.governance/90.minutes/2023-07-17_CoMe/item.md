---
title: Coordination Meeting
date: "2023-07-17"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #302
* Date: 2023-07-17
* Facilitator: Larissa
* Notary: Antonin
* Mika + Levi caretaker: not needed
* Physical board caretaker: Dorota
* Digital calendar: Kito
* Reservation sheet purifier: Cecilia
* Present: Cecilia, Larissa, Dorota, Antonin, Kito, Anneke, Arrakis

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/8d230582-c61c-49af-8bd2-ee8e53a5c049.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 9.6 people/day (+2.7)
- **⚡ Electricity**
    - usage: 18.99 €/week (⬆️+11%)
    - of that for heating: 0.34 €/week
    - of that for hot water: 1.25 €/week
    - paid: -59.83 €/week 
    - ☀️ self produced: 79% (⬇️-2%)
    - emissions: 9 kg CO₂ₑ/week
- **💧 Water**
    - paid: 23.19 €/week (⬆️+54%)
    - emissions: 1.8 kg CO₂ₑ/week

### Expenditure
* 

### Income
* 115€ (shoe)

### Things that happened
#### In or around Kanthaus
* Kanthaus had its 6th birthday and we didn't really celebrate!
* people were in Leipzig at the bookfair
* first German lesson in Kanthaus
* some people were at Andrea's birthday in Leipzig
* multiple groups visited Kanthaus

#### Wider world
* visit to the Hackerspace in Leipzig


<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Tekmîl
* please don't leave toilet seats wet
* please put down the toilet seat after use (and maybe don't even pee while standing)
* [kito] I did not quite do my power hour task last week
* [cecilia] I did not put the bins out

## 2. This week planning

### People arriving and leaving
* **Mon.:** Martin comes back, kito leaves, silvan comes back
* **Tue.:** 
* **Wed.:** Larissa leaves for ~3 weeks, Antonin leaves
* **Thu.:** Silvan leaves
* **Fri.:** 
* **Sat.:** 
* **Sun.:** 
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
    https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
- Cecilia_WA Visitor: Days Visited threshold 27/21 (+29%)
- Dorota_Stilgar Visitor: Days Visited threshold 24/21 (+14%)
- Anneke Volunteer: Days Visited threshold 67/60 (+12%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Larissa]
    * 11:00 Power Hour
* Tuesday
    * black bin [Anneke]
    * 10:00 Cecilia's evaluation [Antonin]
    * 15:00 - 17:00 Open Tuesday [] 
* Wednesday
    * 12:00 - 17:00 MitMachCafé
* Thursday
    * 15:00 social sauna [skip]
    * 13:00 - 18:00 MitMachCafé
* Friday
    * organic waste [Cecilia]
    * Market pick-up [Dorota]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Cecilia]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* 

## 4. To do
_Newest tasks on top_
* [ ] curtain for the Cave
* [ ] maintenance of Phoenix bike - ask Martin?
* [ ] make K20-2 bathroom privacy indicator realiable - ask Tilmann
* [ ] fill holes in Snack kitchen floor where moths breed [Akos]
    * it started but it not done yet, unclear what is the right material
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I would redo the whole thing (but not now) 
* [ ] give away saved windows in K18 (a lot) 
     * maybe https://www.trashgalore.de/ ? [Martin]
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
* [Antonin] looking for a host for Jacki (they) during the building week
    * [Kito] does it, yay!
* [Arrakis] introduces himself

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Antonin] taking over the residency record
    * [Cecilia] can do it
* [kito] did we forget the weather?

### Round 3
* [Antonin] hosting Silvan

### Round 4
* [Antonin] wooden knives in the dishwasher

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Antonin
* **Tue.:** Nofal & Reem
* **Wed.:** Anneke, Arrakis
* **Thu.:** you?
* **Fri.:** Dorota
* **Open Tuesday:** Cecilia

### Unavailabilities
* **Mon.:** 
* **Tue.:** Dorota
* **Wed.:** 
* **Thu.:** Cecilia
* **Fri.:** Cecilia
* **Open Tuesday:** Dorota
* **Week:** kito, Larissa

### Result
- **Mon.:** Antonin
- **Tue.:** nofal & reem
- **Wed.:** Anneke, Arrakis
- **Thu.:** 
- **Fri.:** Dorota
- **Open Tuesday:** Cecilia

## 7. For next week
* 
