---
title: Coordination Meeting
date: "2021-05-24"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #192
- Date: 2021-05-24
- Facilitator: Antonin
- Notary: chandi
- <s>Mika caretaker: </s>
- Physical board caretaker: Nathalie
- Digital calendar: Zui
- Reservation sheet purifier: Doug
- Present: Clara, Talita, Larissa, Antonin, Nathalie, chandi, Silvan, Thore, Zui, romii

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_681224e664e7000c75bb0657c2ab165a.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.1 people/day (-4.3)
- **⚡ Electricity**
    - usage: 5.78 €/day (⬇️-26%)
    - paid: -4.44 €/day 
    - ☀️ self produced: 76% (⬆️+11%)
    - emissions: 5 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.97 €/day (⬇️-23%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- [Antonin] 10€ for cement & lime

### Income
- 50€ in shoe
- 600€ in the cash box

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 4017
- µCOVIDs used last week: 4371
- µCOVIDS balance from last week: -354
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 4646
- µCOVIDs/person mean for this week: 387 (12 people) 

### Things that happened
- Sewage pipe got fixed!
- IDAHIT (Demonstration for Rights of trans,queer,etc. people)
- Garden got many lovely Plants planted
- There was a yoga room building action
- Session on how to make a video
- wonderful lan party
- a closed tuesday happened
- critical masculinity round happened
- a trip to wolfsberg

## 2. This week planning

### People arriving and leaving
- **Mon.:** Anja leaves, Andrea left
- **Tue.:** (Doug has seminar), Larissa + Silvan leaves
- **Wed.:** (Doug has seminar)
- **Thu.:** (Doug has seminar) zui might leave
- **Fri.:** 
- **Sat.:**  romii leaves (maybe later)
- **Sun.:** Book-crew (3ppl) arrive
- **Mon.:** 
- **Some day:** zui might come back

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Talita Volunteer : Absolute Days threshold 80/60 (+33%)
- Chandi Member : Absolute Days threshold 185/180 (+3%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [Antonin]
    - <s>15:00 Sharing Event [Fac: ?]</s>
    - 16:00 Anja's evaluation [Clara]
- Tuesday
    - 17:00 Closed Tuesday [Doug]
    - KMW trip for building rubble
- Wednesday
    - Hausmüll [Antonin]
    - 10:00 Scaffolding take down action [Antonin]
- Thursday
    - 10:00 Power Hour [Fac.: Talita, DJ: chandi]
      - _Blow up pipelines? Tadzio Müller and Andreas Malm on what next for the climate movement_
        - https://podcast.dissenspodcast.de/123-climate
- Friday
    - 14:00 Tag der Nachbarn
    - 15:00 Talita's evaluation [Thore] 
- Saturday
    - Biotonne
    - 20:30 Project updates [Doug]
- Sunday
- Next Monday
    - 10:00 CoMe [Talita]
- Next week summary
    - Sozialsauna & **Gelbe Tonnen!**
    - How to DJ knowledge Skillshare

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)

## 3. Shopping plans
- [Matthias] Duvet covers 155x220, at least ~~2~~ 5 (we have 2 uncovered comfort size duvets and 0 spare covers), ~10€/set
    - can we check the covers in entrance first
    - larissa might have some in the garage
    - if people have some hidden ones at their parents place, get them here
    - no resistance for matthias going forward
- [chandi] Anti-Hausstaubmilben-Set (house dust mites) ~50€

## 4. Discussion & Announcements

### Round 1
- [Doug] In Hedgedoc, a `tab` will give 4 `spaces`. This is how indentation in markdown [should be done](https://github.com/MacDownApp/macdown/issues/681). Also, don't leave empty lines in a list—it breaks formatting.
- [Antonin] Janina would like to host Morgane (see request by email) from mid July to early August. Any concerns?
    - no concerns
- [clara] Working space for the book meeting (So)
    - Fansipan will be available during that time
- [chandi] Thore's Private Agreement expires soon, but keep it as an anti-allergic room?
  - a normal private room, except...
  - Don't use the bed with clothes you weared somewhere else
  - no feather & wool
  - I would buy an anti-hausstaubmilden bezug
  - [matthias] I think we already have some of this stuff
    - where? :D
  - -> no resistance of keeping it "hypoallergic"
  - Thore will update the agreement in the next weeks
  - No resistance for buying some anti allergic bed sheets

### Round 2
- [Antonin] how about getting an ADFC Leipzig membership?
    - would be 68€ per year for a household
    - There are a lot of cool groups out there. why this as a kanthaus expense and not private? 
        - Some reasons: Because it supports the bike lobby and we also might get insurance for riding bikes and using public transport
    - More Information required. Antonin wil lbring it up as an expendentire request next week
- [zui] scaffolding takedown prep
    - no prep meeting. people do the dependencies in small groups before
- [nathalie] yoga room: glas wool in the ceiling. 
    - Ceiling is open to the yoga room.
    - A wooden Board attached there to close it could seal it enough

### Round 3
- [Antonin] Would anyone like to help me finish the wall before Wednesday?
    - romii can help
- [Doug] Tea re-use: please only save tea on your plate shelf, or marked with your name & date! Also, it's already too warm to leave prepared food for days, or not cover food bins.
- [zui] newspaper abo: Idea was to try it out. I would cancel it, because I'm not motivated to pay it further. Get any other one?
- [Saying of the day] For laughing you have to go to the basement! (during come)

## 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)
https://kanthaus.gitlab.io/task-lottery/

### Volunteers
- **Mon.:** 
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** Matthias
- **~~Open Tuesday~~:** 

### Unavailabilities
- **Mon.:** Talita, chandi, Doug, Nathalie, Thore, Antonin
- **Tue.:** Doug, Talita, chandi, Nathalie, Antonin
- **Wed.:** Doug, Talita, Thore
- **Thu.:** Antonin, romii
- **Fri.:** romii
- **~~Open Tuesday~~:** 
- **Week:** Anja, Larissa, Silvan, Clara, Zui

## 6. For next week
- 
