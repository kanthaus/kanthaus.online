---
title: Coordination Meeting
date: "2022-11-21"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #270
* Date: 2022-11-21
* Facilitator: Maxime
* Notary: Antonin
* Mika caretaker: Kita
* Levi caretaker: Kita
* Physical board caretaker: Doug
* Digital calendar: Antonin
* Reservation sheet purifier: Janina
* Present: Maxime, Doug, Tilmann, Larissa, Janina, Antonin, Martin

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
- heating ~64€

![usage graph last 90 days](https://pad.kanthaus.online/uploads/cec040f9-4466-450f-9b17-ec7641dd6646.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.9 people/day (-4.1)
- **⚡ Electricity**
    - usage: 123.7 €/week (⏫+63%)
    - of that for heating: 78.46 €/week
    - paid: 86.48 €/week 
    - ☀️ self produced: 27% (⬇️-20%)
    - emissions: 64 kg CO₂ₑ/week
- **💧 Water**
    - paid: 23.15 €/week (⬇️-4%)
    - emissions: 1.8 kg CO₂ₑ/week

### Expenditure
- [tilmann] things for attic floor & related, will calculate the actual sum next week
    - most of it is already paid

### Income
- nothing in the shoe
- 50€ donation, thank you!

### Things that happened
#### In or around Kanthaus
- the super-size blankets got upgraded to the winter-edition
- The WWW and the CCC (Camel Cup Craze and Cold Critical Crass)
- critical mass!
- we have new doors for the attic, already started putting them in
- a four hour electro sorting party happened and most devices got sorted out
- special edition of Punk Rock Tresen

#### Wider world
- Mastodon is booming!
- more than 8 billion people on the planet

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->


## 2. This week planning

### People arriving and leaving
* **Mon.:** Silvan comes back, Andrea will come too
* **Tue.:** 
* **Wed.:** Anja might come for a night, Anneke returns at 21:15
* **Thu.:** 
* **Fri.:** 4 people of HZ Bauschirm arrive (right?)
* **Sat.:** 
* **Sun.:** some Blindspots people, kito included, might come in the evening for a short visit perhaps until monday noon
* **Mon.:** Doug leaves
* **Some day:** Silvan leaves for the weekend, Lara might come back at some point

### Weather forecast

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Temperatures around 0°C, somewhat sunny around the middle of the week

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):- Anja Volunteer : Absolute Days threshold 266/60 (+343%)
- Thore Volunteer : Absolute Days threshold 73/60 (+22%)
- Silvan Volunteer : Absolute Days threshold 68/60 (+13%)
- Larissa Member : Absolute Days threshold 202/180 (+12%)
- Kito Volunteer : Absolute Days threshold 61/60 (+2%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * Blue bin [Martin]
    * 10:00 CoMe [Maxime]
    * Monday Food Care [Janina, Martin]
* Tuesday
    * Black bin [Antonin]
    * 11:00 Larissa's evaluation [Antonin]
    * 10:00 - 13:00 Bitte wenden coworking @office? [Janina, Unkraut]
    * 15:00 - 17:00 Open Tuesday []
* Wednesday
    * 9:00 - 12:00 Attic cleanup! [Tilmann, Martin, you?]
       * it would be great to have more people participate!
    * before noon: drywall delivery
       * the panels will be stored in the K22 entrance temporarily, the goal is to carry them up to the attic as quickly as possible. Help is appreciated and can happen all day.
    * 18:00 SoLaWi exchange + KüfA @NDK [Janina, Anja, you?]
    * evening: 19:00 Punkrocktresen @NDK
* Thursday
    * Bio waste bin [Martin]
    * 10:00 Power Hour [Fac.: Antonin, DJ:]
    * 14:30 AG PSW meeting @Fansipan? [Janina, Zui, external]
* Friday
    * 16:00 Kita Christmas market @Burkartshain [Janina, Larissa, Bubis, you?]
    * 18:00 Leipzig Critical Mass [Martin, you?]
* Saturday
  
* Sunday

* Next Monday
    * 10:00 CoMe [Antonin]
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* longterm scheduling https://pad.kanthaus.online/KH-plenum#

## 3. Shopping plans
- [tilmann] 8€ CEE 380V wall socket, for insulation blowing machine https://www.ebay.de/itm/185661057345
- [tilmann] 39€ for 100m window sealing tape (D-Profil 9mm), to improve air-tightness of windows in heated rooms. https://www.ebay.de/itm/265336994917
    - [Antonin] support, I recently read a great post which encouraged to do that: [https://matthias-larisch.de/2022/09/29/fensterabdichtung.html](https://matthias-larisch.de/2022/09/29/fensterabdichtung.html) ;)
- [Janina] I often buy some dry ingredients such as rice or pasta, let me know if that should change
     - [various people] shopping locally is often as cheap as buying in big quantities and is easier

## 4. To do

_Newest tasks on top_
* [ ] treat mold in Ex-food-storage and hipster room
     * Those rooms need a fresh air supply (Zuluft)
     * Some more work would be welcome to finish connecting those rooms to the ventilation system
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]
* [ ] fix bike repair station instructions

## 5. Discussion & Announcements
- **put bigger discussions or decision making here: https://pad.kanthaus.online/KH-plenum#**

### Round 1
- [kito, janina] ToI! 
  * Location: Harzgerode, if no concerns are raised until friday
  * Topics/Sessions: Ukuvota will start after CoMe, inspiration from last year can be found [here](https://pad.kanthaus.online/toi2022-sessions) and [here](https://cloud.kanthaus.online/apps/polls/vote/12)
    * the ukuvota will have a week of proposal phase and week of voting phase
    * [Maxime] can we make sure we are in a heated space?
      * [Janina] yes
- [Antonin] please remember to turn off the electric heater above the bath tub when you leave. I have found it left on multiple times already.
    * various proposals about what to do about it
- [Maxime] privatized bathroom in the evening
  * changed the sign to say that bathroom privatization is not wished until midnight
  * [Larissa] the current times on the sign are indeed a trial and not set in stone
  * there is toothpaste in various other places in the house
  * Statement from Janina: we still like visitors!

<!-- check if anyone has a point that didn't speak already -->


### Round 2
- [janina] the next solawi group wants to come! ^\_^ anybody up for hosting in january?
  * 11th to 15th January (Janina cannot host but can do email communication)
  * one member of the group has been here already
  * 10-12 people
  * Doug offers to host with 80% likelihood, with a co-host
  * we'll somehow manage
- [kito] probably tomorrow i will start an ukuvota regarding the FFJ.  together with Doug, I decided to do just one proposal which I could also change if you talk to me, or you add your own proposal. proposal and voting time will be one week each. I'm not in KH until sunday/monday, so please write me a message if you have questions :) 
  * [Doug] there is the idea that we could aim to have one attic finished to have them sleep there
- [Doug] plans to do a CamelCup session arond noon to make it possible for unusual participants to join

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Janina
- **Tue.:** Larissa
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** Maxime
- **Tue.:** 
- **Wed.:** Doug, Antonin
- **Thu.:** Doug
- **Fri.:** Martin, Maxime, Doug
- **Open Tuesday:** Maxime
- **Week:** Tilmann

### Result

- **Mon.:** Janina
- **Tue.:** Larissa
- **Wed.:** Martin
- **Thu.:** Maxime
- **Fri.:** Antonin
- **Open Tuesday:** Doug

## 7. For next week
* 
