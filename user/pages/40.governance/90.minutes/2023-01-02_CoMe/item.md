---
title: Coordination Meeting
date: "2023-01-02"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #275
* Date: 2023-01-02
* Facilitator: Janina
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Tilmann
* Physical board caretaker: Doug
* Digital calendar: Maxime
* Reservation sheet purifier: Martin
* Present: Doug, Nathalie, Anja, Silvan, Martin, Chloé, Larissa, Tilmann, Maxime, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/c799a765-aeea-4bf8-bbeb-3584bb09017f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 5.1 people/day (-5.3)
- **⚡ Electricity**
    - usage: 82.66 €/week (⬇️-31%)
    - of that for heating: 45.94 €/week
    - paid: 58.03 €/week 
    - ☀️ self produced: 28% (⬆️+14%)
    - emissions: 42 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.88 €/week (⬇️-15%)
    - emissions: 1.0 kg CO₂ₑ/week

### Expenditure
- [tilmann] 25€ Kantenschutzprofil + Fugendeckstreifen for drywall
- [tilmann] 12€ for 5 kg Spachtelmasse
- [janina] 5€ work gloves size 7 in eco quality
- [janina] 30€ for bedsheets, a toilet brush and a 50x50 pillow case
- [maxime] 16€ for 25 FFP2 masks
- [maxime] 4€ for vitamin D
- [maxime] 22€+9€ for 40+5 covid tests

### Income

### Things that happened
#### In or around Kanthaus
- Covid, Christmas and New Years
- many delayed visits and cancelled leaves
- heating skillshare
- communal hiking trip + ferry ride
- quite some (board) games, walks and some movies

#### Wider world
- old pope died, probably other old people died as well

## 2. This week planning

### People arriving and leaving
* **Mon.:** Charly and Saranke arrive, Anja and Doug leave, Matthias comes for a day
* **Tue.:** Doug comes back
* **Wed.:** 
* **Thu.:** Tilmann's parents arrive
* **Fri.:** Janina's parents arrive, Emily_WA arrives
* **Sat.:** Chloé leaves, Anja and Antonin pass by
* **Sun.:** 
* **Mon.:** all parents leave
* **Some day:** Silvan and Larissa might leave for the weekend

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins
- Silvan Volunteer : Absolute Days threshold 110/60 (+83%)
- Martin Volunteer : Absolute Days threshold 86/60 (+43%)
- Nathalie

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Janina]
    * Monday food care [Doug]
    * Park trailer on even/this side [Maxime]
* Tuesday
    * AG PSW meeting [Zui, Janina, Charly from L]
    * 14:00 1st ever penny pickup! :tada: [Nathalie, Martin]
    * 15:00-17:00 Open Tuesday []
* Wednesday
    * 11:00 Silvan's evaluation [Janina]
    * 19:00 Punkrocktresen @D5
    * Park trailer on odd/that side [Maxime]
* Thursday
    * 10:00 Power Hour [Fac.: Martin , DJ: ]
    * 14:00 2nd penny pickup [Nathalie, Janina]
    * 15:00 Social sauna [Doug]
* Friday
    * Organic waste [Maxime]
    * Take down Christmas tree and eat galette des roi·enne·s
* Saturday
    * 11:00 @piano room: the volmem merge constitution change coworking session [antonin]
    * 14:00 3rd penny pickup [Nathalie, you?]
* Sunday
    * 16:00 Levi's birthday cake thing
* Next Monday
    * 10:00 CoMe [Doug]
* Next week summary
    * SoLaWi group comes
    * family absent
    * monthly plenum

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [maxime] lentils
- [janina] maybe more bedsheets

## 4. To do

_Newest tasks on top_
* [ ] Sign: Renew the "Klingel" - Sign (it's less and less readable)
* [ ] Light: elephant room
* [ ] Light: Yoga room 
* [ ] Replace shower curtain by (plexi)glass construction
* [x] fix bike repair station instructions [Doug]
* [x] treat mold in Ex-food-storage [Martin]
     * room needs a fresh air supply (Zuluft) [Tilmann]
     * some more work would be welcome to finish connecting this room to the ventilation system [Tilmann]
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

### Round 1
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->
- [tilmann] First penny pickup on Jan 3rd - who would like to go with Nathalie? 
  - [Martin] I signed up
- [janina] door tag problem
    - silvan updated the file, nathalie will test and report


<!-- check if anyone has a point that didn't speak already -->

### Round 2


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Martin
* **Tue.:** 
* **Wed.:** 
* **Thu.:** Janina
* **Fri.:** 
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** Doug
* **Tue.:** Larissa
* **Wed.:** 
* **Thu.:** 
* **Fri.:** larissa, Chloé, Doug
* **Open Tuesday:** larissa, Maxime, Chloé
* **Week:** Tilmann, Silvan, Nathalie, Anja

### Result
- **Mon.:** Martin
- **Tue.:** Chloé
- **Wed.:** Larissa
- **Thu.:** Janina
- **Fri.:** Maxime
- **Open Tuesday:** Doug

## 7. For next week
