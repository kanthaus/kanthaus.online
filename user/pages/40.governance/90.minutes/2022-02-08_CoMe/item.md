---
title: Coordination Meeting
date: "2022-02-08"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #230
* Date: 2022-02-08
* Facilitator: chandi
* Notary: Doug
* Mika caretaker: Janina
* Levi caretaker: Janina
* Physical board caretaker:
* Digital calendar: chandi
* Reservation sheet purifier: (Anja) (probably already done)
* Present: Tilmann, Silvan, Larissa, Andrea, Thore, Nathalie, Doug, Anja, Chandi

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf4800.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 3.3 people/day (-13.4)
- **⚡ Electricity**
    - usage: 68.34 €/week (⏬-46%)
    - of that for heating: 51.87 €/week
    - paid: 45.9 €/week 
    - ☀️ self produced: 26% (⬆️+13%)
    - emissions: 36 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.68 €/week (⏬-78%)
    - emissions: 0 kg CO₂ₑ/week


### Expenditure
- [Antonin] 20€ for frying oil, toilet paper and spüli
- [ToI] 50€ for food, 450€ for the villa

### Income
- none

### Things that happened

#### In or around Kanthaus
- a great communal time of introspection!
- another chimney almost torn down!
- found a lot of food, especially veggies
- the house was resting quite alone

#### Wider world
- might be soon war in the ukraine 

## 2. This week planning

### People arriving and leaving
* **Tue.:** Anja leaves (maybe tomorrow)
* **Wed.:** Matthias, Larry comes, CiJuAl come
* **Thu.:** Maxime and Kito come, the other KIDZ come
* **Fri.:** Larissa and Silvan leave
* **Sat.:** 
* **Sun.:** KIDZ leave, Anja, Larissa und Silvan come back
* **Mon.:** 
* **Some day:** Clara comes

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
windy winter weather, warm in the next days, colder towards the weekend


### Evaluations and check-ins
:judge: Due for evaluation (and seen within the last 7 days):

- *Nathalie* _Volunteer_ : _Absolute Days_ threshold 75/60 (+25%)
- *Tilmann* _Member_ : _Absolute Days_ threshold 193/180 (+7%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 17:00 ROAW meeting [Nathalie, Antonin]
* Tuesday
    * 10:00 CoMe [Chandi]
    * 18:00 Workout@Yogaroom
* Wednesday
    * 10:00 MCM monthly meeting [tilmann]
    * Repark cars on the opposite side [?]
    * 14:00 TOI meeting [Nathalie]
* Thursday
    * 10:00 Power Hour [Fac.: Silvan, DJ: radio-garden]
    * 15:00 social sauna [Larissa]
* Friday
    * yellow bin [Thore]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Nathalie]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [?] Wattestäbchen!

## 4. To do
* [ ] repair WiFi in the Hipster
* [ ] remove washroom chimney [chandi]
* [ ] repair Schaukasten 
* [ ] Clean K18 garden
* [ ] Making new signs for the commode in the hallway
* [ ] Install light in the freeshop hallway
* [ ] Door-closer for Elefant-staircase door
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Install light in the hallway of the K20-1 flat
* [ ] Install light in the hallway of the K20-2 flat

### more advanced tasks
* [ ] continue Grundstücksverschmelzung K20 & K22 combining both properties)


## 5. Discussion & Announcements

### Round 1
* [tilmann] lending our scaffolding to frei feldlage until we need it again?
    * no resistance
    * seems fine for at least one year
    * would be good if they replace parts if they break
    * they ideally organize transportation and inventorize it
    * we can help with loading
* [nathalie] TOI 'aftermath' scheduling, discuss here or bring it to TOI meeting first?
    * day proposal: morning and afternoon sessions, lunch/activity inbetween, communal dinner and maybe evening activity
    * maybe 19.02.? there is critical mass and acrolotl meeting
        * [janina] fears for super low cm participation if there's such a full day around
    * Date to be confirmed via ToI meeting/emoji-vote
* [janina] offer lantern and ex food storage to cijual and let them choose?
    * all good
* [mika&levi] we're a bit sick and want to sleep in the communal sleeping room with our parents for the next days. please join us if you feel like sleeping around 9pm and waking up before 8am! :)
    * (Tilmann & Chandi to coordinate about sleeping arrangements generally.)
    * chandi put effort to make the communal dust free, and would prefer to stay in the communal until Friday and feels a bit pushed away
    * if chandi moves to a different room for the KIDZ meeting, he could do it already today
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Larissa] Let's use the sleeping preference sheet this week!

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)
### Volunteers
* **Tue.:** 
* **Wed.:** Larissa
* **Thu.:** Andrea
* **Fri.:** 
* **Open Tuesday:** Anja

### Unavailabilities
* **Tue.:** chandi
* **Wed.:** Doug, chandi
* **Thu.:** Doug
* **Fri.:** Doug
* **Open Tuesday:** Thore, chandi
* **Week:** Silvan, Tilmann

### Result
- **Tue.:** Nathalie, Doug
- **Wed.:** Larissa
- **Thu.:** Andrea
- **Fri.:** Thore, Chandi
- **Open Tuesday:** Anja


## 7. For next week
* [Doug] suggest that all resource graph lines are made thicker, and the yellow for solar is darker.
    * [Antonin] +1
* [matthias, tilmann] (for Expense request section): 140€ for new Abluftfilter for the ventilation system (should last for about 8 years)