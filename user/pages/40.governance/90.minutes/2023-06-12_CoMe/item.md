---
title: Coordination Meeting
date: "2023-06-12"
taxonomy:
    tag: [come]
---

# CoMe #297
* Date: 2023-06-12
* Facilitator: Martin
* Notary: Martin 
* Children caretaker: JaTiLa
* Physical board caretaker: Anneke
* Digital calendar: Martin
* Reservation sheet purifier: Cecilia 
* Present: Anneke, Cecilia, Martin, Maxime, Axel, Manuellaww


## 0. Check-in round

## 1.  Last week review
* stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/afffdc79-d987-45c9-88c0-de3bd25920ac.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 7.6 people/day (-2.9)
- **⚡ Electricity**
    - usage: 21.52 €/week (⬇️-8%)
    - of that for heating: 0.53 €/week
    - of that for hot water: 1.55 €/week
    - paid: -46.51 €/week 
    - ☀️ self produced: 71% (⬇️-12%)
    - emissions: 10 kg CO₂ₑ/week
- **💧 Water**
    - paid: 14.12 €/week (⬇️-25%)
    - emissions: 1.1 kg CO₂ₑ/week

### Expenditure

### Other Income

### Things that happened

#### In or around Kanthaus
- trip to the lake
- tripto the concert (Embryo @ GarageOst)
- Carla Cargo got reintroduced to service
- Poli got new parts at grumpy self help repair shop

#### Wider world
- Fire in Leipzig
- Forests burning in Canada
- Big Italy flooding

<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Kate_WA arrives
* **Tue.:** 
* **Wed.:** Kito comes back (late)
* **Thu.:** 
* **Fri.:** Blindspots group arrives (8-10 ppl)
* **Sat.:** Antonin and Àkos arrive
* **Sun.:** Blindspots group leaves
* **Mon.:** 
* **Some day:**


### Weather forecast

Nice warm temperatures with increasing chances of some rain towards the weekend. 

### Evaluations and check-ins

- Silvan Visitor
- Martin Volunteer : Days Visited threshold 88/60 (+47%)
- Cecilia_WA Visitor : Days Visited threshold 27/21 (+29%)
- Dorota_Stilgar Visitor : Days Visited threshold 27/21 (+29%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Martin]
    * 11:00 PowerHour 
    * Monday food care [Not neccesary]
* Tuesday
    * 15:00 - 17:00 Open Tuesday [lottery]
    * 21:00 - "How to blow up a pipeline" at Conne Island
* Wednesday
    * 10:30 Silvan evaluation [Maxime]
    * 19:00 Punkrock-Tresen @D5
* Thursday
* Friday
    * Yellow trash [Maxime]
* Saturday
* Sunday
* Next Monday
    * CoMe [Anneke]
* Next week summary

To be scheduled:
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans

- Flour and oats [Anneke]

## 4. To do
_Newest tasks on top_
* [ ] fill holes in Snack kitchen floor where moths ~~might~~ breed
* [ ] fix Daria trailer back lights
     * [Martin] checked. Both rear lights burned. Second time it happens. I have no idea what is causing it. I thought it was some faulty cable, but I'm pretty sure it's not that now. 
* [ ] give away saved windows in K20 basement (a lot) maybe https://www.trashgalore.de/? (martin)
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] de-clog compost toilet drain
* [ ] modify the task lottery to prioritize open tuesday [Maxime]
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

### Round 1
* [Antonin] there are people and groups looking for hosts: Could You Be One?
  * 11.07-14.07: Feuerqualle (5 people). Request the use of the Cloud and Dragon rooms
  * 23.07-04.08: Olli (1 person)
  * 28.07-30.07: SoLaWi learning group (10-20 people)
      * [Anja] this is my group - I'm not part of the place-finding-AG so I guess David didn't know how well I know KH :)
Also, we all haven't met eacht other yet. 
It's definitely not 20 people; so far 9 people said for sure they'd come, 4 people maybe. I guess I could host, but not sure how cool it is with such a big group.
but, end of july means loads of amazing veggies that we can bring :) 
    *Nobody present was available to host.
    

* [Cecilia] Asked about the decision to be able to register at KH. At the "castle" is not possible. We refreshed the legal comments about the rights that would be acquired. She said there is no rush, and we agreed to review the issue early July after her first paycheck. 



## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Result
- **Mon.:** Anneke
- **Tue.:** Axel
- **Wed.:** Cecilia
- **Thu.:** Maxime
- **Fri.:**
- **Open Tuesday:** Manu
