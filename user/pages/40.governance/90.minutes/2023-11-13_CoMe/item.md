---
title: Coordination Meeting
date: "2023-11-13"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #319
* Date: 2023-11-13
* Facilitator: Janina
* Notary: Janina
* Physical calendar: Dorota
* Digital calendar: Antonin
* Reservation sheets: Loup
* Weather forecast: Tilmann
* Present: Klara, Martin, Larissa, Dorota, Venla, Antonin, Janina, Loup, Tilmann, Bodhi, Riell

---

<!-- Time for reflection -->

## 0. Check-in round
[kito] i'm not in KH!

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/cb922048-4ced-4714-9744-f368e5a3056f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.7 people/day (-1.0)
- **⚡ Electricity**
    - usage: 65.71 €/week (⬆️+7%)
    - of that for heating: 18.67 €/week
    - of that for hot water: 5.11 €/week
    - paid: 53.39 €/week 
    - ☀️ self produced: 46% (⬇️-6%)
    - emissions: 31 kg CO₂ₑ/week
- **💧 Water**
    - paid: 17.3 €/week (⬇️-2%)
    - emissions: 1.3 kg CO₂ₑ/week

### Expenditure
- [tilmann] 14€ bike v-brake pads
- [tilmann] 64€ white wall paint
- [bodhi] 15€ vinegar and oil
- [martin] 10€ flour and something
- [janina] 15€ nail clippers

### Income
- 360€ (from DRK)

### Things that happened
#### In or around Kanthaus
- the first part of our journey to refocus our purpose (a.k.a. toi#1)
- social sauna with many interesting topics
- a house-bus node appeared in the silent office
- mushroom picking action
- a big and productive foodsharing district meeting
- more attic work again
- birthday brunch at anja's followed by party at chandi's
- some sickness became apparent in the house

#### Wider world
- run-off electrion for president of Argentina next Sunday

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Popcorn of feedback
<!--
Is there any feedback you would like to give to others - the whole groups or individuals? Is their any reflection about yourself you would like to share with the others? If you would like to give feedback to a person in the room, please do it in third person.
-->
- bathroom door needs to stay closed to contain radiator warmth
- please turn down bike gears before parking, so that starting again isn't so annoying
- nice casual time at anja's party, thanks all!

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** Kate returns, Doug returns, Lise arrives
* **Wed.:** Klara leaves
* **Thu.:** kito arrives late, Bodhi (and probably Lise) leave, Zui comes by for the day
* **Fri.:** FFJ group arrives (orga in the morning, group around 3-4pm), Larissa leaves, Dorota leaves for a week
* **Sat.:** 
* **Sun.:** FFJ group leaves (after lunch), larissa comes back
* **Mon.:** 
* **Some day:** 

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->

warm Tuesday, colder towards the weekend
 
### Evaluations and check-ins

- Antonin Volunteer: Days Visited threshold 66/60 (+10%)
- Thore Visitor: 0 days until Days Visited threshold (21)
- Loup_WA Visitor: 3 days until Days Visited threshold (21)
- Nathalie Visitor: 3 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Janina]
    * 11:00 - Power Hour
    * 13:00 - Riell check-in
    * 14:30 - Ineke moving action [Antonin]
* Tuesday
    * 10:00 - attic work session [Tilmann, you?]
    * 11:00 - Schadstoffmobil @ Rosa-Luxemburg-Str. (gegenüber Nr. 12–13)
    * 18:00 - Penny pickup [Janina]
    * 18:00 - 20:00 Ideenwerkstatt @MitMachCafé
* Wednesday
    * 10:00 - attic cleaning session #1 [Janina, you?]
    * 12:00 - 17:00 MitMachCafé
    * 18:00 - Punkrocktresen @ D5
* Thursday
    * 10:00 - attic cleaning session #2 [Janina, you?]
    * 13:00 - 18:00 MitMachCafé
    * 18:30 - FLINTA thai boxing @ D5
    * 19:00 - Penny pickup [Martin]
* Friday
    * Yellow bins [Martin]
    * 10:00 - PlaMe [Tilmann]
    * 16:00 - saving more stuff from a flat [Martin, Kate]
* Saturday
    * 14:00 - 17:00 Flea Market @MitMachCafé [Kate]
* Sunday
* Next Monday
    * CoMe [Martin]
* Next week summary
    * MSV group over the weekend
    * no trains to Leipzig all week
    * public holiday on Wednesday 22

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans

## 4. To do
*See analog list in the elephant room*

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [larissa] Update MSV-Meeting: see https://yunity.slack.com/archives/C3RS56Z38/p1698921422872049 we will be around 30 people now for sure. I would heat the yoga room and attic before. Also: any thought/comments towards the group to keep in mind that maybe didn't need to be considered in the past because of smaller group size?o
    - help with carrying things around as preperation would be very much appreciated next week
    - more talk in next come
- [kito] FFJ get together at the weekend
    - i would really appreciate, if someone else could do the house tour, friday at 6pm. to deepen the connection between FFJis and the house from the beginning. FFJ orgacrew would take care of cooking in the meantime.
      - [Kate] Martin and I tentatively have a arrangment to complete the flat clearance at 4pm on Friday. If no one else volunteers, I might be able to squeeze it in, but it's a bit of a push. I could do Saturday morning no worries.
      - [janina] if someone plays with the kids during the time, I can do it
    - also a round of hello during the dinner on friday would be great
    - 11 participants + 5 orga people expected, but not all the whole time
    - [kate] venla and loup need to be aware to move out of communal sleeping
        - [kito] i think it's fine, if the communal is not "FFJ only", but some would definitely sleep there. apart from that i would like to reserve the lantern and put the big matress inside, that worked out very well with the last group. do you think there is more sleeping place orga needed before friday? i think it could also work like that..
        - [Kate] Sorry Kito when I put this on it was with reference to Larissa's group. Still good for Venla and Loup to know.
        - [Janina] so communal and lantern. and if more is needed it will hopefully work itself out spontaneously. who worries about their sleepong spot for that time is encouraged to reserve beforehand! 
- [janina] Ineke's moving action today?
    - [Antonin] yes! it's at 14:30, we'll meet in front of sven's flat and then coordinate there. I'll have a van and the new flat is close to Bürgermeister-Schmidt-Platz. Please come if you want to support, the more the merrier! :)
- [tilmann] attic painting action today or tomorrow?
    - [loup] everything's plastered now, but the last finish isn't done. if we want to be perfect there's some more work to be done
    - [antonin] can't it be parallelized?
    - [larissa] no, then we have dust in the paint
    - [kito] will it be plain white or with texture like in Martins slack post? (#kanthaus-attic)
        - nobody really knows how to do texture in a way that looks nice
    - we can still paint next week. then the attic is clean and we can just cover the floors.
    - let's not do it this week and rather talk more about it in plame.
- [kate] I will take a table at the Mitmach Cafe flohmarkt on Saturday. I could probably do with some help (at least getting stock there and back) Of course, I am grateful if someone wants to be there longer because it's more fun. Just let me know if you are available on slack or in-person during the week!
- [dorota] I have borrowed a mattress and now I need a bed frame. I saw one lying around.
    - Just take it! :)

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Janina] Attic cleaning action plan
- [kito] reminder of the Evidence Safari: https://yunity.slack.com/archives/C3RS56Z38/p1699778327130029
- [Larissa] let's go to RIFF together. When should we do it? 6th/7th dec or 19th/20th dec?
    - let's decide on Friday, maybe have a dudle thingy beforehand

### Round 3


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Janina
* **Tue.:** 
* **Wed.:** 
* **Thu.:** maxime
* **Fri.:** FFJ, FFJ
* **Open Tuesday:** opentuesdaykate

### Unavailabilities
* **Mon.:** kate
* **Tue.:** kate, Klara
* **Wed.:** Antonin, Riell, Klara
* **Thu.:** Antonin, Klara
* **Fri.:** kate, Antonin, Klara
* **Open Tuesday:** Klara
* **Week:** Tilmann, Larissa, Dorota

### Result
- **Mon.:** Janina, Antonin
- **Tue.:** Riell, Martin
- **Wed.:** kate, Venla
- **Thu.:** maxime, Loup
- **Fri.:** ffj, ffj
- **Open Tuesday:** opentuesdaykate, Bodhi


## 7. For next week
