---
title: Coordination Meeting
date: "2023-08-07"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #305
* Date: 2023-08-07
* Facilitator: Maxime
* Notary: Kito
* Mika + Levi caretaker: Kita
* Physical board caretaker: Doug
* Digital calendar: Antonin
* Reservation sheet purifier: Anneke
* Present: Martin, Antonin, Anneke, Doug, Bodhi, Janina, Rudi, Juli, Lena, Kito, Tilmann, Maxime, Nofal

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

![usage graph last 90 days](https://pad.kanthaus.online/uploads/59f42850-2904-4321-abf3-e5443ae88cca.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 14.1 people/day (+6.1)
- **⚡ Electricity**
    - usage: 27.47 €/week (⬆️+22%)
    - of that for heating: 0.41 €/week
    - of that for hot water: 1.8 €/week
    - paid: -13.15 €/week 
    - ☀️ self produced: 67% (0%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 23.01 €/week (⬆️+40%)
    - emissions: 1.8 kg CO₂ₑ/week

### Expenditure
- [Antonin] 248€ Complete fuse box for K-20 attic
- [Antonin] 104€ for 11 plates of Gipskarton
- [Tilmann] 40€ drywall Spachtel 25kg
- [Tilmann] 40€ 12x good smaller-sized work gloves
- [Anneke] 40€ rice, oats, flour, spreads, ........

### Income
- no income was registered

### Things that happened
#### In or around Kanthaus
- Mitmachcafé got attacked with paint :/
- Bike repair station got vandalized and fixed again
- a lot of people arrived
- a lot of dancing sessions in the garden where people moved things
- the workshops looks quite clean

#### Wider world

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Tekmîl
Skipped at this CoMe


## 2. This week planning

### People arriving and leaving
* **Mon.:** Bodhi leaves
* **Tue.:** Cecilia comes back, Dorota too
* **Wed.:** kito leaves
* **Thu.:** 
* **Fri.:** Martin leaves
* **Sat.:** 
* **Sun.:** Martin returns, Alejandro_Tin arrives
* **Mon.:** 
* **Some day:** Antonin leaves in the weekend

### Weather forecast
- it will get warmer eventually!!!!!!!!!!!!
- not so much rain
<!-- 
    https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597
-->

### Evaluations and check-ins
Skipped at this CoMe

- ASZ1 Visitor: Days Visited threshold 39/21 (+86%)
- ASZ2 Visitor: Days Visited threshold 39/21 (+86%)
- Anneke Volunteer: Days Visited threshold 84/60 (+40%)
- Larissa Member: Days Visited threshold 182/180 (+1%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 9:00 CoMe [Maxime]
    * 9:30 Distributing Power Hour tasks and filling repro work program []
* Tuesday
    * 15:00 - 17:00 Open Tuesday [lottery] 
* Wednesday
    * 12:00 - 17:00 MitMachCafé
* Thursday
    * Yellow Bins [Maxime]
    * 13:00 - 18:00 MitMachCafé
* Friday
* Saturday
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Anneke]
* Next week summary

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- MAYBE paint

## 4. To do
_Newest tasks on top_
* [ ] Fix the black cupboard for hats, gloves and scarves in K20 entrance which is slowly falling apart
* [x] Fix bicycle holder in right shed [Tilmann]
* [x] Fix the projector in Piano room [Tilmann]
    * Done, HDMI is broken but VGA works
* [x] trash tour to special trash depo ~~(also take unkraut's fridges)~~ [Antonin & Bodhi]
* [ ] fix window in Compost toilethier ist bodhi, ragnars sohn.
 du hast ja bestimmt auch schon den Abschiedsbrief gelesen und so...
* [ ] curtain for the Cave
* [ ] make K20-2 bathroom privacy indicator realiable
    * [Tilmann] what kind of problems did you encounter? I tried to simplify the logic to make it more reliable already, but I wonder if the motion sensor could be flaky. 
    * [Martin] I know Dorota has been working on it. 
* [ ] fill holes in Snack kitchen floor where moths breed [Akos]
    * it started but it's not done yet, unclear what is the right material
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing
* [ ] give away saved windows in K18 (a lot) 
     * maybe https://www.trashgalore.de/ ? [Martin]
* [ ] give away roof windows (in K18)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->
### Round 1
* [Anneke] thank you from my mum
* [kito] first aid kits overview
    * two suitcases, one in the elephant room, one in the K20-2 bathroom, some stuff in the kitchen next to the door, more in the medical storage
* [Janina] k20 entrance
    * please don't leave stuff in the marked areas, please mark your shoe spot
* [Martin] please clean the drain of the shower after showering
    * and clean the floor
     
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [kito] freeshop needs sorting - who's up for that?
    * Doug could give an introduction and do some, Janina is also interested


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Martin
* **Tue.:** Nofal
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Andrea
* **Week:** 

### Unavailabilities
* **Mon.:** Maxime, Antonin, Linnea, Rudi
* **Tue.:** Antonin, Lena
* **Wed.:** Linnea, Doug, Juli
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Maxime, Antonin
* **Week:** Tilmann, Bodhi

### Result
- **Mon.:** Martin, Janina
- **Tue.:** Nofal, Kito
- **Wed.:** Anneke, Antonin
- **Thu.:** Maxime, Rudi
- **Fri.:** Doug, Lena
- **Open Tuesday:** andrea, Linnea


## 7. For next week
* 
