---
title: Coordination Meeting
date: "2022-08-29"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #258
* Date: 2022-08-29
* Facilitator: maxime
* Notary: Martin
* ~~Mika caretaker: ~~
* ~~Levi caretaker: ~~
* Physical board caretaker: Eric
* Digital calendar: Maxime
* Reservation sheet purifier: Martin
* Present: Maxime, Eric, Martin

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/23e0f34b-d5d7-4677-8158-a739278080a9.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 8.0 people/day (-2.9)
- **⚡ Electricity**
    - usage: 31.81 €/week (⬇️-10%)
    - of that for heating: 2.97 €/week
    - paid: -35.16 €/week 
    - ☀️ self produced: 65% (⬇️-1%)
    - emissions: 10 kg CO₂ₑ/week
- **💧 Water**
    - paid: 14.75 €/week (⬇️-8%)
    - emissions: 1.1 kg CO₂ₑ/week

### Expenditure

* 80€ ebike parts and other spares[Martin]

### Income

* 10€ (shoe donation)

### Things that happened
#### In or around Kanthaus
- people went collecting stuff at the Highfield festival
- the aformentioned stuff was sorted and stored: in particular, we don't expect to have to buy beer for the coming year \o/

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* see https://de.wikipedia.org/wiki/Wikipedia:Hauptseite

## 2. This week planning

### People arriving and leaving
* **Mon.:** Zui comes back
* **Tue.:** Martin leaves, kito comes back, Larissa leaves
* **Wed.:** Alex, Cille & Jums arrive, Andrea comes back
* **Thu.:** 
* **Fri.:** 
* **Sat.:** Nathalie & Thore come back
* **Sun.:** Alex, Cille & Jums probaby leave, Larissa comes back
* **Mon.:** 
* **Some day:**

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * ~~Post-Sunday-Dumpster-Diving Food sorting/washing? [?]~~
    * Park KMW & trailer on even/this side [maxime]
* Tuesday
	* Black bin [Martin]
    * 15:00 - 17:00 Open Tuesday [Eric]
* Wednesday
    * ~~Park KMW & trailer on odd/that sidecht []~~
* Thursday
    * 10:00 Power Hour [Fac.: Eric, DJ: ?]
* Friday
	* Biotonne [maxime]
    * 12:00 Market pickup [Eric]
* Saturday
    *
* Sunday
    * Park KMW & trailer on odd/that sidecht [maxime]
* Next Monday
    * 10:00 CoMe [Eric]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans

## 4. To do
_Newest tasks on top_
* [ ] replace broken window handle in Yoga Room (left side)
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
    * [Antonin] I had a look, the sink does not seem blocked to me - did someone fix it already?
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put on and off?
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
<!-- check if anyone has a point that didn't speak already -->
* [anja] reminder that there is an apple tree in Doug's garden that might be ready to get harvested soon

### Round 2


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Eric

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** Martin

### Result


## 7. For next week
* 
