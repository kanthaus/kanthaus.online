---
title: Coordination Meeting
date: "2023-10-16"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #315
* Date: 2023-10-16
* Facilitator: Doug
* Notary: Doug 
* Physical calendar: Larissa
* Digital calendar: Antonin
* Reservation sheets: Doug
* Weather forecast: -
* Present: Doug, Larissa, Yuliya, Kate, Antonin

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

![usage graph last 90 days](https://pad.kanthaus.online/uploads/925d825a-3482-46a7-82f7-627ffe2b7ec0.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 6.0 people/day (-6.9)
- **⚡ Electricity**
    - usage: 27.9 €/week (⬇️-19%)
    - of that for heating: 0.6 €/week
    - of that for hot water: 0.84 €/week
    - paid: -1.42 €/week 
    - ☀️ self produced: 55% (⬇️-12%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 11.94 €/week (⏬-47%)
    - emissions: 0.9 kg CO₂ₑ/week

### Expenditure
- not
- ne

### Income
- 130€

### Things that happened
#### In or around Kanthaus
- skipped

#### Wider world
- skipped

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Popcorn of feedback
- skipped

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** JaTiMiLe arrive
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Sat.:** kito arrives, doug leaves
* **Sun.:** 
* **Mon.:** 
* **Some day:** Antonin leaves

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->
_gets colder, rainer and cloudier. autumn is fully here. but wednesday will be good._
 
### Evaluations and check-ins

- Larissa Member: -50 days until Days Visited threshold (180)
- Maxime Member: -30 days until Days Visited threshold (180) - not here
- Andrea Volunteer: -11 days until Absolute Days threshold (365) - not here

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Doug]
    * 11:00 - Power Hour
* Tuesday
    * 15:00 - 17:00 Open Tuesday
    * 17:00 digital seminar from Dezentrale: "Mit dem Dorf in Kontakt kommen – oder für immer die Neue sein?""
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * 18:00 - Punkrocktresen
* Thursday
    * Finance-Day [Larissa + zui]
    * 10:00 - 11:30 KH and visitors workshop [Kate]
    * 13:00 - 18:00 MitMachCafé
    * 18:30 - FLINTA thai boxing @ D5
* Friday
    * Gelbe Tonne [Kate]
    * 10:00 - PlaMe with HKW MV [Doug] 
        * [kito] i would like to participate remotely
* Saturday
* Sunday
    *  
* Next Monday
    * 10:00 CoMe [Larissa]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
* [Martin] 44 euros for various drill tools that we currently don't have.
    * we trust that it's reasonable
* [Martin] 16 euros for a few E27 light bulb holders for 60W led lamps. 
    * we trust that it's reasonable

## 4. To do
_Newest tasks on top_
* [ ] proof-reading heating page in handbook https://handbook.kanthaus.online/technical/heating.html [kate]
* [ ] fix/improve K20-1 staircase night light (can ask Tilmann for details about wiring)
* [ ] put kanthaus logo on the new beer benches and tables
* [ ] curtains for the dragon room that prevent passersby from staring inside
* [ ] cut branch from tree in the compost corner (after leaves fall)
* [ ] cover the pipes from the rest of the room in the snack kitchen (wooden board angle?) []
    * [tilmann] maybe directly go for drywall as final measure?
      * [maxime] that would be the right but more difficult remediation indeed. I looked at options to hack a wooden corner, but didn't find satisfying options in the wood shed, so I'll leave there.
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing. Also, use a diode to prevent over voltage. 
* [ ] replace shower curtain by (plexi)glass construction

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
* [Larissa] should we start heating? it feels like it's starting to get cold inside
    * Martin did some stuff, Kate will have a look at handbook page and Tilmann will get back tomorrow
* [Kate] a big amount of sorting out needed in FShop. i would like to use the cave private to help sort out and also to resurrect it as a community space during opening - any resistance?
    * proposal 1: Kate to be able to use cave for 4 weeks for stuff sorting, as far as possible leaving the bed (and a passage way to it) free in case needed
        * approved
    * greater clarity about k18 would be helpful -> Doug to get info from others to draft a plan

<!-- check if anyone has a point that didn't speak already -->

### Round 2 
* [Kate] new workaway visitor request from Loup - any resistance
    * no resistance :)

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Kate
* **Tue.:** Doug, Yuliya
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Larissa
* **Open Tuesday:** KateOpenTuesday, Antonin

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:**
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** 

### Result
* **Mon.:** Kate
* **Tue.:** Doug, Yulya
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Larissa
* **Open Tuesday:** KateOpenTuesday, Antonin


## 7. For next week
*
