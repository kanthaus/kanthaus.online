---
title: Coordination Meeting
date: "2023-03-06"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #283
* Date: 2023-03-06
* Facilitator: Martin
* Notary: Janina
* Children caretaker: Kita
* Physical board caretaker: Larissa
* Digital calendar: Tilmann
* Reservation sheet purifier: Veronica
* Present: Larissa, Jums, Veronica, Maxime, Anneke, Tilmann, Martin, Janina

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats

**Present:** 11.6 people/day (+9.4)
- **⚡ Electricity**
    - usage: 203.43 €/week (⏫+141%)
    - of that for heating: 97.5 €/week
    - paid: 105.56 €/week 
    - ☀️ self produced: 42% (⬆️+9%)
    - emissions: 63 kg CO₂ₑ/week

### Expenditure
- 22€ toilet paper, flour and margerine [Martin] 
- 196€ ebike conversation kit [Tilmann]
- 15€ building rubble disposal [Janina, Maxime]
- 60€ for wet wooden trash [Anneke, Bodhi]

### Income
- 8€ deposit (pfand) [maxime]


### Things that happened
#### In or around Kanthaus

* two trash tours that started preparing the garden for spring
* Küfa happened with two people coming (even helped cooking) through the telegram-küfagroup and Ilona
* social sauna with informal part in the sun (and then some unexpected deep topics)
* How to Solizimmer (in L)
* new attic floor work started
* Lantern door was fixed so it doesn't open by itself anymore
* Workshop door opening knob fell off and a new one was put on again
* Bad oat milk was trashed and new one arrived


#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* turkish football fans revolt at the stadium: solidarity with earthquake victims and public demand for government resignation
* a massive fire raced through a crammed refugee camp for Rohingya Muslims in southern Bangladesh
* general strike starting in France on Tuesday
* big strikes in public transport sector in Germany
* global climate strike last Friday

## 2. This week planning

### People arriving and leaving
* **Mon.:** maxime and jums might leave in the evening (or else, Tuesday morning), Andrea spends the night
* **Tue.:** Anjas classmates come for the night (2-4pp)
* **Wed.:** Antonin comes, Anneke leaves (or Thursday)
* **Thu.:** Veronica leaves
* **Fri.:** Anja leaves
* **Sat.:** 
* **Sun.:** 
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_Pretty stable winter weather with minimums around 0C° and rain here and there. Next week though..._

### Evaluations and check-ins
- Silvan Visitor : Days Visited threshold 42/21 (+100%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Martin]
	* Monday stuff care [Larissa]
    * Park trailer on that side [already done]
* Tuesday
    * 10:00 - 13:00 Attic work session [Tilmann, you?]
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * Women's day
    * Park trailer on other side [not needed]
    * 10:00 - 13:00 Attic work session [Tilmann, Janina, you?]
    * 15:00 Dragon room emptying action
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Janina]
    * Evening: Antonin's backstory and party
    * bring out yellow bins [Martin]
* Friday
    * 10:00 PlaMe [Martin]
        * kito would like to join remotely
    * 17:00 @Leipzig FTT [Janina]
* Saturday
    * 10:00 - 13:00 Attic work session [Tilmann, you?]
* Sunday
* Next Monday
    * 10:00 CoMe [Larissa]
* Next week summary
    * 2 small groups coming

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans

- Ei Ersatz Pulver is empty (backing) fyi - probably not needed?

## 4. To do
_Newest tasks on top_
* [x] fix K20-2 bathroom light annoyances [Tilmann]
* [x] repair the RFID door tags - again -.- [Antonin]
* [x] exchange the strings of light-brown guitar [Nathalie]
* [x] bring building rubble to the dump [Lots of people]
* [x] oat milk ~~inspection~~ trashing [Larissa, Silvan + kids]
* [x] deep-clean shower tap to make it easier to adjust temperature [Martin]
* [ ] modify the task lottery to prioritize open tuesday []
* [ ] General trash tour []
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom [Larissa]
* [ ] Light: elephant room
* [ ] Light: Yoga room 
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
<!-- check if anyone has a point that didn't speak already -->
- [Doug] Proposal that mastodon posts are signed (even if just a pseudonym). This allows more freedom in what and how people post. Anonymous posts automatically seem corporate, and I find that a more stressful way to do social media, because it requires more control.
    - people are supportive or don't care
- [Larissa] Sauna trip to Oschatz?
    - this weekend?
    - we could share a train ticket and/or cycle
    - there is interest
    - Larissa will make a Slack post to further coordinate
- [Larissa] stuff in the dragon room
    - Larissa will put the furniture in the free shop right after CoMe, people can get it out again if they want specific pieces
    - Feuerqualle food will stay thzere until the FQ group comes next week
    - oat milk will go to the basement
    - there's an action on Wednesday to make the room nice and cleanable

### Round 2


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)
 
### Result
- **Mon.:** Anneke
- **Tue.:** Veronica
- **Wed.:** 
- **Thu.:** Janina
- **Fri.:** Larissa
- **Open Tuesday:** Martin
* **Week:** maxime, jums, Tilmann
