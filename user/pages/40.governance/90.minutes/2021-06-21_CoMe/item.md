---
title: Coordination Meeting
date: "2021-06-21"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #196
- Date: 2020-06-21
- Present: Silvan, Larissa, Zui, Chandi, Antonin, Andrea, Nathalie, Thore
- Facilitator: chandi
- Notary: antonin
- ~~Mika caretaker:~~
- Physical board caretaker: Larissa
- Digital calendar caretaker: Zui
- Reservation sheet purifier: Andrea

----

<!-- Minute of silence -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_c69f2131673ff7bf2dcc082c7cdb7d91.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 8.4 people/day (+0.9)
- **⚡ Electricity**
    - usage: 3.35 €/day (⬇️-29%)
    - paid: -7.89 €/day 
    - ☀️ self produced: 76% (⬇️-1%)
    - emissions: 3 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.11 €/day (⬇️-12%)
    - emissions: 1 kg CO₂ₑ/week


### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 10.416
- µCOVIDs used last week: 4.867
- µCOVIDS balance from last week: 5549
- µCOVIDs additional this week:  5.000
- µCOVIDs available this week: 10549

### Expenditure
- oil and toilet paper, 10€

### Income
- 24€ in the shoe

### Things that happened
- a new 10-shots film was shot
- the Wolfsberg lake was enjoyed
- Nathanaël arrived
- lots of people got vaccinated

## 2. This week planning

### People arriving and leaving
- **Mon.:** 
- **Tue.:**
- **Wed.:** 
- **Thu.:** 
- **Fri.:** moko, oska, wawa arrive, Larissa leaves
- **Sat.:** Matthias, JaTiMiLe?
- **Sun.:** moko, oska, wawa leave, Danni group
- **Mon.:** Bodhi? Lise? might arrive
- **Some day:**  clara? Silvan will leave and arrive two/three times

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
 
### Evaluations and check-ins
<!-- Avoid scheduling on Mondays to give people time to prepare-->
- *Michal* _Volunteer_ : _Absolute Days_ threshold 90/60 (+50%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
- Tuesday
  - 10:00 Preparation for works on the gas supply [antonin]
  - 16:00 Open Tuesday [Nathalie, Andrea]
- Wednesday
  - 17:30-19:30: FFF meeting in Cloud room
- Thursday 
  - 10:00 Power Hour (Fac.: [Antonin],  DJ/Radio: [?])
  - 12:00 Fika
  - 13:00 Social Sauna [?]
- Friday
- Saturday
- Sunday
- Next Monday
  - 10:00 CoMe [Silvan]
- Next week summary

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00->19:00*)
<!-- Don't forget evaluations! -->

## 3. Shopping plans


## 4. Discussion & Announcements

### Round 1
- [Antonin] the donation shoe is not visible enough, there is only a small post-it next to it indicating what it is there for. Is anyone interested in making its purpose clearer?
    - [Zui] will write on it
- [Clara] Danni-Visit. So after a lot of back and forth (sorry about that), we would like to do the meeting in Kanthaus after all - if that's okay. Around 8 people (might be less) would come on Sunday 27th and stay until Friday 2nd of July. One person would be coming with a dog, I told them it would probably be easier if they stay in a tent in the garden with the dog, but that I would consult with you first. Everyone could do a Corona-Test before/when they arrive. I would propose using the Yoga-Room as a meeting place for us. Any resistances to this visit?
    - [A lot of people] The dog should stay outside (see collective agreements). Also there will be kids around in the garden so he should be nice to them. Concerns around the dog peeing on the vegetables in the garden and in interacting with marias dog. So the dog is accepted only if those concerns are addressed. It would be better if the person responsible for the dog could make sure no dog hair comes in the house via their clothes for instance.
    - [Silvan] The yoga room is still a works site, it would need to be cleaned before. [Nathalie, Andrea]: check with Talita about this. [Zui] there is no electricity in that room too (but possible with extension cord from hipster room)
    - [Silvan] expectation that Clara arrives before the guests and that we coordinate about getting food
        - [clara] yes, I will definitely do that
    - no resistance about them coming apart from those points
        - yay!
        - :)
- [Larissa] The FeuerQualle would like to borrow some tools for the FINTA Building week. (e.g. Akku-Drills, circular-hand-saw, Stichsäge), I will share the pad in slack. Resistance? Questions?
    - [chandi] whats the timeframe? [Larissa] 10-17 July probably, precise dates to be decided
    - [Larissa] also the KMW would be used for this building week
    - [Chandi] support but need to know which tools before agreeing
    - [Silvan] there is already a circular handsaw in HZ and we don't have many akku-drills in KH
    - people should raise resistance until friday, if not then it's agreed
- [chandi] Corona: Even though the incidence rate is so low, we are on the limit of our points - means the risk for kanthaus has not reduced.
    - [zui] the expenditure for a given week is not so significant because people can add things ahead of time
- [zui] group coming on friday and leaving on sunday evening. They would use the Dragon room by default, is it okay with the people working there?
    - [Andrea] OK, but should ask Talita too
- [Thore] is it a problem if the dragon room is used when the cloud room is also used for external people (FFF meeting)?
    - [Zui] maybe the FFF people could use both rooms actually, if they need desks?
    - [Andrea] will check with Talita about this point and the one above
- [Nathalie] Lea the foodsaver is leaving to Thüringen soon, should we invite her with her boyfriend? Would people be interested in a coffee break with them Saturday 15:00?
     - support
- [Andrea] proposes to organize a FLINT-only space (inspired by the kritma round), is there interest for it this week?
    - [Larissa] interest but this week is already quite full
    - [Nathalie] interest but would not do too much organization
    - [Zui] FLINT-coffee?
    - [Silvan] called "kritfe round"?
    - FLINT-coffee happening at 15:30 today

### Round 2
- [Antonin] would anyone like to bring some building trash away with me, on Friday?
    - [Andrea, Nathalie] can imagine helping
- [Thore] the Private Agreement™ ends officially this week, Thore does not intend to renew it as such but intends to have a discussion about how to continue (but would not do the entire process alone)

### Round 3
- [Antonin] there is still a bit of food to be washed
    - [Andrea] will do it
- [Zui] the plants might need watering more than once a week at the moment

## 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** chandi
- **Tue.:** Antonin
- **Wed.:** Andrea
- **Thu.:** Zui, Thore
- **Fri.:** Nathalie
- **Open Tuesday:** Nathalie

### Unavailabilities
- **Mon.:** Antonin, Andrea, Thore
- **Tue.:** Larissa, Andrea
- **Wed.:** Antonin
- **Thu.:** 
- **Fri.:** Larissa
- **Open Tuesday:** 
- **Week:** Silvan

## 6. For next week