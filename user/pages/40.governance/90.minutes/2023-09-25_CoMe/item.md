---
title: Coordination Meeting
date: "2023-09-25"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #312
* Date: 2023-09-25
* Facilitator: Kito
* Notary: Janina
* Physical calendar: Martin
* Digital calendar: Tilmann
* Reservation sheets: Doug
* Weather forecast: Tilmann
* Present: Janina, Doug, ASZ1, Tilmann, Kito, Martin, Kate

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 
![usage graph last 90 days](https://pad.kanthaus.online/uploads/af86bbd4-252d-44d1-8e11-996e0298d3f1.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.7 people/day (-4.7)
- **⚡ Electricity**
    - usage: 27.43 €/week (⬇️-21%)
    - of that for heating: 0.5 €/week
    - of that for hot water: 1.67 €/week
    - paid: -13.13 €/week 
    - ☀️ self produced: 68% (⬆️+1%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 20.2 €/week (⬇️-33%)
    - emissions: 1.6 kg CO₂ₑ/week

### Expenditure
- [tilmann] 9€ dishwasher power, dishwasher salt and better steel sponges
- [Martin] 14€ building material K20-3
- [kito] some money for some masking tape

### Income
- 2 cents (shoe)


### Things that happened
#### In or around Kanthaus
- we sold our property to our neighbours
- Thore's birthday without trip to the lake but cake
- presentation about Moulin Bleu
- potato action was a huge success and rapidly over
- some people went out to see some cats and eat food

#### Wider world
- Mitte Studie: strong growth of fascist ideology in German society (more than 8 percent of Germans have a "closed right-wing extremist worldview," and more than 20 percent partially)
- Azerbaijan launched attack on Nagorno-Karabakh, after the ceasefire there is a threat of genocide
- US sends missiles to Ukraine
- protest and blockades against natural gas terminal on the island of Rügen (Ende Gelände)

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Feedback Round
- Janina is grateful for all the nice support of the potato action
- Doug enjoys that the house wasn't so cluttered with stuff the last week


## 2. This week planning

### People arriving and leaving
* **Mon.:** Larissa comes back
* **Tue.:** Maxime leaves
* **Wed.:** 
* **Thu.:** group starts arriving (6ppl in total)
* **Fri.:** kito leaves for 1.5 weeks
* **Sat.:** Larissa leaves for ~two weeks 
* **Sun.:** Martin and Alejandro leave, group leaves
* **Mon.:** ASZ1 and ASZ2 might leave
* **Some day:** Nettle might come

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
_ nights are cold, days are okay, no rain - beautiful autumn weather!_
 
### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- ASZ1 Visitor: Days Visited threshold 42/21 (+100%)
- ASZ2 Visitor: Days Visited threshold 42/21 (+100%) 
- Larissa Member: Days Visited threshold 225/180 (+25%)
- Maxime Member: Days Visited threshold 209/180 (+16%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Kito]
    * 11:00 - Power Hour
    * 21:30 - Barbie movie screening
* Tuesday
    * black bin [not necessary]
    * 10:00 - ventilation device maintenance skillshare
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * 18:00 - Punkrocktresen
    * 19:00 - Japanese food @dinner! [ale]
* Thursday
    * 13:00 - 18:00 MitMachCafé
    * 18:30 - FLINTA thai boxing @ D5
    * 19:00 - supermarket pickup [janina]
    * 19:00 - ASZ BBQ
* Friday
    * green bin [tilmann]
    * 18:30 - foodsharing newbie meeting & socializing
* Saturday
    * 13:00 - supermarket pickup [janina]
* Sunday
    *  
* Next Monday
    * 10:00 CoMe [doug]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans

## 4. To do
_Newest tasks on top_
* [ ] put kanthaus logo on the new beer benches and tables
* [x] Repair of KTM-bicycle front light. Broken mechanical support. Electrical part OK. This is in the "Front lights" box. [Ale]
* [ ] curtains for the dragon room that prevent passersby from staring inside
* [ ] ~~cut branch from tree in the compost corner~~
* [ ] upstairs toilet K-20 flushing mechanism needs small fix (ask Martin or Doug) [Janina]
* [ ] cover the pipes from the rest of the room in the snack kitchen (wooden board angle?) []
    * [tilmann] maybe directly go for drywall as final measure?
      * [maxime] that would be the right but more difficult remediation indeed. I looked at options to hack a wooden corner, but didn't find satisfying options in the wood shed, so I'll leave there.
* [x] fix up beer benches and tables [Alejandro, Klara]
* [ ] Small bin for every toilet
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing
* [ ] replace shower curtain by (plexi)glass construction

<!-- task archive: https://pad.kanthaus.online/taskarchive -->



## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1

- [Alejandro] Announcement: Preparation of a farewell Japanese Dinner on Wednesday! Vegan and a little bit spicy. 🍛
- [Martin] Kanthaus-gast (mostly) down.
    - seems to be on and off, nobody is especially keen to investigate?
    - we should either fix it or turn it off
    - tilmann will bring it to #kanthaus-sysops and see how to continue
- [kito] ToI!
    - two votings up now: when and how - let's collect proposals until next come and start the process of organizing toi!
    - https://yunity.slack.com/archives/C3RS56Z38/p1695587048861249
- [kate] food/things for free shop for Tuesday
    - kate is happy about nice things for the free shop
    - internal donations should go in the hallway between free shop and free shop storage until thursday morning
    - would be great if in the future the free shop lounge could become available again in one way or another
- [tilmann, kito] some info about the group coming
    - dragon&cloud as group space, individuals using silent office desks
    - sleeping shouldn't be an issue as the house will be super empty on the weekend
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [kate] will need help this week with bureacracy. possibly german speaking too.
    - registration, signing up for bufdi and more
    - doug looks into what is needed
    - some stuff gets started today
- [kate] visitor welcome - self-tour room notes
    - a sheet for each room with 'rules & customs' for that specific room
        - [kito] could also contain fun facts...^^
    - kate will put sheets on walls and people can add points
- [tilmann] ventilation device maintenance - will exchange fresh air filter and wash heat exchanger, probably this week. anybody interested to participate, take pictures?
    - group session about maintaining an important machine, yes!



## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Kate
* **Tue.:** 
* **Wed.:** Martin, Alejandro
* **Thu.:** ASZ1
* **Fri.:** 
* **Open Tuesday:** KateFreeshop 

### Unavailabilities
* **Mon.:** Alejandro
* **Tue.:** Janina
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Janina
* **Open Tuesday:** Alejandro 
* **Week:** maxime, Tilmann, kito, Doug

### Result
- **Mon.:** Kate, Janina
- **Tue.:** 
- **Wed.:** Martin, alejandro
- **Thu.:** ASZ
- **Fri.:** 
- **Open Tuesday:** Kate

## 7. For next week
