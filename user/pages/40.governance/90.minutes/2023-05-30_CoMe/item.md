---
title: Coordination Meeting
date: "2023-05-30"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #295
* Date: 2023-05-30
* Facilitator: Larissa
* Notary: Kito
* Children caretaker: Parents
* Physical board caretaker: Anneke
* Digital calendar: Martin
* Reservation sheet purifier: kito did
* Present: Martin, Larissa, Anneke, Kito, Cecilia

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/2836f4ab-4340-4e29-b1e1-5ab2885cab23.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 5.4 people/day (-3.6)
- **⚡ Electricity**
    - usage: 21.01 €/week (⬆️+5%)
    - of that for heating: 0.36 €/week
    - of that for hot water: 1.62 €/week
    - paid: -53.69 €/week 
    - ☀️ self produced: 80% (⬆️+2%)
    - emissions: 10 kg CO₂ₑ/week
- **💧 Water**
    - paid: 16.44 €/week (⬆️+15%)
    - emissions: 1.3 kg CO₂ₑ/week


### Expenditure
- [Antonin] 12€ for overpriced Wurzener Bio oats which taste like dishwasher powder.
- [kito] 28€ for a pretty big stapler

### Other Income

### Things that happened

#### In or around Kanthaus
- People went places: Freie Feldlage for eletro fest, Spreewald for kayaking, JaTiMiLe did family visit. 
- places went people: Annekes sisters visited
- social sauna got surprisingly hot
- someone put the trash out...

#### Wider world
- Erdoğan got "reelected"...............with big support from turkish people in Germany (big surprise lol)
- other election went not so nice too

<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Martin returns, Larissa returned,
* **Tue.:** kito leaves, Maxime might come back, JaTiMiLe comes back
* **Wed.:** Axel (Martin's brother) arrives , Silvan leaves
* **Thu.:** Larissa might leave
* **Fri.:** 
* **Sat.:** Larissa comes back
* **Sun.:** Antonin returns
* **Mon.:** 
* **Some day:**


### Weather forecast
- lot of solar power!
<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->


### Evaluations and check-ins
 Due for evaluation (and seen within the last 7 days):
- Silvan Visitor : Days Visited threshold 28/21 (+33%)
- Martin Volunteer : Days Visited threshold 72/60 (+20%)
    - no members available :(

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Dorota_Stilgar Visitor : 3 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    *  Monday food care [Anneke and sisters] 
* Tuesday
    * 10:00 CoMe [Larissa]
    * asyncronous Power Hour
    * Cecilia's check in [kito]
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 19:00 Punkrock-Tresen @D5
* Thursday
    * no Thursday power hour this week!
* Friday
    * 10:00 PlaMe [Martin]
* Saturday
    * Yellow Bin [Cecilia]
* Sunday
* Next Monday
    * CoMe [Martin and Anneke]
    * Paper Bin [Martin]
* Next week summary

To be scheduled:
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- oil? oats? flour?

## 4. To do
_Newest tasks on top_
* [ ] fix Daria trailer back lights
     * [Martin] checked. Both rear lights burned. Second time it happens. I have no idea what is causing it. I thought it was some faulty cable, but I'm pretty sure it's not that now. 
* [x] install missing smoke detectors [Martin]
    * Only rooms without are bathrooms, spitzdach and the cave. Since the Cave could be designated incence room, could it trigger the SD? There are 3 left.
    * should be no problem to have the smoke detector also in the cave, so let's put it there
* [ ] give away saved windows in K20 basement (a lot) maybe https://www.trashgalore.de/? (martin)
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] de-clog compost toilet drain
* [ ] modify the task lottery to prioritize open tuesday [Maxime]
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
* [kito] PowerHour Ukuvota
    * Larissa will do it this week

<!-- check if anyone has a point that didn't speak already -->


### Round 2


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** 
- **Tue.:** ~~Larissa~~ Anneke
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** Cecilia

### Unavailabilities
- **Mon.:** 
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** 
- **Week:** Martin, Kito

### Result
- **Mon.:** 
- **Tue.:** Anneke
- **Wed.:** Larissa
- **Thu.:** you?
- **Fri.:** you?
- **Open Tuesday:** Cecilia

## 7. For next week
