---
title: Coordination Meeting
date: "2023-06-05"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #296
* Date: 2023-06-05
* Facilitator: Anneke
* Notary: Anneke/Maxime
* Children caretaker: Kita
* Physical board caretaker: Dorota
* Digital calendar: 
* Reservation sheet purifier: Anneke
* Present: Anneke, Axel_Martin, Dorota, Maxime

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
![usage graph last 90 days](https://pad.kanthaus.online/uploads/df55d301-b85e-4ec3-a2f3-aee219e07ae4.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.4 people/day (+0.0)
- **⚡ Electricity**
    - usage: 18.3 €/week (⬇️-8%)
    - of that for heating: 0.34 €/week
    - of that for hot water: 1.56 €/week
    - paid: -68.19 €/week 
    - ☀️ self produced: 82% (⬆️+2%)
    - emissions: 9 kg CO₂ₑ/week
- **💧 Water**
    - paid: 18.83 €/week (⬆️+14%)
    		- emissions: 1.5 kg CO₂ₑ/week


### Expenditure
- 30 Euro oats, flour,oil and toilet paper [Martin]

### Other Income
/

### Things that happened

#### In or around Kanthaus
- spontanous communal bike trip to the lake
- European premiere of Axels movie, followed by a discussion with the movie soundtrack composer

#### Wider world
- riots in Leipzig after court decision of Lina E.

<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** JaTiMiLeLa leave
* **Tue.:** Dorota leaves
* **Wed.:** Dorota comes back
* **Thu.:** Dorota leaves
* **Fri.:** 
* **Sat.:** 
* **Sun.:** Dorota comes back
* **Mon.:** 
* **Some day:**


### Weather forecast

tomorrow probably rain, end of the week probably thunder

<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->


### Evaluations and check-ins

- Silvan Visitor : Days Visited threshold 31/21 (+48%)
- Martin Volunteer : Days Visited threshold 75/60 (+25%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Anneke + Martin]
    * 11:00 PowerHour ?
    * Monday food care   
* Tuesday
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 19:00 Punkrock-Tresen @D5 with "anbaden" around 8
* Thursday
    * no Thursday power hour this week!
    * Florian should come ~15:00 to pick up stones/bricks
* Friday
* Saturday
    * 15:00 coffe and cake @garden [Nathalie]
* Sunday
* Next Monday
    * CoMe []
* Next week summary

To be scheduled:
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans


## 4. To do
_Newest tasks on top_
* [ ] fix compost toilet funnel, such that pee is directed away from the container
* [ ] fix Daria trailer back lights
     * [Martin] checked. Both rear lights burned. Second time it happens. I have no idea what is causing it. I thought it was some faulty cable, but I'm pretty sure it's not that now. 
* [ ] give away saved windows in K20 basement (a lot) maybe https://www.trashgalore.de/? (martin)
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] de-clog compost toilet drain
* [ ] modify the task lottery to prioritize open tuesday [Maxime]
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
* [Larissa] Last chance to vote for the change of the Collective Agreement about PowerHour until 2pm today! https://legacy.ukuvota.world/#/app/0a7a0fae-ca01-5f5c-b736-5a99ab5ac6a1/vote
<!-- check if anyone has a point that didn't speak already -->


### Round 2


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** 
- **Tue.:** Anneke
- **Wed.:** Antonin
- **Thu.:** Maxime
- **Fri.:** 
- **Open Tuesday:** Axel

### Unavailabilities
- **Mon.:** 
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:**
- **Week:** Martin, Dorota

### Result
- **Mon.:** 
- **Tue.:** Anneke
- **Wed.:** Antonin
- **Thu.:** Maxime
- **Fri.:**
- **Open Tuesday:** Axel

## 7. Power Hour preferences
- [Antonin] would be happy to be assigned a task that I would do on Tuesday

- Kitchen: Maxime
- Snack kitchen: Maxime
- Vacuum: Anneke K20, Axel K20-1,K22-1
- Food care: Axel
- Plant care: Dorota
- Communal closet: Antonin
 
## 8. For next week
