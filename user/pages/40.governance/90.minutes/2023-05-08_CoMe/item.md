---
title: Coordination Meeting
date: "2023-05-08"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #292
* Date: 2023-05-08
* Facilitator: Maxime
* Notary: Janina
* Children caretaker: Kita
* Physical board caretaker: Antonin
* Digital calendar: Tilmann
* Reservation sheet purifier: Larissa
* Present: Tilmann, Larissa, Maxime, Martin, Silvan, Antonin, Janina 

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/9eb96860-53c4-4d3c-9c1a-ada1ee46b4a7.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.9 people/day (+0.3)
- **⚡ Electricity**
    - usage: 25.46 €/week (⬇️-26%)
    - of that for heating: 2.67 €/week
    - of that for hot water: 2.38 €/week
    - paid: -34.73 €/week 
    - ☀️ self produced: 78% (⬇️-4%)
    - emissions: 12 kg CO₂ₑ/week
- **💧 Water**
    - paid: 17.73 €/week (⬇️-6%)
    - emissions: 1.4 kg CO₂ₑ/week

### Expenditure
- 17.5€ toilet paper and white flour [Martin]
- 90€ for 10 smoke detectors [Martin]
- 23€ hopefully final solution for the Carla Cargo hitch (to different seatpost diameters) [Martin]
- 55€ second-hand Makita 18V charger + 3Ah battery [tilmann]
- 152€ 3x second-hand roof window blinds [tilmann]
- 71€ spare parts for Auerworld Makita drills [tilmann]
- some oats [maxime]

### Other Income


### Things that happened
#### In or around Kanthaus
- 3 blinds installed in K20 attic
- grass and flowers are growing
- almost all of KH went to georg-schwarz-straßenfest
- flowmi construction meeting
- maxime deep-cleaned the sinks in the k20 bathrooms
- gaia cleaned a lot of windows and made the oven shine
- our foodsharing district transformed into 'leipziger land' and gained a bot
- martin attended verkerswendecamp in wolfsburg
- antonin hosted a tech cafe about frontend testing with cypress


#### Wider world
- many wars are continuing
- the new Chile constitution proposal will be written by the far right
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Gaia_WA comes back
* **Tue.:** 
* **Wed.:** Kito comes back
* **Thu.:** Larissa leaves
* **Fri.:** 
* **Sat.:**
* **Sun.:** Larissa comes for one night
* **Mon.:** 
* **Some day:** Franzi comes and Antonin leaves someday over the weekend


### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_sunny spring weather with some clouds. might rain thursday/friday._

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Janina Member : Days Visited threshold 181/180 (+1%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Martin Volunteer : 2 days until Days Visited threshold (60)
- Silvan Visitor : 6 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * 11:00 Power Hour trial [Larissa] 
    * Monday food care [part of PH]
* Tuesday
    * black bin [Larissa]
    * bike demonstration against velocity @leipzig
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 10:00 attic work session
    * 16:00 Gedenken an die Bücherverbrennung 1933 @market square
    * 19:00 Punkrock-Tresen @D5
* Thursday
    * no Thursday power hour this week!
* Friday
    * green bin [Maxime ]
* Saturday
    * Wenceslaigassenfest
    * Electroparty @Spitzenfabrik
* Sunday
    * 
* Next Monday
    * 10:00 CoMe with Power Hour task distribution [Janina]
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- spreads and oats

## 4. To do
_Newest tasks on top_
* [ ] fix the K20-2 bathroom shower head holder (missing a screw, but that's not just as trivial as just putting back a screw apparently)
* [ ] install missing smoke detectors [Martin]
* [x] move the beams from the garden inside [Tilmann, Antonin]
* [ ] fix the extension of the private storage with a new shelf to the wall next to the main bathroom
* [ ] give away saved windows in K20 basement (a lot) maybe https://www.trashgalore.de/? (martin)
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] treat mold in Freeshop Lounge and Hipster Room
* [ ] de-clog compost toilet drain
* [ ] modify the task lottery to prioritize open tuesday []
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
- [Martin] Workawayer request (Akos), in search for a host. It´s a coding person. "I am kind of flexible from the first week of june(let's say 7th) till the middle of July." It could be a shared responsability considering planned trips? I will be gone june 24th til July 9th.
	- Settled! Antonin will be hosting. His previous hosting fell down also. 
- [Janina] Wenceslaigassenfest!!! Who's planning on joining when and for what? We'll be happy about a lot of hands for build-up and tear-down, and of course about a lot of friendly faces during the fest itself. :)
    - streets blocked from 10 am to 8 pm, fest itself from 12 am to 6 pm
    - martin and janina will be there from 10 am
    - silvan will be here and can help with build-up
    - maxime and antonin will be around and willing to lend a hand
    - tilmann is childcare back-up
    - kito and gaia will be back, franzi will be there
<!-- check if anyone has a point that didn't speak already -->


### Round 2


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** zui
- **Tue.:** Dorota
- **Wed.:** 
- **Thu.:** Janina
- **Fri.:** 
- **Open Tuesday:** Anja

### Unavailabilities
- **Mon.:** Maxime
- **Tue.:** silvan
- **Wed.:** Antonin
- **Thu.:** silvan
- **Fri.:** 
- **Open Tuesday:** Maxime, silvan
- **Week:** Martin, Tilmann, Larissa

## 7. For next week

### Result
- **Mon.:** zui
- **Tue.:** Dorota, Maxime
- **Wed.:** Silvan
- **Thu.:** Janina
- **Fri.:** Antonin
- **Open Tuesday:** anja
