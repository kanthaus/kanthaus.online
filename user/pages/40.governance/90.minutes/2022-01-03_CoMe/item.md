---
title: Coordination Meeting
date: "2022-01-03"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #225
- Date: 2022-01-03
- Facilitator: Doug
- Notary: Silvan, Doug
- Mika caretaker: Janina
- Levi caretaker: Tilmann
- Physical board caretaker: Larissa
- Digital calendar: Zui
- Reservation sheet purifier: Andrea
- Present: Zui, Matthias, Charly, Andrea, Anja, Doug, Silvan, Larissa, Bella, Chandi

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_9ba820b8e160ea91c73706c91728e0ec.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.4 people/day (+2.6)
- **⚡ Electricity**
    - usage: 102.02 €/week (⏬-42%)
    - of that for heating: 52.18 €/week
    - paid: 88.3 €/week 
    - ☀️ self produced: 13% (⬆️+2%)
    - emissions: 60 kg CO₂ₑ/week
- **💧 Water**
    - paid: 10.47 €/week (⏬-47%)
    - emissions: 1 kg CO₂ₑ/week

### Expenditure
- [chandi] 10€

### Income
- 10€
- ca. 10€ donation for glögi

### Things that happened
- rC3
- calm silvester party
- a lot of food washed
- adventure rain hike
- levi started to walk
- more lithium batteries tested!

#### Wider world
- Winney the Pooh (and lots of other stuff) is now Public Domain :) https://web.law.duke.edu/cspd/publicdomainday/2022/

## 2. This week planning

### People arriving and leaving
- **Mon.:** 
- **Tue.:** Kito & Vroni arrive [Host: Andrea]
- **Wed.:** Wolfram, Anja & Doug leave, Tobi+? arrive [Host: Larissa], Nathalie and Thore come back in the evening
- **Thu.:** 
- **Fri.:** Roswitha & Gerd arrive, Silvan leaves
- **Sat.:** 
- **Sun.:** Charly leaves, Zui leaves (!), Silvan comes back
- **Mon.:** 
- **Some day:** Doug comes back

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
* Charly, decides not worth it

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Doug]
    * Park cars on even/this side [Matthias]
    * ~20:30 YT-Abend, but educational! (see [thread](https://yunity.slack.com/archives/CFDNQL9C6/p1640198902128500)) [Doug]
- Tuesday
    * Hausmüll [Tilmann]
    * 15:00 Freeshop
    * 17:10 workout @ Yogaroom [Silvan & Larissa]
    * 18:00 Bitte wenden meeting @ Lantern [Janina, Unkraut]
- Wednesday
    * 10:00 Monthly teams meeting [Matthias]
    * Park cars on odd/opposite side [Bella]
    * 14:00 ToI meeting @ Lantern [Janina, Larissa, Doug, Nathalie(?)]
- Thursday
    * 10:00 Power Hour [Fac.: Zui , DJ: ?]
    * 15:00 ToI session party @ Pianoroom? []
- Friday
    * Biotonne [Larissa]
- Saturday
    * Levi's 1st birthday
    * Zui and Charly leaving-party
    * Bus party! (after breakfast)
- Sunday
    * Sauna @ VK
- Next Monday
    * 10:00 CoMe [Chandi]
    * Park cars on even/this side []
- Next week summary
    *

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- Shower head, 23€ [Matthias]

## 4. To do
_Small fixes of things in the house_

* [unfinishable] more storage spaces for visitors
* [x] Add 2022 trash dates to the digital calender (OMG Kell has an ICS calendar that can be imported directly ❤)
* [x] KMW: fix driver side door (https://yunity.slack.com/archives/C3RS56Z38/p1636467207030900) [chandi]
* [ ] Electrically disconnect the ceran stove on the right kitchen side
* [ ] fix hipster room window curtain [Larissa & anja]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door

### New this week
* [ ] Install light in the freeshop hallway
* [ ] door-closer for Elefant-staircase door

## 5. Discussion & Announcements

### Round 1
- [Doug] Call for ToI sessions! What we do at the ToI is up to us :) The [poll is in](https://cloud.kanthaus.online/apps/polls/vote/12), with Long-term Residency, Values and Culture being the most-ticked items. Day-time sessions should somehow be relevant for the continued existence of Kanthaus. Soft-deadline for sessions until the 16th, meeting about sessions this Thursday (6th) [The ToIs]
- [Antonin] Dietmar would like to borrow our scaffolding for a few months this spring. Any concerns with lending him? He could come pick it up in Kanthaus, use it in Wurzen (Industriestraße and/or Crostigall), and return it to our basement. I would feel responsible for chasing him up if he is late to return it. 
    - no resistance, people want to inventorize what he takes, he should be capable of building it up safely
- [Silvan] As announced in Slack, on 19.-20.2. we would like to host the Acrolotl like last year from KH. (Using Yoga- and Hipster-Room the whole days, about three guests from Friday evening on) Any concerns/questions?
    - no resistance
- [Matthias] Dumpster kitchen bathtub: I removed the sieve as it blocks soo easy and is always disgusting. The sink itself is easy to unblock by lifting the pipe a bit. I can show if wanted - in my experience, it takes even huge amounts of small dirt perfectly fine.
- [Janina] Quite some people arrive this week, so I suggest we use a sleeping spot planning sheet again to record preferences. There is one already hanging on the magnet board next to the window.
    - question: do you (Janina) want to coordinate it?
    - Chandi would be fall-back facilitator
- Charly brought some clothes from Dresden that are available for everybody in the Vortex now

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Janina] Shall we use the food tasks sheet this week? If yes, who will facilitate it?
    - Seems like yes. Andrea wants to "go around and ask people" who are not in come.

### Round 3

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Matthias
- **Tue.:** Andrea
- **Wed.:** 
- **Thu.:** Zui
- **Fri.:** Janina, Tilmann
- **Open Tuesday:** Anja, Doug

### Unavailabilities
- **Mon.:**
- **Tue.:** Larissa, chandi
- **Wed.:** Larissa
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** chandi
- **Week:** 

### Result
- **Mon.:** Matthias, Larissa
- **Tue.:** Andrea, Doug
- **Wed.:** Charly, Chandi
- **Thu.:** Zui, Bella
- **Fri.:** Janina, Tilmann
- **Open Tuesday:** Anja, Silvan

## 7. For next week
- 

