---
title: Coordination Meeting
date: "2023-10-02"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #313
* Date: 2023-10-02
* Facilitator: Doug
* Notary: Doug
* Children caretaker: -
* Physical calendar: Dima
* Digital calendar: Antonin
* Reservation sheets: Doug
* Weather forecast: 
* Present: Doug, Antonin, Dima

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 
![usage graph last 90 days](https://pad.kanthaus.online/uploads/edebfc5a-1b8b-4bff-9ae2-011e651d1322.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 9.3 people/day (-3.6)
- **⚡ Electricity**
    - usage: 28.28 €/week (⬆️+2%)
    - of that for heating: 0.44 €/week
    - of that for hot water: 2.02 €/week
    - paid: -13.65 €/week 
    - ☀️ self produced: 67% (⬇️-1%)
    - emissions: 14 kg CO₂ₑ/week
- **💧 Water**
    - paid: 22.33 €/week (⬆️+11%)
    - emissions: 1.7 kg CO₂ₑ/week

### Expenditure
- none to our knowledge

### Income
- €10.09 donation

### Things that happened
#### In or around Kanthaus

#### Wider world


<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Feedback Round


## 2. This week planning

### People arriving and leaving
* **Mon.:** Coming: Antonin; going: A^2
* **Tue.:** Coming: Martin, Kate 
* **Wed.:** Going: Dima
* **Thu.:** 
* **Fri.:** 
* **Sat.:** 
* **Sun.:** Coming: kito
* **Mon.:** 
* **Some day:** 

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)

 
### Evaluations and check-ins
- information not available...

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Doug]
    * 11:00 - Power Hour
    * 20:30 - Tor - background & codebase migration [Dima]
* Tuesday
    * 15:00 - 17:00 Open Tuesday [lottery]
    * 14:00 garden-side facade renovation planning [Antonin]
    * ~20:00 - Children's march doku [Kate]
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * 14:30 - FGNW meeting @office?
    * 18:00 - Punkrocktresen
* Thursday
    * 13:00 - 18:00 MitMachCafé
    * ~~18:30 - FLINTA thai boxing @ D5~
* Friday
    * 11:00 Project Updates [Antonin]
* Saturday
    * Gelbe Tonne [Antonin]
* Sunday
    *  
* Next Monday
    * 10:00 CoMe [? fallback Doug]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
- Gipskarton, €100 [Antonin]
    - help needed

## 4. To do
_Newest tasks on top_
* [ ] fix/improve K20-1 staircase night light (can ask Tilmann for details about wiring)
* [ ] put kanthaus logo on the new beer benches and tables
* [ ] curtains for the dragon room that prevent passersby from staring inside
* [ ] ~~cut branch from tree in the compost corner~~
* [x] upstairs toilet K-20 flushing mechanism needs small fix (ask Martin or Doug) [Dima]
* [ ] cover the pipes from the rest of the room in the snack kitchen (wooden board angle?) []
    * [tilmann] maybe directly go for drywall as final measure?
      * [maxime] that would be the right but more difficult remediation indeed. I looked at options to hack a wooden corner, but didn't find satisfying options in the wood shed, so I'll leave there.
* [ ] Small bin for every toilet
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing. Also, use a diode to prevent over voltage. 
* [ ] replace shower curtain by (plexi)glass construction

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
* [kate] tuesday evening We can watch a short documentary about the childrens march - a successful nvda part of the civil right movement in the us
    * yay!
* [Antonin] people looking for a host:
    * Yuliya (WA), from 14th - 22nd October
        * Antonin as fallback host... but prefereably someone else.
* [kito] please vote for your prefered ToI option and enter your availabilities in the nuudle
    * https://yunity.slack.com/archives/C3RS56Z38/p1695587048861249

<!-- check if anyone has a point that didn't speak already -->

### Round 2

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Dima
* **Tue.:** Antonin
* **Wed.:** kate
* **Thu.:** Doug
* **Fri.:** Martin
* **Open Tuesday:** katefreeshop 

### Unavailabilities
* **Mon.:** Janina
* **Tue.:** Janina
* **Wed.:** Janina
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** 

### Result
* **Mon.:** Dima
* **Tue.:** Antonin
* **Wed.:** kate
* **Thu.:** Doug
* **Fri.:** Martin
* **Open Tuesday:** katefreeshop 

## 7. For next week
- Note: this CoMe was completed in 19 minutes :rocket: