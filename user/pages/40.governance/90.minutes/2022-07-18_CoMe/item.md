---
title: Coordination Meeting
date: "2022-07-18"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #252
* Date: 2022-07-18
* Facilitator: Doug
* Notary: Martin
* Mika caretaker: Kita
* Levi caretaker: Janina
* Physical board caretaker: Eric
* Digital calendar: Doug
* Reservation sheet purifier: Tilmann
* Present: Martin, Janina, Tilmann, Eric, Doug

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/4eee6949-a38c-4929-8c49-8ec3bce95e52.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.1 people/day (-1.4)
- **⚡ Electricity**
    - usage: 42.42 €/week (⬇️-12%)
    - of that for heating: 3.42 €/week
    - paid: -57.33 €/week 
    - ☀️ self produced: 82% (⬆️+6%)
    - emissions: 10 kg CO₂ₑ/week
- **💧 Water**
    - paid: 16.15 €/week (⬇️-32%)
    - emissions: 1.3 kg CO₂ₑ/week

### Expenditure
* [matthias] 75 covid tests for 98€ if we want them communally
    * no resistance
* [Martin] 141€ for electric bike stuff, 9€ for baking stuff and vaccuum cleaner bags

### Income
* nothing in shoe or box

### Things that happened

#### In or around Kanthaus
* [5th anniversary of Kanthaus!](https://kanthaus.online/blog/2022-07-12_fifth-anniversary) :tada: 
* Compost toilet care and a [new handbook page about it](https://handbook.kanthaus.online/technical/composttoilet.html)
* Check-in with Eric: All seems fine!
* SiLa went to the Feel! festival
* Radical Bookfair in Leipzig
* Blue Stockings Salon in Pizza Lab
* Antonin's parents visited
* Sickness in the house
* Karrot community days

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* school summer holidays start today in Saxony

## 2. This week planning

### People arriving and leaving
* **Mon.:** zui, Larissa and Silvan come back
* **Tue.:** zui leaves
* **Wed.:** 
* **Thu.:** Silvan leaves
* **Fri.:** Larissa, JaTiMiLe, Martin and Eric leave
* **Sat.:** 
* **Sun.:** Antonin comes back
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_there's hotness coming in the middle of the week (up to ~40°C)! let's be prepared!_

### Evaluations and check-ins
- Doug

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Doug]
    * ~~Park KMW & trailer on even/this side []~~
* Tuesday
    * 15:00 - 17:00 Verschenkeladen [Doug]
    * 17:00 KH anniversary call
    * Hausmüll [Doug]
    * Klimacamp Leipziger Land begins (2 weeks)
* Wednesday
    * ~~Park KMW & trailer on odd/that side []~~
    * Kola support action [Doug]
* Thursday
    * 10:00 Power Hour [Fac.: Martin, DJ: ?]
    * 15:00 Social Sauna [?]
    * 14:00 Bike repair Workshop at Kultur Cafe [Martin]
* Friday
    * Harzgerode Sommerfest (til Sunday)
    * Biotonne [Doug]
* Saturday
    * 
* Sunday
    * Park KMW & trailer away from WR [?]
* Next Monday
    * 10:00 CoMe [?]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [Doug] 10-pack of T20 bits, Wera. [€10](https://www.ebay.de/itm/302278504682).
    * No resistance, Tilmann says mays it a 20-pack

## 4. To do
_Newest tasks on top_
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
* [ ] fix or rework the K20 bathroom occupancy indicator
* [x] fix or replace leaking K20-0# toilet fill valve [Martin]
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?] ~~Denkmalschutz der Fassade?~~
* [ ] sort out garden stuff in woodshed [Anja]
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] mount fire extinguisher [chandi]
* [ ] first aid kits visible in the staricases K20-1 and K22-1
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] add a doorbell in the garden
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Silvan] i will make a list for the things i would like to take to Auerworld and post it in slack. (e.g. all 18V Makita Battery Drill incl. charger and batteries, Kappsäge,..)
    * Makes sense.
* [Anja] is it okay if we take the KMW to the Highfield? (18-22 Aug)
    * Intended use for the KMV, so great. Can take trailer also. 
* [Janina] It's getting super hot! Let's try and make the house as cool as possible before:
    * ventilate _everywhere_ during nighttime
    * close _all_ windows between 9 and 11 am (depending on heat and sun), use curtains to prevent sun coming in. 
    * open again between 8 and 10 pm (depending on heat an sun)
    * this makes a big difference and creates a buffer of coolness!

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Tilmann] Attic work in the second week of August. He wants to make two focus weeks in September for himself. Could be in the context of building weeks, if someone is willing to organize. Childcare help crucial. 

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** Martin, Eric
* **Wed.:** Janina
* **Thu.:** Larissa
* **Fri.:** 
* **Open Tuesday:** Doug

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Martin
* **Open Tuesday:** 
* **Week:** Tilmann

### Result
* **Mon.:** 
* **Tue.:** Martin, Eric
* **Wed.:** Janina
* **Thu.:** Larissa
* **Fri.:** 
* **Open Tuesday:** Doug

## 7. For next week
* 

