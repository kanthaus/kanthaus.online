---
title: Coordination Meeting
date: "2023-09-18"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #311
* Date: 2023-09-18
* Facilitator: Martin
* Notary: Janina
* Physical calendar: Doug
* Digital calendar: Tilmann
* Reservation sheets: Kito
* Weather forecast: Kito
* Present: Kito, Larissa, Doug, Tilmann, ASZ1, Janina, Alejandro, Maxime

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

![usage graph last 90 days](https://pad.kanthaus.online/uploads/7ffa2819-b230-44a4-8c7a-871dc9bf952c.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 17.4 people/day (-1.0)
- **⚡ Electricity**
    - usage: 35.68 €/week (0%)
    - of that for heating: 0.44 €/week
    - of that for hot water: 2.65 €/week
    - paid: -16.61 €/week 
    - ☀️ self produced: 67% (⬇️-8%)
    - emissions: 17 kg CO₂ₑ/week
- **💧 Water**
    - paid: 30.06 €/week (⬇️-4%)
    - emissions: 2.3 kg CO₂ₑ/week


### Expenditure
- [maxime] 8.90€ for 5kg oat
- [tilmann] 1210€ k20-3 heating 
  - 854€ main unit
  - 205€ additional material
  - 151€ tools
- [tilmann] 49€ bike parts (2x trailer tyres, pedals)
- [Doug] 30€ razors, blades, office supplies

### Income
- 23 cents at Spende can 

### Things that happened
#### In or around Kanthaus
- a group in Kanthaus
- kola field work action and pumpkins saved
- attic work sessions, drywall and electricity continues
- parking day / climate strike in Leipzig
- k20-3 heating installed
- big hole in the snack kitchen filled with concrete for fire protection
- kate became a volunteer
- social sauna with interesting topics
- larissa met with local and federal politicians
- another onion savon action
- anneke's farewell party
- pödelwitz thanksgiving party

#### Wider world
- Libya dam/climate disaster
- California is sueing the ass off companies for climate change damages
- Eritreans fight in the streets of Stuttgart

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

<!-- ### Tekmîl-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** kito leaves until tuesday evening, Kate comes back
* **Tue.:** Dima comes back
* **Wed.:** 
* **Thu.:** Alejandro leaves
* **Fri.:** Larissa leaves
* **Sat.:** 
* **Sun.:** 
* **Mon.:** Larissa comes back, Alejandro comes back
* **Some day:** 

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
_warm and cloudy with some thunderstorm/rain, cools down towards the weekend_
 
### Evaluations and check-ins
- Alejandro_Martin Visitor: Days Visited threshold 37/21 (+76%)
- ASZ1 Visitor: Days Visited threshold 35/21 (+67%)
- ASZ2 Visitor: Days Visited threshold 35/21 (+67%)
- Larissa Member: Days Visited threshold 221/180 (+23%)
- Maxime Member: Days Visited threshold 202/180 (+12%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Martin]
    * 11:00 - Power Hour
    * 15:00 - notary (with DRK) [Larissa + Doug]
    * 15:30 - Thore's birthday cake in the garden, then lake trip
        * Moved to 15:30 because of the notary appointment
* Tuesday
    * 10:00 - potato action outreach planning [Kate]
    * 15:00 - 17:00 Open Tuesday [lottery]
    * in the evening: quick presentation about the Moulin Bleu [Antonin]
* Wednesday
    * 10:00 - Alejandro's evaluation [Maxime]
    * 12:00 - 17:00 MitMachCafé
    * 18:00 - Punkrocktresen
* Thursday
    * 13:00 - 18:00 MitMachCafé
    * 18:30 - FLINTA thai boxing @ D5
* Friday
    * Yellow bins [Maxime]
    * 10:00 - PlaMe [Janina]
    * 15:00 - 18:00 foodsharing potato action in market square [Janina]
* Saturday
    * 15:30 - Katzentempel [Antonin, JaMiLe, Franzi, Anja]
* Sunday
    *  
* Next Monday
    * Blue bins [Tilmann]
    * 10:00 CoMe [Kito]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
* Wheat flour [maxime]
* masking tape [kito]
* display port adapter [kito]

## 4. To do
_Newest tasks on top_
* [ ] Repair of KTM-bicycle front light. Broken mechanical support. Electrical part OK. This is in the "Front lights" box.
* [ ] curtains for the dragon room that prevent passersby from staring inside
* [ ] ~~cut branch from tree in the compost corner~~
* [x] put new sign at the bike-repairstation [Antonin]
* [x] properly dispose of the lead battery in the trash corner [Maxime]
   * I gave it to M&D on Dresdener Straße
* [x] fix access to cloud.kanthaus.online [Antonin]
* [x] put Lantern smoke detector back on ceiling [Martin]
* [ ] upstairs toilet K-20 flushing mechanism needs small fix (ask Martin or Doug) [Janina]
* [ ] cover the pipes from the rest of the room in the snack kitchen (wooden board angle?) []
    * [tilmann] maybe directly go for drywall as final measure?
      * [maxime] that would be the right but more difficult remediation indeed. I looked at options to hack a wooden corner, but didn't find satisfying options in the wood shed, so I'll leave there.
* [ ] fix up beer benches and tables [Alejandro, Klara]
* [ ] Small bin for every toilet
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the street side facade [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: K22 garden door


## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1

* [Anneke] thank you everyone for a beautiful farewell party and this amazing booklet, I was touched so much!! love you all
* [kito] visitor requests (Adele, JJ, group)
    * all quite soon, group in mid october
    * kito communicates with group
    * doug might reply to one if the individuals
* [janina] potato action!
    * it's gonna be quite the event: kate prepares snacks to hand out, I will make tiny flyers.
    * could be cool to also have a pavillion with a table and two benches, as well as the big poster holder
        * need help transporting that on Friday at 2 pm and back around 6 pm
        * Kito, Doug and ASZ1 are in
* [doug] Suggest we try not to have more than 10 tasks on list... delete oldest tasks without names first?
    * [kito] a backlog somewhere else would be nice. maybe a pad?
    * [tilmann] it will be like a graveyard...
    * [janina] but it also doesn't hurt to have one
    * [doug] dorota was also interested in more detailed task management, might be good to talk when she's back

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [kito] neighbourhood
    * let's talk more about it in PlaMe
* [Doug] Moved the big communal shoe rack to free shop. Shoes in Vortex.


<!-- check if anyone has a point that didn't speak already -->

### Round 3


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)


### Result
- **Mon.:** Janina
- **Tue.:** Alejandro, Maxime
- **Wed.:** ASZ1, ASZ2
- **Thu.:** Dima
- **Fri.:** Kito, Doug
- **Open Tuesday:** Kate
