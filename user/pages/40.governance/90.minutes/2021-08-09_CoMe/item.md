---
title: Coordination Meeting
date: "2021-08-09"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #204
- Date: 2021-08-09
- Facilitator: Talita
- Notary: Michal
- Mika caretaker: Maxime, Tilmann
- Levi caretaker: Janina
- Physical board caretaker: Doug
- Digital calendar: Maxime
- Reservation sheet purifier: Laura
- Present: Talita, Laura, Andrea, Michal, Tilmann, Mika, Janina, Levi, Doug, Maxime

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_15dd5a54113e915bb46aec641010175a.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 15.1 people/day (+0.0)
- **⚡ Electricity**
    - usage: 5.8 €/day (0%)
    - paid: -2.91 €/day 
    - ☀️ self produced: 69% (⬇️-7%)
    - emissions: 5 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.35 €/day (⬇️-13%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- [matthias, doug] 88,15€ for new black drum for printer
    - talked about it; even if we would want to sell the printer makes sense with a working drum. it should last at least another 40000 pages...
- [Tilmann] 33€ for LED bulbs: mainly some special ones for new light in the communal, also some brighter ones for the sheds

### Income

- 56 EUR in the shoe
- 210 EUR donation


### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- Incidence: 
    - Germany: 21,2
    - Saxony: 
    - Landkreis Leipzig: 12,6  <- masks are obligatory again in markets etc.!
- Adjusted prevalence Saxony: 0,02% (last tuesday)
- µCOVIDs available last week: {COVIDS FOR LAST WEEK NOT FOUND!}
- µCOVIDs used last week: 
- µCOVIDS balance from last week:  
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week:  
- µCOVIDs/person mean for this week:  (18 people) 

### Things that happened
- Visit from GSX with interesting community exchange
- foodsharing brunch with one external visitor
- 2nd Bitte Wenden meeting
- project updates with contributions from 5 people
- a communal Football game
- K-Pop cooking party
- Pasta pickup
- a bulk of quality cinema watched
- garden work party

## 2. This week planning

### People arriving and leaving
- **Mon.:**
- **Tue.:** 
- **Wed.:** JaTiMiLe leave for some weeks
- **Thu.:**
- **Fri.:** 
- **Sat.:** Andrea leaves
- **Sun.:** Matthias likely comes back, Andrea comes back, Maya might arrive
- **Mon.:** 
- **Some day:** maybe Doug leaves for weekend or Anja comes

### Weather forecast

Sunny, no rain, temperature around 24. At the weekend warmer, up to 28.
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins

:judge: Due for evaluation (and seen within the last 7 days):

- *Talita* _Volunteer_ : _Absolute Days_ threshold 73/60 (+22%)
- *Nathalie* _Member_ : _Absolute Days_ threshold 209/180 (+16%)
- *Andrea* _Volunteer_ : _Absolute Days_ threshold 61/60 (+2%)

:ghost: Absent:

- *Kito* _Volunteer_ : _Absence_ threshold 94/90 (+4%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Talita]
    * 14:00 Knowledge sharing []
    * Park cars on even-side [Maxime]
    * Landgut Nemt pickup [Doug]
    * evening: Tax the Rich document in Villa Klug [Doug]
- Tuesday

- Wednesday
    * Park cars on odd-side [Maxime]
    * Visit a house [Doug]
    * Landgut Nemt pickup [Doug]
- Thursday
    * 10:00 Power Hour [Fac.: Andrea, DJ: Maxime]
    * 15:00 Sharing event (Chat in the Garden) [communal, garden]
- Friday
    * gelbe tonne [Talita]
    * 12:00 Market Pickup [Andrea]
    * 13:00 Talita's evaluation [Andrea]
    * Landgut Nemt pickup [Doug]
- Sunday
- Next Monday
    * 10:00 CoMe [Maxime]
- Next week summary
    * 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
- [Tilmann] some more AA and AAA rechargable batteries - I'll bring them in September (~15€)
- [Talita] 130€ paint and gips for finishing walls in yoga room. Paint, white and color (50-100€), plaster (10-30€)

## 4. To do
_Small fixes of things in the house_

* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] make k20 garden door easily closable from outside (or acquire a new door) https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000
* [ ] secure wash kitchen sink
* [ ] think about plastic waste container to be used in dumpster kitchen
* [ ] fix loose table leg in Lantern

## 5. Discussion & Announcements

### Round 1
- [Doug & Max] Roooof month! https://pad.kanthaus.online/roofMonthOct2021
    - 11 Oct - 07 Nov
- [matthias] Monthly teams have been printed and are hanging in dining room. Do we want a weekly teams update section in CoMe?
    - Printer team: Drum exchanged, prints again
    - Bicycle Repair Station team: Blog post published
    - Ventilation: Spontaneous meeting has happened and a ventilation PR team formed :-)
- [janina] the house is so empty, shall we suspend the dinner lottery and just have spontaneous food that is still aimed at 7 pm for the communal experience?
    - suspended! add your name to the board!
    - open Tuesday: Doug as a default
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [janina] childcare needed on Wednesdy morning for packing! Our train leaves at 10:48.
    - add yourselves to the Children Caretaking Schedule

### Round 3

<!-- suspended! -->
## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** 
- **Tue.:** 
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** 
- **Tue.:** 
- **Wed.:**
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** 
- **Week:** Janina, Tilmann

### End result
- **Mon.:** 
- **Tue.:** 
- **Wed.:** 
- **Thu.:**
- **Fri.:** 
- **Open Tuesday:** 

## 7. For next week
- [matthias] Do we want a "Nick-trailer like" trailer? Matthias would like to build it for approx. 280€. Would feature perfect suitability for green boxes, lights and a "tool/waterbottle storage"
