---
title: Coordination Meeting
date: "2023-10-09"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #314
* Date: 2023-10-09
* Facilitator: Janina
* Notary: Janina
* Physical calendar: Tilmann
* Digital calendar: Doug
* Reservation sheets: Janina
* Weather forecast: kito
* Present: Janina, Tilmann, kito, Doug

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 

![usage graph last 90 days](https://pad.kanthaus.online/uploads/925d825a-3482-46a7-82f7-627ffe2b7ec0.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 6.0 people/day (-6.9)
- **⚡ Electricity**
    - usage: 27.9 €/week (⬇️-19%)
    - of that for heating: 0.6 €/week
    - of that for hot water: 0.84 €/week
    - paid: -1.42 €/week 
    - ☀️ self produced: 55% (⬇️-12%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 11.94 €/week (⏬-47%)
    - emissions: 0.9 kg CO₂ₑ/week


### Expenditure
* [Antonin] 125€ for Gipskarton and two small bins (to be put in service in toilets)
* [Janina] 20€ for oats, lentils, couscous, polenta

### Income
* 15€ [Alejandro] Forgot to return a communal power bank, therefore he is paying for it. 

### Things that happened
#### In or around Kanthaus
* Ventilation for Piano and Elephant!
* Heating system water filters cleaned
* Socket in private got fixed
* Presentation about Tor by Dima
* Children's march documentary screening by Kate
* utopia-os presentation and discussion with Anton
* TOI timeframe voting
* k20 attic work continues
* big couch cleaning in the dining room
* more potato saving followed by potato jarring experiments
* hkw mv invitation was sent out
* several joint sessions of vampire survivors


#### Wider world
* elections in Bavaria and Hessen: afd got quite strong, green and left lost a lot
* earthquake mainly in afghanistan, many dead people
* hamas started heavy attacks on israel, more than 1000 people killed already. israel takes it as declaration of war.

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Popcorn of feedback
- remember to close doord to staircases, especially over night!

## 2. This week planning

### People arriving and leaving
* **Mon.:** kito has arrived
* **Tue.:** Antonin comes by for the day, Johann might come until Friday
* **Wed.:** 
* **Thu.:** JaTiMiLe leave for the weekend
* **Fri.:** Larissa comes back
* **Sat.:** Antonin comes back, Yuliya arrives, kito leaves, Martin leaves for one night
* **Sun.:** 
* **Mon.:** 
* **Some day:** Doug

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
_gets colder, rainer and cloudier. autumn is fully here. but wednesday will be good._
 
### Evaluations and check-ins
* Larissa and Maxime - both not here

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Janina]
    * 11:00 - Power Hour
    * 15:00 - Carrot gleaning [Janina, Martin, you?]
* Tuesday
    * Hausmüll [Martin]
    * 15:00 - 17:00 Open Tuesday
* Wednesday
    * 12:00 - 17:00 MitMachCafé
    * 18:00 - Punkrocktresen
* Thursday
    * 13:00 - 18:00 MitMachCafé
    * 18:30 - FLINTA thai boxing @ D5
* Friday
    * Biotonne [bis-quito]
    * 10:00 - Social sauna [kito]
* Saturday
* Sunday
    *  
* Next Monday
    * 10:00 CoMe [Doug]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
* 

## 4. To do
_Newest tasks on top_
* [ ] fix/improve K20-1 staircase night light (can ask Tilmann for details about wiring)
* [ ] put kanthaus logo on the new beer benches and tables
* [ ] curtains for the dragon room that prevent passersby from staring inside
* [ ] cut branch from tree in the compost corner (after leaves fall)
* [ ] cover the pipes from the rest of the room in the snack kitchen (wooden board angle?) []
    * [tilmann] maybe directly go for drywall as final measure?
      * [maxime] that would be the right but more difficult remediation indeed. I looked at options to hack a wooden corner, but didn't find satisfying options in the wood shed, so I'll leave there.
* [x] Small bin for every toilet
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I suggest redoing the whole thing. Also, use a diode to prevent over voltage. 
* [ ] replace shower curtain by (plexi)glass construction

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
* [Janina] Collective Agreements change is underway! Proposals until today 2 pm, then one week of voting. Don't forget to [take part](https://legacy.ukuvota.world/#/app/c78169b2-5ec1-3774-7718-72685e50b997/collect) if you're a volunteer or member!
* [Antonin] could someone reserve a private for Yuliya, arriving on Saturday evening?
    * everything's free, no need to stress about this.
* [Nathalie] I'd like to do some sorting and cleaning (mainly things we currently have in our basement) and I'm looking for a temporary sorting room. Is it possible to claim a room in the K18 and put a sign? Suggested time period 4 weeks.
    * sure, but not in the ground floor. 
* [Doug] door-closers: why not have them on all doors we want to have closed in winter?
    * [tilmann] all doors would be quite a lot. and requires maintenance in spring and fall. the staircase doors would make the biggest difference, maybe they're enough?
    * concern: loud slamming. but there are models that don't do that.
    * consent from this round, doug will start looking into it.
<!-- check if anyone has a point that didn't speak already -->

### Round 2 
* [kito] foodsharing pickup
* [kito] [toi voting result](https://legacy.ukuvota.world/#/app/93f08d4c-9000-391b-52d6-caab09c74634/results): 3 shorter focus times with 3-6 weeks in between

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Janina
* **Tue.:** 
* **Wed.:** kito
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Doug

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:**
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** Tilmann

### Result
* **Mon.:** Janina
* **Tue.:** 
* **Wed.:** kito
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Doug

## 7. For next week
*