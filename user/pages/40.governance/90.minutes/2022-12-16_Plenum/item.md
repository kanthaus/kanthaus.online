---
title: Plenum
date: "2022-12-16"
taxonomy:
    tag: [plenum]
---

# KH - Plenum #2
- Date: 16.12.2022
- Facilitator: Doug
- Notary: Doug, Tilmann
- Present: Doug, Tilmann, Janina, Martin, Silvan, Antonin, Larissa, Zui, Maxime

## Content
<!-- Zeiten sind nur eine grobe orientierung -->
- (10min) check in
- (5min) presentation of discussion points (What do you want to achieve with it)
- (5min) prioritization of discussion points
- (80min) discussion
    - ways of decision making (e.g. consent, sociocracy, ukuvota, general agreement)
- (10min) scheduling
- (10min) check out

## Discussion Points

### 1. [tilmann] communal attic work in January/February?
- To prepare the attic, some more things could be done than I would do by myself. Ideally we figure out the best order so we don't need undo each other's work
- For January/February:
  - finish K20 staircase: drywall, door, wall
  - removing DDT from beams and painting with SchadstoffEx
  - plastering walls
  - bring up OSB and filling
- for later or never:
  - drywall on walls
  - foldable door to top floor (Spitzboden)
  - protective construction (drywall?) around ventilation pipes
  - electricity
  - ventilation
  - heating
  - (tangentially related:) ventilation for K20 staircase toilets - people tend to leave the windows open because of the smell
- [doug] definition-of-done: dust-free. Construction: Janina, vaguely yes; Doug, at least two days altogether preferably planned; Martin, generally yes; Anneke, generally yes; Silvan, no; Antonin, yes, preferably weekend; Larissa, generally yes, preferably planned day, could also do childcare; Zui, 2/3 days in february; Maxime, some hours per week, prerably low-brain tasks.

### 2. [tilmann] extend and improve ventilation system
- baby bathroom
    - add fresh air supply for lantern and ex-food-storage
    - other affected rooms: K22-2 bathroom
    - duration: 3-6 days
    - Tilmann priority 1
- office/dining room
    - [maxime] dinning room/K20-1 office?
    - Tilmann priority 2
- the private
    - other affected rooms: communal closet, communal sleeping room
    - duration: 1-2 days
- elephant and piano room
- [Doug] wavy hands for inconvenience caused: people wanting to do ventilation work socially encouraged. Tilmann not looking for committed working groups... yet.
- add heat exchanger to increase supply air temperature with the heat pump
    - to make the heat pump more efficient (lower overall water temperature)
    - to increase comfort (warmer air flow)
    - 300-400€, plus 200-300€ for connecting the heating pipe to the attic
- [Doug] Enthusiasm to get parts... Tilmann not committing. Talk in CoMe, possibly schedule.

### 3. [doug] selling-the-KMW status update
- [doug] let's sell! 

### 4. [tilmann] Bauhaus gift card shopping
- do other people would like to have a say on what to get?
- shopping trip coordination
- schedule bauhaus trip at come, Zui, Doug, Martin
- https://pad.kanthaus.online/bauhau-shopping

## Things to schedule 
- next plenary 2023-01-13 10:00 [Martin]
- evaluations, skillshare, knowledgeshare
    - heating & ventilation [Tilmann] 28th 18:00
- nice community trip? like hiking etc?
    - [Antonin] 18th Ice-skating @ spitz/wolfberg
    - [Larissa] 29th / 30th Hiking / Sauna
    - [Janina] Sauna VK 31st

## Themenparkplatz (things that come up and we don’t want to forget about)

...

### bigger things (for toi?)

### for social sauna
