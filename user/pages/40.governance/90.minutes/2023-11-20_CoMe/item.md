---
title: Coordination Meeting
date: "2023-11-20"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #320
* Date: 2023-11-20
* Facilitator: Martin
* Notary: Janina
* Physical calendar: Kito
* Digital calendar: Maxime
* Reservation sheets: Larissa
* Weather forecast: Tilmann
* Present: Martin, Venla, Kito, Sanne, Maxime, Larissa, Loup, Tilmann, Janina

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/701c0ef5-44c5-49bd-aac8-f82714b12f90.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 16.9 people/day (+3.0)
- **⚡ Electricity**
    - usage: 97.51 €/week (⬆️+21%)
      - for heating: 33.09 €/week
      - for attic heating: about 3€
      - for hot water: 6.86 €/week
    - paid: 89.75 €/week 
    - ☀️ self produced: 29% (⬇️-17%)
    - emissions: 46 kg CO₂ₑ/week
- **💧 Water**
    - paid: 21.74 €/week (⬆️+26%)
    - emissions: 1.7 kg CO₂ₑ/week


### Expenditure
- [Martin] oats and flour and stuff for 10€

### Income
- maybe none?

### Things that happened
#### In or around Kanthaus
- attic work finished (for now...) and small inauguration party
- FFJ get-together over the weekend
- last market pickup of the year
- interesting plame with many topics
- flea market at mitmachcafé with verschenkeladen-stand
- doug had travel trouble
- evidence safari is ongoing
- washing machine broke and got fixed
- radiator fans now turn on and off together with the heating system
- ex-food-storage shelf is being cleared

#### Wider world
- Argentina elected the far-right president
- genocidal propaganda is raging in the middle east
- demonstration against PKK took place in Berlin
- Linke lost their Bundestag seats
- Nazis got in a fight with themselves in Vienna, because they couldn't tell who was left and who was right

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Popcorn of feedback
<!--
Is there any feedback you would like to give to others - the whole groups or individuals? Is their any reflection about yourself you would like to share with the others? If you would like to give feedback to a person in the room, please do it in third person.
-->
- advertise toilets in K22 more to groups
- attic is really nice, thanks to everyone involved
- bathroom 'public' light is often not trusted

## 2. This week planning

### People arriving and leaving
* **Mon.:** Kate arrives (late)
* **Tue.:** 
* **Wed.:** Klara, Antonin and maybe Liora come
* **Thu.:** Kito leaves, Sanne leaves, Venla leaves
* **Fri.:** MSV-group arrives, Antonin and Liora likely leave
* **Sat.:** Kate leaves
* **Sun.:** MSV-group leaves, Kito comes back, Vroni might arrive, Antonin and Liora likely come back
* **Mon.:** 
* **Some day:** Doug maybe comes back

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->

_It's freezing on Wednesday, warmer in between, with some rain towards the end of the week._

 
### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Loup_WA Visitor: Days Visited threshold 25/21 (+19%)
- Antonin Volunteer: Days Visited threshold 68/60 (+13%)
    - [Antonin] would be happy to be evaluated on thursday or friday, but also happy with next week if this one is too busy

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Martin]
    * 11:00 - Power Hour
* Tuesday
    * Hausmüll [Martin]
    * 18:00 - 30 Jahre alternative Subkultur in Wurzen @NDK [Kito, Larissa]
* Wednesday
    * public holiday - some dumpster diving action? :)
* Thursday
    * 10:00 - Antonin's evaluation [Martin]
    * 13:00 - 18:00 MitMachCafé
    * ~~18:30 - FLINTA thai boxing @ D5~~
    * maybe some time in the day: fruit juice palett action
    * two penny pickups [Janina, Martin]
* Friday
    * there are trains again!
    * 18:00 Leipzig Critical Mass [Martin, you?]
* Saturday
    * Biomüll [Loup]
    * one more penny pickup [Tilmann]
* Sunday
    * 19:00 Rojava people in Leipzig [Kito]
* Next Monday
    * 10:00 CoMe [Kito]
* Next week summary
    * Bausyndikat group over the weekend
    * Project Updates

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
- [Janina] spreads

## 4. To do
*See analog list in the elephant room*

**Done**
- the tree in the compost corner lost a branch

**Feedback**
- some people are too lazy to write on paper

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [larissa] Update MSV-Meeting: see https://yunity.slack.com/archives/C3RS56Z38/p1698921422872049 we will be around 30 people now for sure. I would heat the yoga room and attic before. Also I would join the cooking-team from msv on friday, so we can make dinner for everyone in the house. maybe someone else from kh can join, depending on the amount of kh-people? Also we got a big juice-donation (1 euro-palett) from a company which will probably arrive on thursday. any volunteers for unloading and bringing it down to the basement. KH can of course also drink the juice/smoothies :)
    - dinner will be at 6pm already
    - how many KH people here? less than 10 for sure
    - schedule will be printed and made public so we know busy times for kitchen and bathrooms
    - larissa will make signs and put them everywhere to show where toilets are and such
    - communal cooking is not specifically for kh on the weekend, but it might end up being enough anyway
        - [Martin] what about food? How many meals? I don't think we will have enough for 30 people and KH for a weekend. 
        - there will be shopping and smoothies coming in, penny pickups before and during 
- [Antonin] hosting Lara 
  - we got a request from Lara to come to Kanthaus again (last visit in August 2020 - I don't actually remember meeting her but according to the record I should have, oops!). 
  - she proposes to come on Dec 6th already, leaving before xmas, so that it does not collide with the Month of Calm
  - would anyone like to host? (time-wise I potentially could but I'm already hosting Klara so maybe it's a bit of a stretch)
  - given the MoC and concerns about house fullness in 2024 I totally understand if people prefer to turn down the request. To me she seems to be a great fit for the house (based on what she writes and Anja's memory of her), but if we're full, we're full.
  - No resistance for the time before Christmas. Janina will host. We'll see later how we continue in January.
- [kito] Vroni coming next week
    - is it too full? if not kito would host her.
    - no resistance.
- [kate] can anyone see themselves helping with a couple of small construction tasks in the freeshop on Wednesday/Thursday? Remove the top 2 shelves (no one can reach there and they make the room dark and are dust traps) and hang a mirror. Martin looked at it and said it was simple enough. Message me.
- [Janina] MoC decision following [the corresponding Slack post](https://yunity.slack.com/archives/C3RS56Z38/p1700153404555419)
    - proposed time frame: December 18 to January 14 (4 full weeks)
    - voting results: 3 support, 2 indifference, 1 slight resistance
    - adopted!
- [Loup] Floor plan creation
    - maybe wait until doug comes back so that there's no unnecessary work done
    - propriety formats as output would be uninteresting
    - putting lots of work into completely new plans seems a waste
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Antonin] Liora (mentioned above in the schedule) is a friend from France who is passing by. She's interested in discovering Kanthaus and will likely be keen to meet you all (not just visiting me), I hope it's okay. She's aware that the MSV group is coming so we plan to retreat to OstPost to help leave space in KH during that time. If you have any concerns let me know and I can tell her not to come.
    - no resistance
- [Martin] Biomüll is full. What to do?
    - request another/bigger container
    - until then let's use buckets from the garden shed as temporary storage when the bin is full
- [kito] CryptoKüfa next monday(?)
    - unsure if it will really happen
    - there was no advertisement so far, more help would be needed
    - if there is interest in this please contact kito
- [Janina] Garden prep for the winter
    - stuff could be brought to the attic if they are not too dirty or ugly
    - clearing the hose, switch off tap water
    - communal action?
    - Tilmann would leave the rainwater tank, it hadn't had a problem with freezing in the past
- [Larissa] Riff! It's gonna be the 19th!


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Result
- **Mon.:** Sanne, Janina
- **Tue.:** Martin, Maxime
- **Wed.:** kate
- **Thu.:** antonin, Loup
- **Fri.:** Larissa + group + spontaneous KH helpers
- **Open Tuesday:** opentuesdaykate, Venla

## 7. For next week
