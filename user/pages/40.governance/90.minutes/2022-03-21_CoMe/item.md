---
title: Coordination Meeting
date: "2022-03-21"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #236
* Date: 2022-03-21
* Facilitator: Maxime
* Notary: Janina
* Mika caretaker: Larissa
* Levi caretaker: Tilmann
* Physical board caretaker: Anja
* Digital calendar: Antonin
* Reservation sheet purifier: Kito
* Present: Kito, Larissa, Silvan, Matthias, Doug, Antonin, Prairie, Maxime, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf482c.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.6 people/day (+0.7)
- **⚡ Electricity**
    - usage: 71.95 €/week (⬇️-15%)
    - of that for heating: 51.6 €/week
    - paid: 16.21 €/week 
    - ☀️ self produced: 46% (⬆️+2%)
    - emissions: 30 kg CO₂ₑ/week
- **💧 Water**
    - paid: 19.37 €/week (⬆️+7%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
* [Antonin] 38.23 EUR for washing machine parts
* [Anja] ~20€ for soil (potplants)

### Income
* 0€ in the shoe

### Things that happened
#### In or around Kanthaus
* There was a a bonfire party for Peace at Villa Klug: the war should stop soon now
* Critical mass!
* Repotting action for aaaall Kanthaus indoor plants (with cake!)
* Maxime was reaccepted as Member
* Mika had a fever one night
* Some people went bouldering in Leipzig
* A delegation swam in Wolfsbergsee (and had a look at the Lüptitz place)
* Kaffeekränzchen with Laden, Klugi and NDK people took place again

#### Wider world
* Germany made an energy deal with Qatar to reduce Russian dependence in some years


## 2. This week planning

### People arriving and leaving
* **Mon.:** Andrea arrives, Beere arrives
* **Tue.:**
* **Wed.:** Anja leaves
* **Thu.:** 
* **Fri.:** Beere leaves, Larissa leaves
* **Sat.:** Anja comes back
* **Sun.:** Antonin leaves
* **Mon.:** Larissa comes back, kito leaves probably
* **Some day:** 


### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_Sun all week, temperatures reach up to 15°C in the first half of the week but the nights are still freezing_

### Evaluations and check-ins

- Larissa Member : Absolute Days threshold 181/180 (+1%)
- Matthias : by his own request

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Maxime]
    * Park ~~KMW and ~~trailer on even/this side [Antonin]
    * 15:00 Radeln Ohne Alter Wurzen meeting @elephant room [Antonin, Nathalie]
    * Afternoon: Apple tree pruning knowledge sharing [Anja]
* Tuesday
    * 18:00 Workout @yogaroom [Mika]
    * 18:30 Book club @Villa Klug [Jule]
* Wednesday
    * 11:00 Matthias evaluation [Larissa]
    * Park ~~KMW and ~~trailer on odd/that side [Antonin]
* Thursday
    * 10:00 Power Hour [Fac.: Matthias, DJ: ?]
* Friday
    * Gelbe Tonne [Doug]
    * 11:00 Project updates [Kito]
* Saturday
    * 15:00 Punk Rock concert @Spitzenfabrik [Kito]
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Janina]
* Next week summary
    * Wednesday: Focus group sustainability

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit


## 3. Shopping plans
- [kito] Oats (25kg for 73,75€; Unverpacktladen Leipzig)
- [tilmann] a new used washing machine yet to be found for ~100€ maybe


## 4. To do

_Newest tasks on top_
* [ ] make the wall in K20-2 bathroom where the boiler has been nice
* [ ] remove old gas heater from basement (to trash corner?)
* [x] remove boiler and pass-through heater from K20-2 bathroom [Tilmann]
* [ ] repair WiFi in K22-2
* [ ] repair Schaukasten 
* [ ] Making new signs for the commode in the hallway (?)
* [ ] Door-closer for Elefant-staircase door
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [janina] more visitor requests:
    - helen, workaway: really cool politically active person I wanted to host, but now travel plans changed to early june
    - toby, workaway: musician from montreal, wants to come in june
    - johnny, mail: got our contact from funkenhaus, looks for a place to live but just wants to visit for now
* [maxime] the newspaper reserve is running low
    * [janina] so are the bread bags!
* [doug] dates for
    * summer/spring fest
    * hackathon
    * tag der offenen tür
* [antonin] silent office plug location plan will lead to some expenses soon. give feedback now if you have thoughts! :) https://yunity.slack.com/archives/CNLJL8PEG/p1647507326697899
* [janina/silvan] feedback of Kaffeekränzchen
    * Frauen-Schreibwerkstatt
    * football in June
    * Future of Kaffeekränzchen: Fokusgruppe Nachhaltigkeit (more a meeting than a chat)

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [maxime] vol/mem discussion on zui's message
* [Matthias] lunch Team?
    * meta discussion about exclusivity of lunch club when there's not much nice veggies
    * mon: larissa
    * tue: silvan
    * wed: prairie
    * thu: janina, tilmann
    * fri: doug

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** kito
* **Tue.:** Janina
* **Wed.:**
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** Silvan
* **Tue.:** Larissa, Antonin, Matthias, Maxime, Doug
* **Wed.:** Maxime
* **Thu.:** Larissa, Antonin, Silvan
* **Fri.:** Larissa, Matthias, Silvan
* **Open Tuesday:** Larissa, Antonin, Matthias, Maxime, Prairie
* **Week:**

### Result
- **Mon.:** Kito, Doug
- **Tue.:** Janina
- **Wed.:** Silvan, Larissa
- **Thu.:** Matthias, Prairie
- **Fri.:** Antonin, Maxime
- **Open Tuesday:** Anja

## 7. For next week
* 
