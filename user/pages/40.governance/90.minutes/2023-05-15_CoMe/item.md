---
title: Coordination Meeting
date: "2023-05-15"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #293
* Date: 2023-05-15
* Facilitator: Janina
* Notary: Kito
* Children caretaker: Kita
* Physical board caretaker: Gaia
* Digital calendar: Martin
* Reservation sheet purifier: Gaia
* Present: Tilmann, Martin, Janina, Gaia, Maxime, Kito, Dorota

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/6663f3ab-5bf0-4139-b212-b9c2dc532c3c.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.7 people/day (+1.9)
- **⚡ Electricity**
    - usage: 24.41 €/week (⬇️-3%)
    - of that for heating: 0.77 €/week (probably mostly Standby?)
    - of that for hot water: 1.64 €/week
    - paid: -48.4 €/week 
    - ☀️ self produced: 79% (0%)
    - emissions: 12 kg CO₂ₑ/week
- **💧 Water**
    - paid: 18.07 €/week (⬆️+2%)
    - emissions: 1.4 kg CO₂ₑ/week

### Expenditure
- 55€ roof window blind [tilmann]
    - K20 attic is now fully covered and dark! (~200€ in total, much less then expected)
- 10€ some oat, rice, lentils [maxime]
- 15€ oats, spreads
- Bananeira order was placed!

### Other Income
- shoe was not checked, blame the facilitator!

### Things that happened

#### In or around Kanthaus
- Wenceslaigassenfest!!! many people and it was nice and great
- Mika rode a bicycle
- ESC watching, people enjoyed it
- Maxime fixed his computer, Tilmann an Auerworld power drill
- Mika vacuumed the piano room
- Kito joined a Critical Mass in Leipzig


#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
-  First round of Turkish presidential election, looks baad :/
-  forest occupation in Berlin

## 2. This week planning

### People arriving and leaving
* **Mon.:** Larissa comes back
* **Tue.:** Bodhi and Lise arrive, Cecilia_WA arrives
* **Wed.:** Gaia leaves
* **Thu.:** Martin leaves, JaTiMiLe leave
* **Fri.:** kito leaves probably
* **Sat.:**
* **Sun.:** Martin comes back, JaTiMiLe come back, Antonin comes back
* **Mon.:** 
* **Some day:** Larissa and Silvan leave


### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_some clouds, but generally nice spring weather. gets a bit colder before it gets warmer again._

### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Martin Volunteer : Days Visited threshold 65/60 (+8%)
- Silvan Visitor : Days Visited threshold 22/21 (+5%)
- Janina Member : Days Visited threshold 188/180 (+4%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * 11:00 Power Hour trial [Janina] 
    * Monday food care [part of PH]
* Tuesday
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 15:30 IDAHIT* @market square
    * 19:00 Punkrock-Tresen @D5
* Thursday
    * no Thursday power hour this week!
    * after dinner Project Updates [Kito]
* Friday
     
* Saturday
    * Yellow Bins [Maxime]
* Sunday
    * a Flunkyball tournament and concert (Jazz/Folk/Akustikpunk/...) in Spitzenfabrik Grimma
* Next Monday
    * 10:00 CoMe [Martin]
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* stairs for the attic at some point....

## 4. To do
_Newest tasks on top_
* [x] fix the K20-2 bathroom shower head holder [Martin]
* [ ] install missing smoke detectors [Martin]
* [x] fix the extension of the private storage with a new shelf to the wall next to the main bathroom [Martin]
* [ ] give away saved windows in K20 basement (a lot) maybe https://www.trashgalore.de/? (martin)
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check fire extinguishers [Antonin]
* [ ] de-clog compost toilet drain
* [ ] modify the task lottery to prioritize open tuesday [Maxime]
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1

- [Martin] Workaway request. Kate. Starting June 6th. Need host. I can do it for the first two weeks, but would rather not. 
   - [Antonin] I could do the first week and hand over to someone else after
   - [Janina] I'll be cycling the first two weeks and thewn gone in July but would be happy to host her generally
   - we found enough people
- [Antonin] I am proposing a [change to the collective agreements](https://legacy.ukuvota.world/#/app/f44bd90b-3b7a-ba3d-2564-bea41f82e8ee/collect): stating that incense should not be used in the house.
- [Janina] DRK request to access the construction archives on our behalf
    - do it
- [Maxime] Bio bin was disgusting when it was emptied last week, so it's now cleaned and moved in it's summer location
    - only close it, when it's raining
    - remove as much liquid as possible before putting it to the bin (but clean the sink afterwards)
    - bin garage?
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Janina] (last point) Power Hour! Please add what needs attention!
    - Bins: Dorota
    - Bathrooms K20: you?
    - Vacuuming 1st floor K20+K22: Maxime
    - Incoming food care today: Janina, Tilmann
    - Food sorting later in the week: Kito
    - Plant care: Dorota
    - Vortex: Martin
    - Communal closet:
    - Kitchens: Gaia
- [Kito] Long Term Decision Meeting Update
    - FFJ is in progress, supposed to start on October 9 :tada:
    - guiding principles group had one meeting so far
    - attic has blinds, a plan for electricity and a decision on heating
    - 'getting rid of slack' and 'building weeks' didn't progress so far
- [Maxime] mold/humidity in the Cave
    - ventilation plan for the summer?
    - [tilmann] it's difficult because it's partially under the ground - structural problem, in winter partially possible with heating, also when renovating some cover against moisture was taken
    - [janina] can we raise the floor so it's not underground anymore
    - [tilmann] make a warning sign?


### Last spontaneous points?
- no

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Dorota
- **Tue.:** Gaia
- **Wed.:** 
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** 

### Unavailabilities
- **Mon.:** Kito, maxime
- **Tue.:** Kito
- **Wed.:** Kito
- **Thu.:** 
- **Fri.:** Kito
- **Open Tuesday:** maxime
- **Week:** Martin, Janina, Tilmann

### Result
- **Mon.:** Dorota
- **Tue.:** Gaia
- **Wed.:** Maxime
- **Thu.:** Kito
- **Fri.:** 
- **Open Tuesday:** 

## 7. For next week