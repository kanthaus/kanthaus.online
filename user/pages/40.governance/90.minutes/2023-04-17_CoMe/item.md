---
title: Coordination Meeting
date: "2023-04-17"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #289
* Date: 2023-04-17
* Facilitator: Kito 
* Notary: Janina
* Children caretaker: Kita
* Physical board caretaker: Anneke 
* Digital calendar: Antonin
* Reservation sheet purifier: Larissa
* Present: Anneke, Antonin, Silvan, Janina, Larissa, Martin, Kito, Tilmann

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/cafedf7b-7c68-4fe5-a5e6-44ed467dc322.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.0 people/day (-2.3)
- **⚡ Electricity**
    - usage: 47.63 €/week (⬇️-21%)
    - of that for heating: 15.81 €/week
    - of that for hot water: 3.58 €/week
    - paid: 14.39 €/week 
    - ☀️ self produced: 69% (⬇️-12%)
    - emissions: 23 kg CO₂ₑ/week
- **💧 Water**
    - paid: 15.8 €/week (0%)
    - emissions: 1.2 kg CO₂ₑ/week


### Expenditure
* 5€ Allepo soap (larissa)
* 8€ sunflower oil (Anneke)

### Income
* 30€ "shoe"

### Things that happened
#### In or around Kanthaus
* longterm decision making meeting #2
* Pizza-Party!
* Kito became Member
* Electricity/Heating stats fixed and extended
* K20 attic floor is finished
* Bike hoist system installed in workshop

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->


## 2. This week planning

### People arriving and leaving
* **Mon.:** Martin leaves
* **Tue.:** Lise arrives, kito leaves
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Martin comes back, SiLaMiLe leave, (Lützi Bezugsgruppe arrives?)
* **Sat.:** 
* **Sun.:** LaSiLeMi come back, Lise leaves
* **Mon.:** Gaia_WA and maru_zui arrive
* **Some day:** Antonin leaves in the week-end, Bodhi comes towards the end of the week

### Weather forecast


<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
- still cloudy during most of the week, but it's getting warmer on the weekend (up to 21°C!)


### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Nathalie Visitor : Days Visited threshold 23/21 (+10%)
- Silvan Visitor : Days Visited threshold 22/21 (+5%)

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Tilmann Member : 6 days until Days Visited threshold (180)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Kito]
    * 11:15 Future of attic work [Janina, Martin, Antonin, Tilmann]
    * Monday food care [Anneke, Tilmann, Antonin]
* Tuesday
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 10:00 Attic work session [Janina, Tilmann, you?]
    * 11:00 Silvan's evaluation [Antonin]
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Janina]
* Friday
    * Yellow bins [Larissa]
    * 11:00 Project Updates [Antonin]
    * 19:00 event @D5: Revolution für das Klima - Warum wir eine ökosozialistische Alternative brauchen
* Saturday
    * 10:00 Attic work session [Antonin, Tilmann, you?]
* Sunday
    * afternoon/evening: Annekes backstory (time depending on group hosting)
* Next Monday
    * 10:00 CoMe [Larissa]
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- ~150€ ventilation parts for K20 attic [tilmann]
  - go for it!
- fresh air filter (F7), to be exchanged during/after summer [tilmann]
  - either 1x for 58€
  - or 4x for 49€ per piece (196€)
  - buy 4 then we're settled for 4 years!
- kito's order is still work in progress

## 4. To do
_Newest tasks on top_
* [x] re-solder bathroom bright light switch [Martin]
* [x] properly attach cable of upper staircase toilet light [Martin]
* [ ] give away saved windows in K20 basement (a lot)
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [ ] check all fire alarms [Martin]
* [ ] check fire extinguishers [Antonin]
* [x] fix the physical weekly board [Tilmann+kids]
* [ ] treat mold in Freeshop Lounge and Hipster Room
* [ ] de-clog compost toilet drain
* [ ] modify the task lottery to prioritize open tuesday []
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
- [Antonin] visitors looking for a host
    - Dorota (alone, 2 weeks, arrival date unclear)
        - Janina can host
    - Feuerqualle (4 people, 30.4 - 4.5)
        - Antonin and Janina co-host
- [Doug] Coronavirus [collective agreement process] (https://legacy.ukuvota.world/#/app/c6b4406c-e4c0-8f0e-70a0-bb17924ebe4e/collect) started.
<!-- check if anyone has a point that didn't speak already -->

### Round 2


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Zui
* **Tue.:** 
* **Wed.:** Anneke
* **Thu.:**
* **Fri.:** 
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** 
* **Tue.:** Antonin
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Antonin, Janina
* **Week:** Martin, Kito, Tilmann, Silvan, Larissa

### Result
- **Mon.:** zui
- **Tue.:** 
- **Wed.:** Anneke
- **Thu.:** Janina
- **Fri.:** 
- **Open Tuesday:** Antonin

## 7. For next week

