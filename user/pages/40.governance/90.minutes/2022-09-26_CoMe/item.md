---
title: Coordination Meeting
date: "2022-09-26"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #262
* Date: 2022-09-26
* Facilitator: Tilmann
* Notary: Maxime
* Mika caretaker: Kita 
* Levi caretaker: Janina
* Physical board caretaker: Larissa 
* Digital calendar: Maxime
* Reservation sheet purifier: Larissa 
* Present: Tilmann, Larissa, Maxime, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/e4604dcd-e1e9-4b07-80e7-97e970e9bcd8.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.9 people/day (+1.6)
- **⚡ Electricity**
    - usage: 37.06 €/week (⬇️-10%)
    - of that for heating: 5.62 €/week
    - paid: -27.3 €/week 
    - ☀️ self produced: 68% (⬆️+7%)
    - emissions: 11 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.07 €/week (⬇️-2%)
    - emissions: 0.9 kg CO₂ₑ/week

### Expenditure
* [tilmann] 42€ fresh air filter
* [tilmann] 98€ toilet pipes for attic
* [janina] 15€ for oil, rice and starch

### Income
* 250€

### Things that happened
#### In or around Kanthaus
* more ceiling hooks in yoga room
* new F7 fresh air filter installed
* toilet pipes connected to the roof
* more insulation preparation progress
* flat-warming party at Anja and Antonin's
* the first potatoes and onions of the year were collected
* much food found
* got some free cellulose insulation
* Nathalie became Visitor, Kito stays Volunteer

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* 

## 2. This week planning

### People arriving and leaving
* **Mon.:** Martin returns
* **Tue.:** Zui comes back?
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Sat.:** 
* **Sun.:** 
* **Mon.:** Antonin comes back, Maxime leaves
* **Some day:**

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Andrea Volunteer : Absolute Days threshold 88/60 (+47%)
- Tilmann Member : Absolute Days threshold 216/180 (+20%)
- Maxime Member : Absolute Days threshold 192/180 (+7%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * Paper waste [Tilmann]
    * 10:00 CoMe [Tilmann]
    * Post-Sunday-Dumpster-Diving Food washing/sorting [Janina, Larissa, Tilmann, Maxime]
    * ~~Park KMW & Trailer on even/this side []~~
* Tuesday
    * Rest waste [Larissa]
    * 10:30 KMW tour to Leipzig, to get more free cellulose [Tilmann, Maxime]
    * 14:30 AG PSW meeting @Fansipan? [Janina, Zui, +2]
    * 15:00 - 17:00 Open Tuesday [Silvan]
* Wednesday
    * Park KMW & Trailer on odd/that side [Tilmann]
    * 10:30 Andreas evaluation [Janina]
    * 12:00 Bitte Wenden coworking @office? [Janina, Unkraut, Tin]
* Thursday
    * 10:00 Power Hour [Fac.: Maxime, DJ: ?]
    * Organic waste [Maxime]
* Friday
    * 12:00 Market Pickup [Maxime]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * Levi's 1st day of Kita! :D :tada:
    * 10:00 CoMe [Janina]
* Next week summary
    * Finance days for WaWü
    * Project Updates

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* 

## 4. To do

_Newest tasks on top_
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put up and down?
* [ ] remount snack kitchen radiator [Martin/Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [tilmann] got the insulation contract: ~6000€ - let's do it!?
    * yay!
* [tilmann, janina] attic tour/presentation would be nice, maybe we do a spontaneaous video this week?
* [janina] visitor request from [astrid](https://www.workaway.info/en/workawayer/AstridChevriere) for October 22 - 31
    * Larissa can imagine after 24th
    * We will ask Martin for the first days

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [tilmann] some more free cellulose insulation can be saved in Leipzig (south of Plagwitz, 31km) between Monday and Wednesday - anybody up for a KMW+trailer tour?
    * KMW ready?
    * can be combined with something? KoLa?
        * Janina will ask Anja
* [janina] lunch trial feedback
    * [janina] very different feeling of the day, would need a much longer trial phase to really adapt. happy to drop it for now in favor of dinner.

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Janina, Tilmann
* **Tue.:** 
* **Wed.:** Larissa
* **Thu.:** 
* **Fri.:** Maxime
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** Martin, Maxime
* **Tue.:** Maxime
* **Wed.:** 
* **Thu.:** Maxime
* **Fri.:** 
* **Open Tuesday:** Maxime
* **Week:** 

### Result
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 

## 7. For next week
* 

