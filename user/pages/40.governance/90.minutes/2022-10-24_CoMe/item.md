---
title: Coordination Meeting
date: "2022-10-24"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #266
* Date: 2022-10-24
* Facilitator: Tilmann
* Notary: kito
* Mika caretaker: Kita
* Levi caretaker: Kita
* Physical board caretaker: Anneke
* Digital calendar: Doug
* Reservation sheet purifier: Janina
* Present: Zui, Martin, Anneke, Lara, Larissa, Doug, Janina, Tilmann, Kito, Silvan

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/bff51960-f7b4-48fd-91ff-d5be026acc1a.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 17.0 people/day (+3.7)
- **⚡ Electricity**
    - usage: 60.17 €/week (⬆️+5%)
    - of that for heating: ~~13.06 €/week~~
    - paid: 13.86 €/week 
    - ☀️ self produced: 55% (⬇️-10%)
    - emissions: 22 kg CO₂ₑ/week
- **💧 Water**
    - paid: 22.12 €/week (⬆️+18%)
    - emissions: 1.7 kg CO₂ₑ/week


### Expenditure
- [kito] 73,75€ oats
- [Janina] 8€ margarine
- [Antonin] some stuff from OBI


### Income
- 20€ pfand

### Things that happened
#### In or around Kanthaus
- many visitors!
- Mika's birthday party
- people went to ADI for the introduction to anarchism
- some stomach troubles
- wawü tax report was handed in! 
- a lot of attic clean up, walking the stairs up and down

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->


## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** Martin leaves, AgQFiZo leave, Roswitha & Gerd leave, Silvan leaves
* **Wed.:** Larissas parents come for one night, Willy comes for the day
* **Thu.:** Dorotha leaves
* **Fri.:** Martin comes back (maybe)
* **Sat.:** 
* **Sun.:** Anneke leaving (probably), Silvan comes back
* **Mon.:** 
* **Some day:** 

### Weather forecast
- quite nice, almost no rain, up to 20 degrees, especially towards the weekend it gets nicer
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
:judge: Due for evaluation (and seen within the last 7 days):
- Matthias Volunteer : Absolute Days threshold 104/60 (+73%)
- Zui Member : Absolute Days threshold 201/180 (+12%)
- Bodhi Member : Absolute Days threshold 200/180 (+11%)
- Janina Member : Absolute Days threshold 192/180 (+7%)
:clock1130: Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Larissa Member : 6 days until Absolute Days threshold (180)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe
    * Monday food care []
* Tuesday
    * 15:00 - 17:00 Open Tuesday []
* Wednesday
    * 9:30 Janina Evaluation [Doug]
* Thursday
    * 10:00 Power Hour [Fac.: Janina, DJ: ?]
    * 15:00 CryptoParty [zui]
* Friday
    * 10:00 rhetoric workshop @D5 (10€)
    * 12:00 Market Pickup [Janina]
    * 14:00 - 17:00 Kulturcafe @D5
* Saturday
* Sunday
    * Leipzig Hausprojekt people come visiting [zui]
* Next Monday
    * 10:00 CoMe [zui]
* Next week summary
    * Wednesday: talk about future of Kulturcafe things at D5
    * Thursday: potential 1st meeting of 'Bündnis Sozialproteste'
    * Thursday afternoon: Zui's evaluation [Doug or someone else]
    * Friday: Workshop Reparaturräte at Kanthaus
    * SoLaWi group (9 people) arrives on the weekend and stay for a whole week

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [tilmann] 500€ for attic doors again - still not sure which to buy -> request for 500€ max for 2 fire protection doors, but not sure if they will be bought in the end
-> general approval, option to talk after CoMe

## 4. To do

_Newest tasks on top_
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put up and down? - why?
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
- [Antonin?, Janina] [Ukuvota result about KMW](https://ukuvota.world/#/app/30039c39-9840-301f-f0b0-d70b45d9030f/results) is out: Anybody up for starting the selling process?
    - figure out the price
    - chandi didn't vote, but had enough time to react
    - Doug can imagine doing it, Matthias was up for writing a description about it
- [Anneke] car parking
    - in almost all the streets there is street cleaning
    - giving the key to KH
- [zui] presenting the idea for monthly plenary, for discussion, decision making and longterm scheduling (already talked in social sauna about it)
    - idea just to try it out
    - replacing normal CoMe or extra meeting? maybe extra meeting where Zui has time
    - replacing the MCM, but on friday mornings
    - structure? decide on topics beforehand/in the begining, should not take longer than 2 hours
    - Pad to collect ideas -> Zui will do it
    - first meeting: 11.11., 10 o'clock
- [Silvan] Grundstücksverschmelzung
    - Roman said it's very important
    - not sure what needs to be done, Matthias was last person to work on it, but he doesn't know what to do

<!-- check if anyone has a point that didn't speak already -->
 
### Round 2
- [Doug] Card game which needs 7 people, who's up?
- [Janina] child care is appreciated, especially picking them up at 11 and ride the bike until they fall asleep

<!-- check if anyone has a point that didn't speak already -->

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Janina
* **Tue.:** Anneke
* **Wed.:** kito
* **Thu.:** zui, Doug
* **Fri.:** Larissa
* **Open Tuesday:** Lara

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Doug
* **Open Tuesday:** 
* **Week:** Martin, Silvan, Tilmann

### Result
- **Mon.:** Janina
- **Tue.:** Anneke
- **Wed.:** Kito
- **Thu.:** Zui, Doug
- **Fri.:** Larissa
- **Open Tuesday:** Lara


## 7. For next week
* 
