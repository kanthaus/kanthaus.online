# CoMe #286
* Date: 2023-03-27
* Facilitator: Martin
* Notary: Tilmann/Martin
* Children caretaker: Kita
* Physical board caretaker: Janina
* Digital calendar: Martin
* Reservation sheet purifier: Martin/Janina
* Present: Martin, Silvan, Janina, Tilmann

----

## 0. Check-in round

## 1. Last week review
### Stats
- **Present:** 10.1 people/day (-0.6)
- **⚡ Electricity**
    - usage: 92.42 €/week (⬇️-36%)
    - of that for heating: 12.6 €/week
    - paid: 11.84 €/week 
    - ☀️ self produced: 68% (⬆️+10%)
    - emissions: 20 kg CO₂ₑ/week
- **💧 Water**
    - paid: 11.69 €/week (⬇️-9%)
    - emissions: 0.9 kg CO₂ₑ/week


### Expenditure
- 80€ Bike parts and hoist system [Martin]
- 60€ medicine [Tilmann]
  - Gelomyrtol (against mucus)
  - Bifonazol (against fungal infection)
  - Loratadin (against allergic reaction)
- 22€ lentils, millet, rice [Janina]


### Unusual Income
- 5€ Spenden jar

### Things that happened
#### In or around Kanthaus
- blue-silver ebike is almost ready
- some sickness came over us
- visitors over the weekend
- informal project updates
- mika changed levi for konrad, as his best friend, for two days


#### Wider world
- still many wars all over the world
- Berlin not earlier CO2 neutral (will it ever be?)
- today strike in germany for trains etc.

## 2. This week planning

### People arriving and leaving
* **Mon.:** Larissa comes back (hopefully)
* **Tue.:** Felix and his bike tour companions might arrive, for one night
* **Wed.:** 
* **Thu.:** 
* **Fri.:** zui arrives, Silvan leaves
* **Sat.:** zui leaves, Alba (Andrea's friend) arrives
* **Sun.:** 
* **Mon.:** 
* **Some day:** 

### Weather forecast

some rain, some cold, some sun. Spring arrived. 

### Evaluations and check-ins
- none

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Martin]
	* ~~Monday food care []~~
* Tuesday
    * 10:00 - 13:00 Attic session [Tilmann, you?]
    * 15:00 - 17:00 Open Tuesday [lottery]
    * Black bin [Tilmann]
* Wednesday
    * 10:00 - 13:00 Attic session [Tilmann, Janina, you?]
    * 18:30 Foodsharing meeting
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Martin]
    * 15:00 Social sauna [Tilmann]
* Friday
    * finances [Larissa, zui]
    * 10:00 - 13:00 Attic session [Tilmann, you?]
    * Organic waste [Martin]
* Saturday
    * 10:00 - 13:00 Attic session [Tilmann, you?]
    * anonymous birthday party in Leipzig 
* Next Monday
    * 10:00 CoMe [Janina]
* Next week summary

## 3. Shopping plans

## 4. To do
_Newest tasks on top_
* [ ] fix the phisical weekly board []
* [ ] treat mold in Freeshop Lounge 
* [ ] de-clog compost toilet drain
* [ ] do the oil treatment to wooden tables in dining room and snack Kitchen []
* [ ] modify the task lottery to prioritize open tuesday []
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom [Larissa]
* [ ] Light: elephant room
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

### Round 1
- [Janina] ToI follow-up: Responsibilities and critical project focus
    - Responsibilities system should start in April. [Please revisit the pad(s)](https://pad.kanthaus.online/kh-responsibilities?view#)! 
    - [Critical project focus](https://pad.kanthaus.online/criticalProjectFocus#) could start whenever; first asynchronous reflection, then group session. What time would suit people?
- [Larissa] Anyone interessted in participating in the Maker-fest on 2023-06-10 @Kulturgut Linda. More details see here: https://yunity.slack.com/files/USLACKBOT/F04V4R2PSP8/frohburg_-_made_by_you_10.06.2023
- [Tilmann] Schüttung is still not here, will check for sure this week
<!-- check if anyone has a point that didn't speak already -->

### Round 2


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Result

* **Mon.:** Martin
* **Tue.:** Janina
* **Wed.:** Larissa, Silvan
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Doug
