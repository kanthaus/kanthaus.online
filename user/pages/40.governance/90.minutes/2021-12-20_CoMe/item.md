---
title: Coordination Meeting
date: "2021-12-20"
taxonomy:
    tag: [come]
---

# CoMe #223
- Date: 2021-12-20
- Facilitator: Andrea
- Notary: chandi
- Mika caretaker: Janina
- Levi caretaker: Maxime
- Physical board caretaker:  Antonin
- Digital calendar: Matthias
- Reservation sheet purifier: Doug
- Present: antonin, Charly, Silvan, Doug, Andrea, Tilmann, Matthias, chandi

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_f4f2f09939c62cef70430d4faf13ea00.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 14.7 people/day (+0.3)
- **⚡ Electricity**
    - usage: 16.98 €/day (⬇️-32%)
    - paid: 15.6 €/day 
    - ☀️ self produced: 8% (0%)
    - emissions: 76 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.01 €/day (⬆️+24%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- [chandi] 15€ KMW door handle
- [matthias] 20€ outside door sensor

### Income
none

### Things that happened
- first critial mass ever in wurzen - not the last one!
- we got confirmation that our gas contract will be cancelled and the gas counter will get removed!
- progress with attic insulation
- more usage of our (now) 48V e-bike
- one more roof window installed (now 11 of 12 mounted)
- a game night happened


#### Wider world
- omicron is getting dominant

## 2. This week planning

### People arriving and leaving
- **Mon.:** Bodhi, Clara leaves
- **Tue.:** Maxime & Antonin leave
- **Wed.:** Nono might come, chandi leaves
- **Thu.:** Larissa, Silvan, Matthias leave
- **Fri.:** Charly leaves
- **Sat.:** Santa passes by
- **Sun.:** Wolfram arrives
- **Mon.:** Larissa comes back (probably)
- **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
today the only day of sun, temperatures will not be suuper low :D cloudy most of the week

### Evaluations and check-ins
- *Silvan* _Volunteer_ : _Absolute Days_ threshold 75/60 (+25%)
- *Charly_WA* _Visitor_ : 2 days until _Days Visited_ threshold (21)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * Paper bin [Matthias]
    * 10:00 CoMe [Andrea]
    * Park cars on even/this side [chandi]
- Tuesday
    * Rest waste [Doug]
    * 9-11 chimney cleaner comes [Matthias]
    * 11:00 Silvan's Evaluation [Andrea]
    * 18:00 workout @ Yogaroom
- Wednesday
    * Park cars on odd/that side [Matthias]
- Thursday
    * Biotonne [Doug]
    * 10:00 Power Hour [Fac.: Tilmann, DJ: ?]
- Friday
- Saturday
- Sunday
- Next Monday
    * 10:00 CoMe [Tilmann]
- Next week summary
    * 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [chandi] 54€ for 25 tests. more corona tests? lot's of people will come back at some point
    - no resistance, but recommendation to also use the free test offers from the cities! :)
- [matthias] 20€ for a shower sherbert for the wash room for easier box cleaning

## 4. To do
_Small fixes of things in the house_

* [ ] fix hipster room window curtain [Larissa & anja]
* [ ] more storage spaces for visitors [charly]
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [x] Building week clean up tasks
    * [x] scaffolding net in the garden. [antonin]
* [ ] KMW: fix driver side door (https://yunity.slack.com/archives/C3RS56Z38/p1636467207030900) [chandi]
* [ ] Electrically disconnect the ceran stove on the right kitchen side


### New this week

## 5. Discussion & Announcements

### Round 1
- [ToI team] ToI updates
    - See slack post: https://yunity.slack.com/archives/C3RS56Z38/p1639871284253400
- [kito] I would like to join Kanthaus in the beginning of january and also participate in TOI :) Concerns? 
    - Question: How strong do you plan to be involved in Kanthaus in the next time? participation in TOI might require being volunteer or member
    - doug will aproach kito
- [silvan] freeshop next week? (after christmas)
    - let's have a holiday!

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [doug] tomorrow is winter solstice! let's have a fire in the garden
- [matthias] lot's of mold round the windows in hipster room and the office. Contiuing ventilation system could help. 
    - Doug can support something in the office
    - Larrisa might paint the window in the hipster room
    - For more tasks: approach Matthias or Tilmann
### Round 3
- [doug] Busses are free here on weekends until end of January. We could fill a bus together and have a trip :D

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

- Present: antonin, Charly, Silvan, Doug, Andrea, Tilmann, Matthias, chandi

### Volunteers
- **Mon.:** Antonin
- **Tue.:** 
- **Wed.:** Larissa, Zui
- **Thu.:** Tilmann
- **Fri.:** Anja, Andrea, Janina
- **Open Tuesday:** Anja, Silvan

### Unavailabilities
- **Mon.:** Matthias
- **Tue.:** 
- **Wed.:** 
- **Thu.:** Matthias
- **Fri.:** Matthias, Charly
- **Open Tuesday:** Matthias
- **Week:** Maxime, chandi

### Result
- **Mon.:** Antonin, Charly
- **Tue.:** Matthias
- **Wed.:** Larissa, Zui
- **Thu.:** Tilmann, Doug
- **Fri.:** Anja, Andrea, Janina
- **Open Tuesday:** Anja, Silvan

## 7. For next week
