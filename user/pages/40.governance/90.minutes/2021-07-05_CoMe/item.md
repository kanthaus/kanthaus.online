---
title: Coordination Meeting
date: "2021-07-05"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #198
- Date: 2021-07-05
- Facilitator: Chandi
- Notary: Antonin
- Mika caretaker: Anja
- Levi caretaker: Janina
- Physical board caretaker: Andrea
- Digital calendar: Doug
- Reservation sheet purifier: Clara
- Present: chandi, Thore, Michal, Andrea, Talita, Anja, Tilmann, Mika, Janina, Levi, Clara, Antonin, Samuel, Doug

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_b1412f2de396189f022b4fd840609e7c.png "Usage during the last 90 days")

*(Usage during the last 90 days)*

- **Present:** 19.1 people/day (+4.9)
- **⚡ Electricity**
    - usage: 6.43 €/day (⬆️+23%)
    - paid: -4.0 €/day 
    - ☀️ self produced: 75% (0%)
    - emissions: 5 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.84 €/day (⬆️+51%)
    - emissions: 2 kg CO₂ₑ/week



### Expenditure
- 6 EUR for Rapsöl [Michal]
- moth traps, bratöl, ~10€ in total [Bodhi]


### Income
- nothing in th shoe

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
Due to the recent ukuvota the framework is suspended until the adjusted prevalence exceeds 0.1 again.
<!-- 
- µCOVIDs available last week: 10549
- µCOVIDs used last week: 620
- µCOVIDS balance from last week: 9929
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 15000
- µCOVIDs/person mean for this week: lots (18 people) 
-->

### Things that happened
- the Danni affinity group visited
- both attics were tidied up
- Chandi improved `codi.kanthaus.online` to `pad.kanthaus.online`
- Thore and Janina met with Mr. Döbelt of Landgut Nemt
- a Kritma round happened
- we got more kitchen shelves
- the compost toilet was improved
 
## 2. This week planning

### People arriving and leaving
- **Mon.:** Anja leaves, Silvan leaves, matthias comes back
- **Tue.:** Clara leaves, Silvan comes back
- **Wed.:** JaTiMiLe leave
- **Thu.:** JaTiMiLe come back, Samuel leaves
- **Fri.:** Thore leaves, chandi leaves, Antonin leaves
- **Sat.:** 
- **Sun.:** 
- **Mon.:** 
- **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Some thunderstorms, a bit of rain and quite warm tomorrow


### Evaluations and check-ins
- ~~*Lise* _Volunteer_ : _Absolute Days_ threshold 154/60 (+157%)~~
- ~~*Silvan* _Member_ : _Absolute Days_ threshold 182/180 (+1%)~~


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - Papier
    - 10:00 CoMe [Chandi]
- Tuesday
    - Hausmüll
    - 15:00 internal funding meeting
    - 16:00 Open Tuesday [lottery]
- Wednesday
- Thursday
    - 10:00 Power Hour [Fac.: Thore, DJ: Doug]
<!--    - 14:00 Knowledge Sharing: ? [?] Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit  -->
    - 18:00 foodsharing Wurzen meeting [Janina]
- Friday
    - Biotonne
- Saturday
- Sunday
    - 10:00 foodsharing brunch..?
- Next Monday
    - 10:00 CoMe [Doug]
- Next week summary
    - 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- MCM monthly?! (Or is it cancelled cause of summer and such..?)
    - it is cancelled due to lack of interest

## 3. Shopping plans

## 4. Discussion & Announcements

### Round 1
- [Doug] The Roof: Part 2, section 4 "The Finale". Would start doodle today for months Sept/Oct/Nov. Any comments?
- [Bodhi] only one Covid-Antigen-Quicktest left! should we get more? Probably also really easy to do the test at the pharmacy for free, maybe even the one around the corner. could not test it unfortunately because it was a sunday..
    - [Doug] cost €4 from Aldi now, I would say not we can personalize these costs.
    - [Clara] found it useful to have some stock in Kanthaus when inviting some groups
    - [Chandi] since it benefits the whole group it would make sense to keep it communal. let's get some while it's easy
    - [Antonin] does it
- [Antonin] What to do with the slackline? I am leaving and feel sort of responsible for it. Should I take it down or do people want to continue using it? It's nice if it can be taken down for lawn mowing, and removed if it is not used anymore (so it's not in the sunshine for too long).
    - [Andrea] takes over
- [Janina] Local foodsharing activities: Your chance to jump in! ;)
    - Nathalie is not here and Thore will also leave soon. I'd be very happy for more people taking responsibility for 1. the market pickup on Fridays, 2. checking the FairTeiler everyday and maybe even 3. calling Landgut Nemt.
    - If you want to help but don't have a foodsharing account with foodsaver status talk to me, that's solvable. :)
    - [Doug] volunteers to do the Landgut Nemt pick-ups (Friday at 18:00)
    - [Janina] also see the foodsharing Wurzen meeting this week to talk more about all this
- [chandi] There is now an overview of reproductive tasks in Kanthaus in the hallway. Maybe there are points you aren't aware of? or is there something missing on the list? :)

<!--  let's check whether someone hasn't spoken yet -->

### Round 2
- [Janina] New workaway request in the pipeline: Stefan, asking to come as soon as possible (but not desperate).
     - [Janina] waits for someone to volunteer to host and will deny tomorrow otherwise

### Round 3
- [Janina] Cave usage, recently used by the family because it's further away from the noisy piano room. Is it fine if it lasts for longer? And could we move the yoga mats somewhere else, because they create mould in the cave? Hipster room is too full, maybe some in the sleep kitchen?
     - [Clara] should we formalize an agreement to create some stability for the room usage of the family? Janina not interested in the cave because of the smell.
     - [Doug] it would make sense to use the cave because child buggies always end up there. Will help cleaning the room, making it nice.
     - [Chandi] interest in having formal agreements instead of untold lingering habits
     - JaTiLe will test the cave fore one/two more weeks, and then we will re-evaluate if a formalized agreement makes sense
- [Doug] online conference about bottom-up democracy, see Slack


### Additional points


## 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Janina, Samuel
- **Tue.:** Tilmann
- **Wed.:** Thore
- **Thu.:** Silvan, Chandi
- **Fri.:** Talita, Andrea
- **Open Tuesday:** Doug, Antonin

### Unavailabilities
- **Mon.:** Talita, chandi, Andrea
- **Tue.:** Antonin, Samuel, Talita, chandi, Andrea
- **Wed.:** Antonin, Talita, Tilmann, Janina
- **Thu.:** Tilmann, Janina, Thore
- **Fri.:** Antonin, Samuel, Thore, chandi
- **Open Tuesday:** chandi, Talita
- **Week:** Anja, Clara, Michal, Mika, Levi

## 6. For next week
- 
