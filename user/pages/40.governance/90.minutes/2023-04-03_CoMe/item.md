---
title: Coordination Meeting
date: "2023-04-03"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #287
* Date: 2023-04-03
* Facilitator: Janina
* Notary: Janina
* Children caretaker: Kita
* Physical board caretaker: Larissa
* Digital calendar: Antonin
* Reservation sheet purifier: Alba
* Present: Silvan, Martin, Larissa, Andrea, Tilmann, Alba, Antonin, Janina

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/c13880a3-b23d-4cea-99b9-a5aa8df56a8f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.4 people/day (+0.0)
- **⚡ Electricity**
    - usage: 104.71 €/week (⬆️+13%)
    - of that for heating: 19.34 €/week
    - paid: 9.27 €/week
    - ☀️ self produced: 72% (⬆️+4%)
    - emissions: 21 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.98 €/week (⬆️+10%)
    - emissions: 1.0 kg CO₂ₑ/week


### Expenditure
- 22€ for spread, legumes, pasta
- 50€ for hosting services (Uberspace)

### Unusual Income
- a bit

### Things that happened
#### In or around Kanthaus
- Chandi's party in Leipzig! 
- a great foodsharing meeting
- several attic sessions with 5 different people in total
- some degree of social sauna
- parts of the tax report were submitted
- market pickups are on again

#### Wider world

<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
- wars are continuing in the world


## 2. This week planning

### People arriving and leaving
* **Mon.:** Anja left, Andrea leaves, Antonin arrived
* **Tue.:** 
* **Wed.:** Zui arrives
* **Thu.:** Kito arrives, the Flowmis arrive
* **Fri.:** Mantel and Pluma arrive for 2-3 days (kito hosts)
* **Sat.:** Andrea comes, Larissa leaves
* **Sun.:** Flowmis leave
* **Mon.:** Andrea leaves
* **Some day:** Silvan leaves over Easter, Antonin leaves on the weekend

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_sunny! cold at night, two digits during daytime_


### Evaluations and check-ins
- Nathalie Visitor : 3 days until Days Visited threshold (21)
- Antonin would like to have his soon


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Janina]
	* Monday food care [Alba, Larissa, Tilmann]
* Tuesday
    * 10:00 - 13:00 attic work session [Tilmann, Janina, Alba, you?]
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * 8:00 - 18:00 Schüttung delivery (possibly before noon) [help needed]
    * 10:00 - 13:00 attic work session [Tilmann, you?]
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Larissa]
* Friday
    * 10:00 PlaMe [kito]
* Saturday
    * Yellow bins [Martin]
    * Easter brunch :rabbit:
    * 14:00 - 17:00 attic work session [Tilmann, you?]
* Sunday
    * 15:00 Spanish cafe? Maybe churros.
* Next Monday
    * 10:00 CoMe [Martin]
    * LTDM Meeting
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [tilmann] a better bike spoke key? some spokes are not easy to tighten, need to measure the size again
  - or maybe a Zangenschlüssel (35€) https://www.idealo.de/preisvergleich/OffersOfProduct/4331188_-mini-zangenschluessel-vernickelt-125-mm-86-03-125-knipex.html
  - feel free to buy what you deem necessary!

## 4. To do
_Newest tasks on top_
* [ ] fix the physical weekly board []
* [ ] treat mold in Freeshop Lounge 
* [ ] de-clog compost toilet drain
* [ ] do the oil treatment to wooden tables in dining room and snack Kitchen []
* [ ] modify the task lottery to prioritize open tuesday []
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom [Larissa]
* [ ] Light: elephant room
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
- [kito] Long Term Decision Making
  - last chance to enter your ideas
  - meeting on friday?!
      - let's not just repurpose PlaMe, but make this one point in it
      - Easter Monday is another good candidate for the LTDM meeting, let's find an exact time in PlaMe
<!-- check if anyone has a point that didn't speak already -->

### Round 2


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Larissa
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Martin
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** Silvan, Antonin
* **Tue.:** Janina
* **Wed.:** 
* **Thu.:** Silvan
* **Fri.:** Silvan
* **Open Tuesday:** Janina 
* **Week:** Tilmann, Andrea

### Result

- **Mon.:** Larissa
- **Tue.:** Alba
- **Wed.:** Silvan
- **Thu.:** Janina
- **Fri.:** Martin
- **Open Tuesday:** Antonin



## 7. For next week
* 
