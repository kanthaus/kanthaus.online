---
title: Coordination Meeting
date: "2022-04-18"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #240
* Date: 2022-04-11
* Facilitator: Janina
* Notary: chandi
* Mika caretaker: JaTi
* Levi caretaker: JaTi
* Physical board caretaker: Andrea
* Digital calendar:
* Reservation sheet purifier: Janina
* Present: chandi, janina, tilmann, andrea, doug, thore, matthias, nathalie

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/2d5d9b59ea39a49d4a8cf483c.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 12.6 people/day (+1.6)
- **⚡ Electricity**
    - usage: 37.69 €/week (⏬-46%)
    - of that for heating: ? €/week
    - paid: -24.34 €/week
    - ☀️ self produced: 50% (⬆️+1%)
    - emissions: 15 kg CO₂ₑ/week
- **💧 Water**
    - paid: 14.73 €/week (⬇️-11%)
    - emissions: 1 kg CO₂ₑ/week


### Expenditure
* [tilmann] 90€ washing machine

### Income
*

### Things that happened

#### In or around Kanthaus
* we got a new washing machine
* barbecue party in the garden
* automatic water counter reading
* another e-bike battery built
* lots of food saved
* new wifi setup (hopefully easier to mantain)

#### Wider world
*

## 2. This week planning

### People arriving and leaving
* **Mon.:** Butze und Chris leave
* **Tue.:**
* **Wed.:**
* **Thu.:**
* **Fri.:** Larissa (+maybe Silvan) might come back
* **Sat.:**
* **Sun.:**
* **Mon.:**
* **Some day:**

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
- *Nathalie* _Volunteer_ : 1 days until _Absolute Days_ threshold (60)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Janina]
    * Park ~~KMW & ~~trailer on even/that side [Thore]
* Tuesday
    *
* Wednesday
    * Park KMW & trailer on odd/other side [Thore]
* Thursday
    * 10:00 Power Hour [Fac.: Andrea, DJ: ?]
* Friday
    * 10:00 Attic working session and skillshare [Tilmann]
* Saturday
    * yellow bin [Janina]
* Sunday
    *    
* Next Monday
    * 10:00 CoMe [chandi]
* Next week summary
    *

_To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [Antonin] ~200€ of electrical items, mainly for the silent office but also to replenish supplies of generic items (such as unterputzdosen). Happy to add more to the order. See https://pad.kanthaus.online/silent_office?both#Electricals

## 4. To do

_Newest tasks on top_
* [ ] make the wall nice in K20-2 bathroom where the boiler has been
* [ ] remove old gas heater from basement to trash corner - needs 3-4 people
* [x] repair WiFi in K22-2
* [ ] repair Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [chandi] we have a new visitor called Covid (pronoun "it"). say hello and be friendly :)
    * current state
        * positive quick tests: matthias, nathalie, butze, chris, janina (one positive, followed by two negative ones)
    * measures
        * let's test mika
        * let's buy other tests tomorrow [Thore,?]
        * matthias, nathalie and chandi are isolating individually
        * no dinner lottery
        * no open tuesday -> make a sign [janina]


<!-- check if anyone has a point that didn't speak already -->

### Round 2
*

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)
- none this week, due to lots of ill cases
- healthy people are asked to behave in solidarity and care for the sick

## 7. For next week
*
