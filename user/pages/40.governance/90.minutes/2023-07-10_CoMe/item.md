---
title: Coordination Meeting
date: "2023-07-10"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #301
* Date: 2023-07-10
* Facilitator: Antonin
* Notary: Kito
* Children caretaker: parents probably
* Physical board caretaker: Larissa 
* Digital calendar: Kito
* Reservation sheet purifier: Cecilia
* Present: Antonin, Dorota, Larissa, Cecilia, Akos, Kito

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/0ca65b7f-4ec0-41e8-b656-024c749dc1c1.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 6.9 people/day (-5.1)
- **⚡ Electricity**
    - usage: 16.1 €/week (⬇️-16%)
    - of that for heating: 0.33 €/week
    - of that for hot water: 1.05 €/week
    - paid: -61.78 €/week 
    - ☀️ self produced: 82% (⬆️+1%)
    - emissions: 8 kg CO₂ₑ/week
- **💧 Water**
    - paid: 15.05 €/week (⬇️-21%)
    - emissions: 1.2 kg CO₂ₑ/week


### Expenditure
* probably none

### Income
* same

### Things that happened
#### In or around Kanthaus
* An epic weeding party cleared the pavement in front of our houses
* An IT maintenance session which made our data safer
* The last Punkrocktresen before the summer break
* Various trips to various lakes
* A trip to Doug's graduation
* A nice visit from Patrick

#### Wider world
* Members of the German Bundestag voted in favour of the expansion of the LNG gas infrastructure 
* some people cycle through france 

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->


## 2. This week planning

### People arriving and leaving
* **Mon.:** Anneke comes back (late), Doug and Anja come for 1 night
* **Tue.:** Silvan leaves, Feuerqualle group arrives
* **Wed.:** Antonin leaves, Vroni might arrive, Larissa leaves
* **Thu.:** mediationgroup [Anneke] arrives, Arrakis [Anneke] arrives, kito leaves
* **Fri.:** Feuerqualle group leaves, mediationgroup leaves, Akos leaves (or saturday)
* **Sat.:** 
* **Sun.:** Antonin probably comes back, Larissa comes back, kito comes back for 1 night
* **Mon.:** Martin comes back
* **Some day:** Johanna & Jonna stay for a night towards the end of the week (via Warmshowers)

### Weather forecast
![](https://pad.kanthaus.online/uploads/8cbc50f4-fd85-4974-8edf-9c8c8038c55a.png)
* more trips to the lake!!

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Akos_WA Visitor: 0 days until Days Visited threshold (21)
- Larissa Member: 5 days until Days Visited threshold (180)
- Cecilia_WA Visitor: 1 days until Days Visited threshold (21)
- Dorota_Stilgar Visitor: 4 days until Days Visited threshold (21)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Antonin]
    * 11:00 PowerHour
    * evening: Folk concert in Kulturnhalle Leipzig [Antonin]
    * 20:30 board game evening
* Tuesday
    * 15:00 - 17:00 Open Tuesday [lottery] 
    * Finance Meeting [Larissa, Zui]
* Wednesday
    * 10:00 Akos evaluation [kito]
    * 12:00 - 17:00 Mitmachcafé
    * Kanthaus birthday! 
    * 20:00 Project Updates [kito]
* Thursday
    * 13:00 - 18:00 Mitmachcafé
* Friday
    * Gelbe Tonne [Cecilia]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Larissa]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* oil? [Nofal]

## 4. To do
_Newest tasks on top_
* [x] weed the pavement in front of the houses [Dorota, Akos, Antonin]
* [ ] curtain for the Cave
* [ ] maintenance of Phoenix bike - ask Martin?
* [ ] make K20-2 bathroom privacy indicator realiable - ask Tilmann
* [ ] fill holes in Snack kitchen floor where moths breed [Akos]
    * it started but it not done yet, unclear what is the right material
* [ ] fix Daria trailer back lights
     * [Martin] Cables got pinched, I would redo the whole thing (but not now) 
* [ ] give away saved windows in K18 (a lot) 
     * maybe https://www.trashgalore.de/ ? [Martin]
* [ ] give away roof windows (outside, under green tarp next to palettes)
* [ ] give away roll of Dampfsperre
* [ ] give away 2 rolls of Unterspannbahn
* [x] check fire extinguishers [Antonin]
* [ ] replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom []
* [ ] light: freeshop hallway
* [ ] light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

### Round 1
* [Antonin] reminder of what to put and not to put in the dishwasher
* [Anneke] building week invitation and pad (https://pad.kanthaus.online/ZSJBAJ2qSJW7ICGlbz8I7w?edit) are ready! Please fill in the pad, think about what's still to organise and invite your friends! Also is someone willing to update a post on the website? I don't know how to do it
    * [Antonin] can help with the website
    * [Dorota] i think it's cool
* [Dorota] there is food in the kitchen
* [Larissa] reminder that the fruits and vegetables are in the basement
    * automated food sheet
* [Kito] please put things back where they came from (as a general reminder)

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [Dorota] please remember to throw the empty packages of bread
* [Antonin] a group of about 10 people is interested in doing an IT knowledge sharing session in Kanthaus (29.09.-01.10) and is looking for a host
    * yes, great - Tilmann, take over :)
* [Anneke] can someone host Emma (workaway) last two weeks of August or the beginning of September (flexible on dates)?
    * no one found yet

## Round 3
* [Antonin] please keep windows and doors closed when it's hot outside
    * and also close curtains
* [Cecilia] we have a fly problem - keep garden door and washroom door closed
    * there is some chemical paper in the Cloud Room to kill them :(
* [Antonin] would be nice if Dragon and Cloud room can be cleaned for the group today

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Kito
* **Tue.:** 
* **Wed.:** Feuerqualle
* **Thu.:** 
* **Fri.:** Anneke
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** 
* **Tue.:** Akos
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Akos
* **Open Tuesday:** 
* **Week:** Antonin, Larissa

### Result
- **Mon.:** Kito, Cecilia
- **Tue.:** 
- **Wed.:** feuerqualle
- **Thu.:** Akos
- **Fri.:** anneke
- **Open Tuesday:** Dorota

## 7. For next week
* 
