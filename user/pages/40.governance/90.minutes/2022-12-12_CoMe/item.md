---
title: Coordination Meeting
date: "2022-12-12"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #273
* Date: 2022-12-12
* Facilitator: Kito
* Notary: Janina
* Mika caretaker: Kita
* Levi caretaker: Kita
* Physical board caretaker: Doug
* Digital calendar: Maxime
* Reservation sheet purifier: Martin
* Present: Doug, Anneke, Maxime, Tilmann, Janina, Kito, Silvan, Martin, Nathalie

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/556b2549-987e-427f-81da-6bf9a02f7786.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.9 people/day (-3.6)
- **⚡ Electricity**
    - usage: 134.55 €/week (0%)
    - of that for heating: 95.24 €/week
    - paid: 122.2 €/week 
    - ☀️ self produced: 9% (0%)
    - emissions: 82 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.93 €/week (⬇️-25%)
    - emissions: 1.0 kg CO₂ₑ/week

### Expenditure
* 3 bags of vacuum bags
* some asian supplies
* some baking supplies and spreads

### Income
* 50€ from shoe

### Things that happened
#### In or around Kanthaus
- Hipster room got connected to ventilation system
- More ventilation system work in K22 bathrooms, still WIP
- Nathalie made two food care matrices based on sociocratic principles
- Social sauna
- Christmas baking with Lise
- More contagious diseases thanks to Kita -.-
- Very empty weekend
- Funny game sessions from the advent calendar
- Several movie screenings
- The KMW was reparked (almost) once for all!
- 3rd Advent with cookies, songs and Glühwein

#### Wider world
- Today is foodsharing's 10th anniversary! :tada:
- Corruption crisis in the EU parliament

## 2. This week planning

### People arriving and leaving
* **Mon.:** Kito leaves, Zui probably comes back
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Kito comes back for one night, GemÖk Friedrich (7ppl) comes for the weekend
* **Sat.:** 
* **Sun.:** 
* **Mon.:** 
* **Some day:** 

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
_Cold and snowy, frost even during daytime, Sunday night down to -9°C_

### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Nathalie Visitor : Days Visited threshold 39/21 (+86%)
- Thore Volunteer : Absolute Days threshold 94/60 (+57%)
- Silvan Volunteer : Absolute Days threshold 89/60 (+48%)
- Kito Volunteer : Absolute Days threshold 82/60 (+37%)
- Martin Volunteer : Absolute Days threshold 65/60 (+8%)
Absent:
- Chandi Volunteer : Absence threshold 114/90 (+27%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Kito]
    * straight after CoMe: foodsharing onboarding [Janina, Martin, you?]
    * monday food care [Anneke]
    * Park ~~KMW &~~ :tada: Trailer on even/this side [frost]
* Tuesday
    * 15:00-17:00 Open Tuesday []
* Wednesday
    * ~8:00 Insulation Part 2 [Tilmann]
    * Park ~~KMW &~~ :tada: Trailer on odd/that side [frost] 
    * 17:00 - 19:00 FGNW meeting @Lantern [Janina + some more Wurzen people]
    * 19:00 Punkrocktresen @D5
* Thursday
    * 10:00 Power Hour [Fac.: Janina, DJ: ?]
    * 15:00 Thore's evaluation
* Friday
    * yellow bin [Maxime]
    * 10:00 Plenum [Doug]
    * 15:00 FFJ proposal chat @dining room [kito]

* Saturday
    * 15:00 Critical Mass @market square
* Sunday
    * 4th Advent
* Next Monday
    * paper bin [Martin]
    * 10:00 CoMe [Janina]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- nope

## 4. To do

_Newest tasks on top_
* [ ] Light: elephant room
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] fix bike repair station instructions [Doug]
* [ ] treat mold in Ex-food-storage
     * room need a fresh air supply (Zuluft)
     * Some more work would be welcome to finish connecting these room to the ventilation system
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [x] Light: hallway of the K20-1 flat [JaTi]
    * It might be temporary (or not?) but it's there! ;)
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

### Round 1
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->
- [Janina] It's snowy so we need to keep the sidewalk walkable, especially in the mornings!
- [Tilmann] Dining room and piano room are leading the heating system of the whole house: If they fall below 19.5 °C the heating will give more power (and thus gets less efficient). Keeping the doors to these rooms closed makes sense to avoid unnecessary heatpump activity.
- [Kito] FFJ-Ukuvota just started: https://ukuvota.world/#/app/c8c22f1f-69fb-69f7-8d8e-ea84cf1c602e/collect
    - Friday 3pm there's an additional in-person chat about this in the dining room. Very casual and optional.
- [Doug] I started a Lead–acid battery (Bleisäure-Akku) sale. If you're interested in that come to the workshop.
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* 

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Larissa
* **Tue.:** Janina, Silvan
* **Wed.:** Anneke
* **Thu.:** Zui
* **Fri.:** Maxime
* **Open Tuesday:** Andrea

### Unavailabilities
* **Mon.:** Doug
* **Tue.:** 
* **Wed.:** Doug
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:**
* **Week:** Kito, Tilmann, Nathalie

### Result
- **Mon.:** larissa
- **Tue.:** Janina, Silvan
- **Wed.:** Anneke
- **Thu.:** zui, Doug
- **Fri.:** Maxime
- **Open Tuesday:** andrea, Martin

## 7. For next week
* 
