---
title: Coordination Meeting
date: "2023-11-27"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #321
* Date: 2023-11-27
* Facilitator: Kito
* Notary: Martin
* Physical calendar: Dorota
* Digital calendar: Doug
* Reservation sheets: Larissa
* Weather forecast: Martin
* Present: Kito, Klara, Liora, Dorota, Kate, Riëll, Tilmann, Martin, Larissa, Maxime, Antonin, Doug, Vroni

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/b6be4262-0a14-479d-abe6-57482a1c2839.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 20.0 people/day (+3.1)
- **⚡ Electricity**
    - usage: 160.26 €/week (⬆️+42%)
    - of that for heating: 84.32 €/week
    - of that for attic heating: 5€
    - of that for hot water: 9.43 €/week
    - paid: 155.62 €/week 
    - ☀️ self produced: 13% (⬇️-15%)
    - emissions: 75 kg CO₂ₑ/week (probably more, it's coal season)
- **💧 Water**
    - paid: 33.71 €/week (⬆️+56%)
    - emissions: 2.6 kg CO₂ₑ/week

### Expenditure
- 20€ rice, oil ...

### Income
- 90€ MSV donation
- 50€ donation box

### Things that happened
#### In or around Kanthaus
- the biggest group in KH history was successfully hosted!
- black friday special free shop opening
- come 320 and evaluation 320 coincided
- kanthaus-gast wifi fell into a coma, but there's still hope...
- the first snow of the season
- a palette of fancy juice arrived
- Karrot and Inventaire got confirmation that they would receive european funding again
- trains are back

#### Wider world
- Financial crisis in Germany. Budget cuts and social spending. 
- Locomotive drivers go on strike, maybe open ended. 
- Game show from polish tv, someone got the highest score in 30 years. 
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Popcorn of feedback
- no feedback this week
<!--
Is there any feedback you would like to give to others - the whole groups or individuals? Is their any reflection about yourself you would like to share with the others? If you would like to give feedback to a person in the room, please do it in third person.
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Vroni and Doug arrived, Antonin leaves
* **Tue.:** Antonin comes back
* **Wed.:** 
* **Thu.:** Bausyndikat group arrives for the weekend
* **Fri.:** Martin leaves, Klara leaves, Vroni leaves
* **Sat.:** JaTiMiLe leave for a night
* **Sun.:** Bausyndikat group leaves, Klara comes back
* **Mon.:** Kito leaves
* **Some day:** Martin comes back
                       

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->

More precipitation at the beginning of the week. Less so after. Temperatures hovering around 0°C, with a slump to -8°C on Thursday. 

 
### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Loup_WA Visitor: Days Visited threshold 30/21 (+43%)
Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Venla_WA Visitor: 0 days until Days Visited threshold (21)
- Riëll_AAA Visitor: 3 days until Days Visited threshold (21)
- Klara_Law Visitor: 6 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 - CoMe [Kito]
    * 11:00 - Power Hour
* Tuesday
    * 12:00 - Current events lunch
* Wednesday
    * 12:00 - 17:00 - MitMachCafé 
    * 17:00 - Pottery in Wurzen thing
    * 18:00 - Punkrocktresen
* Thursday
    * 10:00 - Miele vacuum cleaner repair party
    * 13:00 - 18:00 - MitMachCafé
    * 18:00 - Talk: "KulturGeschichten: Industriegeschichtliche Spurensuche in Wurzen und im Wurzener Land" @Museum
    * 18:30 - FLINTA Thai boxing
    * 19:00 - Movie&Talk "König hört auf" @NDK
* Friday
    * Yellow Bin [Liora]
    * 10:00 - Project Updates [Antonin]
    * 16:00 - Kita Weihnachtsmarkt @Burkartshain
    * 19:00 - reading/book presentation "Wir waren wie Brüder" @NDK
    * First Advent calendar opening!
* Saturday
    * 09:00 - 15:30 - Zukunftswerkstatt Wurzen @Lichtwer-Gymnasium
    * 19:30 - HipHop concert @NDK
* Sunday
    * first advent
* Next Monday
    * 10:00 CoMe [Maxime]
* Next week summary


<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
- [Janina] bed sheets/Spannbettlaken (both sizes: 90 and 140)
    - can we ask around in our families first? many people have loads of sheets...

## 4. To do
*See analog list in the elephant room*

**Done**
- Freeshop shelf dissasemble and mirror hanging [Martin]

**Feedback**


<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [Janina] hosting for the group on the weekend
    - Kito is available on Thursday and Saturday
    - Janina is (probably) available on Thursday but not afterwards.
    - Doug takes main role on Thursday/Friday.
    - Talks to rope someone in for Saturday. 
    - Sleeping: Communal, Lantern, maybe something else. 
    - We need a week reservation sheet for communal!
- [Kate] TOI 2 preparation tasks
    - Internal workshop. Bring in topics so it's not only the orga. Mission, vision, values, will be discussed, bring examples that pick your interest. Companies, associations, groups... whatever.
    - Visual artifact representing the Spirit of KH. Bring your own to be shared in the first couple of days. 
    - Evidence Safari ongoing. 
- [kito] other group over the weekend
    - 3 FFJ people from other group that would like to go to an event in Leipzig and would like to know KH. Friday to Sunday. 
    - [Maxime] If Kito takes care of the intro, Maxime would take over after. 
- [Kate] December 14 special opening of the freeshop for the Frauen NDK group. Reopening of the Lounge? Help clearing stuff and moving furniture. 
- [Larissa] Fill up Advent Calendar!
<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [Janina] Eumel+1 visitor request during TOI 2 (12.12.-17.12.)
    - He's considering making Kanthaus his base in the foreseeable future and wants to get a feel again.
    - He knows about ToI/MoC and FFJ and that if this week doesn't work it's end of January the earliest again.
    - Is there even enough space for a new person? No idea...
    - He knows we'll have meetings in the mornings, and says so does he.
    - I'm already hosting Lara, so I can't. But I vouch for this person! :)
    - no resistance in the room
    - Martin can imagine to host
- [Vroni] Relationship status change means potential uncommon behavior. FYI. 

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Kate
* **Tue.:** Vroni
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** opentuesdaykate

### Unavailabilities
* **Mon.:** Janina, Antonin, Maxime, Doug
* **Tue.:** Janina, Liora 
* **Wed.:** Antonin, Maxime
* **Thu.:** Janina, Klara
* **Fri.:** Janina, Klara, Martin, Doug, 
* **Open Tuesday:** Janina, Antonin, Maxime
* **Week:** Tilmann, Larissa, Riëll, Kito

### Result
- **Mon.:** Kate
- **Tue.:** Vroni, Doug
- **Wed.:** Martin, janina
- **Thu.:** Liora, Antonin
- **Fri.:** Dorota, Maxime
- **Open Tuesday:** opentuesdaykate, Klara

## 7. For next week
