---
title: Coordination Meeting
date: "2023-04-10"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #288
* Date: 2023-04-10
* Facilitator: Doug
* Notary: Janina
* Children caretaker: Tilmannn
* Physical board caretaker: Martin
* Digital calendar: Doug
* Reservation sheet purifier: Kito
* Present: Janina, Anneke, Martin, Pluma, Mantel, Andrea, Kito

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/ff8ead56-c95a-42c5-8b0d-3188899a3e00.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 8.6 people/day (-1.9)
- **⚡ Electricity**
    - usage: 136.02 €/week (⬆️+30%)
    - of that for heating: 78.39 €/week
    - paid: -8.54 €/week 
    - ☀️ self produced: 81% (⬆️+9%)
    - emissions: 23 kg CO₂ₑ/week
- **💧 Water**
    - paid: 15.76 €/week (0%)
    - emissions: 1.2 kg CO₂ₑ/week


### Expenditure
- 25€ for covid test, salt for spulmittel and vacuum bags

### Income
- 10€ donation

### Things that happened
#### In or around Kanthaus
* longterm decision making meeting with project idea presentation
* schüttung was delivered
* quite a lot of attic work with 5 ppl at once at one point
* flowmi group had a small but efficient working session and even sanded the garden table
* nice easter walk to Spitzberg
* easter brunch and egg hunt
* wild garlic collecting and processing (and eating)
* fire next to train tracks left people stranded in wurzen (like anja and felix_dd)

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* finland joins nato
* trump has 34 charges of falsifying business records


## 2. This week planning

### People arriving and leaving
* **Mon.:** Mantel, Pluma, Vroni and Andrea leave, Silvan comes back
* **Tue.:** Larissa comes back
* **Wed.:** 
* **Thu.:** Alba leaves
* **Fri.:** FFJ-orga-group arriving (2-4ppl)
* **Sat.:** 
* **Sun.:** FFJ-orga-group leaving
* **Mon.:** Martin
* **Some day:** Annekes friend Lilli for 1/2/3 days, Doug is here the beginning of the week

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/meteogramm
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_week starts sunny and warm, deteriorates over the course of time. some rain on thursday._


### Evaluations and check-ins
- Antonin would like to have his soon but not this week
- Kito would like to be evaluated, too
- Nathalie Visitor : 1 days until Days Visited threshold (21)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Martin]
	* 12-15 Attic work session [Tilmann, you?]
* Tuesday
	* Paper waste [Doug]
    * Tuesday food care [Anneke, Janina]
    * 10:00 - 13:00 attic work session [Tilmann, you?]
    * 15:00 - 17:00 Open Tuesday [lottery]
* Wednesday
    * Rest waste [Tilmann]
    * 10:00 - 13:00 attic work session [Tilmann, Janina, you?]
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Janina]
    * 12:00 Kito's evaluation [Janina]
    * 17:00 Gedenken an den Muldenwiesen (NDK event)
* Friday
    * 10:00 LTDM meeting? (discussion)
* Saturday
    * 10:00 - 13:00 attic work session [Tilmann, Antonin, you?]
    * Biowaste
* Sunday
* Next Monday
    * 10:00 CoMe [Kito]
* Next week summary


To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- Kito is still on the food order, but not for this week

## 4. To do
_Newest tasks on top_
* [ ] fix the physical weekly board []
* [ ] treat mold in Freeshop Lounge and Hipster Room
* [ ] de-clog compost toilet drain
* [x] do the oil treatment to wooden tables in dining room, elephant room and snack Kitchen [Antonin]
* [ ] modify the task lottery to prioritize open tuesday []
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom [Larissa]
* [x] Light: elephant room [Martin]
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
- [kito] Long Term Decision Making
    - Ukuvota for prioritization https://ukuvota.world/process/1e178195-9680-4d3a-a896-b92806d7ce84/proposals (proposal phase until tonight, vote until thursday night)
    - Pad to enter your commitment: https://pad.kanthaus.online/ltdm-2023#
    - Meeting on friday or (how to) find another day?
        - Janina will talk to Zui
        - Andrea would like to be informed about the date we choose (Sunday would work for her)
        - Let's find the date this week.
<!-- check if anyone has a point that didn't speak already -->

### Round 2


### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:**
* **Fri.:** 
* **Open Tuesday:** 

### Unavailabilities
* **Mon.:** Anneke
* **Tue.:** kito, Anneke
* **Wed.:** kito
* **Thu.:** 
* **Fri.:** kito
* **Open Tuesday:** Anneke, Kito
* **Week:** Martin, Mantel, Pluma, Tilmann, Doug, Andrea

### Result



## 7. For next week

