---
title: Coordination Meeting
date: "2023-12-18"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #324
* Date: 2023-12-18
* Facilitator: Janina
* Notary: Janina
* Physical calendar: Dorota
* Digital calendar: Maxime
* Task board: Dorota
* Reservation sheets: Riell 
* Weather forecast: Kito
* Present: Dorota, Maxime, Tilmann, Riell, Janina, Kito

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/09fa77f3-18e8-4b04-b2e4-83b5183ec678.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 13.3 people/day (+0.6)
- **⚡ Electricity**
    - usage: 110.08 €/week (⬇️-28%)
        - of that for heating: 59.9 €/week
        - of that for hot water: 5.2 €/week
        - of that for K20-3 heatpump: 5.89 €/week
    - paid: 103.68 €/week 
    - ☀️ self produced: 20% (⬆️+10%)
    - emissions: 90 kg CO₂ₑ/week
- **💧 Water**
    - paid: 12.13 €/week (⬇️-18%)
    - emissions: 0.9 kg CO₂ₑ/week

### Expenditure
- [tilmann] 11€ mill bits (Fräser)
- [tilmann] 13€ rivet nuts (Nietmuttern)

### Income

### Things that happened
#### In or around Kanthaus
* ToI #2! - Kanthausians being a group in Kanthaus
* projector mounted in k20-3
* a nice cooperative quiz night followed by a shitty movie
* sauna+concert excursion to Leipzig
* Mika's performance on the Wurzen Christmas market
* Wurzen Christmas market pick up
* moldy table from K22-B got sanded
* 1st game ever of dance-guesser
* lots of coloring on paper by kids and adults
* AOE
* advent crepe breakfest again
* linner on saturday

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* big change in Polish politics: PiS is out of office and people are cheering
* COP28 in UAE ended with a deal "calling on" all nations to "transition away" from fossil fuels "in a just, orderly and equitable manner"
* occupation of uni building in Frankfurt
* first Oberbürgermeister from AfD a Sachsen (in Pirna)

### Popcorn of feedback

<!--
Is there any feedback you would like to give to others - the whole groups or individuals? Is their any reflection about yourself you would like to share with the others? If you would like to give feedback to a person in the room, please do it in third person.
-->

## 2. This week planning

### People arriving and leaving
* **Mon.:** Matthias arrives, Martin comes back
* **Tue.:** 
* **Wed.:** Maxime leaves :cry:
* **Thu.:** kito leaves 
* **Fri.:** 
* **Sat.:** Larissa leaves for ~4 days
* **Sun.:** Riell leaves for Christmas
* **Mon.:** Matthias leaves
* **Some day:** Zui(+1?) comes for Christmas


### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->
_not really cold, sunny, cloudy, sometimes also rain, bit windy on the weekend_

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):
- Nathalie Visitor: Days Visited threshold 28/21 (+33%)
- Thore Visitor: Days Visited threshold 26/21 (+24%)
- Dorota Volunteer: Days Visited threshold 62/60 (+3%)

:clock1130: Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Tilmann Member: 1 days until Days Visited threshold (180)
- Kate Volunteer: 1 days until Days Visited threshold (60)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * Paper bin [Tilmann]
    * Month of Calm starts!
    * 10:00 - CoMe [Janina]
    * 11:00 - Power Hour
    * 19:15 - Christmas market pickup @Leipzig [Janina]
* Tuesday
    * Hausmüll [Tilmann]
    * Riff!
* Wednesday
    * 12:00 - 17:00 - MitMachCafé
    * 17:30 - Landgut Nemt pickup [Janina, you?]
    * 18:00 - Punkrocktresen
* Thursday
    * 13:00 - 18:00 - MitMachCafé
        * 15:30 Finissage of the exhibition
    * 18:30 FLINTA* thai-boxing @d5
* Friday
    * Biotonne [Janina]
* Saturday
    
* Sunday
    * Christmas!
    * festive dumpster diving
* Next Monday
    * 10:00 CoMe []
* Next week summary
    * no kita, no kito
    * last week of the year

<!-- To be scheduled:_(*avoid conflicts, e.g. cooking team: 17:00-19:00*) * Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

### Hats for the week
* E-Mails: Janina
* Telephone: Kito (until he leaves)

## 3. Shopping plans
- [tilmann/dorota] about 18€: toxic fume exhaust for the electronic workshop

## 4. To do

- todos older than 1 month get removed
- every todo needs a person with knowledge and a person who's doing it
- random selection of ~8 todos got read out and people who can help or commit were sometimes found

*See analog list*

**Done**
-

**Feedback**
-

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
* [Larissa] adventure @RIFF tomorrow! See https://yunity.slack.com/archives/CE4FV59BR/p1702735277376069 for the result of how to get there but either way we will leave around 10:30am :)
* [Thore] Nathalie is already in Dortmund, but I'm staying until Friday. Would be nice if I can come around for communal dinner from Mon - Thu. Can you imagine to prepare some food with a low risk of mold? It would be also okay if only some parts are "Thore compatible". Can also someone feel responsible to send me a message on signal when dinner is ready? Thank you :)
    * Yes, sure!
    * Kito signals him today, afterwards we'll see who can do it.
* [Antonin] please let me know if you have any concerns about migrating away from GitLab to https://git.kanthaus.online. See also [the thread in #kanthaus](https://yunity.slack.com/archives/C3RS56Z38/p1702768153855119).
* [Janina] Painting motivation for K20 attic? Communal action on Thursday?
    * yep, general interest is there
    * we can take turns and don't need to wash the brushes so often
    * we start at 10.
* [tilmann] weird email about imprint on kanthaus.online
    * https://yunity.slack.com/archives/G7E8RPFMH/p1702628311790749
    * antonin is on it, except for facebook
    * janina will also look at it
* [kito] print group photos for (christmas) cards - how many?
    * [Antonin] oh yes please, I want to send one to Patrick
    * [Janina] more than 5 but less than 10
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [tilmann] Uberspace wants money again… https://yunity.slack.com/archives/G7E8RPFMH/p1702629208419539
    * it's at 5€ per month now, we need to pay before January.
    * for now we need a volunteer to take care of the payment.
    * for the future we could think about if we can reduce the cost somehow. seems a bit much...
    * Janina asks Larissa


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Kito
* **Tue.:** Riell
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** closed, reallyclosed

### Unavailabilities
* **Mon.:** Janina
* **Tue.:**
* **Wed.:** Janina
* **Thu.:**
* **Fri.:**
* **Open Tuesday:**
* **Week:** Tilmann, Maxime

### Result
- **Mon.:** Kito
- **Tue.:** Riell
- **Wed.:** 
- **Thu.:** Dorota
- **Fri.:** Janina
- **Open Tuesday:** closed, reallyclosed


## 7. For next week