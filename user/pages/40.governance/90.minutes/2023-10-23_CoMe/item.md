---
title: Coordination Meeting
date: "2023-10-23"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #316
* Date: 2023-10-23
* Facilitator: larissa
* Notary: kyto+janina
* Physical calendar: dorota
* Digital calendar: janina
* Reservation sheets: larissa
* Weather forecast: janina
* Present: larissa, dorota, kate, janina, kyto, doug

---

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats 
![usage graph last 90 days](https://pad.kanthaus.online/uploads/ffae1f59-8e1d-4aba-8e56-1eecbfd68533.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.1 people/day (+4.4)
- **⚡ Electricity**
    - usage: 45.81 €/week (⏫+71%)
    - of that for heating: 9.96 €/week
    - of that for hot water: 2.92 €/week
    - paid: 25.05 €/week 
    - ☀️ self produced: 58% (⬆️+6%)
    - emissions: 22 kg CO₂ₑ/week
- **💧 Water**
    - paid: 13.64 €/week (⬆️+26%)
    - emissions: 1.1 kg CO₂ₑ/week


### Expenditure
- [Martin] 48 euros for special drill bits and e27 bulb holders
- [Janina] I bought some spreads for 10€ approximately

### Income
- 30€ donation from the neighbour without them knowing

### Things that happened
#### In or around Kanthaus
* Big finance day!
* HKW MV
* very full PlaMe
* trip to Spitzberg
* attic work continues
* informal knowledge sharing :scream: 
* communal series watching

#### Wider world
* a tree fell in Lausitz and in other places aswell

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->

### Popcorn of feedback
- Janina is happy that people welcomed the visitors
- Kito complains that he forgets to do things in the right time and annoys people with that
- Kate is happy for getting help with buerocracy
- the doors to the staircases are open all the time - stop that!!

## 2. This week planning

### People arriving and leaving
* **Mon.:** Janina's parents will come to Wurzen, friend of Kito might come for a night
* **Tue.:** 
* **Wed.:** Riëll comes by, maxime comes back, all grandparents leave
* **Thu.:** 
* **Fri.:** Loup_WA arrives, kito might leave
* **Sat.:** 
* **Sun.:** 
* **Mon.:** 
* **Some day:** Antonin might come and go, and Doug and Matthias too

### [Weather forecast](https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597)
<!-- [max] This one might be even more readable? https://www.yr.no/en/forecast/daily-table/2-2805597/Germany/Saxony/Wurzen -->
_autumny weather with not too cold nights and days, but a lot of wetness_
 
### Evaluations and check-ins
Due for evaluation (and seen within the last 7 days):
- Larissa Member: Days Visited threshold 240/180 (+33%)
 
Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Antonin Volunteer: 1 days until Days Visited threshold (60)
- Thore Visitor: 6 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * paper bins - done!
    * 10:00 - CoMe [Larissa]
    * 11:00 - Power Hour
    * 19:00 - movie screening @NDK
* Tuesday
    * black bin []
    * Mika's birthday
    * 15:00 - 17:00 Open Tuesday
    * 18:00 - Penny pickup [Janina]
* Wednesday
    * noonish - chat with Riell
    * 12:00 - 17:00 MitMachCafé
    * 16:00 - NDK autum-fest
    * 18:00 - Punkrocktresen
* Thursday
    * 10:00 - Larissa's evaluation [Kito]
    * 13:00 - 18:00 MitMachCafé
    * 18:30 - FLINTA thai boxing @ D5
    * 18:00 - Penny pickup [Martin]
* Friday
    * organic bin []
    * 16:30 - FLINTA* Tech Treff in Leipzig [Janina, you?]
    * 18:00 - Leipzig Critical Mass [Martin, you?]
* Saturday
    * 11:00 - Penny pickup [janina]
    * 21:00 - punkrock at the kathedral
* Sunday
* Next Monday
    * 10:00 CoMe [Martin]
* Next week summary

<!-- To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->

## 3. Shopping plans
- [Janina] 25€ for [Eurotopia community directory book](https://eurotopiaversand.de/en/Book-Print/eurotopia-Directory-low-cost-worldwide-shipping.html)
    - no resistance, but maybe there's also a used version?
- [Kate] 15-30 euros for a steam brush to make clothes mote takeable
    - no resistance

## 4. To do
_Newest tasks on top_
* [ ] proof-reading heating page in handbook https://handbook.kanthaus.online/technical/heating.html [kate]
* [ ] fix/improve K20-1 staircase night light (can ask Tilmann for details about wiring)
* [ ] put kanthaus logo on the new beer benches and tables
* [ ] curtains for the dragon room that prevent passersby from staring inside
* [ ] cut branch from tree in the compost corner (after leaves fall)
* [ ] cover the pipes from the rest of the room in the snack kitchen (wooden board angle?) []
    * [tilmann] maybe directly go for drywall as final measure?
      * [maxime] that would be the right but more difficult remediation indeed. I looked at options to hack a wooden corner, but didn't find satisfying options in the wood shed, so I'll leave there.
* [ ] fill holes in Snack kitchen floor where moths breed
    * it started but it's not done yet, unclear what is the right material
        * [doug] recommend a bit of building foam for a base, then concrete
* [ ] fix Daria trailer back lights [Dorota]
     * [Martin] Cables got pinched, I suggest redoing the whole thing. Also, use a diode to prevent over voltage. 
* [ ] replace shower curtain by (plexi)glass construction

<!-- task archive: https://pad.kanthaus.online/taskarchive -->

## 5. Discussion & Announcements
<!-- for significant or longer term decisions, consider using the PlaMe instead: https://pad.kanthaus.online/plame -->

### Round 1
- [kate] i would like to shut the free shop next week - 31.10 - for the feiertag. last feiertag wasnt worth opening (only 3 people) I will put up a sign etc.
    - makes perfect sense, go for it!
- [janina] on mika's birthday there will be some cake in the afternoon and sushi in the evening. if the sushi shall be the main dinner, I'd need help with preparation. :)
    - might be quite chaotic, childfriendly and fun...^^
    - can also be more of a 'build your own sushi'-party
- [doug] soybeans! today?
    - enthusiasm from dorota
    - janina will communicate

<!-- check if anyone has a point that didn't speak already -->

### Round 2 


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** Janina
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Kateopentuesday

### Unavailabilities
* **Mon.:** 
* **Tue.:** Kate, kyto
* **Wed.:** Kate
* **Thu.:** kyto
* **Fri.:** Martin, Kate, kyto
* **Open Tuesday:** kyto
* **Week:** Larissa, Doug

### Result
* **Mon.:** Kate
* **Tue.:** Janina
* **Wed.:** Kyto
* **Thu.:** Martín
* **Fri.:** Dorota
* **Open Tuesday:** Kateopentuesday


## 7. For next week
*
