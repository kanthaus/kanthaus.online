---
title: Coordination Meeting
date: "2022-06-13"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements:
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md
-->

# CoMe #247
* Date: 2022-06-13
* Facilitator: Nathalie
* Notary: chandi
* Physical board caretaker: Martin
* Digital calendar: maxime
* Reservation sheet purifier: Louis
* Present: chandi, maxime, Thore, Silvan, Louis, Martin, Andrea, Nathalie

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/0bcf519b-2286-41cd-b05e-dd9cc6a0087d.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 19.9 people/day (+3.3)
- **⚡ Electricity**
    - usage: 60.35 €/week (⬆️+15%)
    - of that for heating: 4.95 €/week
    - paid: -45.06 €/week
    - ☀️ self produced: 80% (0%)
    - emissions: 15 kg CO₂ₑ/week
- **💧 Water**
    - paid: 28.64 €/week (⬆️+9%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- [chandi] 36€ for 9L bio oil (3.99€/l)
    - 3.75L frying oil (eg-bio)
    - 1.5L frying oil (naturland)
    - 2.75L sunflower oil (eg-bio)
    - 1L sunflower oil (naturland)


### Income
70€ in the shoe

### Things that happened

#### In or around Kanthaus
* a choir group was here for practicing
* elections in wurzen
* a lot of Kanthausins left
* NDK Sommerfest
* Herr Dr. Delpeuch presented his work
* Low quality movie was watched


#### Wider world
* Elections in france happened
* Oil companies eat up almost all of the Tankrabatt -> more funding for them, jippieh!


## 2. This week planning

### People arriving and leaving
* **Mon.:** Jon and Teresa arrive
* **Tue.:** Martin leaves
* **Wed.:** Larissa and Andrea leave
* **Thu.:** Andrea and Paqui come
* **Fri.:** Jon and Teresa leave, chandi leaves, maybe Silvan leaves
* **Sat.:**
* **Sun.:** chandi comes back
* **Mon.:** Thore and Paqui leave
* **Some day:**

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins

Due for evaluation (and seen within the last 7 days):

- Andrea Volunteer : Absolute Days threshold 104/60 (+73%)
- Chandi Member : Absolute Days threshold 185/180 (+3%)

-> next week

Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
- Martin_WA Visitor : 4 days until Days Visited threshold (21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Nathalie]
    * Post-Sunday-Dumpster-Diving sorting/cleaning [Martin]
    * Festival saving [Silvan, maxime]
    * Post-Festival-Sorting
    * Park KMW & Trailer on even/thit side [Silvan, maxime]
* Tuesday
* Wednesday
    * Park KMW & Trailer on odd/that side [Louis]
    * Punkrocktresen
* Thursday
    * 10:00 Power Hour [Fac.: chandi, DJ: ?]
    * 20:00 project updates [Nathalie]
* Friday
    * yellow bin [Thore]
    * 12:00 Market pickup [maxime]
* Saturday
* Sunday
* Next Monday
    * 10:00 CoMe [louis]
* Next week summary
    *

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [maxime] We are out of oat: lets buy oat?
    - maxime buys smaller amouts now
    - maxime chats with kito about getting a big package again
* [Silvan] 2x 12€ for good useful comunal bike lamps? https://www.mydealz.de/deals/lidl-led-fahrradleuchtenset-mit-703015-lux-und-osram-led-fur-1199-eur-online-und-offline-2000166
    * no enthusiasm but also no resistance -> accepted


## 3b. bis: Non-Shopping plans
* We have plenty of Roggen that needs to be used before it gets bad/pests-infested


## 4. To do
_Newest tasks on top_
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] sort out garden stuff in woodshed [Anja]
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] mount fire extinguisher & first aid kits visible in the staricases K20-1 and K22-1
* [ ] repair right bike shed light
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair Schaukasten
* [ ] add a doorbell in the garden
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
* [Antonin] volunteers and members, please fill in the [K18 poll](https://cloud.kanthaus.online/apps/forms/AEzfKtedLMf5xNym) if you have not done so yet! Visitors are also welcome to if they have thoughts about this building.
* [Anja] please water the veggies in the garden (highbed, pots, main bed, sometimes the herbs) and don't use the smelly IBC water for herbs or plants you want to eat directly (or just water at the base)
    * Silvan takes care and will hand over to Andrea
* [Larissa] The choir group was very happy to be here and had lots of positive feedback :)
    * feedback was really positive
    * maybe would be cool to have the host more present to avoid communication lacks
    * was apparently a really pleasent experience also for kanthausians
* [Nathalie] next week open tuesdays - any volunteers? Or shall we close?
    * [Nathalie] actually I might still be there or check with Antonin
        * apparently Antonin said that he will do it. Nathalie checks back
* [Maxime] fridge in the wood shed: keep it on?
    * let's put it back in the basement
        * louis is doing it, maxime maybe helping
    * it will be used as extended storage for fresh **and washed** food

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [maxime] special stuff requests for the Festival saving?


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:**
* **Tue.:**
* **Wed.:**
* **Thu.:** chandi
* **Fri.:** Thore
* **Open Tuesday:** Nathalie

### Unavailabilities
* **Mon.:** Maxime
* **Tue.:**
* **Wed.:**
* **Thu.:**
* **Fri.:** Maxime
* **Open Tuesday:** Maxime
* **Week:** Silvan, Andrea, Martin

### Result
- **Mon.:**
- **Tue.:** maxime
- **Wed.:** Louis
- **Thu.:** chandi
- **Fri.:** Thore
- **Open Tuesday:** Nathalie

## 7. For next week
*
