---
title: Coordination Meeting
date: "2023-01-30"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #279
* Date: 2023-01-30
* Facilitator: Janina
* Notary: Janina
* Children caretaker: Kita
* Physical board caretaker: Maxime 
* Digital calendar: Tilmann
* Reservation sheet purifier: Larissa
* Present: Maxime, Tilmann, Letti, William, Silvan, Larissa, Janina

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/9b14e623-62a9-424b-bfd9-f913ea1954b8.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.4 people/day (-4.7)
- **⚡ Electricity**
    - usage: 219.58 €/week (⬆️+4%)
        - of that for heating: 155.95 €/week
    - paid: 196.19 €/week 
    - ☀️ self produced: 11% (⬇️-2%)
    - emissions: 94 kg CO₂ₑ/week
- **💧 Water**
    - paid: 5.39 €/week (⏬-64%)
    - emissions: 0.4 kg CO₂ₑ/week
    - (wrong because of a problem with the water meter reader)

### Expenditure
- Bread cutting machine, 6 € (don't force bread through!)
- Safety glasses 16 €
- Ball bearing for vacuum cleaners 16€
- sugar, spreads and oil ~10 €

### Income
- 

### Things that happened

#### In or around Kanthaus
- 3 attic work sessions
- project updates
- matthias reaccepted as volunteer
- many meat pots
- ebike battery maintenance
- crepe session and general nice weekend feeling

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
- conflict is growing again in Israel


## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** 
* **Wed.:** Kristin + 2 kids for the afternoon, William leaves
* **Thu.:** 
* **Fri.:** 
* **Sat.:** 
* **Sun.:**
* **Mon.:** Martin and Verónica arrive (or Sunday), same with Anneke probably
* **Some day:** Antonin comes by, possibly twice even!

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_rainy the whole week, between 1 and 5 °C and a bit warmer towards the weekend_

### Evaluations and check-ins
- Nathalie Visitor : Days Visited threshold 51/21 (+143%)
- Martin Volunteer : Absolute Days threshold 114/60 (+90%)
- Anneke_Martin Visitor : Days Visited threshold 33/21 (+57%)
- Will_Anneke Visitor : Days Visited threshold 24/21 (+14%)
- Silvan Visitor : Days Visited threshold 22/21 (+5%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Janina]
    * 11:00 - 13:00 Attic work session [Tilmann, you?]
	* Monday food care [Letti]
	* 13:00 - 14:30 Focus group sustainability @Laden [Janina]
	* 15:00 William's evaluation [Maxime]
	* 16:30 Cake session [Larissa]
    * Park trailer on even/this side [Maxime]
    * Ventilation work prep
* Tuesday
    * Black bin [William]
    * Ventilation work [Maxime, Letti]
    * 15:00 - 17:00 Open Tuesday
* Wednesday
    * 10:00 - 13:00 Attic session [Tilmann, you?]
    * Park trailer on odd/that side [Maxime]
    * Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Larissa]
    * 15:00 Social sauna [Janina]
* Friday
    * 10:00 - 13:00 Attic session [Tilmann, you?]
    * Green bin [Larissa]
* Saturday
    * 10:00 - 13:00 Attic session [Tilmann, you?]
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [Larissa]
* Next week summary
    * Flowmi group over the weekend
    * two personal guests
    * monthly PlaMe (like the name? ;)) on Friday

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* 

## 4. To do
_Newest tasks on top_
* [ ] oat milk inspection
* [ ] treat mold around bathroom window
* [ ] deep-clean shower tap to make it easier to adjust temperature
* [ ] Potato sorting action
* [x] Elektroschrott tour [Martin and Nathanael]
* [ ] General trash tour
* [ ] Sign: Renew the "Klingel" - Sign (it's less and less readable)
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: elephant room
* [ ] Light: Yoga room 
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
* [Antonin] the [second proposal phase](https://ukuvota.world/#/app/d9b3a17f-866c-3df8-65e6-5f3227787884/collect) about changing the position system is ending soon. Volmems, make sure the option(s) you want to vote for are in there :)
* [maxime] Ventilation work in K20-1 dining room/office/hall way
  - monday: starting to empty the dining room, where most dust is expected, and 
      - people can already start moving stuff during the day
      - sofa to piano room?
      - chairs to lantern?
      - plants?
      - cables on the wall?
      - plastic tarps of different sizes in K22 basement
  - tuesday: drilling in dining room/office/hall way
      - maxime and letti are happy to do the drilling
      - if there's a lot of motivation the chimney could go way down
          - tilmann can help with plastering
- [Larissa] the music studio went through the vortex and chandi wants to reprivatize everything people don't want to keep here. maybe have a look again before he takes everything to leipzig.
- [Janina] heating behaviour: the heatpump also makes hot water and when the hot water is empty the heatpump takes 1,5h to heat it up again. during that time the radiators are not supplied with heat at all! so if you take long showers frequently because you're cold, it could lead to the whole house getting colder - just to be aware. :)

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- 


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Antonin
* **Tue.:** Janina
* **Wed.:** 
* **Thu.:** Larissa
* **Fri.:** 
* **Open Tuesday:** Tilmann

### Unavailabilities
* **Mon.:** 
* **Tue.:** Letti
* **Wed.:** 
* **Thu.:** William
* **Fri.:** William
* **Open Tuesday:** William 
* **Week:** Silvan, Maxime

### Result
- **Mon.:** Antonin
- **Tue.:** Janina
- **Wed.:** William
- **Thu.:** Larissa
- **Fri.:** Letti
- **Open Tuesday:** Tilmann

## 7. For next week
* 
