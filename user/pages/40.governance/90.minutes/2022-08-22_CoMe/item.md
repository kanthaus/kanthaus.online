---
title: Coordination Meeting
date: "2022-08-22"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #257
* Date: 2022-08-22
* Facilitator: Andrea
* Notary: Zui
* ~~Mika caretaker: ~~
* ~~Levi caretaker: ~~
* Physical board caretaker: kiki
* Digital calendar: Zui
* Reservation sheet purifier: Maxime
* Present: Andrea, Zui, Kiki, Maxime

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/3b5d002d-0b6b-4494-8293-739dc84a569a.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.9 people/day (-1.9)
- **⚡ Electricity**
    - usage: 35.4 €/week (⏬-50%)
    - of that for heating: 3.45 €/week
    - paid: -25.41 €/week 
    - ☀️ self produced: 66% (⬇️-21%)
    - emissions: 11 kg CO₂ₑ/week
- **💧 Water**
    - paid: 16.0 €/week (⬆️+2%)
    - emissions: 1.2 kg CO₂ₑ/week

### Expenditure

### Income
- 31 cents in the shoe

### Things that happened
#### In or around Kanthaus
- pyjama party
- carla cago celebration
- trashy movie
- 

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* see https://de.wikipedia.org/wiki/Wikipedia:Hauptseite

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** Larissa comes back
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Eric leaves, and so does Kiki
* **Sat.:** Larissa leaves for one night
* **Sun.:** Eric comes back
* **Mon.:** 
* **Some day:** Martin, Nathalie, yantin 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins
- Kito Volunteer : Absolute Days threshold 101/60 (+68%)
    - could happen thursday, starting between 12 and 2
    - we wait till next week? no one has time this thursday
- Nathalie Volunteer : Absolute Days threshold 80/60 (+33%)
- Thore Volunteer : Absolute Days threshold 76/60 (+27%)
- Tilmann Member : Absolute Days threshold 181/180 (+1%)


### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe []
    * Post-Sunday-Dumpster-Diving Food sorting/washing [?]
    * Highfield saving action [Matthias]
    * Park KMW & trailer on even/this side [maxime]
* Tuesday
    * 15:00 - 17:00 Open Tuesday [Eric]
* Wednesday
    * Park KMW & trailer on odd/that sidecht [maxime]
    * Finance-meeting [Finance team]
* Thursday
    * 10:00 Power Hour [Fac.: Eric, DJ: ?]
* Friday
    * 12:00 Market pickup [zui]
    * Yellow trash [maxime]
    * Crossover Festival @Spitzenfabrik Grimma
* Saturday
    *
* Sunday
    * 
* Next Monday
    * 10:00 CoMe [maxime]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans

## 4. To do
_Newest tasks on top_
* [ ] replace broken window handle in Yoga Room (left side)
* [ ] unblock k22-upper bathroom sink [Doug would _help_, 2-30 min]
    * [Antonin] I had a look, the sink does not seem blocked to me - did someone fix it already?
* [ ] fix or rework the K20 bathroom occupancy indicator
* [ ] fix weekly-report script
    * electricity: include solar production estimation from the unrecorded panels []
* [ ] renew the "Kanthaus"- sign on the Kanthaus [spraypaint by Kito?]
    * [Antonin] Can it not be done on a panel or other surface that can be easily put on and off?
* [ ] remount snack kitchen radiator [Silvan?]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] repair/replace Schaukasten
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: hallway of the K20-2 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties)

## 5. Discussion & Announcements

### Round 1
- [Andrea] There's a workaway request to come from the 27th (Saturday) to the 4th. Anyone interested?  
    - no host found
- [zui] we need to sort the stuff coming from highfield, wash dumpsterdived food
- [maxime] reminder to clean up after yourself/leave no trace
- [kiki] kick off meeting for orginizing the next ffj (september in salzderhelden)
<!-- check if anyone has a point that didn't speak already -->

### Round 2


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** zui
* **Tue.:** kito
* **Wed.:** Larissa
* **Thu.:** 
* **Fri.:** maxime
* **Open Tuesday:** eric

### Unavailabilities
* **Mon.:** maxime
* **Tue.:** 
* **Wed.:** 
* **Thu.:** maxime
* **Fri.:** Eric
* **Open Tuesday:** 
* **Week:** Martin

### Result


## 7. For next week
* 
