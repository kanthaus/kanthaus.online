---
title: Coordination Meeting
date: "2021-05-31"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #193
- Date: 2021-05-31
- Facilitator: Talita
- Notary: Antonin
- ~~Mika caretaker:~~
- Physical board caretaker: Doug 
- Digital calendar: Zui
- Reservation sheet purifier: Doug
- Present: Thore, Nathalie, Doug, Matthias, Zui, Talita, chandi, Antonin

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_a125e255d2af0bf4850b0c098fde4b87.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.4 people/day (-1.3)
- **⚡ Electricity**
    - usage: 5.69 €/day (⬇️-3%)
    - paid: -4.75 €/day 
    - ☀️ self produced: 75% (⬇️-1%)
    - emissions: 5 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.49 €/day (⬇️-16%)
    - emissions: 1 kg CO₂ₑ/week


### Expenditure
- 15€ for bringing trash (bauschutt) away

### Income
- 17.54€ Pfand
- 20€ donation

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 4646
- µCOVIDs used last week: 1119
- µCOVIDS balance from last week: 3527
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 8527

### Things that happened
- scaffolding taken down
- we have a new laundry drying thing
- project updates happened
- Kanthaus feels calmer - and emptier
- "Tag der Nachbarn" from NDK happened
- A documentary about apes was watched
- We watched Al Jazeera as alternative news
- A lamp was fixed

## 2. This week planning

### People arriving and leaving
- **Mon.:** More book project people arrive
- **Tue.:** Andrea comes back
- **Wed.:** 
- **Thu.:** Larissa & Silvan come back
- **Fri.:** Larissa & Silvan leave, Brieuc WA may come (or Saturday)
- **Sat.:** Clara leaves
- **Sun.:** chandi leaves
- **Mon.:** zui may come back
- **Some day:** Book project people leave, zui leaves, doug leaves for the weekend

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
- Matthias Member : Absolute Days threshold 196/180 (+9%)
- Chandi Member : Absolute Days threshold 192/180 (+7%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [Talita]
    - 15:30 Coffee and Cake in the garden
- Tuesday
    - 14:00 Kritma round [Matthias]
- Wednesday
    - 10:00 Chandi's evaluation [Antonin]
    - 16:00 Money fish bowl [Nathalie]
- Thursday
    - 10:00 Power Hour [Fac.: Doug]
    - 14:00 Knowledge Sharing: ? [?] <!-- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
    - 15:00 Social sauna [Chandi]
- Friday
    - Yellow Trash
    - 10:00 Matthias' evaluation [Doug]
- Saturday
- Sunday
- Next Monday
    - 10:00 CoMe [Matthias]
- Next week summary
    - 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- KMW tour for trash [Doug, Antonin] spontaneous

## 3. Shopping plans
- 

## 4. Discussion & Announcements

### Round 1
- [Nathalie] funding with Wandel würzen e.V. - project/ personal money vs Kanthaus ratio
  - [Antonin] more generally, for paid work made in Kanthaus, how to determine how much money should go to Kanthaus?
- [Antonin] hosting guests from [WarmShowers](https://www.warmshowers.org/) - can we do this? how much notice do we need?
  - no concerns about hosting people from WarmShowers
- [Doug] session for avoiding annoying house phenomena (e.g. food not getting washed, surfaces not getting cleaned, etc)

### Round 2
- [Antonin] should we reopen the freeshop?
    - [Doug] yes!
    - [chandi] If the house stays that empty for the upcoming time, have the remaining people enough capacities and motiviation to run the freeshop?
- [Doug] spare 140 mattress?
    - [Matthias] could imagine finding one

### Round 3
- [Antonin] there is food to be washed
- [Doug] yellow bags really need to go this week
    - Matthias will do it
- [Zui] noodles this week!

## 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Zui
- **Tue.:** Matthias, Antonin
- **Wed.:** Doug
- **Thu.:** Talita, Nathalie
- **Fri.:** Thore, Chandi
- **~~Open Tuesday~~:** 

### Unavailabilities
- **Mon.:** Talita, Thore, Nathalie, Doug, Matthias
- **Tue.:** Talita, chandi, Thore, Nathalie, Doug
- **Wed.:** Talita, Thore
- **Thu.:** chandi
- **Fri.:** Doug
- **~~Open Tuesday~~:** 
- **Week:** 

## 6. For next week
- session on solving kanthaus collective issues
