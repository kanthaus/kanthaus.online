---
title: Coordination Meeting
date: "2021-12-06"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #221
- Date: 2021-12-06
- Facilitator: chandi
- Notary: Silvan
- Mika caretaker: Matthias
- Levi caretaker: Tilmann
- Physical board caretaker: Doug
- Digital calendar: chandi
- Reservation sheet purifier: Nathalie
- Present: Nathalie, Silvan, Anja, Doug, Charly, Chandi, Janina, Tilmann, Maxime, Zui, Janina, Larissa

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_35ba25eae6fe666d183ff57ef9b50812.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 17.4 people/day (+4.4)
- **⚡ Electricity**
    - usage: 20.15 €/day (⬆️+13%)
        - from that 13.70€/day for heating
    - paid: 17.42 €/day 
    - ☀️ self produced: 14% (⬇️-4%)
    - emissions: 86 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.6 €/day (⬇️-6%)
    - emissions: 1 kg CO₂ₑ/week


- Heating: 13,70€/day for 184 kWh/day (with 8% solar coverage); equivalent gas cost would have been about 15,23€ / day

### Expenditure
- [tilmann] 73€ for screws (for insulation), sawblades, Hohlraumdübel, Trockenbaubithalter, TX15 bits
- [Doug] 10€ Ibuprofen

### Income
- nothing in the shoe

### Things that happened
- ❄️ first snow has fallen ❄️
- Christmas cookies baking session
- First interaction choir (online) session in the piano room
- First meeting of ToI planning group
- Yet another AoE session
- We had Glühwein and cake at NaThos and some speeches were held
- Sporty Saturday with heated yoga room
- Feuerzangenbowle incl. watching the corresponding movie and playing geoguesser afterwards
- got a lot of nice things out of the advent calender
- the kitchen food got sorted and a little bit rearranged
- attic cleanup
- some more rooms got ventilation 
- spontaneous finta café

#### Wider world
- Omikron variant with probably higher infection rate and less vaccine effectiveness spreadig world wide
- Torchlight march in Grimma by Querdenken people in front of the health minister's private residence. More escalation expected today in Dresden
- radical right-wing presidential candidate in france "Éric Zemmour" with growing popularity
- war in the ukraine might be escalating further

## 2. This week planning

### People arriving and leaving
- **Mon.:** Lise just left - oh noo, kathi leaves
- **Tue.:** Carolina comes, Zui leaves
- **Wed.:** änski comes back
- **Thu.:** 
- **Fri.:** Anja, Matthias, Nathalie,  and Thore leave
- **Sat.:** Carolina leaves, Andrea might leave 
- **Sun.:** Anja comes back
- **Mon.:** Zui, Nathalie and Thore comes back
- **Some day:** 

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->


### Evaluations and check-ins
- *Andrea* _Volunteer_ : _Absolute Days_ threshold 110/60 (+83%)
    - evaluation record missing? :D @Nathalie
- *Chandi* _Member_ : _Absolute Days_ threshold 187/180 (+4%)
- *Matthias* _Member_ : _Absolute Days_ threshold 185/180 (+3%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [chandi]
    * Park cars on even/this side [done]
    * after Dinner: knot tying workshop [doug]
- Tuesday
    * Rest waste [Larissa]
    * 15:00 - 17:00 Open Tuesday
    * 18:00 - Sitzung des Stadtrates, Schweizerhaus
- Wednesday
    * 11:00 Matthias evaluation [maxime]
    * Park cars on odd/that side [Nathalie]
    * 14:00 ToI ToI introspection meeting [Nathalie]
    * 19:00 Interaction choir online [Nathalie]
- Thursday
    * 10:00 Power Hour [Fac.: Anja, DJ: ?]
    * 13:00 scaffholding tear down preperation meeting [doug]
    * 15:00 Social Sauna [Janina]
    * 18:00 foodsharing Wurzen meeting [Janina]
- Friday
    * 10:00 chandi's evaluation [doug]
    * 12:00/12:30 take down scaffholding [all people who are present]
    * bio waste [Charly]
- Saturday
    * 
- Sunday
    * 
- Next Monday
    * 10:00 CoMe [Maxime]
- Next week summary
    * 

_to be scheduled:_

(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
- knowledge sharing proposals:
    - knot-tying workshop [Doug]
    - https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [matthias] 150€ for warm water pipes to prepare heating for attic/central ventilation

## 4. To do
_Small fixes of things in the house_

* [x] Fix the scarves drawer in the K20 hallway [chandi]
* [x] repair right bike shed light [Silvan]
* [x] fix K22-2 toilet flush pipe [chandi]
* [x] sort and introduce a categorization in the kitchen shelves [änski,chandi]

* [ ] fix hipster room window curtain [Larissa & anja]
* [ ] more storage spaces for visitors [charly]
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] KMW: buy new windscreen wiper and change it
* [ ] KMW: fix the ventilation switch
* [ ] Install some minimal light outside of K22 garden door
* [ ] Building week clean up tasks
    * [ ] scaffolding net in the garden
         - [antonin] if we are to store the scaffolding in the basement again, then I would wait for that to happen first (to leave space for big pieces first), and then store it there too, maybe?
* [ ] KMW: fix driver side door (https://yunity.slack.com/archives/C3RS56Z38/p1636467207030900)

### New this week

## 5. Discussion & Announcements

### Round 1
- [janina] the scaffolding needs to be taken down by Sunday. Is someone motivated to tear down the chimney in the dining room before then? The benefit would be that the rubble could go down via the lift and it's necessary for ventilation progress.
    - [Doug] and let's schedule takedown!
    - many preperation tasks need to be done
        - prepare space in basement -> prep meeting thursday 13:00
        - chimney teardown can be done until friday 12:00
- [lise] a few weeks ago i asked if i could take the wooden grain mill to HZ. There was no resistance especially if there would be another working one in KH. I checked the Nutrimill (which is in the intermediate) and its working. Any resistance for the wooden mill going to HZ on friday?
    - it is fine, have fun with it :)
    - can HZ also take the pile of stuff out of intermediate and the pile of something out of Kleingarten? [Doug]
- [matthias] Snack kitchen disruptance: Ventilation needs to be progressed further down, there is a short time where we need bathroom + snack kitchen a bit dirty simultaneosly. Also, unfortunately the **Snack kitchen boiler** providing warm water for kitchen/snackkitchen, is in the way. Proposal: Take it down. Central hot water system for hot water also here will be build in february/march
    - resistance on the missing warm water - conect it back? temporary Durchlauferhitzer?
    - MaTi would be happy if people join the building work
    - no resistance to doing it this week in general
- [Doug] Report/reflections on work sheet experiment? Proposal for this week? (Perhaps better done outside of CoMe)
    - Pause this week

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [matthias] Trash tour. Anybody likes to do it? Back of KMW to Wertstoffhof (Tue to Fri 9-17), "front of back" to Bauschutt-Disposal-Place (Mon-Fri 6-16).
    - Larissa and Anja would like to be a "By-driver"
- [Doug] compost bin left full in hallway, glass waste bag not replaced -> please empty and return bins _immediately_ or else your good contribution is cancelled out by confusion/unpredictability.

<!-- check if anyone has a point that didn't speak already -->

### Round 3
- [matthias] Oschatz Baunativ Pickup: KMW or 2 trailers (biketrip?), 64km in total. Anybody motivated? This week, if KMW is chosen, it is only possible after Wertstoffhof/bauschutt. -> Baunativ also wants to know, when we do the pickup.
    - [Silvan] Does it fit in one OBI-Trailer? -> can do it with the Golf
- [Doug] the driftwood -> firewood for us/Klugis?
    - chop it and keeo it for bonfire ;)



## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Silvan, Janina
- **Tue.:** Andrea
- **Wed.:** Larissa
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** Anja

### Unavailabilities
- **Mon.:** 
- **Tue.:** chandi
- **Wed.:** 
- **Thu.:** 
- **Fri.:** Matthias, chandi
- **Open Tuesday:** chandi, Maxime 
- **Week:** Tilmann, Nathalie, Zui

### Result
- **Mon.:** Janina, Silvan
- **Tue.:** Andrea, Maxime
- **Wed.:** Larissa, Chandi
- **Thu.:** Matthias, Doug
- **Fri.:** Charly + X (?)
- **Open Tuesday:** Anja + Y (?)

