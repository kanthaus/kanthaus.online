---
title: Plenum
date: "2022-11-11"
taxonomy:
    tag: [plenum]
---

# KH - Plenum #1
- Date: 11.11.2022
- Facilitator: Zui
- Notary: Kito, Tilmann
- Present: Martin, Janush, Janina, Tilmann, Doug, Maxime, Zui, Lara, Kito

## Content
<!-- Zeiten sind nur eine grobe orientierung -->
- (10min) check in
- (5min) presentation of discussion points (What do you want to achieve with it)
- (5min) prioritization of discussion points
- (80min) discussion
    - ways of decision making (e.g. consent, sociocracy, ukuvota, general agreement)
- (10min) scheduling
- (10min) check out

## Discussion Points
### FFJ [kito] 
-  see: https://pad.kanthaus.online/ffj_im_kanthaus?both
    - summary of points to be discussed from the pad: number of participants that feel comfortable, participation in KH-structures (CoMe, PowerHour, evaluations, other commitments), general "diffusion of structures"
- full-time in Kanthaus for at least 2-3 months, they will also visit other places later during the 6-month-period
- they would participate in CoMe and Power Hour
- Evaluations might be done a bit differently, still to be decided
- details are still very open, can be influenced
- how to make a decision, which details should be included?
- how much do they integrate with Kanthaus people, Kanthaus and local projects?
- supportworthy goals of FFJ?: self-organization, political involvement, engange young people from east Germany
- Kito would prefer 10 people (range of 7-11), including orga people (but excluding Kito)
- Doug and Kito will prepare an ukuvota

### Decision making in building processes [zui]
- general "why do we build this?" for specific cases would be nice, to get everyone involved
- feel empowered to ask your questions

### Attic, next steps  [tilmann]
- Tilmann will leave at some point, doesn't want to leave it with a mess
- one of Tilmanns motivation for construction work is also balance with childcare
- would like to stop after winter with attic work
- insulation will happen in 3 weeks, what are the next step?
- seal the staircases and entrance (fire protection, limit heat exchange): buy doors, drywall, some silicon stuff - 800€
    - drywall will protect the roof from fire
    - didn't found fire protection doors in ebay, drywall is difficult (but it's recyclable), Tilmann didn't find really other options
    - support in the room :)
-  but some "rough edges": old walls need to be plastered, drywall the roof, old beams need to be coated, connecting the heating "Making It Nice" is the second step
-  floor: keep the existing floor, put filling material for balancing and reducing noise transmission (clay-like), OSB on top as new floor (advantage: don't need connection to the old floor), maybe coated
-  K22 is more complex because of more corners, and because the building material is stored there
- options: 
    - floor 4200€ -> https://kanthaus.online/de/governance/minutes/2022-10-31 (material for both attics with OSB and filling material), glue is consideration, should be ventilated well in the beginning, and connect ventilation system, ceiling is already out of OSB, 170sqm in total
    - "gypsum fiber": doesn't look as nice, but is not as sealed as OSB, no experience with this material
    - wooden planks like in the hipster room: more natural, doesn't contain glue, much more work to make it even and not noisy, would need to be screwed in something below (existing floor is not suitable), so it would be higher, 800€-3000€ more expensive than OSB, depending on the wood we get, Tilmann would only do it if there is an experienced person
- heating? would make sense to connect it, various options in the same price range (3000-4000€), Tilmann prefers heating panels (wall heating), seems not too complex, unclear who will do it

### [janina] toi 23 in harzgerode?
- [zui] toi in general (what do we want from it, who is organizing  etc.)
- [antonin] can unfortunately not attend this meeting and so wrote things on slack: https://yunity.slack.com/archives/C3RS56Z38/p1667723506259919
- Janina and kito can imagine to be in the orga-group, maybe more
- Doug and Martin might not be able to participate during the week
    - could be easier for them if it's closer to Leipzig
    - maybe also find a structure that is more fitting for those people
- everything else will be discussed, orga-team prepares

### [Doug] **Stainless steel kitchen sink.**
Digging up an [old thread](https://yunity.slack.com/archives/CNLJL8PEG/p1619036886009100) to continue this topic, with feedback from Bodhi about sink heights at Harzgerode. I would propose getting the ['GSB140'](https://www.ebay.de/itm/323192843906) which is slightly smaller than the one I originally proposed, yet still double the sink capacity to now. **400 €**, but I would be willing to personally subsidise to an collectively acceptable sum.
- a lot of problems with the current one, get something better fitting to our needs
- buy it from a butcher equipment store
- closing hazard to children? should be fine
- hole for tab needs to be cut or it is mounted on the wall
- maybe the start for bigger rearrangements
- not really alternatives to stainless steel
- everyone in favour :) 
- second hand options that are a bit more expensive (up to 600€) are also appreciated

## Things to schedule 
- evaluations, skillshare, knowledgeshare
    - maybe have more upcoming evaluations listed
- nice community trip? like hiking etc?
- next plenary
    - [Doug] Proposal to hold next plenum ~~on the empty week~~/week which had Monthly Teams Meeting
    - in 5 weeks, friday 16.12. 10.00 [Doug?] - for the next ones facilitator could be found in CoMe 

## Themenparkplatz (things that come up and we don't want to forget about)

...

### bigger things (for toi?)

### for social sauna

