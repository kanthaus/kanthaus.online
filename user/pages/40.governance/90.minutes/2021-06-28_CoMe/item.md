---
title: Coordination Meeting
date: "2021-06-28"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #197
- Date: 2021-06-28
- Facilitator: Silvan
- Notary: Janina
- ~~Mika caretaker:~~
- Physical board caretaker: Antonin
- Digital calendar: Doug
- Reservation sheet purifier: Talita
- Present: Andrea, Antonin, Anja, Doug, Tilmann, Levi, Janina, Nathalie, Thore, Zui, chandi, Michal, Silvan

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_65040120486ec19ebac59532cb637426.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 14.1 people/day (+5.7)
- **⚡ Electricity**
    - usage: 5.28 €/day (⬆️+57%)
    - paid: -4.15 €/day 
    - ☀️ self produced: 76% (⬇️-1%)
    - emissions: 4 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.54 €/day (⬆️+20%)
    - emissions: 1 kg CO₂ₑ/week


### Expenditure


### Income
- 220€ (shoe + cash box)

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 10549
- µCOVIDs used last week: 620
- µCOVIDS balance from last week: 9929
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 15000
- µCOVIDs/person mean for this week: lots (18 people) 

### Things that happened
- Zui's educational collective had a meeting and turned one year old
- Danni debrief meeting started
- Lots of people came back
- The bins corner got smaller
- project update took place
- FFF Meeting took place here
- coffee and cake with Lea und Max in the garden

## 2. This week planning

### People arriving and leaving
- **Mon.:** Bodhi, Lise, Mika arrive, Anja leaves, Matthias arrives
- **Tue.:** Matthias leaves
- **Wed.:** Silvan leaves, Zui leaves
- **Thu.:** Lise leaves, Silvan comes back, Andrea leaves
- **Fri.:** Nathalie leaves
- **Sat.:** 
- **Sun.:** Samuel arrives, Andrea comes back
- **Mon.:** 
- **Some day:** end of the week Bodhi leaves

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Hot! Thunderstorms towards the end of the week.

### Evaluations and check-ins
- Due for evaluation (and seen within the last 7 days):
    - Michal Volunteer : Absolute Days threshold 97/60 (+62%)
    - Clara Volunteer : Absolute Days threshold 80/60 (+33%)
- Due for evaluation soon (in the next 7 days, and seen within the last 7 days):
    - Silvan Member : 5 days until Absolute Days threshold (180)

[clara] you could schedule my evaluation for Friday 3pm?

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [Silvan]
- Tuesday
    - 16:00 Open Tuesday
    - 14:00 Kritma round [Silvan]
- Wednesday
    - 10:00 Attic cleanup [Tilmann]
    - 14:00 FINTA* coffee
    - 16:00 Michal evaluation [Doug]
- Thursday
    - 10:00 Power Hour [Fac.: Talita, DJ: surprise!]
    - 14:00 Knowledge Sharing: ? [?] <!-- Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->
    - yellow bin on friday  [Antonin]
- Friday
    - 15:00 Clara's evaluation [Thore]
- Saturday
- Sunday
- Next Monday
    - 10:00 CoMe [chandi]
- Next week summary
    - 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)

## 3. Shopping plans
- [tilmann] 35€ for 25 filter bags for sanding machine https://werkzeugstore24.de/festool-turbofilter-tf-ii-rs-es-et-25.html
- 

## 4. Discussion & Announcements

### Round 1
- [janina] cherry-picking at the Rotter's place in Altenbach. Anybody up for it?
    - Many people interested, they will corrdinate among themselves.
    - Given a preferred time Janina will call Andre Rotter and tell him when we come.
- [Doug] With low cases, R-value and increasing vaccination rate, I would propose we stop using the framework. I would propose to have an ukuvota and/or discussion this week.
    - [chandi] maybe it gets reactivated automatically with an R value > 1 once? so we don't need to do another plenary and decision process first
    - Okay, ukuvota will be started.
- [Silvan] Problem with digital calendar and dinner, who is motivated and competent enough to solve it?
    - Doug will fix our individual issue by recreating events, but is not motivated to look into the deeper problem of the software.
    - Thanks Doug!
- [Tilmann] Attic could use some tidying up after the building week, maybe Wednesday morning for 2 hours - who's in?
    - Mild interest, but not enough for scheduling. Also childcare would be needed.
    - Tentatively put down for around 10 am.

<!--  let's check whether someone hasn't spoken yet -->

### Round 2
- [janina] The family is back! Let's revive the caretaking schedule! ^_^
- [Silvan] where did all the tooth brushes go and did someone see mine?
    - maybe next to the microwave?

### Round 3
- [Silvan] Garden watering plan?
    - [janina] yes! to mark when you did the watering, so other people know that it has been done. (not before, because then you can still forget... ;))

### Additional points
- [janina] Let's keep the windows and doors closed over the day when it's so hot!
- [Zui] If you see moths (who live in the food), kill them!

## 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** 
- **Tue.:** Danni group, Danni group
- **Wed.:** Nathalie, Thore
- **Thu.:** 
- **Fri.:** 
- **Open Tuesday:** Doug

### Unavailabilities
- **Mon.:** chandi, antonin
- **Tue.:** chandi, antonin, michal
- **Wed.:** chandi, Silvan, antonin, michal
- **Thu.:** Silvan, antonin
- **Fri.:** 
- **Open Tuesday:** antonin, michal, chandi
- **Week:** Andrea, Talita, Zui, Levi, Anja, michal

## 6. For next week
- 
