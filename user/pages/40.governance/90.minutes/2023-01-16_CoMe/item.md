---
title: Coordination Meeting
date: "2023-01-16"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #277
* Date: 2023-01-16
* Facilitator: Doug
* Notary: Kito
* Mika caretaker: their parents
* Levi caretaker: their parents
* Physical board caretaker: Anneke
* Digital calendar: Larissa
* Reservation sheet purifier: Kito
* Present: Silvan, Larissa, Anneke, Doug, Kito, William

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/9c0214fb-5a3a-4f94-9083-0c0ce4235c5f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 5.9 people/day (-9.1)
- **⚡ Electricity**
    - usage: 85.16 €/week (⬆️+8%)
    - of that for heating: 50.46 €/week
    - paid: 59.46 €/week 
    - ☀️ self produced: 28% (⬆️+4%)
    - emissions: 43 kg CO₂ₑ/week
- **💧 Water**
    - paid: 37490.32 €/week (⏫+255419%)
    - emissions: 2915.9 kg CO₂ₑ/week
    - we are broke now :/


### Expenditure
* 55 € 2 * Beglaubigungen
* 45 € bathroom building supplies +

### Income
* 1.100€ for selling KMW
* 300€ donation

### Things that happened

#### In or around Kanthaus
* monthly plenum with 4 people talking about insurance, liability and responsibility
* more guests in the house than residents
* nice time with Flotte Rotte
* kitchens were made (much more) moth-proof
* some Kanthausians going to Lützi, others to Luftschlosserei
* KMW got sold
* spice shelf was rearranged

#### Wider world
* eviction of Lützi :(
* big demonstration in israel
* aircraft crashed in Nepal

<!--
https://en.wikipedia.org/wiki/Main_Page
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
* 

## 2. This week planning

### People arriving and leaving
* **Mon.:** JaTiMiLe come back
* **Tue.:** Andrea comes for the day
* **Wed.:** Martin and Maxime probably come back
* **Thu.:** Vroni arrives for 1 night, Larissa leaves
* **Fri.:** GemÖk (3-4ppl), Andrea+1 arrive
* **Sat.:** Matthias and Marieke arrive
* **Sun.:** zui arrives (maybe already Sat), Andrea+1 leave
* **Mon.:** zui, Matthias, Marieke leave, Larissa comes back
* **Some day:** Antonin? Bodhi? Emily?

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
- right now: super nice
- it will get a bit colder with a bit of rain but also more sunshine!

### Evaluations and check-ins
- pipeline couldnt be run :)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Doug]
	* Monday food care [William]
    * Park trailer on even/this side [Doug]
* Tuesday
    * Hausmüll [William]
    * 15:00 - 17:00 Open Tuesday
* Wednesday
    * Park trailer on odd/that side [Doug]
    * 10:00 - 13:00 attic work session [tilmann, antonin, you?]
    * 19:00 Punkrock-Tresen
* Thursday
    * 10:00 Power Hour [Larissa]
* Friday
    * Biotonne [William]
    * 19:00 Küfa with anarchist Input @ADI
* Saturday
    * 
* Sunday
    * [kito] hiking trip
    * meeting of the old KH-gemök
* Next Monday
    * 10:00 CoMe [Kito]
* Next week summary
    * Küfa next tuesday (24th)

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
* [kito] Oats, 25kg for 75-85€
    * no resistance, yay
* [Doug] 5 * 4m Letra labelmaker tape, 12€
    * more labels!!

## 4. To do
_Newest tasks on top_
* [ ] Potato sorting action needed
* [ ] Electroschrott tour needed! [Martin]
* [ ] General trash tour needed [matthias?]
* [ ] Sign: Renew the "Klingel" - Sign (it's less and less readable)
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: elephant room
* [ ] Light: Yoga room 
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
* [Antonin] members can [vote](https://ukuvota.world/#/app/f7f38d31-afbb-9a42-5919-35f351b619c1/vote) about the possible evolution of the position system
* [Andrea] Greetings from Merseburg! like I wrote on slack (quite late I'm sorry), I will like to do a video about KH for one of my university courses. I was planning on coming today but I dont think its going to work. I might come tomorrow to do some filming and also spend some time around. On friday a friend will come with me and we will stay until Sunday morning. So it would be nice to know who would be up for talking in front of the camera. I thought the topic could be KH in Wurzen. Let me know what you think! 
    * be prepared to also film on the weekend, there will be more people in the house
    * interest from Doug and Janina, also Kito
    * niceeeee! thank youuu <3
* [kito] Küfa next week, maybe tuesday or thursday?
    * maybe tuesday because of Open Tuesday? - let's do it!
    * 
<!-- check if anyone has a point that didn't speak already -->

### Round 2
*

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** Larissa, Silvan
* **Tue.:** 
* **Wed.:** janina
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:**

### Unavailabilities
* **Mon.:** 
* **Tue.:** kito
* **Wed.:** 
* **Thu.:** Doug
* **Fri.:** kito, Doug
* **Open Tuesday:** 
* **Week:** 

### Result
* **Mon.:** Larissa, Silvan
* **Tue.:** Doug
* **Wed.:** Janina
* **Thu.:** Anneke
* **Fri.:** William
* **Open Tuesday:** Kito

## 7. For next week
* 