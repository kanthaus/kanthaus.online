---
title: Coordination Meeting
date: "2021-06-07"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #194
- Date: 2021-06-07
- Facilitator: Matthias
- Notary: Antonin
- ~~Mika caretaker:~~
- Physical board caretaker: Talita
- Digital calendar: Matthias
- Reservation sheet purifier: Talita
- Present: Matthias, Thore, Andrea, Nathalie, Brieuc, Talita, Antonin

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_569cc42239164a1bcb4c11f5dba84644.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 9.7 people/day (-0.9)
- **⚡ Electricity**
    - usage: 4.25 €/day (⬇️-25%)
    - paid: -5.93 €/day 
    - ☀️ self produced: 73% (⬇️-2%)
    - emissions: 4 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.12 €/day (⬇️-15%)
    - emissions: 1 kg CO₂ₑ/week

### Expenditure
- 50€ for paint for the yoga room

### Income
- 400€ donation

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
- µCOVIDs available last week: 8527
- µCOVIDs used last week: 1610
- µCOVIDS balance from last week: 6917
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week: 11917

### Things that happened
- a fishbowl on money in kanthaus happened
- two working days in the yoga room happened, the ceiling is painted and the wall is plastered
- a trash tour happened
- we have more space in the garden after some washing lines were removed
- the grass was nicely mown

## 2. This week planning

### People arriving and leaving
- **Mon.:** doug arrives
- **Tue.:** zui leaves, Silvan arrives
- **Wed.:** matthias leaves
- **Thu.:** doug leaves
- **Fri.:** 
- **Sat.:** 
- **Sun.:** zui comes back
- **Mon.:** 
- **Some day:** antonin?, larissa

### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
- Sunny and (too) warm

### Evaluations and check-ins
- Thore (62/60)
- Andrea (21/21)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [Matthias]
- Tuesday
    - Restmüll [Antonin]
    - 16:00 Open Tuesday [Doug]
- Wednesday
    - 10:00 Power Hour [Fac.: Antonin, DJ: maybe Nathalie]
    - 13:00 Andrea's evaluation [Nathalie]
    - 15:00 Kaffeekränzschen at villa klug
- Thursday
- Friday
    - Biotonne [Brieuc]
    - 10:00 Thore's evaluation [Talita]
- Saturday
- Sunday
- Next Monday
    - 10:00 CoMe [Thore]
- Next week summary
    - Foodsharing Wurzen meeting

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)

## 3. Shopping plans
- [antonin] we don't seem to have a lot of dowels ("Dübel" auf Deutsch) left. Should I get some and where?
  - Did you check the "Nachfülllager"? There is a box somewhere...
  - antonin will look for this box

## 4. Discussion & Announcements

### Round 1
- [zui] (slack post) visitor announcement: moko want to vistit from the 20th till end of the month. and when we both are already here we might also do a meeting of our collective from the 25th till the 28th in KH, what would mean that for that time also oska and wawa come here. let me know if there are concerns
- [Doug] Verschenkeladen: will open this week! I can do it alone, but might be more fun with someone. I would be really happy to have responsible people for the next two Tuesdays, when I'm not here: we can have a mini-meeting before I go.
   - [Brieuc] is keen, but maybe not alone
   - [Antonin] can do next week
   - [Nathalie] the week after
- [Thore] Trip to saxony switzerland on Thursday. Who will join? The plan is to take the train at 09:18 and to come back in the evening. Proposal for the train costs: Soli based and GemÖk pays the rest. 

### Round 2
- [Doug] Kleingarten watering: Plants should be watered once every 3 days, 2 if really hot. Heavy rainfall counts as watering ;p Waterer gets first claim on usage in my opinion. Small tour of garden for anyone who's interested, just let me know.
- [Brieuc] would be interested in having access to a laptop during his stay
   - [Matthias] the situation of communal laptops is not great but could have a look if something could be usable
   - [Nathalie] Brieuc can take the one Doug brings back today, if that fails he talks to Thore

### Round 3
- [Doug] I had announced to hold a session this week on solving annoying house stuff, but won't have time! Sorry.
- [Matthias] migrating to fansipan
   - [Talita] will still do yoga room work including sanding
   - [Matthias] will stay in the cave
- [Andrea] proposes a meeting about film making, check slack

## 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Matthias
- **Tue.:** Thore, Brieuc
- **Wed.:** Andrea, Nathalie
- **Thu.:** Antonin
- **Fri.:** Talita
- **Open Tuesday:** Doug, Brieuc for the first hour

### Unavailabilities
- **Mon.:** Thore, Nathalie, Talita, Andrea
- **Tue.:** Nathalie, Talita, Antonin
- **Wed.:** Thore, Matthias, Antonin
- **Thu.:** Thore, Matthias, Nathalie, Talita, Andrea, Brieuc
- **Fri.:** Matthias
- **Open Tuesday:** Matthias
- **Week:** 

## 6. For next week
