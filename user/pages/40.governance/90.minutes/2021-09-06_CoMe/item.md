---
title: Coordination Meeting
date: "2021-09-06"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #208
- Date: 2021-09-06
- Facilitator: Doug
- Notary: Doug
- Mika caretaker: Roswitha
- Levi caretaker: Janina
- Physical board caretaker: Antonin
- Digital calendar: Tilmann
- Reservation sheet purifier: Larissa
- Present: Antonin, Thore, Andrea, Silvan, Anja, Larissa, Maxime, Tilmann, Matthias, Clara, Janina, Doug

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_4b894c72d5faa8692fb8a2c2dd14dd57.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 11.1 people/day (-1.1)
- **⚡ Electricity**
    - usage: 6.33 €/day (⬆️+12%)
    - paid: -2.43 €/day 
    - ☀️ self produced: 73% (⬆️+4%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.26 €/day (⬆️+9%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- 

### Income
- 800€

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)

> From [Collective Agreements](https://kanthaus.online/en/governance/collectiveagreements): *If adjusted prevalence is < 0.1%, we have no formal restrictions.*
> Current **adjusted prevalence** can be found [here](https://www.microcovid.org/?duration=600&interaction=oneTime&personCount=1&riskBudget=100000&riskProfile=average&scenarioName=custom&setting=indoor&subLocation=Germany_Sachsen&theirMask=none&topLocation=Germany&voice=loud&yourMask=none&yourVaccineType=johnson): click on "Details" in the "Step 1" column

- Incidence
    - Germany: 90,6
    - Saxony: 32,5
    - Landkreis Leipzig: 21,7
- Adjusted prevalence Saxony: 0,11% 
- µCOVIDs available last week: 0
- µCOVIDs used last week: 0
- µCOVIDS balance from last week:  0
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week:  5000
- µCOVIDs/person mean for this week:  (18 people) 

### Things that happened
#### In and around Kanthaus
* We harvested 5 boxes of beans!
* People went to Klima camp!
* Anja and Doug found amazing fruit trees nearby with many delicious fruits!
* there was an AoE Lan-Party!
* Tomato plants got a fungus and died
* nice project updates in a round happened
* the stack of bikes in the yard decreased
* a lot of leftover wedding food arrived - all the best to arnulf and janine!

#### In the wider world
* More extreme weather events
* German parliament elections might get interesting :-)

## 2. This week planning

### People arriving and leaving
- **Mon.:** Jums arrives, Silvan leaves, Zui leaves, roswitha and gerd leave
- **Tue.:** 
- **Wed.:** Doug and Anja leave, JaTiMiLe leave
- **Thu.:** Jums leaves, Clara leaves
- **Fri.:**
- **Sat.:**
- **Sun.:** Clara probably comes back
- **Mon.:**
- **Some day:**


### Weather forecast

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

- Continuously nice, sunny, warm. Maybe thunderstorms with a bit of rain on the weekend.

### Evaluations and check-ins

- Anja Volunteer : Absolute Days threshold 105/60 (+75%)
- Antonin Member : Absolute Days threshold 234/180 (+30%)
- Clara Volunteer : Absolute Days threshold 66/60 (+10%)
- Maxime Member : Absolute Days threshold 181/180 (+1%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Doug]
    * 12:00 Roof tech meetup [Doug]
        * updating flowchart for differences (e.g. chimney)
        * assigning responsibilities for parts
    * 15:00 Knowledge sharing: 
    * Park cars on even side [already done]
- Tuesday
    * Open Tuesday [Anja]
- Wednesday
    * 15:00 Antonin eval. [Andrea]
    * Park cars on odd side [Maxime]
    * Landgut Nemt pickup []
- Thursday
    * 10:00 Power Hour [Fac.: Larissa, DJ: ?]
    * 15:00 Sharing event []
- Friday
    * Gelbe Tonne [Matthias]
    * 11:00 Maxime eval. [Larissa]
    * 12:00 Market Pickup [Andrea]
    * Landgut Nemt pickup []
- Saturday
- Sunday
    * Landgut Nemt Pickup []
- Next Monday
    * 10:00 CoMe [Larissa]
- Next week summary

- Weeks until Roof month: 5.5!

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
* [matthias] Duvet covers/ bed sheets https://yunity.slack.com/archives/CT0K2AY87/p1630836077001900
* [matthias] Window + outside window board for about 400€ -> see discussion and https://yunity.slack.com/archives/CNLJL8PEG/p1630777360003600
* [Silvan] Aleppo Soap (maybe 5,40Eur for 4 pcs.) 

## 4. To do
_Small fixes of things in the house_

### High priority
* [ ] secure wash kitchen sink
* [ ] secure k22-2 bathroom sink

### Medium priority
* [ ] make k20 garden door easily closable from outside (or acquire a new door) https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000 [Doug may do this out of frustration :( ]
* [ ] fix loose table leg in Lantern
* [ ] empty dry food storage shelf (find new places for the stuff)
* Ventilation preparation:
    * [ ] Tear down chimney in Snack Kitchen
    * [ ] Tear down chimney in communal sleeping room
    * [ ] Tear down chimney in dining room
    * [ ] Tear down chimney in piano room
* [ ] Kitchen shelf is slanting
* [ ] fix drying racks in staircase [Antonin]
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]


## 5. Discussion & Announcements

### Round 1
* [clara] painting rooms
    * Inspired by Yoga room action: Taking down wallpaper, repaint makes rooms much nicer. Would like to do that in another room, maybe during building week (if time left). Concerns?
    * only that it is/might more and more annoying work than it might seem
    * Matthias can imagine doing this in the silent office and is also interested in concerns. He will coordinate with users of the silent office anyway.
* [matthias] (Properly!) exchanging ONE window on Garden side: Hipster? Sleep Kitchen? Food storage? Freeshop Lounge?? https://yunity.slack.com/archives/CNLJL8PEG/p1630777360003600
    * Concerns of having it at same time as roof building week
    * resistance against doing it in the hipster room? no. (but concerns that this room is then even more liked)
* [doug] moth casualties: lasagne sheets, 300g black sesame, ~5Kg wheat... storage needs to be airtight!
* [anja] there is plants just outside that might not like being in the cold. Please get them in before it gets cold!
* [janina] We brought quite some food, especially pumpkins, please don't forget to eat them relatively soon! (The pumpkins are all edible, most also with peel!)

### Round 2
* [doug] repeat: roof month 13 Oct - 10 Nov, [pad](https://pad.kanthaus.online/roofMonthOct2021?both#), [poll](https://poll.disroot.org/373TNY9W87QM3TH9)
    * We expect approximately 15 people
* [matthias] Picking up a washing machine right after CoMe only 150m up the street
    * Antonin, Maxime will help. Thanks!

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** 
- **Tue.:** 
- **Wed.:** Andrea
- **Thu.:** Thore
- **Fri.:** Maxime
- **Open Tuesday:** Anja, Clara, Doug

### Unavailabilities
- **Mon.:** 
- **Tue.:**
- **Wed.:** Tilmann, Janina
- **Thu.:** Tilmann, Clara, janina
- **Fri.:** Antonin, Tilmann, janina, Clara
- **Open Tuesday:** Matthias, Tilmann, Larissa
- **Week:** Silvan

### End result
- **Mon.:** Tilmann
- **Tue.:** Matthias, Janina
- **Wed.:** Andrea, Antonin
- **Thu.:** Thore
- **Fri.:** Maxime, Larissa
- **Open Tuesday:** Anja, Clara, Doug

## 7. For next week
