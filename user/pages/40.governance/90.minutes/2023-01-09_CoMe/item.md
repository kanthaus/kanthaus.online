---
title: Coordination Meeting
date: "2023-01-09"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #276
* Date: 2023-01-09
* Facilitator: Doug
* Notary: Doug
* Mika caretaker: Kita
* Levi caretaker: Kita
* Physical board caretaker: Larissa
* Digital calendar: Martin
* Reservation sheet purifier: Emily
* Present: Martin, Emily, Larissa, Maxime, Matthias, Doug

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/70e1190e-541c-4245-aad4-dd8f7604f22b.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 15.0 people/day (+5.1)
- **⚡ Electricity**
    - usage: 78.6 €/week (⬇️-5%)
    - of that for heating: 36.55 €/week
    - paid: 59.01 €/week 
    - ☀️ self produced: 24% (⬇️-4%)
    - emissions: 42 kg CO₂ₑ/week
- **💧 Water**
    - paid: 14.67 €/week (⬆️+14%)
    - emissions: 1.1 kg CO₂ₑ/week

### Expenditure
- staubsauger beutel, 15€
- spülmaschinensalz, 4€
- rundfunkbeitrag, 4.75€
- lentils and rice, ?€
- spülmaschinenpulver 4€

### Income
- 1€

### Things that happened
#### In or around Kanthaus
- Levi got older! Cake, cookies and cream (also vegan)
- 2022 reflection
- Vol/mem merge prop session
- Group visited GfzK
- New, exciting foodsharing cooperation started 

#### Wider world
- coup attempt in Brazil
- Lützerath bleibing

## 2. This week planning

### People arriving and leaving
* **Mon.:** Matthias, Clara, extended family members, Zui leave, Kito arrives
* **Tue.:** 
* **Wed.:** JaTiMiLe leave, Flotte Rotte group arrive (up to 12!)
* **Thu.:** Max leaves ?
* **Fri.:** Anja comes, Silvan leaves
* **Sat.:** 
* **Sun.:** Flotte Rotte group and Anja leaves
* **Mon.:** 
* **Some day:** Martin leaves, Silvan arrives

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

### Evaluations and check-ins
* kito: i could have my evaluation tuesday vor wednesday, i am already quite some weeks over the threshold

- Anja Volunteer : Absolute Days threshold 315/60 (+425%)
- Nathalie Visitor : Days Visited threshold 48/21 (+129%)
- Martin Volunteer : Absolute Days threshold 93/60 (+55%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Doug]
    * Monday food care [x]
    * Park trailer on even/this side [x]
* Tuesday
    * 11:00 Kito eval [Maxime]
    * 15:00 - 17:00 Open Tuesday []
* Wednesday
    * 19:00 Punkrocktresen @D5
    * Park trailer on odd/that side [Martin]
* Thursday
    * 10:00 Power Hour [Fac.: , DJ: ]
* Friday
    * yellow waste [Doug]
    * 10:00 monthly plenum [Doug]
* Saturday
    * 
* Sunday
    * 
* Next Monday
    * paper waste [Doug]
    * 10:00 CoMe [Doug]
* Next week summary

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans

## 4. To do
_Newest tasks on top_
* [ ] Electroschrott tour needed! [Martin]
* [ ] Sign: Renew the "Klingel" - Sign (it's less and less readable)
* [ ] Light: elephant room
* [ ] Light: Yoga room 
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

### Round 1
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->
* [Antonin] there is a constitution change proposal and [the ukuvota for it is open](https://ukuvota.world/#/app/f7f38d31-afbb-9a42-5919-35f351b619c1/collect). Volunteers and members are invited to propose more changes until Friday evening. See the constitution for more details about the process.
- [doug] plenum
    - yes

<!-- check if anyone has a point that didn't speak already -->

### Round 2
- [doug] network and rfid issues: who is responsible, who is capable? Are there people willing to take (more) responsibility?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** maxime
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Martin

### Unavailabilities
* **Mon.:** Larissa
* **Tue.:** Larissa
* **Wed.:** 
* **Thu.:** 
* **Fri.:** Larissa, Emily, Doug
* **Open Tuesday:** Larissa
* **Week:** Matthias

### Result
- **Mon.:** Doug
- **Tue.:** Maxime
- **Wed.:** Emily
- **Thu.:** Larissa
- **Fri.:** 
- **Open Tuesday:** Martin

## 7. For next week
