---
title: Coordination Meeting
date: "2021-08-23"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #206
- Date: 2021-08-23
- Facilitator: Matthias
- Notary: chandi
- ~~Mika caretaker:~~
- ~~Levi caretaker:~~
- Physical board caretaker: Nathalie
- Digital calendar:  Zui
- Reservation sheet purifier: Doug
- Present: Matthias, Nathalie, zui, Mariha, Maxime, Andrea, Doug, chandi

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_84bc990e540fbb7a219bd58d8c453f21.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.4 people/day (+3.3)
- **⚡ Electricity** 
    - usage: 5.04 €/day (⏫+61%)
    - paid: -2.33 €/day 
    - ☀️ self produced: 67% (⬇️-3%)
    - emissions: 11 kg CO₂ₑ/week
- **💧 Water**
    - paid: 3.23 €/day (⏫+68%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
- [talita] 37€ for the yoga room white paint (generally I spent what I declared in two last CoMe + 5€)

### Income
- None

### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)

> From [Collective Agreements](https://kanthaus.online/en/governance/collectiveagreements): *If adjusted prevalence is < 0.1%, we have no formal restrictions.*
> Current **adjusted prevalence** can be found [here](https://www.microcovid.org/?duration=600&interaction=oneTime&personCount=1&riskBudget=100000&riskProfile=average&scenarioName=custom&setting=indoor&subLocation=Germany_Sachsen&theirMask=none&topLocation=Germany&voice=loud&yourMask=none&yourVaccineType=johnson): click on "Details" in the "Step 1" column

- Incidence: 
    - Germany: 57.5 (+57%)
    - Saxony: 20.4 (+62%)
    - Landkreis Leipzig: 31.8 (-2%)
- Adjusted prevalence Saxony: 0,08% (last check: 2021-08-23) / Germany: 0.21%
- µCOVIDs available last week: {COVIDS FOR LAST WEEK NOT FOUND!}
- µCOVIDs used last week: 
- µCOVIDS balance from last week:  
- µCOVIDs additional this week: 5000 
- µCOVIDs available this week:  
- µCOVIDs/person mean for this week:  (18 people) 

### Things that happened
#### In and around Kanthaus
* Inauguration of Yoga room
* Yoga room cleanup
* Wash kitchen laundry shelf rework for less dirt and hopefully easier accessibility
* Freeshop has been emptied a lot through ebay Kleinanzeigen
* Game night - with cooporative Uno
* High quality Movies in the Communal Room
* Watched 4 hours of Afganistan documentaries

#### In the wider world
* Afghanistan takeover by the Taliban continues
* Kultur-ohne-Kohle Festival happened


## 2. This week planning

### People arriving and leaving
- **Mon.:** Larissa comes back
- **Tue.:**
- **Wed.:** Clara comes back
- **Thu.:**
- **Fri.:** Larissa leaves
- **Sat.:** chandi leaves
- **Sun.:** zui leaves
- **Mon.:**
- **Some day:** Mariha and Nathalie leave


### Weather forecast

<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->

We still have a warning of "heavy, persistent rain" up to 80l/m² until this evening. Besides, it is rainy (except tomorrow) and with 12-20 degrees quite a bit colder.

### Evaluations and check-ins
- Nathalie Member : Absolute Days threshold 223/180 (+24%)



### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe [Matthias]
    * 15:00 Knowledge sharing:
        * Radio-waves & SDR by matthias
    * Park cars on even-side [Maxime]
- Tuesday
    * Open Tuesday []
- Wednesday
    * 14:00 Nathalie's Evaluation [chandi]
    * Park cars on odd-side [Maxime]
    * Landgut Nemt pickup [Doug]
    * 19:00 NDK movie "Lesbisch l(i)eben in der DDR"+talk with Regisseur
- Thursday
    * 10:00 Power Hour [Fac.: Matthias, DJ: ?]
    * 16:30 Social Sauna [Matthias]
    * 19:00 NDK movie "Unter Männern - Schwul in der DDR"+talk with Regisseur
    * late dinner ;)
    * [Community Upcycling Festival](https://co-up-festival.de/) begins, goes until Saturday, 16-21 each day, Leipzig West. Doug will go for a day :)
- Friday
    * yellow bin in the morning [matthias]
    * 12:00 Market Pickup [Mariha,Nathalie]
    * Landgut Nemt pickup [Doug]
    * 16 - 18:00 NDK Podiumsdiskussion about the Exhibition (Marketplace)
    * 19:00 NDK movie "Neubau"
    * [Climate Camp Leipzig](https://www.klimacamp-leipzigerland.de) starts (until the 7., should register if you stay overnight)
- Saturday
- Sunday
    * Landgut Nemt Pickup [Doug]
- Next Monday
    * 10:00 CoMe [Andrea]
- Next week summary
    * 

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
* [Matthias] 300-330€ for Bicycle trailer, see https://pad.kanthaus.online/2021-08-06_trailer-planning and/or https://yunity.slack.com/archives/CQPK7722K/p1628252355001100
    * concerns
        * that a bought one would be almost as much as the DIY one. 
        * starting a new bike trailer project if there is an open bike trailer project
    * matthias will do it :)
    * Mariha is interested in helping
* [Matthias] Tools and Material to "Spontaneously paint rooms & put new, in-wall power installation":
    * [Matthias] 60-80€ for Schlitzfräse
    * [Matthias] 20€ for Bohrkrone 68mm for power sockets
    * [Matthias] 100€ for different "second hand" electronics equipment: Power sockets, Switches for In-Wall installation and the fitting "Unterputzdosen" and maybe some more cables
        * Nathalie wants to see more effort to borrow stuff
        * matthias thinks it's too exhausting (wants to do spontaneous things, won't do it if he has to always look for it first)
        * Doug also thinks it's nice to have it as a first reaction to borrow when someone needs something

## 4. To do
_Small fixes of things in the house_

* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] make k20 garden door easily closable from outside (or acquire a new door) https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000
* [ ] secure wash kitchen sink
* [ ] secure k22-2 bathroom sink
* [ ] think about plastic waste container to be used in dumpster kitchen
  - [maxime] what about using a big cardboard that could be emptied in the yellow bin, and replaced from time to time when too nasty. This would go with a recommendation to empty liquids before throwing there and/or wrapping nasty stuff in newspaper
    -> invited to do a prototype 
* [ ] fix loose table leg in Lantern
* Ventilation preparation:
    * [ ] Tear down chimney in Snack Kitchen
    * [ ] Tear down chimney in communal sleeping room
    * [ ] Tear down chimney in dining room
    * [ ] Tear down chimney in piano room
    

## 5. Discussion & Announcements

### Round 1
- [doug] File organization proposals: 
    1. remove 'topics' folders in `kanthaus-private` and `kanthaus-public` and move contents respectively up one level. [Example.](https://cloud.kanthaus.online/f/140550) (This will remove a superficial folder, and decrease number of folder levels, but might break some 'hard-written' filepaths (e.g. on printed signs))
    3. move all Haus Kante Wurzen & Wandel Wurzen files from `kanthaus-public` to `kanthaus private` (This will unify the files in one place, but will remove some files from being publicly accessible.)
        * thoughts about not having the public/private seperation anymore
        * no concerns for doug going forward with the proposals
- [Matthias] Coordinate yoga room cleanup
    - Cleaning windows [doug]
    - Mopping a second time [andrea]
        - Yoga room
        - K22-2 hallway
        - K22-1/2 staircase
    - Moving Puzzle mat parts from Wood Shed to Yoga room
    - Cleaning workshop tables
    - Tidying up workshop (Paintung utensils, dust masks)
    - Cleaning tools/buckets? (or throwing away)
    - Tidying up cleaning utensils
    - Tidying up K22-2 staircase area
* [maxime] Strategy to keep the Biotone not too disgusting
  * recommandation to never let wheat/gluten products uncontained in the bin (otherwise the bin gets super sticky and extra gross). Containing can be done with several layers of newspaper
* [chandi] KIDZ (my affinity group) meeting in Kanthaus, 10.-12. September, 5-7 ppl
    * no concerns
* [zui] moths
    * exponential grow!
    * everything should be packaged properly (glas, proper plastic containers)
    * we found even some in the rye bucket (it could be that the bucket lid wasn't exactly the right one)
<!-- check if anyone has a point that didn't speak already -->

### Round 2
* [maxime] onions in every dish! \o/

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Nathalie
- **Tue.:** 
- **Wed.:** Andrea, maxime
- **Thu.:** zui
- **Fri.:**
- **Open Tuesday:**

### Unavailabilities
- **Mon.:** maxime, chandi, matthias
- **Tue.:**
- **Wed.:**
- **Thu.:** 
- **Fri.:** chandi, doug
- **Open Tuesday:** maxime, chandi, matthias
- **Week:** 

### End result
- **Mon.:** Nathalie
- **Tue.:** Doug, chandi
- **Wed.:** Andrea, maxime
- **Thu.:** zui
- **Fri.:** Matthias 
- **Open Tuesday:** Mariha, Doug (the first half)

## 7. For next week

