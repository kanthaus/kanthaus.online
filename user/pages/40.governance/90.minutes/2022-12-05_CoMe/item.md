---
title: Coordination Meeting
date: "2022-12-05"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #272
* Date: 2022-12-05
* Facilitator: Martin
* Notary: kito
* Mika caretaker: Janina/Kita
* Levi caretaker: Janina/Kita
* Physical board caretaker: Anneke
* Digital calendar: Larissa
* Reservation sheet purifier: Lara 
* Present: Martin, Lara, Anneke, Larissa, Kito, Tilmann, Maxime

----

<!-- Time for reflection -->

## 0. Check-in round

## 1. Last week review
### Stats

![usage graph last 90 days](https://pad.kanthaus.online/uploads/40197dc0-0dad-4f4b-b90a-eb4e47534247.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 14.4 people/day (+2.3)
- **⚡ Electricity**
    - usage: 134.41 €/week (⬆️+13%)
    - of that for heating: 89.91 €/week
    - paid: 121.73 €/week 
    - ☀️ self produced: 9% (⬇️-15%)
    - emissions: 82 kg CO₂ₑ/week
- **💧 Water**
    - paid: 17.27 €/week (⬆️+6%)
    - emissions: 1.3 kg CO₂ₑ/week


### Expenditure
* [Martin] 12 euros for toilet paper

### Income
* 5 euros in the boot

### Things that happened

#### In or around Kanthaus
* Roof insulation
* 2nd Advent + roof insulation celebration
* presentation from bitte wenden about good traffic planning in other places
* disappointment of failed communication about the actionday #DefendKurdistan #WeSeeYourCrimes

#### Wider world
* during the worldcup catastrophe in Katar the German *green* economy minister has nothing better to do than making a deal about LNG with Katar for the next 15 years.
* Germany out of the worldcup, lol

## 2. This week planning

### People arriving and leaving
* **Mon.:** Silvan comes back
* **Tue.:** Lise is arriving
* **Wed.:** Lara leaves :( 
* **Thu.:** 
* **Fri.:** Lise leaving, Zui leaves, Larissa & silvan leave
* **Sat.:** 
* **Sun.:** larissa & silvan come back, Antonin too
* **Mon.:** Zui comes back
* **Some day:** 

### Weather forecast
<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
* Temperatures around zero celsius the whole week. High chances of rain (or snow?!) Tuesday and Wednesday. We might see some sun on Sunday! 

### Evaluations and check-ins

- Nathalie Visitor : Days Visited threshold 36/21 (+71%)
- Thore Volunteer : Absolute Days threshold 87/60 (+45%)
- Kito Volunteer : Absolute Days threshold 75/60 (+25%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Martin]
    * monday food care [Lara, Anneke, Janina?]
    * Park KMW & Trailer on even/this side []
* Tuesday
    * Hausmüll [Larissa]
    * 15:00-17:00 Open Tuesday
* Wednesday
    * 11:00 Lise's evaluation [Larissa]
    * Park KMW & Trailer on odd/that side []
    * 19:00 Punkrocktresen @D5
* Thursday
    * 10:00 Power Hour [Fac.: Martin, DJ: ?]
    * 15:00 Social Sauna [Maxime]
* Friday
    * Biotonne [Anneke]
    * 19:30 nice concert @D5
* Saturday
    * 
* Sunday
    * 3rd Advent
* Next Monday
    * 10:00 CoMe [kito]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit
* Lise Evaluation (Wednesday or Friday during Kita hours.)

## 3. Shopping plans
* [martin] The blender container is broken! Is it an appliance that we want to keep? I found only new ones on ebay, and they start at around 50 euros. I don't know of a better way to get one. https://www.ebay.de/itm/222475091387?hash=item33cc8c7dbb:g:SnUAAOSwpkFY8RNH&amdata=enc%3AAQAHAAAAoFfnC12zWD7w1nk%2B5ZiTD7%2Biuz6J0%2B1UDgt3wtOf2CN2KwlTfB8a5r06f%2FVG3PEEWU66H7jJLsZvjYNGzhXepy3qb6luXIvJ2olw1LaQwD2liHl3acsLxB6pyynuiZpPU0phkM4djdVSCurVieETgQsSsaPXE%2BLjiv2%2Bd80sF9EOuYxMeo2s5EVdMS5JHaHcn2x8%2FGbmmW6rCwEf3%2BYxf1k%3D%7Ctkp%3ABk9SR4TUroaYYQ
    * [zui] I bought it on ebay kleinanzeigen, should arrive today or tomorrow
## 4. To do

_Newest tasks on top_
* [x] fix K22 door code [Martin, Antonin]
* [ ] fix bike repair station instructions
* [ ] treat mold in Ex-food-storage and hipster room
     * Those rooms need a fresh air supply (Zuluft)
     * Some more work would be welcome to finish connecting those rooms to the ventilation system
* [ ] renew the "Kanthaus"- sign on the Kanthaus
* [ ] make the wall nice in K20-2 bathroom where the boiler has been [Larissa]
* [ ] Light: hallway of the K20-1 flat
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements

### Round 1
* [maxime] the KMW battery is flat, and Matthias says that "If it has been on that charge level last week at freezing temperatures, don't bother charging, throw it :slightly_smiling_face:"
    * if it stays there and we get bills it's still cheaper than buying a new battery ;) 
    * try to restart with old battery from workshop on sunday
    * also possible to take battery out of KMW and charge it
    * Martin and Maxime will try to something on the weekend
* [anneke] I'd like to introduce you to my christmas project and ask to use the infrastructure of KH for the 23rd of December
    * project called "Homeless Christmas Matters": give food, blankets... to homeless people in Leipzig and Chemnitz on the 24th
    * there is a crowdfunding for this project
    * anyone knows where to borrow a thermos-pot?
    * Anneke would like to cook with 2 extra people for this project on the 23rd
        * if you have concerns please talk to Anneke the next days
    * idea to Anneke to ask for support in Slack when knowing more concrete what to do when
    * there is a portable gas stove in KH
* [antonin] the stats and people to evaluate are available now, see above
    * thank you <3
* [Larissa] foodsharing pickup in Leipzig tonight - Silvan can't do it, who is up for it?
* [tilmann] potential local foodsharing cooperation - let's talk to Nathalie again and join the foodsharing team!
* [maxime] let's get rid of the old car batteries in the workshop!
  * [larissa] we should make sure that they end up in a better place
* [kito] remember to vote on the ToI sessions! https://ukuvota.world/#/app/0bd37663-60a4-2673-240f-a16ba6c96653/collect 

<!-- check if anyone has a point that didn't speak already -->

### Round 2
* 

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** 
* **Wed.:** Anneke
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Lara

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** 
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** Tilmann, Larissa, Kito

### Result
* **Mon.:** Maxime
* **Tue.:** Martin
* **Wed.:** Anneke
* **Thu.:** 
* **Fri.:** 
* **Open Tuesday:** Lara 
