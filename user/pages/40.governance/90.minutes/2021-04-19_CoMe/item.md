---
title: Coordination Meeting
date: "2021-04-19"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #187

- Date: 2021-04-19
- Facilitator: Talita
- Notary: Janina
- Corona commissioner: chandi
- Mika caretaker: Tilmann
- Levi caretaker: Janina
- Physical board caretaker: Zui
- Digital calendar: Doug
- Reservation sheet purifier: Doug
- Present: chandi, Maxime, Larissa, Andrea, Silvan, Janina, Nathalie, Thore, Antonin, Doug, Zui, Talita, Kito
----

<!-- Minute of silence (?) -->

### 0. Check-in round

### 1. Last week review
##### Stats
![usage graph last 90 days](https://codi.kanthaus.online/uploads/upload_9526c3fe09c3eabbba1c1ad1a0358605.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 15.7 people/day (+1.6)
- **⚡ Electricity**
    * usage: 9.18 €/day (⬆️+24%)
    * paid: 0.93 €/day 
    * ☀️ self produced: 70% (⬆️+5%)
    * emissions: 8 kg CO₂ₑ/week

- **💧 Water**
    * paid: 3.98 €/day (⬇️-4%)
    * emissions: 2 kg CO₂ₑ/week

- **🔥 Gas**
    * usage: 5.2 €/day (⬇️-8%)
    * emissions: 123 kg CO₂ₑ/week


<!-- add output from script here -->

##### Expenditure
- 25€ More Gerüstschutznetz [chandi]
- 889.11€ for roof windows [chandi/antonin]
- 32€ 10L H2O2 12% [maxime]
- 500€ metal parts for roof

##### Income
- ~900€ donation

##### Corona (_[Framework](https://cloud.kanthaus.online/s/S9HsWHANkmakixr/download)_)
<!-- Update the current probability on the sheet! -->
- µCOVIDs available last week: 11524
- µCOVIDs used last week: 3770
- µCOVIDS balance from last week: 7754
- µCOVIDs additional this week: 5000
- µCOVIDs available this week: 12754

###### Corona Status
- Numbers compared to the week before
  - 7-day incidence (Germany): xx per 100.000
      - +xx% deaths (~xx per day)
      - +xx% intensive care patients
  - 7-day incidence (Leipziger Land): xx per 100.000
- tba

Source: https://www.revosax.sachsen.de/vorschrift/19062

##### Things that happened
- a looot of roof work!
    - scaffolding was built down and up again
    - bitumen was removed
    - temporary tarp was installed
    - almost all nails were removed from the wood
    - chimney and roof windows were removed
    - holes for new windows were cut and old ones filled
    - first piece of unterspannbahn was installed with konterlattung
    - the repro plan was also filled very well
    - reflection session on Saturday
- foodsharing Wurzen coworking happened
- we got more food from Christa's basement
- yoga room cables are covered in the walls
- communal room private storage got new signs
- two movies were communally watched

### 2. This week planning

##### People arriving and leaving
- **Mon.:** 
- **Tue.:** Lea and Paul arrive
- **Wed.:** 
- **Thu.:** Matthias (latest Friday lunchtime)
- **Fri.:** Rob will visit
- **Sat.:** maybe Vroni arrives
- **Sun.:** 
- **Mon.:** 
- **Some day:** 

##### Weather forecast
<!-- 
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
A bit warmer, mostly dry, some sun

##### Evaluations and check-ins
- none

##### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    - 10:00 CoMe [talita]
    - 13:00 Lunch
    - 14:30 Check-in [Doug]
    - 15:00 Roof work
- Tuesday
    - 8:30 to 9:30 Breakfast
    - 9:30 Check-in [Janina]
    - 10:00 to 13:00 Roof work
    - 13:00 to 14:30 Lunch break
    - 14:30 Check-in
    - 14:45 to 18:00 Roof work
    - 19:00 Dinner
    - 20:00 Check-out
    - 21:00 Price-guessing game [Kito]
- Wednesday
    - 7:00 to 8:30 Early morning roof work shift
    - 8:30 to 9:30 Breakfast
    - 9:30 Check-in [Nathalie]
    - 10:00 to 13:00 Roof work
    - 13:00 to 14:30 Lunch break
    - 14:30 Check-in
    - 14:45 to 18:00 Roof work
    - 19:00 Dinner
    - 20:00 Check-out
    - 21:00 Movie-time [Zui]
- Thursday
    - 10:00 Power Hour [Fac.: Larissa, DJ: Silvan backup]
    - 15:00 Social sauna [Fac.: Janina]
    - 21:00 Debate club [Kito, Janina]
- Friday
    - trash - yellow bag
    - Roof work skillshare day
    - 7:00 to 8:30 Early morning roof work shift
    - 8:30 to 9:30 Breakfast
    - 9:30 Check-in [Kito]
    - 10:00 to 13:00 Roof work
    - 13:00 to 14:30 Lunch break
    - 14:30 Check-in
    - 14:45 to 18:00 Roof work
    - 19:00 Dinner
    - 20:00 Check-out
    - 21:00 Werewolf [chandi]
- Saturday
    - 7:00 to 8:30 Early morning roof work shift
    - 8:30 to 9:30 Breakfast
    - 9:30 Check-in [Thore]
    - 10:00 to 13:00 Roof work
    - 13:00 to 14:30 Lunch break
    - 14:30 Check-in
    - 14:45 to 18:00 Roof work
    - 19:00 Dinner
    - 20:00 Check-out
- Sunday
- Some day

- Next Monday
  - 10:00 CoMe [Fac: chandi]
- Next week summary

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)

### 3. Shopping plans
- [doug?] ~500€ for kitchen sink (e.g. https://www.ebay.de/itm/Spultisch-mit-ausziehbarer-Handbrause-tiefem-Becken-und-Schiebeturenschrank/265094772908?LH_ItemCondition=3000) 
- [tilmann] ~20€ for K22 street side rainwater collector
  - rough plan here: https://yunity.slack.com/archives/CFDNQL9C6/p1617561787019800

### 4. Discussion & Announcements

#### Round 1
- [Doug] Corona flat-rate proposal: consider 'full-contact' people part of kh household for one week.
- [tilmann] washing powder for tap water is empty!
  - get more?
  - please use the one for soft water ("weiches Wasser") with rain water only, otherwise it won't wash properly
  - rain water system is usable, but still needs a personal introduction and manual switching back to tap water. I will probably make more improvements soon.
  - we ask Matthias to place an order (you fine with that, Matthias? ^^)
- [chandi] weekly script breaks almost every week. more checks should be implemented. could anyone imagine doing that?
    - Talita is up for that, will collaborate with chandi on this.
- [Thore] Where are all the bike locks? If you have some, please bring them back to the bike shed.
- [Silvan] status of the tarps laying outside in the garden
    - they are a task on the todo list of the roof people - a task for today even
- [Zui] We forgot to schedule the evening activities, let's do it now! :)
- [Nathalie] Finances for Fairteiler from Wandel würzen
    - acceptance
    - can check with Bodhi if we need to immediately refill that account

#### Round 2
- [Doug] The vacuum cleaners: who is responsible for getting them working again?
    - Nathalie can feel responsible for one but lacks a bit overview
    - Talita can have a look at the other
    - Nobody really knows what to do, so it's gonna be a trial and error experience...^^'

#### Round 3
- [Doug] Always keep one window-sill totally free. Do we need to make it a rule? (Thinking of Electroworkshop)
    - Well, it already is in the Collective Agreements, so please people, stick to it!

### 5. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

#### Volunteers
- **Mon.:** Antonin, Kito
- **Tue.:** Maxime, Silvan
- **Wed.:** Janina, Andrea
- **Thu.:** Zui, chandi
- **Fri.:** Larissa, Talita
- **Sat.:** Thore, Doug
- **~~Open Tuesday~~:** 

#### Unavailabilities
- **Mon.:** Thore, Talita, chandi, zui
- **Tue.:** Talita, Larissa, Kito, chandi
- **Wed.:** Thore, Talita, Kito, zui, chandi
- **Thu.:** Janina
- **Fri.:** zui, chandi
- **Sat.:** Larissa, Talita
- **~~Open Tuesday~~:** 
- **Week:** Nathalie

#### Lunch cooking this week
- **Mon.:**
- **Tue.:**
- **Wed.:** 
- **Thu.:**
- **Fri.:** 
- **Sat.:** 

#### Weekend team this week
- **Participants**: 

### 6. For next week
- Project updates?
