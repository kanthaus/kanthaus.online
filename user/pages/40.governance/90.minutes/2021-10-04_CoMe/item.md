---
title: Coordination Meeting
date: "2021-10-04"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md -->

# CoMe #212
- Date: 2021-10-04
- Facilitator: Janina
- Notary: Janina
- Mika caretaker: Thore, Matthias, Larissa
- Levi caretaker: Tilmann
- Physical board caretaker: Matthias, Larissa
- Digital calendar: chandi
- Reservation sheet purifier: Silvan
- Present: Janina, Matthias, Maxime, chandi, Silvan, Larissa, Tilmann

----

<!-- Minute of silence (?) -->

## 0. Check-in round

## 1. Last week review
### Stats
![usage graph last 90 days](https://pad.kanthaus.online/uploads/upload_6096b2a3491514e9dbe05f4be4555a7f.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 10.6 people/day (-0.3)
- **⚡ Electricity**
    - usage: 6.12 €/day (⬇️-13%)
    - paid: -0.83 €/day
    - ☀️ self produced: 70% (⬆️+5%)
    - emissions: 13 kg CO₂ₑ/week
- **💧 Water**
    - paid: 2.95 €/day (⬆️+2%)
    - emissions: 2 kg CO₂ₑ/week

### Expenditure
* [matthias] 15€ for Anemometer (air flow measurement for setting up ventilation stuff)
* [matthias] 65€ for new soldering station tips (requested some months ago)
* [matthias] 10€ for new toilet brush as potato cleaner
* [matthias] 2550€ for central ventilation/heat exchanger device


### Income
* 10€ donation in the free shop
* 200€ and 50€ donation on the account

### Things that happened
#### In and around Kanthaus
* K20 main bathroom has warm water at the sink now
* Clara and Janina were evaluated and reaccepted at their previous positions
* Collective Agreements updated with English as default language and COVID behavioral rules instead of framework 3.0
* The coins for the public pool were reactivated by JaTiMiLe and Thore
* Matthias did a roadtrip and came back with a ventilation device
* The Ella movie was screened in the garden and attracted some visitors
* Anja did more freeshop sorting
* Levi's bed moved to the free shop, which leaves communal and cave child bed free
* Some people visited Kanthaus for a night or two


#### In the wider world
* UK is out of fuel
* gaz price spikes
* "Pandora Papers" released
* Pipeline leak in california

## 2. This week planning

### People arriving and leaving
- **Mon.:**
- **Tue.:** zui might come back
- **Wed.:** Andrea leaves
- **Thu.:**
- **Fri.:** Silvan leaves
- **Sat.:**
- **Sun.:** Antonin comes back, Andrea comes back, Doug comes back, Silvan comes back
- **Mon.:** FFJ people, workawayers for building week
- **Some day:** Larissa and Zui might leave for some days


### Weather forecast

<!--
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
-->
Rainy in the first days, generally it will get colder but still top temperatures of 15-16°C.

### Evaluations and check-ins
- Silvan Volunteer : Absolute Days threshold 74/60 (+23%)
- Lise Volunteer : Absence threshold 95/90 (+6%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
- Monday
    * 10:00 CoMe
    * 15:00 Knowledge sharing with tips on cleaning utensils + How to have cloths not smell? [matthias]
    * Park cars on even side [done]
- Tuesday
    * final solar panel delivery 🎉 [chandi]
    * Open Tuesday
- Wednesday
    * 15:00 Silvan's evaluation [Janina]
    * Park cars on odd side [maxime]
- Thursday
    * 10:00 Power Hour [chandi]
    * 15:00 Social sauna [Maxime]
- Friday
    * yellow bin [Tilmann]
    * 12:00 Market Pickup [Maxime/Andrea]
- Saturday
    * 14:00 - 20:00 Party in Pödi
- Sunday

- Next Monday
    * 10:00 CoMe [Matthias]
- Next week summary
    * Roof month start!

- Days until Roof month: 9!

_to be scheduled:_
(*when possible, avoid scheduling events at times that would conflict with being part of the cooking team: 17:00-19:00*)
<!-- Knowledge sharing proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit -->


## 3. Shopping plans
- [matthias] Ventilation parts for 2500€, to be bought during the building month (in addition to 2550€ for the main ventilation device)

## 4. To do
_Small fixes of things in the house_

### High priority
* [ ] secure k22-2 bathroom sink

### Medium priority
* [ ] More storage spaces for visitors
* [ ] make k20 garden door easily closable from outside (or acquire a new door) -> apparently old door handle in workshop on left https://yunity.slack.com/archives/C3RS56Z38/p1627483587048000
* [ ] fix loose table leg in Lantern
* Ventilation preparation:
    * [ ] remove chimney and ventilation pipes in K22-2 bathroom
    * [ ] remove chimney in K20-2 communal sleeping room
* [ ] replace broken/string/toaster shoe rack spots [Andrea, Doug]
* [ ] yellow bin in snack kitchen - new solution
* [x] bed in private is dissolving and making many small crumbs [Larissa]
* [ ] reinforce hook for laundry rack K22-1.5
* [ ] toilet fill valve in baby bathroom is leaking - maybe clean it?

## 5. Discussion & Announcements

### Round 1
* [matthias] Winter is coming: temperatures are from **today on** all around the clock colder than what we want to have inside! Don't leave windows open for more than 5 minutes, never put them on the tilt-open position, keep all doors to staircase and garden closed. The better we do this, the later we have to start heating, the more energy we save :-)
* [janina] Raheem's workaway request.
    * [Andrea] I think what we should ask ourselves is if we want to host this person and the responsibility that it entails. If we don't want to host him we should tell him that, so that he can keep looking for something else and not lose time in our interaction.
    * Not much enthusiasm in the room, better to decline.
* [tilmann] renewal of sleep kitchen agreement for 6 months, mainly for Mika (night) and Levi (daytime). would set up an ukuvota. (is it actually a Collective Agreement?)
    * [chandi] we should define what it is because it decides who can vote
    * [janina] I like to have it somewhere central so for me collective agreements make sense
* [maxime] discomfort with kids running naked indoors, which assumes that people are fine with accidents
    * [janina] mika is potty training now and doesn't really have accidents at the moment, but when it gets colder it won't happen anyore anyway
    * [tilmann] the exception is during changing the diapers alone, then there is usually a small timeframe in which the kids are naked. but that's just brief.
    * [maxime] ok.

<!-- Anybody who hasn't spoken so far wants to add a point? -->
### Round 2
- [tilmann/matthias] unload ventilation device to the attic, ideally before the car needs to be reparked on Wednesday evening
    * [antonin] would be happy to help today (Monday)
    * [tilmann] we need to remove some bits from the device to make it lighter and make an opening in the attic to get it up. also there's the kids, so I can't really say a time. maybe we do it spontaneously, with only the necessary time in advance to get everything in order?
    * Everybody in the room is up for somehow helping! :D
    * [tilmann] it won't happen before 4 pm realistically

<!-- Anybody who hasn't spoken so far wants to add a point? -->
### Round 3
- [tilmann] get more potatoes...?
    - the next two days it's rainy but afterwards it would be cool!
    - [tilmann] seems too much effort to schedule something, but if someone wants to go, I'll happily join


## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
- **Mon.:** Larissa, Silvan
- **Tue.:** Matthias
- **Wed.:**
- **Thu.:** Thore
- **Fri.:**
- **Open Tuesday:**

### Unavailabilities
- **Mon.:** chandi
- **Tue.:**
- **Wed.:**
- **Thu.:**
- **Fri.:** Maxime
- **Open Tuesday:** Maxime
- **Week:** Tilmann

### End result
- **Mon.:** Larissa, Silvan
- **Tue.:** Matthias
- **Wed.:** Maxime
- **Thu.:** Thore
- **Fri.:** chandi
- **Open Tuesday:** Janina

## 7. For next week
