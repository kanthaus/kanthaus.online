---
title: Coordination Meeting
date: "2023-02-27"
taxonomy:
    tag: [come]
---

<!-- CoMe facilitation advice and requirements: 
https://gitlab.com/kanthaus/kanthaus-governance/-/blob/master/documents/coordinationMeeting/coMeFacilitationAdvice.md 
-->

# CoMe #282
* Date: 2023-02-27
* Facilitator: Janina
* Notary: Kito
* Children caretaker: Kita 
* Physical board caretaker: Martin
* Digital calendar: Tilmann
* Reservation sheet purifier: Kito
* Present: Veronica, Martin, Anneke, Larissa, Tilmann, Maxime, Janina, Kito

----

<!-- Time for reflection -->


## 0. Check-in round

## 1. Last week review
### Stats
* for heating and warm water: 44.5€/week

![usage graph last 90 days](https://pad.kanthaus.online/uploads/1fdbbfb2-1d06-4e6e-95e8-fa157b2d29b9.png "Usage during the last 90 days")
*(Usage during the last 90 days)*

- **Present:** 2.0 people/day (-6.1)
- **⚡ Electricity**
    - usage: 84.46 €/week (⬇️-40%)
    - of that for heating: 44.5 €/week
    - paid: 46.55 €/week 
    - ☀️ self produced: 33% (⬇️-10%)
    - emissions: 29 kg CO₂ₑ/week
- **💧 Water**
    - paid: ? €/week (to be fixed)

### Expenditure
* 12 € WD40 [Martin]
* 6 € flour and margarine [Martin]
* 60 € for gas/km-pauschale for the HZ-car + ~40€ more for the rubble/cröbern tour [Larissa/Bodhi]
* 30€ for ebike battery electronics: 2x active balancer [tilmann]

### Income

### Things that happened

#### In or around Kanthaus
- we all went to Harzgerode for ToI23
- epic food+furnituresaving on the way back

#### Wider world
<!--
https://en.wikipedia.org/wiki/Main_Page 
https://de.wikipedia.org/wiki/Wikipedia:Hauptseite
-->
- war in Ukraine is now going on for over one year
- scientists are worried about not enough snow in the Alps and not enough ice in the Arctic Sea

## 2. This week planning

### People arriving and leaving
* **Mon.:** 
* **Tue.:** Anja comes for the afternoon
* **Wed.:** Larissa leaves, kito leaves, Bodhi leaves (maybe an other day)
* **Thu.:** Jums arrives for a few days
* **Fri.:** 
* **Sat.:** 
* **Sun.:** Larissa comes back (or monday)
* **Mon.:** 
* **Some day:** Antonin comes for a few days towards the end of the week, Anneke may leave at the end of this or at the beginning of next week (plans will be made in the next days)

### Weather forecast
<!--
https://mosmix.de/online.html#/station/P0292/station
https://www.wetteronline.de/wetter/wurzen
https://www.meteoblue.com/en/weather/week/wurzen_germany_2805597?day=3
https://wetterstationen.meteomedia.de/?station=104800&wahl=vorhersage
-->
_frosty nights but days between 3 and 6°C, cloudy but mostly dry. sun on wednesday_

### Evaluations and check-ins

- Martin Volunteer : Days Visited threshold 105/60 (+75%)

### [Schedule](https://cloud.kanthaus.online/apps/calendar/)
* Monday
    * 10:00 CoMe [Janina]
	* Monday food care [Janina, Veronica, everyone?]
	* 11:30 Load trash on car [Tilmann, Larissa, Martin]
	* Cröbern trash tour [Bodhi, Anneke]
    * Park trailer on that side [Martin]
* Tuesday
    * Black bins [Tilmann]
    * 11:00 Building rubble tour (not far from Wurzen) [Maxime, Janina, Tilmann]
    * 15:00 - 20:00 Open Tuesday [Anja, you?]
    * 17:00 KüfA [Anneke, kito]
* Wednesday
    * Park trailer on other side [Martin]
    * 19:00 Punkrock-Tresen @NDK
* Thursday
    * 10:00 Power Hour [Anneke]
    * 15:00 Social sauna [Janina]
* Friday
    * Organic waste [Maxime]
    * 11:00 Martin's evaluation [Janina]
    * 15:00 Climate strike @Leipzig, Augustusplatz
* Saturday
    * 15:00 "How to Solizimmer" @Index Leipzig (Breite Str 1) 
* Sunday
* Next Monday
    * 10:00 CoMe [Martin]
* Next week summary
    * 

To be scheduled:_
(*avoid conflicts, e.g. cooking team: 17:00-19:00*)
* Proposals: https://codi.kanthaus.online/KnowledgeSharingSessions?edit

## 3. Shopping plans
- [tilmann] 195€ complete e-bike conversion kit: https://pswpower.com/products/mxus-electric-bicycle-kit-36v-250w-26%E2%80%9D28%E2%80%9D700c-xf08c-cassette-rear-brushless-gear-motor-bicycle-e-bike-conversion-kit-124
  - to have a second KH ebike
  - 3 batteries in total (+ chandi-battery without case)
  - rear hub motor, will be a bit like phoenix e-bike
  - blue silver mounain bike would be easiest, higher handlebar would be awesome

## 4. To do
_Newest tasks on top_
* [ ] repair the RFID door tags - again -.-
* [ ] exchange the strings of light-brown guitar
* [x] repair schaukasten - window is pressed in [Antonin]
* [ ] bring building rubble to the dump
* [ ] oat milk ~~inspection~~ trashing
* [ ] deep-clean shower tap to make it easier to adjust temperature
* [x] Potato sorting action [Martin]
* [ ] General trash tour [Antonin]
* [x] Sign: Renew the "Klingel" - Sign (it's less and less readable) [Larissa]
* [ ] Replace shower curtain by (plexi)glass construction
* [ ] renew the "Kanthaus"- sign on the Kanthaus [Kito]
* [ ] grout the mosaique in the K20-2 bathroom [Larissa]
* [ ] Light: elephant room
* [ ] Light: Yoga room 
* [ ] Light: freeshop hallway
* [ ] Light: K22 garden door
* [ ] continue Grundstücksverschmelzung K20 + K22 (combining both properties) [Larissa]

## 5. Discussion & Announcements
<!-- Let's try to keep this section short, prefer the Plenum https://pad.kanthaus.online/KH-plenum for non-urgent potentially long discussions -->

### Round 1
<!-- check if anyone has a point that didn't speak already -->
- [janina] money for our stay at FFL harzgerode
    - they heated for us and were nice!
    - asynchronous discussion on Slack in the next days
    - maybe a bit more than in Könnern?
- [tilmann] trash tour(s)
  - Cröbern (old wood) today? yes!
  - Building rubble tomorrow? yes!
  - Metal trash to Trebsen tomorrow? Bodhi recommends putting it to ebay Kleinanzeigen and let someone pick it up
  - Sperrmüll? could be done with Carla Cargo, Martin is up for doing it at some point
- [Kito] Küfa on tuesday
    - people should come in through K20, ring the door bell
    - tell people that they should not just wander around the house
    - could become a regular thing but Anneke and Kito are not regularly here
        - but maybe it's not necessary to have regular dates, but make it nice so people come again and tell or bring other people
    - event on foodsharing could be created
- [Anneke] making Lantern different/nicer!
    - support!
    - are people attached to the Moomin picture? a bit...
    - room is without carpet, so it's nice for keeping it dustfree, would be nice if that can stay

### Round 2
- [tilmann] things to give to FFL
  - old (feather)pillows
  - (feather)blankets?
  - 3 games from Vortex
  - big salad drainer
  - druckluft-schlagschrauber (compressed air impact driver)
- [kito] remove wishlist from website?
    - remove or someone should feel responsible for it

### Last spontaneous points?

## 6. [Task lottery & food planning](https://kanthaus.gitlab.io/task-lottery/)

### Volunteers
* **Mon.:** 
* **Tue.:** kito, Anneke
* **Wed.:** 
* **Thu.:** 
* **Fri.:** maxime, jums
* **Open Tuesday:** Anja

### Unavailabilities
* **Mon.:** 
* **Tue.:** 
* **Wed.:** Martin, Veronica
* **Thu.:** Martin, Veronica
* **Fri.:** 
* **Open Tuesday:** 
* **Week:** Larissa, Tilmann

### Result
- **Mon.:** Veronica, Martin
- **Tue.:** Kito, Anneke
- **Wed.:** 
- **Thu.:** Janina
- **Fri.:** Maxime, jums
- **Open Tuesday:** anja

## 7. For next week
* 
