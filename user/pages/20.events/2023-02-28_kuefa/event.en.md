---
title: 'Küfa in Kanthaus'
date: '2023-02-28 17:00'
enddate: '2023-02-28 22:00'
header_image: false
---

<div markdown="1" style="text-align:center;">
![](plakat_kuefa.jpeg)
</div>

We invite you to the Kitchen for All! Let's have a nice evening together with a relaxed atmosphere and delicious food. Feel free to come and cook together from 5pm or eat together from 7pm. Also, our free shop will be open longer than usual and invite you to browse. We are looking forward to seeing you!
