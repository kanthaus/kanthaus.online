---
title: 'Küfa im Kanthaus'
date: '2023-02-28 17:00'
enddate: '2023-02-28 22:00'
header_image: false
---

<div markdown="1" style="text-align:center;">
![](plakat_kuefa.jpeg)
</div>

Wir laden euch ein zur Küche für Alle! Lasst uns gemeinsam einen schönen Abend mit entspannter Atmosphäre und leckerem Essen haben. Kommt gern zum gemeinsamen Kochen ab 17 Uhr oder zum Essen ab 19 Uhr vorbei. Auch wird unser Umsonstladen länger als gewöhnt geöffnet haben und zum Stöbern einladen. Wir freuen uns auf euch!
