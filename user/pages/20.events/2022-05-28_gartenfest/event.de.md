---
title: 'Kanthaus Gartenfest'
date: '2022-05-28 12:00'
enddate: '2022-05-28 18:00'
header_image: false
---

<div markdown="1" style="text-align:center;">
![](gartenfest.png)
</div>

Am kommenden Samstag, den 28.05., von 12 - 18 Uhr veranstalten wir ein Gartenfest, quasi einen Tag der offenen Tür. Es wird Essen, Getränke und Musik geben. Ihr könnt durch den Umsonstladen [1] stöbern und an Haustouren teilnehmen, um etwas zu nachhaltigem funktionalen Wohnen zu lernen, aber auch zu coolen Bau- und Renovierungsprojekten (Indoor Komposttoilette [2], Regenwasser-Klospülung, Heizen mit Wärmepumpe und Solarstrom, ...).

Das Haus versteht sich als internationales Commons-Projekt [3], welches Menschen ermöglichen möchte mit geringerem ökonomischen Druck an politischen Projekten zu arbeiten. Gleichzeitig möchten wir gern nur die Dinge nutzen, die wir wirklich brauchen und Sachen wiederverwenden und davor retten in den Müll geworfen zu werden. Dieses Mindset hat zu einer gemeinsamen Nutzung aller Infrastruktur, Räume und der meisten Dinge geführt hat.

Vielleicht interessieren euch unsere verschieden Ebenen der gemeinschaftlichen Entscheidungsfindung? Oder ihr habt Interesse an einem der vielen Projekte, welche von hier lebenden Menschen verwirklicht werden? Oder ihr wollt euch einfach mit interessanten Leuten aus aller Welt austauschen? Dann kommt vorbei und lasst uns eine gute Zeit zusammen haben!


Worterläuterungen:

[1] [Was ist ein Umsonstladen?](/projects/open-tuesday) Ein Ort an dem Menschen Sachen hinbringen die sie nicht mehr nutzen und andere sich Sachen mitnehmen. Das ist nicht Tauschlogik-basiert, heißt du kannst jederzeit Sachen mitnehmen ohne welche hinbringen. Ist es nur ein Schrank der an der Straße steht anstatt ein Raum nennt es sich Tauschschrank, wenn es für Lebensmittel genutzt wird ist es z.B. ein Foodsharing-Fairteiler.

[2] [Was ist eine Komposttoilette?](https://de.wikipedia.org/wiki/Komposttoilette) Bei herkommlichen Toiletten kommen die organischen Abfälle (z.B. Fäkalien) zusammen mit schädlichen Abwässern (Chemikalien, Mikroplastik, Medizinreste etc.) in Kläranlagen, was eine schadstofffreie Weiterverwendung schwer bis unmöglich macht. Außerdem wird sehr viel Wasser gebraucht. Kompottoiletten funktioniern anders.

[3] [Was sind Commons?](https://de.wikipedia.org/wiki/Commons) z.B. Open-Souce-Software, Wikipedia, Umsonstläden...
