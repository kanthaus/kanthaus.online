---
title: 'Kanthaus Sommerfest'
date: '2023-09-02 14:00'
enddate: '2023-09-02 21:00'
header_image: false
---

<div markdown="1" style="text-align:center;">
![](sommerfest-2023_small.jpg)
</div>

Unser traditionell spätes Sommerfest steht wieder an! Genieße einen der letzten Sonnentage und lern das Kanthaus kennen!

===

Es wird Hausführungen, Musik, einen Forrò Tanzworkshop, gemeinsames Schnippeln und Kochen von gerettetem Essen, sowie eine Menge netter Leute geben, mit denen mensch rumhängen kann. Unser Verschenkeladen wird geöffnet und der foodsharing Fairteiler bestückt sein. Im Garten werden Tische und Bänke aufgestellt sein, an denen lachende und sich unterhaltende Menschen sitzen werden. Am Abend gibt es vielleicht ein kleines Feuer.

Wir freuen uns sehr darauf dich willkommen zu heißen! Bring auch gerne deine Kinder, Eltern, Nachbar/*Innen, Freundis und entfernte Bekannte mit!