---
title: 'Kanthaus summer fair'
date: '2023-09-02 14:00'
enddate: '2023-09-02 21:00'
header_image: false
---

<div markdown="1" style="text-align:center;">
![](sommerfest-2023_small.jpg)
</div>

Our traditionally late summer fair is due again! Enjoy one of the last days of sunshine and get to know Kanthaus!

===

There will be house tours, music, a forrò dancing workshop, communal cooking of saved food, and lots of nice people to hang out with. Our free shop will be open, the foodsharing fair-share point stocked and the garden full of benches, tables, laughter and chatter. In the evening there might be a fire.

We are very much looking forward to welcoming you, your children, your neighbors, your parents, your friends and distant acquaintances! Do not hesitate to bring them all! 