---
title: 'Foodsharing Kartoffelaktion'
date: '2023-09-22 15:00'
enddate: '2023-09-22 18:00'
header_image: false
---

<div markdown="1" style="text-align:center;">
![](kartoffelaktion_2023.png)
</div>

Kommt zum Marktplatz in Wurzen und rettet unverkäufliche aber tolle Bio-Kartoffeln!

===

Unserer lokale [Foodsharing](https://foodsharing.de)-Bezirk lädet euch zu unserer Aktion, 15 Uhr, 22. September am Wurzenen Marktplatz. Bringt Tüten, Kisten oder Körbe mit!
