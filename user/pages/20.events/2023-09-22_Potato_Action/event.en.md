---
title: 'Foodsharing Kartoffelaktion'
date: '2023-09-22 15:00'
enddate: '2023-09-22 18:00'
header_image: false
---

<div markdown="1" style="text-align:center;">
![](kartoffelaktion_2023.png)
</div>

Come to Wurzen's market place to save organic potatos that cannot be sold but are still amazing!

===

Our local [Foodsharing](https://foodsharing.de/) group invites you to sort and collect potatoes on Wurzen's market place, from 15:00 on Friday 22nd September. Bring your own bags or boxes!
