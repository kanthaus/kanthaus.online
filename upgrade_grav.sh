#!/bin/sh

bin/gpm self-upgrade
git restore README.md LICENSE.txt
rm -r CODE_OF_CONDUCT.md CONTRIBUTING.md SECURITY.md .github
git add .
